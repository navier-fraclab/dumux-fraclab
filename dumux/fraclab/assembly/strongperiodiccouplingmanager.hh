
// -*- mode: C++; tab-width: 4; indent-tabs-mode: nil; c-basic-offset: 4 -*-
// vi: set et ts=4 sw=4 sts=4:
/*****************************************************************************
 *   See the file COPYING for full copying permissions.                      *
 *                                                                           *
 *   This program is free software: you can redistribute it and/or modify    *
 *   it under the terms of the GNU General Public License as published by    *
 *   the Free Software Foundation, either version 3 of the License, or       *
 *   (at your option) any later version.                                     *
 *                                                                           *
 *   This program is distributed in the hope that it will be useful,         *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of          *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the            *
 *   GNU General Public License for more details.                            *
 *                                                                           *
 *   You should have received a copy of the GNU General Public License       *
 *   along with this program.  If not, see <http://www.gnu.org/licenses/>.   *
 *****************************************************************************/

#ifndef DUMUX_BOX_STRONG_PERIODIC_COUPLING_MANAGER_HH
#define DUMUX_BOX_STRONG_PERIODIC_COUPLING_MANAGER_HH

//Periodic coupling manager for periodic meshes (strongly periodic)

#include "interpolperiodiccouplingmanager.hh"

// Periodic Coupling Manager for STRONG Periodicity (periodic meshes)

namespace Dumux
{

  template<class TypeTag>
  class StrongPeriodicCouplingManager : public PeriodicCouplingManagerBase<TypeTag>
  {
    using ParentType = PeriodicCouplingManagerBase<TypeTag>;
    using Scalar = GetPropType<TypeTag, Properties::Scalar>;
    using Problem = GetPropType<TypeTag,Properties::Problem>;
    using NumEqVector = GetPropType<TypeTag, Properties::NumEqVector>;
    using PrimaryVariables = GetPropType<TypeTag, Properties::PrimaryVariables>;
    using GridGeometry = GetPropType<TypeTag, Properties::GridGeometry>;
    using FVElementGeometry = typename GridGeometry::LocalView;
    using SubControlVolume =typename  GridGeometry::SubControlVolume;
    using SubControlVolumeFace = typename GridGeometry::SubControlVolumeFace;
    using GridView = typename GridGeometry::GridView;
      
    static constexpr int dim = GridView::dimension;
    static constexpr int dimWorld = GridView::dimensionworld;
      
    using Element = typename GridView::template Codim<0>::Entity;
    using Vertex = typename GridView::template Codim<dim>::Entity;
    using GlobalPosition = typename Element::Geometry::GlobalCoordinate;
    using GridIndexType = typename GridView::IndexSet::IndexType;
    using GridVariables = GetPropType<TypeTag, Properties::GridVariables>;
    using GridVolumeVariables = typename GridVariables::GridVolumeVariables;
    using ElementVolumeVariables = typename GridVolumeVariables::LocalView;
    using VolumeVariables = typename ElementVolumeVariables::VolumeVariables;
    using GridFluxVariablesCache = typename GridVariables::GridFluxVariablesCache;
    using ElementFluxVariablesCache = typename GridFluxVariablesCache::LocalView;
    using SolutionVector = GetPropType<TypeTag, Properties::SolutionVector>;
    using GridSolutionVector = SolutionVector;
    using SystemSolutionVector = GridSolutionVector;
    using JacobianMatrix = GetPropType<TypeTag, Properties::JacobianMatrix>;      
    using GradientVector = Dune::FieldVector<Scalar, dim>;
    using GridIndexMap = std::map<GridIndexType, GridIndexType>;

  public:

    StrongPeriodicCouplingManager(std::shared_ptr<const GridGeometry> gridGeometry,
				  const GradientVector& gradP,
				  int n = 10)
      :ParentType(gridGeometry, gradP, n)
    {
    }

    StrongPeriodicCouplingManager(std::shared_ptr<const GridGeometry> gridGeometry)
      :ParentType(gridGeometry)
    {
    }
      
    const GridIndexType getMasterDofIdx(const GridIndexType originalDofIdx) const
    {
      auto it = periodicDofMap_.find(originalDofIdx);
      return it->second;
    }

    const GridIndexType getMasterDofSystemIdx(const GridIndexType originalDofIdx) const
    {
      auto it = periodicDofMap_.find(originalDofIdx);
      return getNewDofIdx(it->second);
    }

    void setPeriodicDofMap() 
    {
        std::set<GridIndexType> assignedDofs;
        GridIndexType masterCorner;
        
        for (const auto& element : elements(this->gridGeometry_->gridView())){

	  auto fvGeometry = localView(*(this->gridGeometry_));
	  fvGeometry.bind(element);

            for (const auto& scv : scvs(fvGeometry)){
	      if (!fvGeometry.gridGeometry().dofOnBoundary(scv.dofIndex()))
		continue;
	      auto it = assignedDofs.find(scv.dofIndex());
	      if(it != assignedDofs.end()) continue;
	      const auto globalPos = scv.dofPosition();
	      
	      const auto dofIdx = scv.dofIndex();
              
	      if(this->atCorners_(globalPos)) {
		this->corners_.push_back(dofIdx);
		if(this->atMinCorner_(globalPos)) masterCorner = dofIdx;
	      }          
	      else attributeMapPosition(dofIdx, globalPos);
              
	      assignedDofs.insert(dofIdx);
            }
        }

        assignedDofs.clear(); 

        for(int dir = 0; dir < dimWorld; dir++){
            for(int i = 0; i < this->ndiv; i++) {
                if(this->minDofs_[dir][i].size() != this->maxDofs_[dir][i].size()){

                    std::cout << "unequal number of nodes stored in div. number "
                              << i << " plane id "<< dir << "\n" << std::endl;
                    exit(1);
                }    
            }
        }
        
        for(int dir = 0; dir < dimWorld; dir++)
        {
            for(int i = 0; i < this->ndiv; i++)
            {
                findAndInsertMirrorDofs(this->maxDofs_[dir][i], this->minDofs_[dir][i], dir);

                this->minDofs_[dir][i].clear();
                this->maxDofs_[dir][i].clear();
            } 
        }

        for(auto dof : this->corners_)
        {
            if(dof == masterCorner) continue;
            periodicDofMap_.insert(std::pair<GridIndexType, GridIndexType>(dof, masterCorner));
        }
        
    };
      
    void findAndInsertMirrorDofs(const std::map<GridIndexType, GlobalPosition>& mirrorMap,
                                 const std::map<GridIndexType, GlobalPosition>& imageMap,
                                 int direction)
    {
        GlobalPosition dist(this->maxCoor);
        dist -= this->minCoor;

        const auto eps = this->eps_;
        
        for(auto& [dof, coord] : mirrorMap)
        {
            auto imageIt = find_if(imageMap.begin(),
                                    imageMap.end(),
                                    [coord, direction, dist, eps](const auto& mo)
                                    {
				      GlobalPosition dp(mo.second);
				      dp -= coord; 
				      return abs(dp.two_norm() - dist[direction]) < eps;
                                    });
            
            if(imageIt == imageMap.end()){
                std::cout << "Coordinate correspondency not found for node " << dof <<
                    "position " << coord << "at plane " << direction << "\n" << std::endl;
                exit(1);
            }

            periodicDofMap_.insert(std::pair<GridIndexType, GridIndexType>(dof, imageIt->first));
            //mirrorMap.erase(mirrorIt);
        }
    }

    void attributeMapPosition(const GridIndexType dofIdx, const GlobalPosition& globalPos)
    {    
      GlobalPosition dcoor(this->maxCoor);
      dcoor -= this->minCoor;
      dcoor *= 1.0/this->ndiv;

      for(int dir = 0; dir < dimWorld; dir++){

          bool min = this->onMinBoundary_(dir, globalPos);
          bool max = this->onMaxBoundary_(dir, globalPos);
          if(!(min || max)) continue;

          int dir2 = dir == 0 ? dimWorld - 1 : dir - 1;

          for(int i = this->ndiv; i > 0; i--){

              GlobalPosition limCoor(dcoor);
              limCoor *= (i-1);

              if(globalPos[dir2] > limCoor[dir2] ||
                 abs(globalPos[dir2]-limCoor[dir2]) < this->eps_)
              {
                  if(min) this->minDofs_[dir][i-1].insert(std::pair<GridIndexType, GlobalPosition>
                                                       (dofIdx, globalPos));
                  else this->maxDofs_[dir][i-1].insert(std::pair<GridIndexType, GlobalPosition>
                                                 (dofIdx, globalPos));
                  break;
              }
          }

          break;
          
      }
      
    }

    const PrimaryVariables getImageSolution(const GridIndexType vIdx,
					    const GlobalPosition& globalPos,
					    const GridSolutionVector& curSol) const
    {
      const auto vIdxMaster = getMasterDofIdx(vIdx);
      PrimaryVariables sol = curSol[vIdxMaster];
      GlobalPosition dist = this->distanceToImageDof(globalPos);

      // this will have to become primary variables
      // and gradP will need to be problem-dependent(mech or flux)
      
      const auto dp = dist.dot(this->gradP_);
      sol += dp;

      return sol;
    }

    bool isDependentDofIdx(const GridIndexType originalDofIdx) const
    {
      return periodicDofMap_.count(originalDofIdx);
    }

    void enforceSystemPeriodicConstraints(JacobianMatrix& jac,
					  SolutionVector& res,
					  const SolutionVector& curSol) const
    {

      for (const auto& vertex : vertices(this->gridGeometry().gridView())){
	
	if(!onMirrorBoundary_(vertex.geometry().center())) continue;
	const auto slaveDof = this->gridGeometry().gridView().indexSet().index(vertex);
	const auto masterDof = getMasterDofIdx(slaveDof);

	// add dependent dof row to correspondent independent
            
	res[masterDof] += res[slaveDof];
	const auto end = jac[slaveDof].end();
	for (auto it = jac[slaveDof].begin(); it != end; ++it)
	  jac[masterDof][it.index()] += (*it);

	// enforce dependency
	const auto coord = vertex.geometry().center();
	const auto dx = this->primaryVariablesDiff(coord);

	res[slaveDof] = curSol[slaveDof] - curSol[masterDof] - dx;
	for (auto it = jac[slaveDof].begin(); it != end; ++it)
	  (*it) = it.index() == slaveDof ? 1.0 : it.index() == masterDof ? -1.0 : 0.0;
      }

    }

    template<class JacobianPattern>
    void extendJacobianPattern(JacobianPattern& pattern) const
    {
        for (const auto& element : elements(this->gridGeometry_->gridView()))
        {
	  auto fvGeometry = localView(*(this->gridGeometry_));
	  fvGeometry.bind(element);

	  for (const auto& scv : scvs(fvGeometry)){
	    if(!fvGeometry.gridGeometry().dofOnBoundary(scv.dofIndex())) continue;
	    if(!this->onMaxBoundary_(scv.dofPosition())) continue;
	    
	    const auto masterDof = getMasterDofIdx(scv.dofIndex());
	    for (const auto& scv : scvs(fvGeometry)){
	      pattern.add(masterDof, scv.dofIndex());
	    }
	  }
        }

        for(auto& [slaveDof, masterDof] : periodicDofMap_)
            pattern.add(slaveDof, masterDof);
    }

    bool onMirrorBoundary_(const GlobalPosition& globalPos) const
    {
      return this->onMaxBoundary_(globalPos);
    }

    bool onMirrorBoundary_(const GlobalPosition& globalPos) 
    {
      return this->onMaxBoundary_(globalPos);
    }

  private:

    std::map<GridIndexType, GridIndexType> periodicDofMap_;
  };
}

#endif
