#ifndef DUMUX_MULTIDOMAIN_INTERFACE_2NODE_MORTAR_PERIODIC_COUPLING_MANAGER_HH
#define DUMUX_MULTIDOMAIN_INTERFACE_2NODE_MORTAR_PERIODIC_COUPLING_MANAGER_HH

#include "periodiccouplingmanager.hh"
#include <dune/geometry/referenceelements.hh>
#include <random>

// Class to manage the imposition of periodic boundary conditions
// in non-periodic meshes that contain two-nodded zero-thickness interface elements 

namespace Dumux
{

template<class MDTraits, class BulkTypeTag, class FacetTypeTag, class CouplingManager, class CouplingMapper, std::size_t bulkDomainId = 0, std::size_t lowDimDomainId = 1>
class InterfaceMortarPeriodicCouplingManager
: public PeriodicCouplingManagerBase<BulkTypeTag>
{
    using ParentType = PeriodicCouplingManagerBase<BulkTypeTag>;

    // convenience aliases and instances of the two domain ids
    using BulkIdType = typename MDTraits::template SubDomain<bulkDomainId>::Index;
    using LowDimIdType = typename MDTraits::template SubDomain<lowDimDomainId>::Index;
        
    static constexpr auto bulkId = BulkIdType();
    static constexpr auto lowDimId = LowDimIdType();
        
    template<std::size_t id> using SubDomainTypeTag = typename MDTraits::template SubDomain<id>::TypeTag;
    template<std::size_t id> using GridGeometry = GetPropType<SubDomainTypeTag<id>, Properties::GridGeometry>;
    template<std::size_t id> using GGWeakPtr = std::weak_ptr<const GridGeometry<id>>;
    template<std::size_t id> using LocalResidual = GetPropType<SubDomainTypeTag<id>, Properties::LocalResidual>;
    using GridGeometries = typename MDTraits::template Tuple<GGWeakPtr>;
    template<std::size_t id> using NumEqVector = GetPropType<SubDomainTypeTag<id>, Properties::NumEqVector>;
    template<std::size_t id> using GridView = typename GridGeometry<id>::GridView;
    template<std::size_t id> using Element = typename GridView<id>::template Codim<0>::Entity;
    template<std::size_t id> using SubControlVolume = typename GridGeometry<id>::SubControlVolume;
    template<std::size_t id> using SubControlVolumeFace = typename GridGeometry<id>::SubControlVolumeFace;
    template<std::size_t id> using Intersection = typename GridView<id>::Intersection;
    template<std::size_t id> using FVElementGeometry = typename GridGeometry<id>::LocalView;
    template<std::size_t id> using ElementBoundaryTypes = GetPropType<SubDomainTypeTag<id>, Properties::ElementBoundaryTypes>;

    using BulkSubControlVolumeFace = SubControlVolumeFace<bulkId>;
    using BulkSubControlVolume = SubControlVolume<bulkId>;
    using BulkIntersection = Intersection<bulkId>;
      
    using GridIndexType = typename GridView<bulkId>::IndexSet::IndexType;
    using Scalar = GetPropType<SubDomainTypeTag<bulkId>,Properties::Scalar>;
    
    using BulkGridGeometry = GetPropType<BulkTypeTag, Properties::GridGeometry>;
    using FacetGridGeometry = GetPropType<FacetTypeTag, Properties::GridGeometry>;
    using BulkGridView = typename BulkGridGeometry::GridView;
    using FacetGridView = typename FacetGridGeometry::GridView;
    using PrimaryVariables = GetPropType<BulkTypeTag, Properties::PrimaryVariables>;
      
    static constexpr int dimWorld = BulkGridView::dimensionworld;
    static constexpr int dimBulk = BulkGridView::dimension;
    static constexpr int dimFacet = FacetGridView::dimension;

    enum { numEq = GetPropType<BulkTypeTag, Properties::ModelTraits>::numEq() };
      
    using BulkElement = typename BulkGridView::template Codim<0>::Entity;
    using Vertex = typename BulkGridView::template Codim<dimBulk>::Entity;
    using GlobalPosition = typename BulkElement::Geometry::GlobalCoordinate;

    using GradientVector = Dune::FieldVector<PrimaryVariables, dimWorld>;
    using JacobianMatrix = typename MDTraits::JacobianMatrix;
    using SolutionVector = typename MDTraits::SolutionVector;

    using GridIndexTypeGlobalPositionMap =  std::multimap<GridIndexType, GlobalPosition>;
    using GridIndexTypeMap = std::map<GridIndexType, GridIndexType>;
    using GridIndexTypeMultiMap = std::multimap<GridIndexType, GridIndexType>;
    using GridIndexTypePair = std::pair<GridIndexType, GridIndexType>;
    using GridIndexTypePairMultiMap = std::multimap<GridIndexType, GridIndexTypePair>;
    using GridIndexTypeScalarPair = std::pair<GridIndexType, Scalar>;
    using GridIndexTypeScalarMap =  std::map<GridIndexType, Scalar>;
    using GridIndexTypeScalarMapMultiMap = std::multimap<GridIndexType, GridIndexTypeScalarPair>;
    using GridIndexTypeSet =  std::set<GridIndexType>;
    using GridIndexTypeVector =  std::vector<GridIndexType>;
    using GridIndexTypeVectorVector =  std::vector<GridIndexTypeVector>;
    using IntVector = std::vector<int>;
    using ScalarVector = std::vector<Scalar>;
    using ScalarMatrix = std::vector<ScalarVector>;
    using GlobalPositionVector = std::vector<GlobalPosition>;
    using GlobalPositionVectorVector = std::vector<GlobalPositionVector>;
    using BulkSubControlVolumeVector = std::vector<BulkSubControlVolume>;
      
    using ReferenceBulkElements = typename Dune::ReferenceElements<typename BulkGridView::ctype, dimBulk>;

public:

    InterfaceMortarPeriodicCouplingManager(std::shared_ptr<const BulkGridGeometry> bulkGridGeometry,
                                            std::shared_ptr<const FacetGridGeometry> facetGridGeometry,
                                            std::shared_ptr<CouplingManager> couplingManager,
                                            std::shared_ptr<CouplingMapper> couplingMapper)
    :ParentType(bulkGridGeometry)
    ,couplingManager_(couplingManager)
    ,couplingMapper_(couplingMapper)
    {
        setDomainGridGeometry(bulkGridGeometry, bulkId);
        setDomainGridGeometry(facetGridGeometry, lowDimId);

        quadraticElements();

        bulkDofIsCoupled_.assign(bulkGridGeometry->numDofs(), false);
        const auto& lowDimMap = couplingMapper->couplingMap(lowDimId, bulkId);
        auto it = lowDimMap.begin();

        // identify and store which elements are coupled by interfaces
        while(it != lowDimMap.end())
        {
            std::vector<int> bulkElems;
            const auto embedments = it->second.embedments;

            for(const auto& pair : embedments){
      
                const auto& eIdx = pair.first;
                const auto& scvfIndices = pair.second;
                bulkElems.push_back(eIdx);
                bool min = false;

                const auto& element = bulkGridGeometry->element(eIdx);
                auto bulkFvGeometry = localView(*bulkGridGeometry);
                bulkFvGeometry.bind(element);
        
                for(const auto& scvfIdx : scvfIndices){
                    const auto& scvf = bulkFvGeometry.scvf(scvfIdx);
                    const auto& scv = bulkFvGeometry.scv(scvf.insideScvIdx());			 
                    bulkDofIsCoupled_[scv.dofIndex()] = true;
                }

                for(const auto& scvB : scvs(bulkFvGeometry)){
                    if(this->onMinBoundary_(scvB.dofPosition())) min = true;
                }

                if(min){
                  for(const auto& pairB : embedments){
                      if(pairB.first == eIdx) continue;
                      coupledBulkElements_.insert(std::pair<int, int>(eIdx, pairB.first));
                  }
                }
              }
              it++;
        }    
    }

    template<class GG, std::size_t i>
    void setDomainGridGeometry(std::shared_ptr<GG> gridGeometry, Dune::index_constant<i> domainIdx)
    { std::get<i>(gridGeometries_) = gridGeometry; }

    void setVirtualLinePoints()
    {
        virtualLinePoints_.resize(dimWorld);
      
        const auto& bulkGridGeometry = fvGridGeometry(bulkId);
        GridIndexTypeSet assignedDofs;

        for(const auto& element : elements(bulkGridGeometry.gridView()))
        {
            auto fvGeometry = localView(bulkGridGeometry);
            fvGeometry.bind(element);
            
          for(const auto& scv : scvs(fvGeometry))
          {
              auto dofCoord = scv.dofPosition();
              const auto& dofIdx = scv.dofIndex();
              if(!this->onBoundary_(dofCoord)) continue;
              if(assignedDofs.find(dofIdx) != assignedDofs.end()) continue;
              assignedDofs.insert(dofIdx);

              if(this->atMaxCorner_(dofCoord)) bulkMirrorCorner = dofIdx;
              bool maxBound = this->onMaxBoundary_(dofCoord);
              if(maxBound) mirrorVertices_.insert(std::pair<GridIndexType, GlobalPosition>(dofIdx, dofCoord));
              
              for(int dir = 0; dir < dimWorld; dir++){
                if(this->onMinBoundary_(dir, dofCoord) || this->onMaxBoundary_(dir, dofCoord)){
                    dofCoord[dir] = this->minCoor[dir];
                    virtualLinePoints_[dir].push_back(dofCoord);
                    dofCoord = scv.dofPosition();
                }
              }
          }
        }

        for(int dir = 0; dir < dimWorld; dir++)
        {
          for(auto itI = virtualLinePoints_[dir].begin(); itI <  virtualLinePoints_[dir].end(); itI++)
          {
              auto itJ = itI + 1;
              while (itJ !=  virtualLinePoints_[dir].end())
              {

                  auto distI = *itI;
                  auto distJ = *itJ;
                  auto distIJ = distI - distJ;

                  distI -= this->minCoor;
                  distJ -= this->minCoor;

                  if(distJ.two_norm() < distI.two_norm()) std::swap(*itI, *itJ);

                  if(distIJ.two_norm() < this->eps_)
                      itJ =  virtualLinePoints_[dir].erase(itJ);
                  else itJ++;
              }
          }
        }  
    }
  
    void setVirtualLineBoundaryElements()
    {
        virtualLineMirrorElements.resize(dimWorld);
        virtualLineImageElements.resize(dimWorld);
        
        for(int dir = 0; dir < dimWorld; dir++)
        {
            int nline = virtualLinePoints_[dir].size()-1;
            virtualLineMirrorElements[dir].resize(nline);
            virtualLineImageElements[dir].resize(nline);
        }

        const auto& bulkGridGeometry = fvGridGeometry(bulkId);

        for(const auto& element : elements(bulkGridGeometry.gridView()))
        {
            const auto eIdx = bulkGridGeometry.elementMapper().index(element);
            auto fvGeometry = localView(bulkGridGeometry);
            fvGeometry.bind(element);
        
            GlobalPositionVectorVector elemBoundaryPos(dimWorld);

            for(auto scvf: scvfs(fvGeometry))
            {
                const auto& scvfCenter = scvf.center();
                if(!this->onBoundary_(scvfCenter)) continue;
                int dir = this->getBoundaryDirection(scvfCenter);
                const auto& scv = fvGeometry.scv(scvf.insideScvIdx());
                auto dofCoord = scv.dofPosition();
                elemBoundaryPos[dir].push_back(dofCoord);
            }
        
            for(int dir = 0; dir < dimWorld; dir++)
            {
                if(elemBoundaryPos[dir].size() == 0) continue;
                  
                IntVector virtualPoints;
                bool min = false;
                    
                for(int idof = 0; idof < elemBoundaryPos[dir].size(); idof++)
                {
                  auto coorI = elemBoundaryPos[dir][idof];
                  min = this->onMinBoundary_(dir, coorI);
                  coorI[dir] = this->minCoor[dir];
                    
                  for(int ip = 0; ip < virtualLinePoints_[dir].size(); ip++)
                  {
                    const auto& coorJ = virtualLinePoints_[dir][ip];
                    const auto dist = coorI - coorJ;
                    if(dist.two_norm() < this->eps_){
                      virtualPoints.push_back(ip);
                      break;
                    }
                  }
                }

                auto beginPoint = *std::min_element(virtualPoints.begin(), virtualPoints.end());
                auto endPoint = *std::max_element(virtualPoints.begin(), virtualPoints.end());

                for(int ip = beginPoint; ip < endPoint; ip++)
                {
                    if(min) virtualLineImageElements[dir][ip] = eIdx;
                    else virtualLineMirrorElements[dir][ip] = eIdx;
                }
            }
        }		
    }

    void buildInterpolationMaps()
    {
        GridIndexTypeScalarMap imageCoefficients;
        GridIndexTypeScalarMapMultiMap mirrorCoefficients, mirrorCoefficients1;
      
        const auto& bulkGridGeometry = fvGridGeometry(bulkId);

        const auto gaussLocalPositions = virtualGaussPointsLocalCoord();
        const auto gaussWeigths =  virtualGaussPointsWeights();
        
        for(int dir = 0; dir < dimWorld; dir++)
        {
            auto nline = virtualLinePoints_[dir].size()-1;
            for(int il = 0; il < nline; il++)
            {
                //asser if line has mirror and image associated elements 
                const auto mirrorElementIdx = virtualLineMirrorElements[dir][il];
                const auto imageElementIdx = virtualLineImageElements[dir][il];
                const auto& mirrorElement = bulkGridGeometry.element(mirrorElementIdx);
                const auto& imageElement = bulkGridGeometry.element(imageElementIdx);
                const auto& mirrorScvs = elementBoundaryConnectivity(dir, mirrorElement);
                const auto& imageScvs = elementBoundaryConnectivity(dir, imageElement);

                int numBoundDof = imageScvs.size();

                GlobalPositionVector mirrorPos;
                GlobalPositionVector imagePos;
                GlobalPositionVector linePos;

                for(const auto& scv : mirrorScvs) mirrorPos.push_back(scv.dofPosition());
                for(const auto& scv : imageScvs) imagePos.push_back(scv.dofPosition());
                for(auto& mirrorCoord : mirrorPos) mirrorCoord[dir] = this->minCoor[dir];

                const auto p1 = virtualLinePoints_[dir][il];
                const auto p2 = virtualLinePoints_[dir][il+1];

                linePos.push_back(p1);
                linePos.push_back(p2);

                const auto distVector = p2 -p1;
                const auto segmentLength = distVector.two_norm();
                const auto detJac = segmentLength/2.0;
                
                ScalarVector imageCoef(numBoundDof, 0.0);
                ScalarMatrix  mirrorCoef(numBoundDof);
                ScalarMatrix mirrorCoef1(numBoundDof);
                bool imageHasCorner = false;
                for(const auto& imageCoor : imagePos){
                  if(this->atCorners_(imageCoor)){
                    imageHasCorner = true;
                    break;
                  }
                }

                for(int i = 0; i < numBoundDof; i++) mirrorCoef[i].resize(numBoundDof);
                for(int i = 0; i < numBoundDof; i++) mirrorCoef1[i].resize(numBoundDof);	
                for(int ig = 0; ig < gaussLocalPositions.size(); ig++)
                {

                    const auto gaussGlobalPos = getIntersectionGlobalPosition(dimBulk-1, linePos, gaussLocalPositions[ig]);
                    const auto xi = getIntersectionLocalPosition(imagePos, gaussGlobalPos);
                    const auto dzeta = getIntersectionLocalPosition(mirrorPos, gaussGlobalPos);
                    const auto shapeValuesImage = getIntersectionShapeValues(xi);
                    const auto shapeValuesMirror = getIntersectionShapeValues(dzeta);
                    const auto residualShapeValues = getResidualShapeValues(xi, imagePos);
                    const auto residualShapeValues1 = getResidualShapeValues1(xi);
                    
                    for(int ii = 0; ii < imageScvs.size(); ii++)
                    {
                      imageCoef[ii] += shapeValuesImage[ii]*detJac*gaussWeigths[ig];
                      
                      for(int im = 0; im < mirrorScvs.size(); im++)
                        mirrorCoef[ii][im] += residualShapeValues[ii]*shapeValuesMirror[im]*detJac*gaussWeigths[ig];
                      for(int im1 = 0; im1 < mirrorScvs.size(); im1++)
                        mirrorCoef1[ii][im1] += residualShapeValues1[ii]*shapeValuesMirror[im1]*detJac*gaussWeigths[ig];
                    }
              }

              for(int ii = 0; ii < imageScvs.size(); ii++)
              {
                  const auto imageDofIdx = imageScvs[ii].dofIndex();
                  auto iti = imageCoefficients.find(imageDofIdx);

                  if(iti != imageCoefficients.end()) iti->second += imageCoef[ii];
                  else imageCoefficients.insert(std::pair<GridIndexType, Scalar>(imageDofIdx, imageCoef[ii]));
            
                  for(int im = 0; im < mirrorScvs.size(); im++)
                  {
                      // if dof is on image corners, prescribed value is zero = no contribution
                      if(this->atCorners_(mirrorScvs[im].dofPosition()) &&
                      !this->atMaxCorner_(mirrorScvs[im].dofPosition())) continue;
                      
                      const auto mirrorDofIdx = mirrorScvs[im].dofIndex();
                      bool mirrorMapHasDofs = false;

                      auto it = mirrorCoefficients.find(imageDofIdx);
                      while(it != mirrorCoefficients.end())
                      {
                          if(it->second.first == mirrorDofIdx) {
                              mirrorMapHasDofs = true;
                              break;
                          }

                          if(it->first != imageDofIdx) break;
                          it++;
                      }

                      if(mirrorMapHasDofs) 
                          it->second.second += mirrorCoef[ii][im];
                      else {
                          GridIndexTypeScalarPair mirrorPair;
                          mirrorPair.first = mirrorDofIdx;
                          mirrorPair.second = mirrorCoef[ii][im];
                          mirrorCoefficients.insert(std::pair<GridIndexType, std::pair<GridIndexType, Scalar>>(imageDofIdx, mirrorPair));
                      }

                      if(!imageHasCorner) continue;

                      // if dof is on image corners, prescribed value is zero = no contribution
                      if(this->atCorners_(mirrorScvs[im].dofPosition()) &&
                      !this->atMaxCorner_(mirrorScvs[im].dofPosition())) continue;

                      mirrorMapHasDofs = false;

                      it = mirrorCoefficients1.find(imageDofIdx);
                      while(it != mirrorCoefficients1.end())
                      {
                          if(it->second.first == mirrorDofIdx) {
                              mirrorMapHasDofs = true;
                              break;
                          }

                          if(it->first != imageDofIdx) break;
                          it++;
                      }

                      if(mirrorMapHasDofs) 
                          it->second.second += mirrorCoef1[ii][im];
                      else {
                          GridIndexTypeScalarPair mirrorPair;
                          mirrorPair.first = mirrorDofIdx;
                          mirrorPair.second = mirrorCoef1[ii][im];
                          mirrorCoefficients1.insert(std::pair<GridIndexType, std::pair<GridIndexType, Scalar>>(imageDofIdx, mirrorPair));
                      }	    
                    }
                }
            }

            for(auto& [imageDof, mirrorPair] : mirrorCoefficients)
            {
                auto& mirrorCoef = mirrorPair.second;
                const auto& imageCoef = imageCoefficients.find(imageDof)->second;
                mirrorCoef *= 1.0/imageCoef;
                if(abs(mirrorCoef) > this->eps_)
                    interpolationCoefficients_.insert(std::pair<GridIndexType, std::pair<GridIndexType, Scalar>>(imageDof, mirrorPair));
            }

            for(auto& [imageDof, mirrorPair] : mirrorCoefficients1)
            {
                auto& mirrorCoef = mirrorPair.second;
                const auto& imageCoef = imageCoefficients.find(imageDof)->second;
                mirrorCoef *= 1.0/imageCoef;
                if(abs(mirrorCoef) > this->eps_)
                    residualIntCoefficientsCornerNeighbors_.insert(std::pair<GridIndexType, std::pair<GridIndexType, Scalar>>(imageDof, mirrorPair));
            }
          
            imageCoefficients.clear();
            mirrorCoefficients.clear();
            mirrorCoefficients1.clear();
        }
    }
    
    BulkSubControlVolumeVector elementBoundaryConnectivity(int dir, const Element<bulkId>& element)
    {
        const auto& bulkGridGeometry = fvGridGeometry(bulkId);
        auto fvGeometry = localView(bulkGridGeometry);
        fvGeometry.bind(element);

        BulkSubControlVolumeVector boundaryScvs;

        for(const auto& scv : scvs(fvGeometry))
          if(this->onBoundary_(dir, scv.dofPosition())) boundaryScvs.push_back(scv);

        //sort according to positon

        for(auto itI = boundaryScvs.begin(); itI < boundaryScvs.end(); itI++)
        {
          for(auto itJ = itI + 1; itJ < boundaryScvs.end(); itJ++)
          {
              const auto& scvI = *itI;
              const auto& scvJ = *itJ;
              auto distI =  scvI.dofPosition();
              auto distJ =  scvJ.dofPosition();
              distI -= this->minCoor;
              distJ -= this->minCoor;
              if(distJ.two_norm() < distI.two_norm()) std::swap(*itI, *itJ);
          }
        }

        return boundaryScvs;
    }
                    
    const ScalarVector virtualGaussPointsLocalCoord() const
    {
        ScalarVector gCoord;

        if(dimBulk == 2 && !quadraticElements_){
          gCoord.push_back(-1.0/(sqrt(3.0)));
          gCoord.push_back(1.0/(sqrt(3.0)));
        }
        
        if(dimBulk == 2 && quadraticElements_){
          gCoord.push_back(-sqrt(3.0/5.0));
          gCoord.push_back(0.0);
          gCoord.push_back(sqrt(3.0/5.0));
        }

        return gCoord;
    }

    const std::set<int> getCoupledBulkElements(const GridIndexType eIdx) const
    {
        std::set<int> bulkElems;
        auto it = coupledBulkElements_.find(eIdx);
        while(it != coupledBulkElements_.end())
        {
          if(it->first != eIdx) break;
          bulkElems.insert(it->second);
          it++;
        }

        return bulkElems;
    }
                  
    const GlobalPosition getIntersectionGlobalPosition(int dimIntersection,
                  const GlobalPositionVector& intersecGlobalPos,
                  const Scalar localPos) const
    {
        Scalar refDist = localPos + 1.0;
        auto distVector = intersecGlobalPos[1] - intersecGlobalPos[0];
        Scalar coef = refDist/2.0;
        distVector *= coef;
          
        auto globalPos(intersecGlobalPos[0]);
        globalPos += distVector;

        return globalPos;
    }

    const Scalar getIntersectionLocalPosition(const GlobalPositionVector& intersecGlobalPos,
                const GlobalPosition globalPos)
    {
        GlobalPosition segVector = intersecGlobalPos[intersecGlobalPos.size()-1] - intersecGlobalPos[0];
        Scalar segLeng = segVector.two_norm();
        GlobalPosition distVector = globalPos - intersecGlobalPos[0];
        Scalar coef = distVector.two_norm()/segLeng;
        Scalar localPos = -1.0 + coef*2.0;

        return localPos;
    }

    const ScalarVector virtualGaussPointsWeights() const
    {
        ScalarVector gWeig;
        if(dimBulk == 2 && !quadraticElements_){
          gWeig.push_back(1.0);
          gWeig.push_back(1.0);
        }
        
        if(dimBulk == 2 && quadraticElements_){
          gWeig.push_back(5.0/9.0);
          gWeig.push_back(8.0/9.0);
          gWeig.push_back(5.0/9.0);
        }

        return gWeig; 
    }
    
    void quadraticElements()
    {
        const auto& bulkGridGeometry = fvGridGeometry(bulkId);
        const auto& element = bulkGridGeometry.element(0);
        const auto& elemGeometry = element.geometry();
        const auto& referenceElement = ReferenceBulkElements::general(elemGeometry.type());
        const auto numDofsAtSegment = referenceElement.size(0, dimBulk-1, dimBulk);

        quadraticElements_ =  numDofsAtSegment == 3;
    }
    
    void setPeriodicDofMap()
    {
        setVirtualLinePoints();
        setVirtualLineBoundaryElements();
        buildInterpolationMaps();
    }
            
    template<std::size_t i>
    const GridGeometry<i>& fvGridGeometry(Dune::index_constant<i> domainIdx) const
    {
        if (!std::get<i>(gridGeometries_).expired())
          return *std::get<i>(gridGeometries_).lock();
        else
          DUNE_THROW(Dune::InvalidStateException, "The grid geometry pointer was not set or has already expired. Use setDomainGridGeometry() before calling this function");
    }
    
    const std::vector<GridIndexType> getMirrorDofIndices(const GridIndexType dofIdx) const
    {
        std::vector<GridIndexType> mirrorDofs;

        for (auto it = interpolationCoefficients_.find(dofIdx); it != interpolationCoefficients_.end(); it++)
        {
          if(it->first != dofIdx) break;
          mirrorDofs.push_back(it->second.first);
        }

        return mirrorDofs;
    }

    const GridIndexTypeVector getMirrorDofIndices(const GridIndexType dofIdx)
    {
        std::vector<GridIndexType> mirrorDofs;

        for (auto it = interpolationCoefficients_.find(dofIdx); it != interpolationCoefficients_.end(); it++)
        {
          if(it->first != dofIdx) break;
          mirrorDofs.push_back(it->second.first);
        }

        return mirrorDofs;
    }

    const Scalar getInterpolationCoefficient(const GridIndexType imageDofIdx,
              const GridIndexType mirrorDofIdx) const
    {
        auto it = interpolationCoefficients_.find(imageDofIdx);
        while(it != interpolationCoefficients_.end())
        {
          if(it->first != imageDofIdx) break;
          if(it->second.first == mirrorDofIdx) return it->second.second;
          it++;
        }
        return 0.0;
    }

    const Scalar getInterpolationCoefficient(const GridIndexType& imageDofIdx, const GridIndexType& mirrorDofIdx)
    {
        auto it = interpolationCoefficients_.find(imageDofIdx);
        while(it != interpolationCoefficients_.end())
        {
          if(it->first != imageDofIdx) break;
          if(it->second.first == mirrorDofIdx) return it->second.second;
          else it++;
        }

        return 0.0;
    }

    const Scalar getResidualInterpolationCoefficient(const GridIndexType imageDofIdx,
                  const GridIndexType mirrorDofIdx) const
    {
        GridIndexTypeScalarMapMultiMap coefficientsMap;
        if(residualIntCoefficientsCornerNeighbors_.count(imageDofIdx)) coefficientsMap = residualIntCoefficientsCornerNeighbors_;
        coefficientsMap = interpolationCoefficients_;

        Scalar coef(0.0), sum(0.0);
        auto it = coefficientsMap.find(imageDofIdx);
        while(it != coefficientsMap.end())
        {
          if(it->first != imageDofIdx) break;
          sum += it->second.second;
          if(it->second.first == mirrorDofIdx) coef = it->second.second;
          it++;
        }

        if(sum > this->eps_) return coef;///sum;
        else return 0.0;
    }

    const Scalar getResidualInterpolationCoefficient(const GridIndexType& imageDofIdx, const GridIndexType& mirrorDofIdx)
    {
        GridIndexTypeScalarMapMultiMap coefficientsMap;
        if(residualIntCoefficientsCornerNeighbors_.count(imageDofIdx)) coefficientsMap = residualIntCoefficientsCornerNeighbors_;
        else coefficientsMap = interpolationCoefficients_;

        Scalar coef(0.0), sum(0.0);
        auto it = coefficientsMap.find(imageDofIdx);
        while(it != coefficientsMap.end())
        {
          if(it->first != imageDofIdx) break;
          sum += it->second.second;
          if(it->second.first == mirrorDofIdx) coef = it->second.second;
          it++;
        }

        if(sum > this->eps_) return coef;///sum;
        else return 0.0;
    }
    /*
    ScalarVector getShapeValues(const GlobalPosition& localPos) 
    {

    }
    */
    ScalarVector getIntersectionShapeValues(const Scalar& localPos) 
    {
        ScalarVector shapeValues;
        int size = quadraticElements_ ? 3 : 2;
        shapeValues.resize(size);

        if(size == 2){
          shapeValues[0] = 0.5*(1.0-localPos);
          shapeValues[1] = 0.5*(1.0+localPos);
        }

        if(size == 3){
          shapeValues[0] = 0.5*(localPos - localPos*localPos);
          shapeValues[1] = 1.0-localPos*localPos;
          shapeValues[2] = 0.5*(localPos + localPos*localPos);
        }

        return shapeValues;
    }

    ScalarVector getResidualRegularShapeValues(const Scalar& localPos, const GlobalPositionVector& imagePos)
    {
        ScalarVector shapeValues;
        int size = quadraticElements_ ? 3 : 2;
        shapeValues.resize(size);

        bool corner = this->atCorners_(imagePos[0]) || this->atCorners_(imagePos[size-1]);
        bool mirrorCorner = this->atMaxCorner_(imagePos[0]) || this->atMaxCorner_(imagePos[size-1]);
        bool hasPrescribedCorner = corner && !mirrorCorner;

        if(!hasPrescribedCorner)
        {
            if(size == 2){
                shapeValues[0] = 0.5*(1.0-localPos);
                shapeValues[1] = 0.5*(1.0+localPos);
            }

            if(size == 3){
                shapeValues[0] = 0.5*(localPos - localPos*localPos);
                shapeValues[1] = 1.0-localPos*localPos;
                shapeValues[2] = 0.5*(localPos + localPos*localPos);
            }
        } else {

            int cornerPos = this->atCorners_(imagePos[0]) ? 0 : size-1;
        
            if(size == 2){
                shapeValues[0] = cornerPos == 0 ? 0.0 : 1.0;
                shapeValues[1] = cornerPos == 0 ? 1.0 : 0.0;
            }
        }

        return shapeValues; 
    }

    ScalarVector getResidualShapeValues1(const Scalar& localPos)
    {
        ScalarVector shapeValues;
        int size = quadraticElements_ ? 3 : 2;
        shapeValues.resize(size);
        if(size == 2){
          shapeValues[0] = 0.5*(1.0-3.0*localPos);
          shapeValues[1] = 0.5*(1.0+3.0*localPos);
        }
        if(size == 3){
          shapeValues[0] = 0.25*(5.0*pow(localPos, 2.0) - 2.0 * localPos - 1.0);
          shapeValues[1] = 0.5*(3.0-5.0*pow(localPos, 2.0));
          shapeValues[2] = 0.25*(5.0*pow(localPos, 2.0) + 2.0 * localPos -1.0);
        }

        return shapeValues;
    }
      
    ScalarVector getResidualShapeValues(const Scalar& localPos, const GlobalPositionVector& imagePos) 
    {
        ScalarVector shapeValues;
        int size = quadraticElements_ ? 3 : 2;
        shapeValues.resize(size);

        bool corner = this->atCorners_(imagePos[0]) || this->atCorners_(imagePos[size-1]);
        bool mirrorCorner = this->atMaxCorner_(imagePos[0]) || this->atMaxCorner_(imagePos[size-1]);
        bool hasPrescribedCorner = corner && !mirrorCorner;

        if(!hasPrescribedCorner)
        {
            if(size == 2){
                shapeValues[0] = 0.5*(1.0-3.0*localPos);
                shapeValues[1] = 0.5*(1.0+3.0*localPos);
            }

            if(size == 3){
                shapeValues[0] = 0.25*(5.0*pow(localPos, 2.0) - 2.0 * localPos - 1.0);
                shapeValues[1] = 0.5*(3.0-5.0*pow(localPos, 2.0));
                shapeValues[2] = 0.25*(5.0*pow(localPos, 2.0) + 2.0 * localPos -1.0);
            }
        } else {

            int cornerPos = this->atCorners_(imagePos[0]) ? 0 : size-1;
              
            if(size == 2){
                shapeValues[0] = cornerPos == 0 ? 0.0 : 1.0;
                shapeValues[1] = cornerPos == 0 ? 1.0 : 0.0;
            }

            if(size == 3 && cornerPos == 0){
                shapeValues[0] = 0.0;
                shapeValues[1] = 1.0 - localPos;
                shapeValues[2] = localPos;
            } else if(size == 3 && cornerPos == 2){
                shapeValues[0] = -localPos;
                shapeValues[1] = 1.0 + localPos;
                shapeValues[2] = 0.0;
            }
        }

        return shapeValues;
    }

    const GlobalPosition distanceToOppositeBoundary(const GlobalPosition& globalPos) const
    {
        bool corner = this->atCorners_(globalPos);

        GlobalPosition dist(0.0);
        
        if(!corner)
        {
          auto refCoord = globalPos;
          int dir = this->getBoundaryDirection(globalPos);
          refCoord[dir] = this->onMinBoundary_(dir, globalPos) ? this->maxCoor[dir] : this->minCoor[dir];
          dist = globalPos - refCoord;
        } else {
          dist = globalPos - this->maxCoor;
        }

        return dist;
    }

    const PrimaryVariables linearPVComponent(const GlobalPosition& globalPos) const
    {
        GlobalPosition dist = globalPos - this->maxCoor;
        PrimaryVariables x(0.0);
        for(int ipv = 0; ipv < numEq; ipv++)
          x[ipv] = dist.dot(this->gradX_[ipv]);
        return x;
    }

    PrimaryVariables computeResidual(GridIndexType dofIdx,
                                    const GlobalPosition& globalPos,
                                    const SolutionVector& curSol) const
    {
        PrimaryVariables res = curSol[dofIdx];
        
        const auto mirrorDofs = getMirrorDofIndices(dofIdx);
        for(const auto mirrorDof : mirrorDofs)
        {
          const auto coef = getInterpolationCoefficient(dofIdx, mirrorDof);
          const auto mirrorCord = mirrorVertices_.find(mirrorDof)->second;
          const auto mirrorX = linearPVComponent(mirrorCord);
          res -= coef * curSol[mirrorDof];
          res += coef * mirrorX;
        }

        const auto x = linearPVComponent(globalPos);

        res -= x;
          
        return res;
    }

    Scalar computeDerivative(GridIndexType dofIdx,
                            GridIndexType mirrorDof,
                            const SolutionVector& curSol) const
    {
        Scalar derivative =  getInterpolationCoefficient(dofIdx, mirrorDof);
        derivative *= -1.0;

        return derivative;
    }

    template<class Assembler>
    void modifyResidualVector(SolutionVector& res,
            const Assembler& assembler,
            const SolutionVector& curSol) const
    {
      
        const auto& bulkGridGeometry = fvGridGeometry(bulkId);
        GridIndexTypeSet assignedDofs;

        for (const auto& element : elements(bulkGridGeometry.gridView()))
        {
            auto fvGeometry = localView(bulkGridGeometry);
            fvGeometry.bind(element);
            
            for (const auto& scv : scvs(fvGeometry))
            {
                const auto dofIdx = scv.dofIndex();
                const auto coord = scv.dofPosition();
                bool corner = this->atCorners_(coord);
        
                if(!this->onMinBoundary_(coord)) continue;
                if(assignedDofs.find(scv.dofIndex()) != assignedDofs.end()) continue;

                assignedDofs.insert(scv.dofIndex());
                
                GridIndexTypeVector mirrorDofs;
                if(!corner) mirrorDofs = getMirrorDofIndices(dofIdx);
                else mirrorDofs.push_back(bulkMirrorCorner);
                for(auto mirrorDof : mirrorDofs)
                {
                  Scalar coef = corner ? 1.0 : getInterpolationCoefficient(dofIdx, mirrorDof);
                  res[mirrorDof] +=  coef * res[dofIdx];
                }
                  
                res[dofIdx] = computeResidual(bulkId, dofIdx, coord, curSol);
        
                assignedDofs.insert(dofIdx);
            }
        }  
    }
    
    void enforceSystemPeriodicConstraints(JacobianMatrix& jac,
                                          SolutionVector& res,
                                          const SolutionVector& curSol) const
    {
        const auto& bulkGridGeometry = fvGridGeometry(bulkId);
        GridIndexTypeSet assignedDofs;
        
        for (const auto& element : elements(bulkGridGeometry.gridView()))
        {
            auto fvGeometry = localView(bulkGridGeometry);
            fvGeometry.bind(element);
            
            for (const auto& scv : scvs(fvGeometry))
            {

                const auto dofIdx = scv.dofIndex();
                const auto coord = scv.dofPosition();
                bool corner = this->atCorners_(coord);
    
                if(!this->onMinBoundary_(coord)/* or corner*/) continue;
                if(assignedDofs.find(scv.dofIndex()) != assignedDofs.end()) continue;

                assignedDofs.insert(scv.dofIndex());
                
                GridIndexTypeVector mirrorDofs;
                if(!corner) mirrorDofs = getMirrorDofIndices(dofIdx);
                      else mirrorDofs.push_back(bulkMirrorCorner);

                const auto end = jac[dofIdx].end();
                for(auto mirrorDof : mirrorDofs)
                {
                  Scalar coef = corner ? 1.0 : getResidualInterpolationCoefficient(dofIdx, mirrorDof);
                  res[mirrorDof] +=  coef * res[dofIdx];
                  for (auto it = jac[dofIdx].begin(); it != end; ++it)
                    jac[mirrorDof][it.index()] += coef * (*it);
                }
      
                res[dofIdx] = computeResidual(dofIdx, coord, curSol);
                
                for (auto it = jac[dofIdx].begin(); it != end; ++it)
                    (*it) = 0.0;

                for(int ieq = 0; ieq < numEq; ieq++)
                    jac[dofIdx][dofIdx][ieq][ieq] = 1.0;
                
                if(corner) continue;
                
                for(auto mirrorDof : mirrorDofs){
                    const auto deriv = computeDerivative(dofIdx, mirrorDof, curSol);
                    for(int ieq = 0; ieq < numEq; ieq++)
                        jac[dofIdx][mirrorDof][ieq][ieq] = deriv;
                }
                
                assignedDofs.insert(dofIdx);
              }   
          }
      
          assignedDofs.clear();

      }

    template<class JacobianPattern>
    void extendJacobianPattern(JacobianPattern& pattern) const
    {
        const auto& bulkGridGeometry = fvGridGeometry(bulkId);
        GridIndexTypeSet assignedDofs;
        
        for (const auto& element : elements(bulkGridGeometry.gridView()))
        {
            auto fvGeometry = localView(bulkGridGeometry);
            fvGeometry.bind(element);
            
            for (const auto& scv : scvs(fvGeometry))
            {

                const auto dofIdx = scv.dofIndex();

                if(!this->onMinBoundary_(scv.dofPosition())) continue;

                bool corner = this->atCorners_(scv.dofPosition());
                GridIndexTypeVector mirrorDofs;
                if(!corner) mirrorDofs = getMirrorDofIndices(dofIdx);
                else mirrorDofs.push_back(bulkMirrorCorner);

                for (const auto& scvJ : scvs(fvGeometry))
                  for(auto mirrorDof : mirrorDofs)
                    pattern.add(mirrorDof, scvJ.dofIndex());

                if(bulkDofIsCoupled_[dofIdx])
                {
                  const auto eIdx = bulkGridGeometry.elementMapper().index(element);
                  const auto coupledBulkElems = getCoupledBulkElements(eIdx);
                  for(const auto bulkIdx : coupledBulkElems)
                  {
                    const auto coupledBulk = bulkGridGeometry.element(bulkIdx);
                    auto coupledBulkFvGeometry = localView(bulkGridGeometry);
                    coupledBulkFvGeometry.bind(coupledBulk);
                    for(const auto& scvC : scvs(coupledBulkFvGeometry))
                      for(auto mirrorDof : mirrorDofs)
                  pattern.add(mirrorDof, scvC.dofIndex());
                  }
                }
      
                auto it = assignedDofs.find(dofIdx);
                if(it != assignedDofs.end() || corner) continue;
                
                for(const auto& mirrorDof : mirrorDofs)
                  pattern.add(dofIdx, mirrorDof);

                for(const auto& mirrorDof0 : mirrorDofs)
                  for(const auto& mirrorDof1 : mirrorDofs)
                    pattern.add(mirrorDof0, mirrorDof1);

                assignedDofs.insert(dofIdx);
            }
        }
    }

    const GlobalPositionVectorVector getVirtualLinePoints() const
    {
      return virtualLinePoints_;
    }

    const GridIndexTypeVectorVector getVirtualLineMirrorElements() const
    {
      return virtualLineMirrorElements;
    }

    const GridIndexTypeVectorVector getVirtualLineImageElements() const
    {
      return virtualLineImageElements;
    }

private:

    GridIndexType    bulkImageCorner;
    GridIndexType    bulkMirrorCorner;
    GridIndexType    facetImageCorner;
    GridIndexTypeSet bulkCorners_;
    GridGeometries   gridGeometries_;

    GlobalPositionVectorVector virtualLinePoints_;
    bool quadraticElements_;

    GridIndexTypeSet cornerNeighbors_;
    GridIndexTypeVectorVector virtualLineMirrorElements;
    GridIndexTypeVectorVector virtualLineImageElements;
    GridIndexTypeScalarMapMultiMap interpolationCoefficients_;
    GridIndexTypeScalarMapMultiMap residualIntCoefficientsCornerNeighbors_;
    GridIndexTypeGlobalPositionMap mirrorVertices_;
    GridIndexTypeSet boundaryLeadEnrichedVertices_;  
    std::shared_ptr<CouplingManager> couplingManager_;
    std::shared_ptr<CouplingMapper> couplingMapper_;
    std::vector<bool> bulkDofIsCoupled_;
    std::multimap<int,int> coupledBulkElements_;
  
};
}

#endif
