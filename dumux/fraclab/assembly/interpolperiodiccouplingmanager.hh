
// -*- mode: C++; tab-width: 4; indent-tabs-mode: nil; c-basic-offset: 4 -*-
// vi: set et ts=4 sw=4 sts=4:
/*****************************************************************************
 *   See the file COPYING for full copying permissions.                      *
 *                                                                           *
 *   This program is free software: you can redistribute it and/or modify    *
 *   it under the terms of the GNU General Public License as published by    *
 *   the Free Software Foundation, either version 3 of the License, or       *
 *   (at your option) any later version.                                     *
 *                                                                           *
 *   This program is distributed in the hope that it will be useful,         *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of          *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the            *
 *   GNU General Public License for more details.                            *
 *                                                                           *
 *   You should have received a copy of the GNU General Public License       *
 *   along with this program.  If not, see <http://www.gnu.org/licenses/>.   *
 *****************************************************************************/

#ifndef DUMUX_BOX_GENERAL_PERIODIC_COUPLING_MANAGER_HH
#define DUMUX_BOX_GENERAL_PERIODIC_COUPLING_MANAGER_HH

#include <dune/geometry/referenceelements.hh>
#include "periodiccouplingmanager.hh"

namespace Dumux
{

  //valid only for 2d,todo: specialize for 3d, and for discretization method
  template<class TypeTag>
  class InterpolationPeriodicCouplingManager : public PeriodicCouplingManagerBase<TypeTag>
  {
    using ParentType = PeriodicCouplingManagerBase<TypeTag>;
    using Scalar = GetPropType<TypeTag, Properties::Scalar>;
    using Problem = GetPropType<TypeTag,Properties::Problem>;
    using NumEqVector = GetPropType<TypeTag, Properties::NumEqVector>;
    using PrimaryVariables = GetPropType<TypeTag, Properties::PrimaryVariables>;
    using GridGeometry = GetPropType<TypeTag, Properties::GridGeometry>;
    using FVElementGeometry = typename GridGeometry::LocalView;
    using SubControlVolume =typename  GridGeometry::SubControlVolume;
    using SubControlVolumeFace = typename GridGeometry::SubControlVolumeFace;
    using GridView = typename GridGeometry::GridView;
      
    static constexpr int dim = GridView::dimension;
    static constexpr int dimWorld = GridView::dimensionworld;
      
    using Element = typename GridView::template Codim<0>::Entity;
    using Vertex = typename GridView::template Codim<dim>::Entity;
    using GlobalPosition = typename Element::Geometry::GlobalCoordinate;
    using GridIndexType = typename GridView::IndexSet::IndexType;
    using GridVariables = GetPropType<TypeTag, Properties::GridVariables>;
    using GridVolumeVariables = typename GridVariables::GridVolumeVariables;
    using ElementVolumeVariables = typename GridVolumeVariables::LocalView;
    using VolumeVariables = typename ElementVolumeVariables::VolumeVariables;
    using GridFluxVariablesCache = typename GridVariables::GridFluxVariablesCache;
    using ElementFluxVariablesCache = typename GridFluxVariablesCache::LocalView;
    using SolutionVector = GetPropType<TypeTag, Properties::SolutionVector>;
    using GridSolutionVector = SolutionVector;
    using SystemSolutionVector = GridSolutionVector;
    using JacobianMatrix = GetPropType<TypeTag, Properties::JacobianMatrix>;  
    using GradientVector = Dune::FieldVector<Scalar, dim>;
    using ReferenceElements = typename Dune::ReferenceElements<typename GridView::ctype, dim>;

    using GridIndexTypeMap = std::map<GridIndexType, GridIndexType>;
    using GridIndexTypeMultiMap = std::multimap<GridIndexType, GridIndexType>;
    using IntMultiMap = std::multimap<int,int>;
    using GridIndexTypeGlobalPositionMap =  std::multimap<GridIndexType, GlobalPosition>;
    using GridIndexTypeScalarPair = std::pair<GridIndexType, Scalar>;
    using GridIndexTypeScalarMap =  std::map<GridIndexType, Scalar>;
    using GridIndexTypeScalarMapMultiMap = std::multimap<GridIndexType, GridIndexTypeScalarPair>;
    using GridIndexTypeSet =  std::set<GridIndexType>;
    using GridIndexTypeVector =  std::vector<GridIndexType>;
    using GridIndexTypeVectorVector =  std::vector<GridIndexTypeVector>;
    using IntVector = std::vector<int>;
    using IntVectorVector = std::vector<IntVector>;
    using IntVectorVectorVector = std::vector<IntVectorVector>;
    using ScalarVector = std::vector<Scalar>;
    using ScalarMatrix = std::vector<ScalarVector>;
    using GlobalPositionVector = std::vector<GlobalPosition>;
    using GlobalPositionVectorVector = std::vector<GlobalPositionVector>;
    using SubControlVolumeVector = std::vector<SubControlVolume>;

  public:

    InterpolationPeriodicCouplingManager(std::shared_ptr<const GridGeometry> gridGeometry,
					 const GradientVector& gradP)
      :ParentType(gridGeometry, gradP)
    {
      quadraticElements();
    }

    InterpolationPeriodicCouplingManager(std::shared_ptr<const GridGeometry> gridGeometry)
      :ParentType(gridGeometry)
    {
      quadraticElements();
    }

    bool quadraticElements()
    { 
      const auto& element = this->gridGeometry_->element(0);
      const auto& elemGeometry = element.geometry();
      const auto& referenceElement = ReferenceElements::general(elemGeometry.type());
      const auto numDofsAtSegment = referenceElement.size(0, dim-1, dim);
      return numDofsAtSegment == 3;
    }

    void setVirtualLinePoints()
    {
      virtualLinePoints_.resize(dimWorld);
    
      GridIndexTypeSet assignedDofs;
      const auto& gridGeometry = *(this->gridGeometry_);
      
      for(const auto& vertex : vertices(gridGeometry.gridView()))
      {
	auto dofCoord = vertex.geometry().center();
	const auto& dofIdx = gridGeometry.gridView().indexSet().index(vertex);
	if(!this->onBoundary_(dofCoord)) continue;

	bool minCorner = this->atMinCorner_(dofCoord);
	bool maxCorner = this->atMaxCorner_(dofCoord);
	bool maxBound = this->onMaxBoundary_(dofCoord);
	bool corner = this->atCorners_(dofCoord);
	if(minCorner) bulkImageCorner = dofIdx;
	if(maxCorner) bulkMirrorCorner = dofIdx;
	if(corner) corners_.insert(dofIdx);
	if(maxBound) mirrorVertices_.insert(std::pair<GridIndexType, GlobalPosition>(dofIdx, dofCoord));
	
	for(int dir = 0; dir < dimWorld; dir++)	  
	  if(this->onMinBoundary_(dir, dofCoord) || this->onMaxBoundary_(dir, dofCoord)){
	    dofCoord[dir] = this->minCoor[dir];
	    virtualLinePoints_[dir].push_back(dofCoord);
	    dofCoord = vertex.geometry().center();
	  }
      }
      
      for(int dir = 0; dir < dimWorld; dir++){
	for(auto itI = virtualLinePoints_[dir].begin(); itI <  virtualLinePoints_[dir].end(); itI++){
	  auto itJ = itI + 1;
	  while (itJ !=  virtualLinePoints_[dir].end()){
	    
	    auto distI = *itI;
	    auto distJ = *itJ;
	    auto distIJ = distI - distJ;
	    
	    distI -= this->minCoor;
	    distJ -= this->minCoor;

	    if(distJ.two_norm() < distI.two_norm()) std::swap(*itI, *itJ);
	    
	    if(distIJ.two_norm() < this->eps_)
	      itJ =  virtualLinePoints_[dir].erase(itJ);
	    else itJ++;
	  }
	}
      }    
    }

    void setVirtualLineBoundaryElements()
    {
      virtualLineMirrorElements.resize(dimWorld);
      virtualLineImageElements.resize(dimWorld);
    
      for(int dir = 0; dir < dimWorld; dir++)
      {
	int nline = virtualLinePoints_[dir].size()-1;
	virtualLineMirrorElements[dir].resize(nline);
	virtualLineImageElements[dir].resize(nline);
      }

      const auto& gridGeometry = *(this->gridGeometry_);
      
      for(const auto& element : elements(gridGeometry.gridView()))
      {
	const auto eIdx = gridGeometry.elementMapper().index(element);
	auto fvGeometry = localView(gridGeometry);
	fvGeometry.bind(element);
	
	GridIndexTypeVector elemBoundaryDofs;
	GlobalPositionVectorVector elemBoundaryPos(dimWorld);

	for(auto scvf: scvfs(fvGeometry))
	{
	  const auto& scvfCenter = scvf.center();
	  if(!this->onBoundary_(scvfCenter)) continue;
	  int dir = this->getBoundaryDirection(scvfCenter);
	  const auto& scv = fvGeometry.scv(scvf.insideScvIdx());
	  auto dofCoord = scv.dofPosition();
	  elemBoundaryPos[dir].push_back(dofCoord);
	}
	
	for(int dir = 0; dir < dimWorld; dir++)
        {
	  if(elemBoundaryPos[dir].size() == 0) continue;
	  
	  IntVector virtualPoints;
	  bool min = false;
	    
	  for(int idof = 0; idof < elemBoundaryPos[dir].size(); idof++)
	  {
	    auto coorI = elemBoundaryPos[dir][idof];
	    min = this->onMinBoundary_(dir, coorI);
	    coorI[dir] = this->minCoor[dir];
	    
	    for(int ip = 0; ip < virtualLinePoints_[dir].size(); ip++)
	    {
	      const auto& coorJ = virtualLinePoints_[dir][ip];
	      const auto dist = coorI - coorJ;
	      if(dist.two_norm() < this->eps_){
		virtualPoints.push_back(ip);
		break;
	      }
	    }
	  }

	  auto beginPoint = *std::min_element(virtualPoints.begin(), virtualPoints.end());
	  auto endPoint = *std::max_element(virtualPoints.begin(), virtualPoints.end());

	  for(int ip = beginPoint; ip < endPoint; ip++)
	  {
	    if(min) virtualLineImageElements[dir][ip] = eIdx;
	    else virtualLineMirrorElements[dir][ip] = eIdx;
	  }
	}
      }	
    }

    void buildInterpolationMaps()
    {      
      const auto& gridGeometry = *(this->gridGeometry_);
      const auto gaussLocalPositions = virtualGaussPointsLocalCoord();
      const auto gaussWeigths =  virtualGaussPointsWeights();

      GridIndexTypeScalarMap imageCoefficients;
      GridIndexTypeScalarMapMultiMap mirrorCoefficients;
    
      for(int dir = 0; dir < dimWorld; dir++)
      {
	auto nline = virtualLinePoints_[dir].size()-1;
	for(int il = 0; il < nline; il++)
	{
	  //asser if line has mirror and image associated elements 
	  const auto mirrorElementIdx = virtualLineMirrorElements[dir][il];
	  const auto imageElementIdx = virtualLineImageElements[dir][il];
	  const auto& mirrorElement = gridGeometry.element(mirrorElementIdx);
	  const auto& imageElement = gridGeometry.element(imageElementIdx);
	  const auto& mirrorScvs = elementBoundaryConnectivity(dir, mirrorElement);
	  const auto& imageScvs = elementBoundaryConnectivity(dir, imageElement);

	  int numBoundDof = imageScvs.size();

	  GlobalPositionVector mirrorPos;
	  GlobalPositionVector imagePos;
	  GlobalPositionVector linePos;
	  
	  for(const auto& scv : mirrorScvs) mirrorPos.push_back(scv.dofPosition());
	  for(const auto& scv : imageScvs) imagePos.push_back(scv.dofPosition());
	  for(auto& mirrorCoord : mirrorPos) mirrorCoord[dir] = this->minCoor[dir];
	  
	  const auto p1 = virtualLinePoints_[dir][il];
	  const auto p2 = virtualLinePoints_[dir][il+1];
	  
	  linePos.push_back(p1);
	  linePos.push_back(p2);
	  
	  const auto distVector = p2 -p1;
	  const auto segmentLength = distVector.two_norm();
	  const auto detJac = segmentLength/2.0;
	  
	  ScalarVector imageCoef(numBoundDof, 0.0);
	  ScalarMatrix  mirrorCoef(numBoundDof);

	  for(int i = 0; i < numBoundDof; i++) mirrorCoef[i].resize(numBoundDof);
	  
	  for(int ig = 0; ig < gaussLocalPositions.size(); ig++){

	    const auto gaussGlobalPos = getIntersectionGlobalPosition(dim-1, linePos, gaussLocalPositions[ig]);
	    const auto xi = getIntersectionLocalPosition(imagePos, gaussGlobalPos);
	    const auto dzeta = getIntersectionLocalPosition(mirrorPos, gaussGlobalPos);
	    const auto shapeValuesImage = getIntersectionShapeValues(xi);
	    const auto shapeValuesMirror = getIntersectionShapeValues(dzeta);
	    const auto residualShapeValues = getLagrangeMultipliers(xi, imagePos);

	    for(int ii = 0; ii < imageScvs.size(); ii++)
	    {
	      imageCoef[ii] += shapeValuesImage[ii]*detJac*gaussWeigths[ig];

	      for(int im = 0; im < mirrorScvs.size(); im++)
		mirrorCoef[ii][im] += residualShapeValues[ii]*shapeValuesMirror[im]*detJac*gaussWeigths[ig];
	    }
	  }

	  for(int ii = 0; ii < imageScvs.size(); ii++)
	  {
	    const auto imageDofIdx = imageScvs[ii].dofIndex();
	    auto iti = imageCoefficients.find(imageDofIdx);
	    
	    if(iti != imageCoefficients.end()) iti->second += imageCoef[ii];
	    else imageCoefficients.insert(std::pair<GridIndexType, Scalar>(imageDofIdx, imageCoef[ii]));
	    
	    for(int im = 0; im < mirrorScvs.size(); im++)
	    {
	      const auto mirrorDofIdx = mirrorScvs[im].dofIndex();
	      bool mirrorMapHasDofs = false;
	      
	      auto it = mirrorCoefficients.find(imageDofIdx);
	      while(it != mirrorCoefficients.end())
	      {
		if(it->second.first == mirrorDofIdx) {
		  mirrorMapHasDofs = true;
		  break;
		}

		if(it->first != imageDofIdx) break;
		it++;
	      }

	      if(mirrorMapHasDofs) 
		it->second.second += mirrorCoef[ii][im];
	      else {
		GridIndexTypeScalarPair mirrorPair;
		mirrorPair.first = mirrorDofIdx;
		mirrorPair.second = mirrorCoef[ii][im];
		mirrorCoefficients.insert(std::pair<GridIndexType, std::pair<GridIndexType, Scalar>>(imageDofIdx, mirrorPair));
	      }   
	    }
	  }
	}
	
	for(auto& [imageDof, mirrorPair] : mirrorCoefficients)
        {
	  auto& mirrorCoef = mirrorPair.second;
	  const auto& imageCoef = imageCoefficients.find(imageDof)->second;
	  mirrorCoef *= 1.0/imageCoef;
	  if(isCornerVertex(mirrorPair.first) && mirrorPair.first != bulkMirrorCorner) continue;
	  if(abs(mirrorCoef) > this->eps_)
	  interpolationCoefficients_.insert(std::pair<GridIndexType, std::pair<GridIndexType, Scalar>>(imageDof, mirrorPair));
	}

	imageCoefficients.clear();
	mirrorCoefficients.clear();
	
      }

      virtualLineMirrorElements.clear();
      virtualLineImageElements.clear();
    }
    
    void setPeriodicDofMap()
    {
      setVirtualLinePoints();
      setVirtualLineBoundaryElements();
      buildInterpolationMaps();
    }

    void sortInterpolPoints(std::vector<std::vector<std::pair<GridIndexType, GlobalPosition>>>& intPts)
    {
      for(int dir = 0; dir < dimWorld; dir++){
	for(auto itI = intPts[dir].begin(); itI < intPts[dir].end(); itI ++){
	  for(auto itJ = itI + 1; itJ < intPts[dir].end(); itJ ++){
	  
	    GlobalPosition distI(itI->second);
	    GlobalPosition distJ(itJ->second);

	    distI -= this->minCoor;
	    distJ -= this->minCoor;

	    if(distJ.two_norm() < distI.two_norm()) std::swap(*itI, *itJ);
	  }
	}
      }
    }

    const ScalarVector virtualGaussPointsLocalCoord()
    {
      ScalarVector gCoord;

      if(dim == 2 && !quadraticElements_){
	gCoord.push_back(-1.0/(sqrt(3.0)));
	gCoord.push_back(1.0/(sqrt(3.0)));
      }
    
      if(dim == 2 && quadraticElements_){
	gCoord.push_back(-sqrt(3.0/5.0));
	gCoord.push_back(0.0);
	gCoord.push_back(sqrt(3.0/5.0));
      }

      return gCoord;
    }

    const ScalarVector virtualGaussPointsWeights()
    {
      ScalarVector gWeig;
      if(dim == 2 && !quadraticElements_){
	gWeig.push_back(1.0);
	gWeig.push_back(1.0);
      }
    
      if(dim == 2 && quadraticElements_){
	gWeig.push_back(5.0/9.0);
	gWeig.push_back(8.0/9.0);
	gWeig.push_back(5.0/9.0);
      }

      return gWeig;
    }

    const GlobalPosition getIntersectionGlobalPosition(int dimIntersection,
						     const GlobalPositionVector& intersecGlobalPos,
						     const Scalar localPos)
    {
      Scalar refDist = localPos + 1.0;
      auto distVector = intersecGlobalPos[1] - intersecGlobalPos[0];
      Scalar coef = refDist/2.0;
      distVector *= coef;
      
      auto globalPos(intersecGlobalPos[0]);
      globalPos += distVector;
      
      return globalPos;
    }

    const Scalar getIntersectionLocalPosition(const GlobalPositionVector& intersecGlobalPos,
					    const GlobalPosition globalPos)
    {
      GlobalPosition segVector = intersecGlobalPos[intersecGlobalPos.size()-1] - intersecGlobalPos[0];
      Scalar segLeng = segVector.two_norm();
      GlobalPosition distVector = globalPos - intersecGlobalPos[0];
      Scalar coef = distVector.two_norm()/segLeng;
      Scalar localPos = -1.0 + coef*2.0;
      
      return localPos;
    }

    ScalarVector getIntersectionShapeValues(const Scalar& localPos) 
    {
      ScalarVector shapeValues;
      int size = quadraticElements_ ? 3 : 2;
      shapeValues.resize(size);
      
      if(size == 2){
	shapeValues[0] = 0.5*(1.0-localPos);
	shapeValues[1] = 0.5*(1.0+localPos);
      }
      
      if(size == 3){
	shapeValues[0] = 0.5*(localPos - localPos*localPos);
	shapeValues[1] = 1.0-localPos*localPos;
	shapeValues[2] = 0.5*(localPos + localPos*localPos);
      }
      return shapeValues;
    }

    ScalarVector getLagrangeMultipliers(const Scalar& localPos, const GlobalPositionVector& imagePos) 
    {
      ScalarVector lagrangeValues;
      int size = quadraticElements_ ? 3 : 2;
      lagrangeValues.resize(size);
      
      bool corner = this->atCorners_(imagePos[0]) || this->atCorners_(imagePos[size-1]);
      bool mirrorCorner = this->atMaxCorner_(imagePos[0]) || this->atMaxCorner_(imagePos[size-1]);
      bool hasPrescribedCorner = corner && !mirrorCorner;
      
      if(!hasPrescribedCorner)
      {
	if(size == 2){
	  lagrangeValues[0] = 0.5*(1.0-3.0*localPos);
	  lagrangeValues[1] = 0.5*(1.0+3.0*localPos);
	}

	if(size == 3){
	  lagrangeValues[0] = 0.25*(5.0*pow(localPos, 2.0) - 2.0 * localPos - 1.0);
	  lagrangeValues[1] = 0.5*(3.0-5.0*pow(localPos, 2.0));
	  lagrangeValues[2] = 0.25*(5.0*pow(localPos, 2.0) + 2.0 * localPos -1.0);
	}
      } else {

	int cornerPos = this->atCorners_(imagePos[0]) ? 0 : size-1;
      
	if(size == 2){
	  lagrangeValues[0] = cornerPos == 0 ? 0.0 : 1.0;
	  lagrangeValues[1] = cornerPos == 0 ? 1.0 : 0.0;
	}
	
	if(size == 3 && cornerPos == 0){
	  lagrangeValues[0] = 0.0;
	  lagrangeValues[1] = 1.0 - localPos;
	  lagrangeValues[2] = localPos;
	} else if(size == 3 && cornerPos == 2){
	  lagrangeValues[0] = -localPos;
	  lagrangeValues[1] = 1.0 + localPos;
	  lagrangeValues[2] = 0.0;
	}
      }

      return lagrangeValues;
    }
    
    SubControlVolumeVector elementBoundaryConnectivity(int dir, const Element& element)
    {
      const auto& gridGeometry = *(this->gridGeometry_);
      auto fvGeometry = localView(gridGeometry);
      fvGeometry.bind(element);

      SubControlVolumeVector boundaryScvs;

      for(const auto& scv : scvs(fvGeometry))
	if(this->onBoundary_(dir, scv.dofPosition())) boundaryScvs.push_back(scv);

      //sort according to positon

      for(auto itI = boundaryScvs.begin(); itI < boundaryScvs.end(); itI++){
	for(auto itJ = itI + 1; itJ < boundaryScvs.end(); itJ++){
	  const auto scvI = *itI;
	  const auto scvJ = *itJ;
	  auto distI =  scvI.dofPosition();
	  auto distJ =  scvJ.dofPosition();
	  distI -= this->minCoor;
	  distJ -= this->minCoor;
	  if(distJ.two_norm() < distI.two_norm()) std::swap(*itI, *itJ);
	}
      }

      return boundaryScvs;
    }

    int getNewDofIdx(const GridIndexType dofIdx) const
    {
      return 0;
    }

    bool isCornerVertex(const GridIndexType dofIdx) const
    {
      return corners_.count(dofIdx);
    }

    const std::vector<GridIndexType> getMirrorDofIndices(const GridIndexType dofIdx) const
    {
      std::vector<GridIndexType> mirrorDofs;
      
      for (auto it = interpolationCoefficients_.find(dofIdx); it != interpolationCoefficients_.end(); it++)
      {
	if(it->first != dofIdx) break;
	mirrorDofs.push_back(it->second.first);
      }
      return mirrorDofs;
    }

    const Scalar getInterpolationCoefficient(const GridIndexType imageDofIdx,
					   const GridIndexType mirrorDofIdx) const
    {
      auto it = interpolationCoefficients_.find(imageDofIdx);
      while(it != interpolationCoefficients_.end())
      {
	  if(it->first != imageDofIdx) break;
	  if(it->second.first == mirrorDofIdx) return it->second.second;
	  it++;
      }
      return 0.0;
    }

    const PrimaryVariables linearPVComponent(const GlobalPosition& globalPos) const
    {
      GlobalPosition dist = globalPos - this->maxCoor;
      PrimaryVariables x = dist.dot(this->gradP_);
      return x;
    }

    Scalar computeResidual(GridIndexType dofIdx,
			   const GlobalPosition& globalPos,
			   const SolutionVector& curSol) const
    {
      Scalar res = curSol[dofIdx];
      
      const auto mirrorDofs = getMirrorDofIndices(dofIdx);
      for(const auto mirrorDof : mirrorDofs)
      {
	const auto coef = getInterpolationCoefficient(dofIdx, mirrorDof);
	const auto mirrorCord = mirrorVertices_.find(mirrorDof)->second;
	const auto mirrorX = linearPVComponent(mirrorCord);
	res -= coef * curSol[mirrorDof];
	res += coef * mirrorX;
      }

      const auto x = linearPVComponent(globalPos);      
      res -= x;
 
      return res;
    }

    Scalar computeDerivative(GridIndexType dofIdx,
			     GridIndexType mirrorDof,
			     const SolutionVector& curSol) const
    {
      Scalar derivative =  getInterpolationCoefficient(dofIdx, mirrorDof);
      derivative *= -1.0;
      
      return derivative;
    }
    
    void enforceSystemPeriodicConstraints(JacobianMatrix& jac,
					  SolutionVector& res,
					  const SolutionVector& curSol) const
    {
      for (const auto& vertex : vertices(this->gridGeometry().gridView())){

	const auto coord = vertex.geometry().center();
	const auto dofIdx = this->gridGeometry().gridView().indexSet().index(vertex);
	
	if(!this->onMinBoundary_(coord)) continue;
	
	const auto end = jac[dofIdx].end();	
	auto mirrorDofs = getMirrorDofIndices(dofIdx);
       
	for(auto mirrorDof : mirrorDofs)
	{
	  Scalar coef = getInterpolationCoefficient(dofIdx, mirrorDof);
	  res[mirrorDof] +=  coef * res[dofIdx];	   
	  for (auto it = jac[dofIdx].begin(); it != end; ++it)
	    jac[mirrorDof][it.index()] += coef * (*it);	   
	}

	res[dofIdx] = computeResidual(dofIdx, coord, curSol);
	
	for (auto it = jac[dofIdx].begin(); it != end; ++it)
	  (*it) = it.index() == dofIdx ? 1.0 : 0.0;

	for(auto mirrorDof : mirrorDofs)
	  jac[dofIdx][mirrorDof] = computeDerivative(dofIdx, mirrorDof, curSol);
       }
    }

    template<class JacobianPattern>
    void extendJacobianPattern(JacobianPattern& pattern) const
    {
      std::set<GridIndexType> assignedDofs;
	
      for (const auto& element : elements(this->gridGeometry_->gridView()))
      {
	auto fvGeometry = localView(*(this->gridGeometry_));
	fvGeometry.bind(element);
	
	for (const auto& scv : scvs(fvGeometry)){

	  if(!this->onMinBoundary_(scv.dofPosition())) continue;
	  
	  const auto dofIdx = scv.dofIndex();
	  const auto mirrorDofs = getMirrorDofIndices(dofIdx);
	    
	  for (const auto& scvJ : scvs(fvGeometry))
	    for(auto mirrorDof : mirrorDofs)
	      pattern.add(mirrorDof, scvJ.dofIndex());
	  
	  auto it = assignedDofs.find(dofIdx);
	  if(it != assignedDofs.end()) continue;
	  
	  for(const auto& mirrorDof : mirrorDofs)
	    pattern.add(dofIdx, mirrorDof);
	  
	  for(const auto& mirrorDof0 : mirrorDofs)
	    for(const auto& mirrorDof1 : mirrorDofs)
	      pattern.add(mirrorDof0, mirrorDof1);
	  
	  assignedDofs.insert(dofIdx);
	}
      }
      
      assignedDofs.clear();
    }

  private:

    GridIndexTypeVectorVector virtualLineMirrorElements;
    GridIndexTypeVectorVector virtualLineImageElements;
    GlobalPositionVectorVector virtualLinePoints_;
    GridIndexTypeGlobalPositionMap mirrorVertices_;
    GridIndexTypeScalarMapMultiMap interpolationCoefficients_;
    
    std::set<GridIndexType> corners_;
    GridIndexType minCornerVidx;
    GridIndexType bulkImageCorner;
    GridIndexType bulkMirrorCorner;
    bool quadraticElements_;
 
  };
}


#endif
