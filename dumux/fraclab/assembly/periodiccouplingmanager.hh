// -*- mode: C++; tab-width: 4; indent-tabs-mode: nil; c-basic-offset: 4 -*-
// vi: set et ts=4 sw=4 sts=4:
/*****************************************************************************
 *   See the file COPYING for full copying permissions.                      *
 *                                                                           *
 *   This program is free software: you can redistribute it and/or modify    *
 *   it under the terms of the GNU General Public License as published by    *
 *   the Free Software Foundation, either version 3 of the License, or       *
 *   (at your option) any later version.                                     *
 *                                                                           *
 *   This program is distributed in the hope that it will be useful,         *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of          *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the            *
 *   GNU General Public License for more details.                            *
 *                                                                           *
 *   You should have received a copy of the GNU General Public License       *
 *   along with this program.  If not, see <http://www.gnu.org/licenses/>.   *
 *****************************************************************************/

#ifndef DUMUX_BOX_PERIODIC_COUPLING_MANAGER_HH
#define DUMUX_BOX_PERIODIC_COUPLING_MANAGER_HH

//Periodic coupling manager for periodic meshes (strongly periodic)

#include <memory>
#include <tuple>
#include <vector>
#include <set>
#include <algorithm>
#include <cassert>
#include <dune/common/exceptions.hh>
#include <dune/common/indices.hh>
#include <dumux/assembly/numericepsilon.hh>
#include <dumux/common/properties.hh>
#include <dumux/common/typetraits/typetraits.hh>
#include <dumux/common/properties.hh>
#include <dumux/common/indextraits.hh>
#include <dumux/discretization/method.hh>
#include <dumux/discretization/elementsolution.hh>
#include <dumux/discretization/evalsolution.hh>

// Periodic Coupling Manager for STRONG Periodicity (periodic meshes)

namespace Dumux
{

  template<class TypeTag>
  class PeriodicCouplingManagerBase
  {
    using Scalar = GetPropType<TypeTag, Properties::Scalar>;
    using Problem = GetPropType<TypeTag,Properties::Problem>;
    using NumEqVector = GetPropType<TypeTag, Properties::NumEqVector>;
    using PrimaryVariables = GetPropType<TypeTag, Properties::PrimaryVariables>;
    using GridGeometry = GetPropType<TypeTag, Properties::GridGeometry>;
    using FVElementGeometry = typename GridGeometry::LocalView;
    using SubControlVolume =typename  GridGeometry::SubControlVolume;
    using SubControlVolumeFace = typename GridGeometry::SubControlVolumeFace;
    using GridView = typename GridGeometry::GridView;
      
    static constexpr int dim = GridView::dimension;
    static constexpr int dimWorld = GridView::dimensionworld;
    enum { numEq = GetPropType<TypeTag, Properties::ModelTraits>::numEq() };
      
    using Element = typename GridView::template Codim<0>::Entity;
    using Vertex = typename GridView::template Codim<dim>::Entity;
    using GlobalPosition = typename Element::Geometry::GlobalCoordinate;
    using GridIndexType = typename GridView::IndexSet::IndexType;
    using GridVariables = GetPropType<TypeTag, Properties::GridVariables>;
    using GridVolumeVariables = typename GridVariables::GridVolumeVariables;
    using ElementVolumeVariables = typename GridVolumeVariables::LocalView;
    using VolumeVariables = typename ElementVolumeVariables::VolumeVariables;
    using GridFluxVariablesCache = typename GridVariables::GridFluxVariablesCache;
    using ElementFluxVariablesCache = typename GridFluxVariablesCache::LocalView;
    using SolutionVector = GetPropType<TypeTag, Properties::SolutionVector>;
    using GridSolutionVector = SolutionVector;
    using SystemSolutionVector = GridSolutionVector;
    using JacobianMatrix = GetPropType<TypeTag, Properties::JacobianMatrix>;
    using GradientPV = Dune::FieldVector<GlobalPosition, numEq>;

  public:

    PeriodicCouplingManagerBase(std::shared_ptr<const GridGeometry> gridGeometry,
                                const GradientPV& gradX,
                                int n = 10)
        :gridGeometry_(gridGeometry)
        ,gradX_(gradX)
        ,ndiv(n)
    {
         masterCornerValues_ = 0.0;
         init();  
    }

    PeriodicCouplingManagerBase(std::shared_ptr<const GridGeometry> gridGeometry)
    :gridGeometry_(gridGeometry)
    {
        masterCornerValues_ = 0.0;
        init();
    }

    void init()
    {
        minDofs_.resize(dimWorld);
        maxDofs_.resize(dimWorld);

        for(int i = 0; i < dimWorld; i++){
            minDofs_[i].resize(ndiv);
            maxDofs_[i].resize(ndiv);
        }
      
        minCoor = gridGeometry_ -> bBoxMin();
        maxCoor = gridGeometry_ -> bBoxMax();

    }

    const PrimaryVariables primaryVariablesDiff(const GlobalPosition& coord) const
    {
        PrimaryVariables dvar(0.0);
        const auto dist = distanceToImageDof(coord);
        for(int ipv = 0; ipv < numEq; ipv++)
            dvar[ipv] = dist.dot(gradX_[ipv]);
        return dvar;
    }

    const Scalar primaryVariablesScalarDiff(const GlobalPosition& coord) const
    {
        const auto dist = distanceToImageDof(coord);
        return dist.dot(gradX_[0]);
    }

    const GlobalPosition distanceToImageDof(const GlobalPosition& globalPos) const
    {
      GlobalPosition dist(0.0);
      
      if (atCorners_(globalPos))
      {
          dist = globalPos;
          dist -= this->minCoor;
      } else if (onMaxBoundary_(globalPos)){
          int dir = getBoundaryDirection(globalPos);
          dist[dir] = this->maxCoor[dir] - this->minCoor[dir]; ///
      }

      return dist;	
    }

    //virtual bool isDependentDofIdx(const GridIndexType originalDofIdx) const = 0;

    bool isDependentDof(const GridIndexType originalDofIdx) const
    {
      return this->isDependentDofIdx(originalDofIdx);
    }

    bool onMinBoundary_(int dir, const GlobalPosition& globalPos) const
    {
        return abs(globalPos[dir]-minCoor[dir]) < eps_;
    }

    bool onMinBoundary_(int dir, const GlobalPosition& globalPos)
    {
        return abs(globalPos[dir]-minCoor[dir]) < eps_;
    }

    bool onMaxBoundary_(int dir, const GlobalPosition& globalPos) const
    {
        return abs(globalPos[dir]-maxCoor[dir]) < eps_;
    }

    bool onMaxBoundary_(const GlobalPosition& globalPos)
    {
        bool max = false;
        for(int dir = 0; dir < dimWorld; dir++){
            if(onMaxBoundary_(dir, globalPos))
            {
                //if(!atCorners_(globalPos)){
                    max = true;
                    break;
            }
        }

        return max;       
    }

    bool onMinBoundary_(const GlobalPosition& globalPos)
    {
        bool min = false;
        for(int dir = 0; dir < dimWorld; dir++){
            if(onMinBoundary_(dir, globalPos))
            {
                min= true;
                break;
            }
        }

        return min;
    }

    bool onMinBoundary_(const GlobalPosition& globalPos) const
    {
        bool min = false;
        for(int dir = 0; dir < dimWorld; dir++){
            if(onMinBoundary_(dir, globalPos))
             {
                 min= true;
                 break;
             }
        }
        
        return min; 
    }
      
    bool onMaxBoundary_(const GlobalPosition& globalPos) const
    {
        bool max = false;
        for(int dir = 0; dir < dimWorld; dir++){
            if(onMaxBoundary_(dir, globalPos))
            {
                // if(!atCorners_(globalPos)){
                    max = true;
                    break;
            }
        }


        return max;       
    }

    bool onBoundary_(int dir, const GlobalPosition& globalPos)
    {
        return onMinBoundary_(dir, globalPos) || onMaxBoundary_(dir, globalPos);
    }

    bool onBoundary_(int dir, const GlobalPosition& globalPos) const
    {
        return onMinBoundary_(dir, globalPos) || onMaxBoundary_(dir, globalPos);
    }

    bool onBoundary_(const GlobalPosition& globalPos)
    {
        bool bound = false;
        for(int dir = 0; dir<dimWorld; dir++){
            if(onMinBoundary_(dir,globalPos) || onMaxBoundary_(dir, globalPos))
                return true;
        }

        return bound;
    }
      
    bool onBoundary_(const GlobalPosition& globalPos) const
    {
        bool bound = false;
        for(int dir = 0; dir<dimWorld; dir++){
            if(onMinBoundary_(dir,globalPos) || onMaxBoundary_(dir, globalPos))
                return true;
        }

        return bound;
    }

    bool atMaxCorner_(const GlobalPosition& globalPos) const
    {
      GlobalPosition dist(maxCoor);
      dist -= globalPos;

      return dist.two_norm() < eps_;
    }

    bool atMaxCorner_(const GlobalPosition& globalPos) 
    {
      GlobalPosition dist(maxCoor);
      dist -= globalPos;

      return dist.two_norm() < eps_;
    }
    
    bool atCorners_(const GlobalPosition& globalPos) const
    {
        int nbd = 0;

        for(int dir = 0; dir < dimWorld; dir++)
            if(onMaxBoundary_(dir,globalPos) || onMinBoundary_(dir, globalPos)) nbd++;

        return nbd == dimWorld;    
    }

    bool atCorners_(const GlobalPosition& globalPos) 
    {
        int nbd = 0;

        for(int dir = 0; dir < dimWorld; dir++)
            if(onMaxBoundary_(dir,globalPos) || onMinBoundary_(dir, globalPos)) nbd++;

        return nbd == dimWorld;    
    }

    bool atMirrorCorner_(const GlobalPosition& globalPos)
    {
        return atCorners_(globalPos) && !atMinCorner_(globalPos);
    }

    bool atMirrorCorner_(const GlobalPosition& globalPos) const
    {
        return atCorners_(globalPos) && !atMinCorner_(globalPos);
    }

    bool atMinCorner_(const GlobalPosition& globalPos)
    {
        GlobalPosition dist(minCoor);
        dist -= globalPos;

        return dist.two_norm() < eps_;
    }

    bool atMinCorner_(const GlobalPosition& globalPos) const
    {
        GlobalPosition dist(minCoor);
        dist -= globalPos;

        return dist.two_norm() < eps_;
    }
    
    int getBoundaryDirection(const GlobalPosition& globalPos)
    {
         for(int dir = 0; dir < dimWorld; dir++)
         {
	   if((onMaxBoundary_(dir, globalPos) || onMinBoundary_(dir, globalPos)))
		// && !atCorners_(globalPos))
                 return dir;
         }

         return dimWorld + 1;
    }

    int getBoundaryDirection(const GlobalPosition& globalPos) const
    {
         for(int dir = 0; dir < dimWorld; dir++)
         {
             if((onMaxBoundary_(dir, globalPos) || onMinBoundary_(dir, globalPos)))
                 // && !atCorners_(globalPos))
                 return dir;
         }

         return dimWorld + 1;
    }

    void setBoundaryRegionsNumber(int n)
    {
        ndiv = n;
    }

    const GridGeometry& gridGeometry() const
    { return *gridGeometry_; }
    
    const GradientPV& primaryVariablesGradient() 
    {
        return gradX_;
    }


    const GradientPV& primaryVariablesGradient() const
    {
        return gradX_;
    } 

    void setPrimaryVariablesGradient(const GradientPV gradX)
    {
        gradX_ = gradX;
    }

    const GlobalPosition getMinCoor() const
    {
        return minCoor;
    }
    const GlobalPosition getMaxCoor() const
    {
        return maxCoor;
    }

     const PrimaryVariables presribedValueAtMasterCorner() const
    {
      return masterCornerValues_;
    }
      
    void prescribeMasterCorner(const PrimaryVariables& value)
    {masterCornerValues_ = value;}

protected:

    GlobalPosition minCoor;
    GlobalPosition maxCoor;
    std::shared_ptr<const GridGeometry> gridGeometry_;
    std::vector<GridIndexType> corners_;
    std::vector<std::vector<std::map<GridIndexType, GlobalPosition>>> minDofs_;
    std::vector<std::vector<std::map<GridIndexType, GlobalPosition>>> maxDofs_;
    GradientPV gradX_;
    int ndiv = 10;
    Scalar eps_ = 1e-6;
    PrimaryVariables masterCornerValues_;
  };
}

#endif
