// -*- mode: C++; tab-width: 4; indent-tabs-mode: nil; c-basic-offset: 4 -*-
// vi: set et ts=4 sw=4 sts=4:
/*****************************************************************************
 *   See the file COPYING for full copying permissions.                      *
 *                                                                           *
 *   This program is free software: you can redistribute it and/or modify    *
 *   it under the terms of the GNU General Public License as published by    *
 *   the Free Software Foundation, either version 3 of the License, or       *
 *   (at your option) any later version.                                     *
 *                                                                           *
 *   This program is distributed in the hope that it will be useful,         *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of          *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the            *
 *   GNU General Public License for more details.                            *
 *                                                                           *
 *   You should have received a copy of the GNU General Public License       *
 *   along with this program.  If not, see <http://www.gnu.org/licenses/>.   *
 *****************************************************************************/
/*!
 * \file
 * \ingroup Flux
 * \brief class containing the return algorithm to the yield surface
 * \The mohr coulomb model is treated as a particular case because it requires a treatment for 
 * its singularities. Here, the return algorithm proposed by Crisfield (reference below) was selected.
 */
#ifndef DUMUX_PLASTIC_RETURN_ALGORITHM_HH
#define DUMUX_PLASTIC_RETURN_ALGORITHM_HH

#include <dune/istl/scalarproducts.hh>
#include <dumux/fraclab/geomechanics/mohrcoulomb.hh>

namespace Dumux {

template< class Scalar, class GridGeometry>
class ReturnAlgorithmBase
{  
    using GridView = typename GridGeometry::GridView;
    //using FluxVarsCache = StressHistoryCache<Scalar,GridGeometry>;
    static constexpr int dim = GridView::dimensionworld;
    static constexpr int numStre = dim == 2 ? 4 : 2*dim;

public:

    using StressVector = Dune::FieldVector<Scalar, numStre>;
    //! export the type used for normal vector to yield surface
    using YieldNormal = Dune::FieldVector<Scalar, numStre>;
    //! export the type used for constitutive elastic and elastoplastic matrices
    using ElasticMatrix = Dune::FieldMatrix<Scalar, numStre, numStre>;
    using ElastoplasticMatrix = Dune::FieldMatrix<Scalar, numStre, numStre>;

    using StressTensor = Dune::FieldMatrix<Scalar, 3, 3>;

    static Scalar lambdaIncrement(const Scalar yieldFunction, 
                                  const ElasticMatrix& elasticMatrix,
                                  const YieldNormal& af, 
                                  const YieldNormal& ag)
    {
        YieldNormal eag;
        elasticMatrix.mv(ag, eag);
        Scalar aea = af.dot(eag);
        Scalar dlambda = yieldFunction/aea;

        return dlambda;
    }

    static StressVector stressIncrement(const Scalar dlambda, 
                                        const ElasticMatrix& elasticMatrix,
				                        const YieldNormal& ag)
    {
        StressVector dstress;
        elasticMatrix.mv(ag, dstress);
        dstress *= (-dlambda);

        return dstress;
    }

    template<class PlasticModel>
    static const ElastoplasticMatrix elastoplasticTangentMatrix(const StressTensor& stressTensor,
                                                                 const StressVector& stressVector,
                                                                 const PlasticModel& plasticModel,
                                                                 const ElasticMatrix& elasticMatrix)                                                      
    {
        auto epMatrix = elasticMatrix;
        const auto& flowNormal = plasticModel.flowFunctionNormal(stressTensor);
        const auto& yieldNormal = plasticModel.yieldFunctionNormal(stressTensor);

        StressVector eaf(0.0), eay(0.0);
        elasticMatrix.mv(flowNormal, eaf);
        elasticMatrix.mtv(yieldNormal, eay);
        Scalar yef = yieldNormal.dot(eaf);

        for (int i = 0; i < numStre; i++)
            for (int j = 0; j < numStre; j++)
                epMatrix[i][j] -= eaf[i]*eay[j]/yef;
            
        return epMatrix;
    }

    template<class FluxVarsCache, class PlasticModel, class ElasticMatrix, class Tensor, class StressVector>
    static void returnAlgorithm(const FluxVarsCache& fluxVarCache,
                                const ElasticMatrix& elasticMatrix,
                                const PlasticModel& plasticModel,
                                const Scalar& trialYieldFunction,
                                const Tensor& initialStressTensor,
                                Tensor& stressTensor,
                                StressVector& stress
                                )
    {
        DUNE_THROW(Dune::NotImplemented, "Please define a valid return algorithm class for the ReturnAlgorithm property. This class needs to overload the function "
        " returnAlgorithm of class ReturnAlgorithmBase");
    }
};


// RETURN ALGORITHM FOR MOHR-COULOMB MODEL PROPOSED BY CRISFIELD (1987):
// Crisfield, M.A. (1987), "Plasticity computations using the Mohr—Coulomb yield criterion", 
// Engineering Computations, Vol. 4 No. 4, pp. 300-308. https://doi.org/10.1108/eb023

template<class Scalar, class GridGeometry>
class MohrCoulombReturn
: public ReturnAlgorithmBase<Scalar, GridGeometry>
{
    using Parent = ReturnAlgorithmBase<Scalar, GridGeometry>;
    using FVElementGeometry = typename GridGeometry::LocalView;
    using SubControlVolumeFace = typename FVElementGeometry::SubControlVolumeFace;

    using GridView = typename GridGeometry::GridView;
    using Element = typename GridView::template Codim<0>::Entity;

    static constexpr int dim = GridView::dimension;
    static constexpr int dimWorld = GridView::dimensionworld;
    static constexpr int numStre = dimWorld == 2 ? 4 : 6;

public:

    using Tensor = Dune::FieldMatrix<Scalar, 3, 3>;
    //! export the type used for the stress tensor
    using StressTensor = Dune::FieldMatrix<Scalar, dim, dimWorld>;
    //! export the type used for force vectors
    using ForceVector = typename StressTensor::row_type;
    //! export the type used for normal vector to yield surface
    using StressVector = Dune::FieldVector<Scalar, numStre>;
    using YieldNormal = StressVector;
    //! export the type used for constitutive elastic matrix
    using ElasticMatrix = Dune::FieldMatrix<Scalar, numStre, numStre>;

    using CamInvar = CambridgeInvariants<Scalar, dimWorld>;
    //static inline bool atApex = false;
    static inline bool cornerToCorner_ = false;

    template<class FluxVarsCache, class PlasticModel, class ElasticMatrix>
    static void returnAlgorithm(const FluxVarsCache& fluxVarCache,
                                const ElasticMatrix& elasticMatrix,
                                const PlasticModel& plasticModel,
                                const Scalar& trialYieldFunction,
                                const Tensor& initialStressTensor,
                                Tensor& stressTensor,
                                StressVector& stress
                                )
    {
        // return algorithm for Mohr-Coulomb Model proposed by Crisfield(1987)
        //atApex = false;
        cornerToCorner_ = false;
        Scalar theta30 = (30.0/180.0)*M_PI;

        
        auto initialTheta = CamInvar::lodeAngle(initialStressTensor);
        auto trialTheta= CamInvar::lodeAngle(stressTensor);
        if(fluxVarCache.isAtApex())
        {   
            returnToApex<FluxVarsCache>(plasticModel,fluxVarCache,stressTensor,stress);
            stressTensor -= initialStressTensor;    
            return;
        }
          
        // if initial and trial stresses are at corner
       auto yf = trialYieldFunction;

        if(abs(initialTheta) ==  theta30 and abs(trialTheta) == theta30)
        {
            //std::cout << " CORNER TO CORNER " << std::endl;
            cornerToCorner_ = true;
            const auto& dpOuter = plasticModel.equivalentDruckerPragerOuter();
            const auto& dpInner = plasticModel.equivalentDruckerPragerInner();
            yf = trialTheta > 0 ? dpOuter.yieldFunction(stressTensor) :
                                  dpInner.yieldFunction(stressTensor);          
        }
        
        const auto trialYieldNormal = plasticModel.yieldFunctionNormal(stressTensor);
        const auto trialFlowNormal = plasticModel.flowFunctionNormal(stressTensor);
        
        auto dlambda = Parent::lambdaIncrement(yf, elasticMatrix, trialYieldNormal,trialFlowNormal);
        auto dstress = Parent::stressIncrement(dlambda, elasticMatrix, trialFlowNormal);
        stress += dstress;
        stressTensor = fluxVarCache.stressVectorToTensor(stress);
        const auto yieldNormal = plasticModel.yieldFunctionNormal(stressTensor);
        auto a = yieldNormal.dot(trialYieldNormal);
        auto normTrial = trialYieldNormal.two_norm();
        auto norm = yieldNormal.two_norm();
        auto cosb = a/(normTrial*norm);

        //if apex is crossed, return stresses to apex
        if(cosb < 0.0)
        {  
            stress -= dstress;
            returnToApex<FluxVarsCache>(plasticModel,fluxVarCache,stressTensor,stress);      
            //atApex = true;
        }
        // if a corner is crossed, return to corner with the secondary yield functions proposed by Crisfield
        if(cosb >=0 and cosb < cos(M_PI/360.0))// and theta != theta30)
        {
            stress -= dstress;
            stressTensor = fluxVarCache.stressVectorToTensor(stress);

            TwoVectorReturn<FluxVarsCache>(fluxVarCache,plasticModel, elasticMatrix, trialFlowNormal,
                                            trialYieldNormal, trialYieldFunction, stressTensor, stress);
        }

   }

    template<class PlasticModel>
    static bool isAtApex(const PlasticModel& plasticModel,
                         const Tensor& stress)
    {
        const auto& cohesion = plasticModel.cohesion();
        const auto& frictionAngle = plasticModel.frictionAngle();

        Scalar sc = cohesion/tan(frictionAngle);

        return (stress[0][0] == sc and stress[1][1] == sc and stress[2][2] == sc);
      
    } 

    template<class PlasticModel>
    static bool isAtApex(const PlasticModel& plasticModel,
                         const StressVector& stress)
    {
        const auto& cohesion = plasticModel.cohesion();
        const auto& frictionAngle = plasticModel.frictionAngle();

        Scalar sc = cohesion/tan(frictionAngle);

        return (stress[0] == sc and stress[1]== sc and stress[2]== sc);
      
    } 

    template<class FluxVarsCache, class PlasticModel>
    static void returnToApex(const PlasticModel& mohrCoulomb,
                             const FluxVarsCache fluxVarCache,
                             Tensor& stressTensor,
			                 StressVector& stress)
    {
        const auto& cohesion = mohrCoulomb.cohesion();
        const auto& frictionAngle = mohrCoulomb.frictionAngle();

        StressVector apexStress(0.0);    
        for (int i = 0; i < 3; ++i) apexStress[i] = cohesion/tan(frictionAngle);

        /*if(NumericDifferentiation::jacobianAssembly) stress -= apexStress;
        else*/ stress = apexStress;

        stressTensor = fluxVarCache.stressVectorToTensor(stress);
        //PlasticCorrection::cornerToCorner = true;
    }
  
    template<class FluxVarsCache,class PlasticModel>
    static void TwoVectorReturn(const FluxVarsCache fluxVarCache,
                                const PlasticModel& mohrCoulomb,
				                const ElasticMatrix& elasticMatrix,
				                const YieldNormal& trialFlowNormal,
				                const YieldNormal& trialYieldNormal,
				                const Scalar& trialYieldFunction,
				                Tensor& stressTensor,     
				                StressVector&  stress)					        
    {
        // normal to second yield function
        Scalar secondYieldFunction;
        YieldNormal secondYieldNormal = secondaryYieldFunction(mohrCoulomb, stressTensor, secondYieldFunction);
        YieldNormal secondFlowNormal;
        for (int i = 0; i < numStre; ++i) secondFlowNormal[i] = secondYieldNormal[i];

        StressVector dsig1(0.0), dsig2(0.0);
        elasticMatrix.mv(trialFlowNormal, dsig1);
        elasticMatrix.mv(secondFlowNormal, dsig2);

        auto a1 = trialYieldNormal.dot(dsig1);
        auto a2 = trialYieldNormal.dot(dsig2);
        auto b1 = secondYieldNormal.dot(dsig1);
        auto b2 = secondYieldNormal.dot(dsig2);
        auto dlamba = b1 - (a1/a2)*b2;
        dlamba = (secondYieldFunction - trialYieldFunction*(b2/a2))/dlamba;
        auto dlambb = (trialYieldFunction - a1*dlamba)/a2;

        dsig1 *= dlamba;
        dsig2 *= dlambb;
        stress -= dsig1;
        stress -= dsig2;

        stressTensor = fluxVarCache.stressVectorToTensor(stress);
  }

    template<class PlasticModel>
    static YieldNormal secondaryYieldFunction(const PlasticModel& mohrCoulomb,	    				                     
					                          const Tensor&  stressTensor,
				          	                  Scalar& yieldFunction)
    {
        const auto& co = mohrCoulomb.cohesion();
        const auto& fi = mohrCoulomb.frictionAngle();
    
        auto p = CamInvar::meanStress(stressTensor);
        auto q = CamInvar::deviatoricStress(stressTensor);
        auto theta = CamInvar::lodeAngle(stressTensor);
    
        Scalar At, dAdt;

        if(theta > 0.0)
        {
            At = 0.5*cos(theta)*(1.0 - sin(fi)) + (sin(theta) / (2.0*sqrt(3.0)))*(3.0 + sin(fi));
            dAdt = -0.5*sin(theta)*(1.0 - sin(fi)) + (cos(theta) / (2.0*sqrt(3.0)))*(3.0 + sin(fi));
        } else {
            At = 0.5*cos(theta)*(1.0 + sin(fi)) + (sin(theta) / (2.0*sqrt(3.0)))*(sin(fi) - 3.0);
            dAdt = -0.5*sin(theta)*(1.0 + sin(fi)) +  (cos(theta) / (2.0*sqrt(3.0)))*(sin(fi) - 3.0);
        }

        yieldFunction = p * sin(fi) + q * At - co * cos(fi);

        auto J2 = DeviatoricTensorInvariants<Scalar, numStre>::secondInvariant(stressTensor);
        Dune::FieldVector<Scalar,3> df;
        df[0] = sin(fi)/3.0;
        df[1] = (0.5/q)*(At - tan(3.0*theta)*dAdt);
        df[2] = -0.5 * sqrt(3.0) * dAdt * (1.0/(J2*cos(3.0*theta)));

        //equal to drucker prager if theta = 30
    
        auto dInv = DeviatoricTensorInvariants<Scalar, numStre>::getDerivatives(stressTensor);
        YieldNormal af;
        dInv.mtv(df,af);
    
        return af;
    }
};

} // end namespace Dumux

#endif
