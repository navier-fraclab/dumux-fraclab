// -*- mode: C++; tab-width: 4; indent-tabs-mode: nil; c-basic-offset: 4 -*-
// vi: set et ts=4 sw=4 sts=4:

/*****************************************************************************
 *   See the file COPYING for full copying permissions.                      *
 *                                                                           *
 *   This program is free software: you can redistribute it and/or modify    *
 *   it under the terms of the GNU General Public License as published by    *
 *   the Free Software Foundation, either version 3 of the License, or       *
 *   (at your option) any later version.                                     *
 *                                                                           *
 *   This program is distributed in the hope that it will be useful,         *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of          *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the            *
 *   GNU General Public License for more details.                            *
 *                                                                           *
 *   You should have received a copy of the GNU General Public License       *
 *   along with this program.  If not, see <http://www.gnu.org/licenses/>.   *
 *****************************************************************************/
/*!
 * \file
 * \ingroup BoxFlux
 * \brief This class computes stresses and surface forces due to defornation
 * when elastoplastic models are used
 * It requires the definition of a class for the elastic law 
 * and for the return algorithm (when yield surface is surpassed)
 */

#ifndef DUMUX_DISCRETIZATION_PLASTIC_CORRECTION_HH
#define DUMUX_DISCRETIZATION_PLASTIC_CORRECTION_HH

#include <dumux/flux/box/hookeslaw.hh>
#include <dumux/flux/box/effectivestresslaw.hh>
#include "returnalgorithm.hh"

namespace Dumux {

// class for purely mechanical elastoplastic models (effective stress principle not used)

template<class ScalarType, class GridGeometry, class ElasticType>
class PlasticCorrection
{
    using ThisType = PlasticCorrection<ScalarType, GridGeometry, ElasticType>;
    using FVElementGeometry = typename GridGeometry::LocalView;
    using SubControlVolumeFace = typename FVElementGeometry::SubControlVolumeFace;
    using GridView = typename GridGeometry::GridView;
    using Element = typename GridView::template Codim<0>::Entity;
    static constexpr
    int dim = GridView::dimension;
    static constexpr int dimWorld = GridView::dimensionworld;
    static constexpr int numStre = dim == 2 ? 4 : 2*dim;
    static_assert(dim == dimWorld, "class PlasticCorrection does not apply to network/surface grids");

public:
    using ElasticLaw = ElasticType;
    //! export the type used for scalar values
    using Scalar = ScalarType;
    //! General 3x3 tensor
    using Tensor = Dune::FieldMatrix<Scalar, 3, 3>;
    //! export the type used for the stress tensor
    using StressTensor = Dune::FieldMatrix<Scalar, dim, dimWorld>;
    //! export the type used for force vectors
    using ForceVector = typename StressTensor::row_type;
    //! export the type used for normal vector to yield surface
    using YieldNormal = Dune::FieldVector<Scalar, numStre>;
    //! export the type used for stress vectors
    using StressVector = Dune::FieldVector<Scalar, numStre>;
    //! export the type used for constitutive elastic matrix
    using ElasticMatrix = Dune::FieldMatrix<Scalar, numStre, numStre>;

    
    //! computes the force INCREMENT acting on a sub-control volume face
    template<class Problem, class ElementVolumeVariables, class ElementFluxVarsCache>
    static ForceVector force(const Problem& problem,
                             const Element& element,
                             const FVElementGeometry& fvGeometry,
                             const ElementVolumeVariables& elemVolVars,
                             const SubControlVolumeFace& scvf,
                             ElementFluxVarsCache& elemFluxVarCache)
    {
        auto& fluxVarCache = elemFluxVarCache[scvf];
        const auto sigmaTensor = stressTensor(problem, element, scvf, fvGeometry, elemVolVars, fluxVarCache);

        StressTensor sigma(0.0);
        for (int i = 0; i < dim; ++i) {
            for (int j = 0; j < dimWorld; ++j) {
               sigma[i][j] = sigmaTensor[i][j];
            }
        }

        ForceVector scvfForce(0.0);
        sigma.mv(scvf.unitOuterNormal(), scvfForce);
        scvfForce *= scvf.area();

        return scvfForce;
    }

    //! assembles the stress tensor INCREMENT at a given integration point
    template<class Problem, class ElementVolumeVariables, class FluxVarsCache>
    static Tensor stressTensor(const Problem& problem,
                               const Element& element,
                               const SubControlVolumeFace& scvf,
                               const FVElementGeometry& fvGeometry,
                               const ElementVolumeVariables& elemVolVars,
                               FluxVarsCache& fluxVarCache)
    {
        auto sigmaIncrement = ElasticType::stressTensor(problem, element, fvGeometry, elemVolVars, fluxVarCache);  //stress increment (if entirely elastic)
        auto sigma = fluxVarCache.stressTensorAtFace(); // initial stress tensor
        auto initialSigma = sigma; 
 
        //compute elastic trial 
        for (int i = 0; i < dim; ++i) 
            for (int j = 0; j < dimWorld; ++j) 
                sigma[i][j] += sigmaIncrement[i][j];
 
        // if 2D, compute normal stress in the z-direction with plane strain assumption
        if(dim == 2 and dimWorld == 2)
        {
            auto sigmazIncrement = ElasticType::planeStrainStressZ(problem, element, fvGeometry, elemVolVars, fluxVarCache, sigmaIncrement);
            sigma[2][2] += sigmazIncrement;
        }
	
        const auto& spatialParams =  problem.spatialParams(); 
        const auto& epModel = spatialParams.plasticModel(element, fvGeometry, elemVolVars, fluxVarCache);
        // obtain yield function value for elastic trial
        auto sigmaVector = FluxVarsCache::stressTensorToVector(sigma);  
        Scalar yf = epModel.yieldFunction(sigma);
   
        // if yield function is surpassed, called return algorithm
        if(yf > yfTol)
        {
            const auto& elasticMatrix = spatialParams.elasticMatrix(element, fvGeometry, elemVolVars, fluxVarCache);
            bool isMc = spatialParams.isMohrCoulomb(element);
            if(isMc)
                ReturnAlgorithm<Scalar, GridGeometry,true>::returnAlgorithm(fluxVarCache,elasticMatrix, epModel, yf,initialSigma, sigma,sigmaVector);
            else
                ReturnAlgorithm<Scalar, GridGeometry,false>::returnAlgorithm(fluxVarCache,elasticMatrix, epModel, yf,initialSigma, sigma,sigmaVector);
        } else
            sigma -= initialSigma;

        //yf = problem.spatialParams().yieldFunction(element, fvGeometry, sigma);

//!!!!!!!!!!!!!!!!!!
        //if(!(cornerToCorner && NumericDifferentiation::jacobianAssembly)) dsigma -= initialSigma;
        // if(apex && NumericDifferentiation::jacobianAssembly) dsigma *= 0.0;

        return sigma;
  }

  // get current stresses (initial+variation) at a given integration point 

  template<class Problem, class ElementVolumeVariables, class FluxVarsCache>
  static Tensor updatedStressTensor(const Problem& problem,
                                    const Element& element,
                                    const SubControlVolumeFace& scvf,
                                    const FVElementGeometry& fvGeometry,
                                    const ElementVolumeVariables& elemVolVars,
                                    FluxVarsCache& fluxVarCache)
    {
        auto stress = fluxVarCache.stressTensorAtFace();
        const auto dstress = stressTensor(problem,element,scvf,fvGeometry,elemVolVars,fluxVarCache);
        stress += dstress;
        return stress;
    }
        
  //! update stress vector (last argument) at a point 
  // returns boolean that indicates if updated stresses are at yield surface or not 

  template<class Problem, class ElementVolumeVariables, class FluxVarsCache>
  static bool stressVector(const Problem& problem,
                           const Element& element,
                           const FVElementGeometry& fvGeometry,
                           const ElementVolumeVariables& elemVolVars,
                           FluxVarsCache& fluxVarCache,
                           StressVector& sigmaVector)
    {
        bool yielded = false;
        auto sigma = fluxVarCache.stressVectorToTensor(sigmaVector);
       
        auto sigmaIncrement = ElasticType::stressTensor(problem, element, fvGeometry, elemVolVars, fluxVarCache);
        auto initialSigma = sigma;
        for (int i = 0; i < dim; ++i) 
            for (int j = 0; j < dimWorld; ++j) 
               sigma[i][j] += sigmaIncrement[i][j];

        if(dim == 2 and dimWorld == 2)
        {   
            auto sigmazIncrement = ElasticType::planeStrainStressZ(problem, element, fvGeometry, elemVolVars, fluxVarCache, sigmaIncrement);
            sigma[2][2] += sigmazIncrement;
        }
	
        sigmaVector = FluxVarsCache::stressTensorToVector(sigma);
        const auto& spatialParams =  problem.spatialParams(); 
        const auto& epModel = spatialParams.plasticModel(element, fvGeometry, elemVolVars, fluxVarCache);
        const Scalar yf = epModel.yieldFunction(sigma);
        
        if(yf > -yfTol) yielded = true;
        if(yf > yfTol)
        {
            const auto& elasticMatrix = spatialParams.elasticMatrix(element, fvGeometry, elemVolVars, fluxVarCache);
            bool isMc = spatialParams.isMohrCoulomb(element);
            if(isMc)
                ReturnAlgorithm<Scalar, GridGeometry,true>::returnAlgorithm(fluxVarCache,elasticMatrix, epModel, yf,initialSigma, sigma,sigmaVector);
            else
                ReturnAlgorithm<Scalar, GridGeometry,false>::returnAlgorithm(fluxVarCache,elasticMatrix, epModel, yf,initialSigma, sigma,sigmaVector);
        } 

        return yielded;
   }

    static void setYieldFunctionTolerance(Scalar tol){yfTol = tol;}
    
    static Scalar getYieldFunctionTolerance(){return yfTol;}
  
protected:

   static inline Scalar yfTol = 1e-5;
    
};

// class for poroplasticity (effective stress principle)

template<class ScalarType, class GridGeometry, class ElasticType>
class PoroPlasticCorrection : public PlasticCorrection<ScalarType,GridGeometry, ElasticType>
{
    using ParentType = PlasticCorrection<ScalarType, GridGeometry, ElasticType>;
    using ThisType = PoroPlasticCorrection<ScalarType, GridGeometry, ElasticType>;
    using FVElementGeometry = typename GridGeometry::LocalView;
    using SubControlVolumeFace = typename FVElementGeometry::SubControlVolumeFace;
    using GridView = typename GridGeometry::GridView;
    using Element = typename GridView::template Codim<0>::Entity;
    static constexpr int dim = GridView::dimension;
    static constexpr int dimWorld = GridView::dimensionworld;
    static constexpr int numStre = (dim == 2 && dimWorld == 2) ? 4 : 2*dim;
    static_assert(dim == dimWorld, "Hookes Law not implemented for network/surface grids");

    public:
    //! export the type used for scalar values
    using Scalar = ScalarType;
    //! General 3x3 tensor
    using Tensor = Dune::FieldMatrix<Scalar, 3, 3>;
    //! export the type used for the stress tensor
    using StressTensor = Dune::FieldMatrix<Scalar, dim, dimWorld>;
    //! export the type used for force vectors
    using ForceVector = typename StressTensor::row_type;
    //! export the type used for normal vector to yield surface
    using YieldNormal = Dune::FieldVector<Scalar, numStre>;
    //! export the type used for stress vectors
    using StressVector = Dune::FieldVector<Scalar, numStre>;
    //! export the type used for constitutive elastic matrix
    using ElasticMatrix = Dune::FieldMatrix<Scalar, numStre, numStre>;

    //! computes the force INCREMENT acting on a sub-control volume face
    template<class Problem, class ElementVolumeVariables, class ElementFluxVarsCache>
    static ForceVector force(const Problem& problem,
                             const Element& element,
                             const FVElementGeometry& fvGeometry,
                             const ElementVolumeVariables& elemVolVars,
                             const SubControlVolumeFace& scvf,
                                   ElementFluxVarsCache& elemFluxVarCache)
    {
        auto& fluxVarCache = elemFluxVarCache[scvf];
        const auto sigmaTensor = totalStressTensor(problem, element, scvf, fvGeometry, elemVolVars, fluxVarCache);

        StressTensor sigma(0.0);
        for (int i = 0; i < dim; ++i) 
            for (int j = 0; j < dim; ++j) 
               sigma[i][j] = sigmaTensor[i][j];

        ForceVector scvfForce(0.0);
        sigma.mv(scvf.unitOuterNormal(), scvfForce);
        scvfForce *= scvf.area();

        return scvfForce;
    }
    
    // computes total stress tensor INCREMENT at a given integration point 
    template<class Problem, class ElementVolumeVariables, class FluxVarsCache>
    static Tensor totalStressTensor(const Problem& problem,
                                    const Element& element,
                                    const SubControlVolumeFace& scvf,
                                    const FVElementGeometry& fvGeometry,
                                    const ElementVolumeVariables& elemVolVars,
                                    FluxVarsCache& fluxVarCache)
    {
        auto sigma = ParentType::stressTensor(problem,element,scvf,fvGeometry,elemVolVars,fluxVarCache);
        const auto biotCoeff = problem.spatialParams().biotCoefficient(element, fvGeometry, elemVolVars, fluxVarCache);
        const auto effPress = problem.effectivePorePressure(element, fvGeometry, elemVolVars, fluxVarCache);
        const auto bcp = biotCoeff*effPress;
        for(int dir = 0; dir < 3; dir++) sigma[dir][dir] -= bcp;
        return sigma;
    }
    
     // computes effective stress tensor INCREMENT at a given integration point 
    template<class Problem, class ElementVolumeVariables, class FluxVarsCache>
    static Tensor effectiveStressTensor(const Problem& problem,
                                        const Element& element,
                                        const SubControlVolumeFace& scvf,
                                        const FVElementGeometry& fvGeometry,
                                        const ElementVolumeVariables& elemVolVars,
                                        FluxVarsCache& fluxVarCache) 
    {
        auto sigma = ParentType::stressTensor(problem,element,scvf,fvGeometry,elemVolVars,fluxVarCache);
        return sigma;
    }
    
    //! update stress vector (last argument) at a point 
    // returns boolean that indicates if updated stresses are at yield surface or not

    template<class Problem, class ElementVolumeVariables, class FluxVarsCache>
    static bool effectiveStressVector(const Problem& problem,
                                      const Element& element,
                                      const FVElementGeometry& fvGeometry,
                                      const ElementVolumeVariables& elemVolVars,
                                      FluxVarsCache& fluxVarCache,
                                      StressVector& sigmaVector)
    {
        return ParentType::stressVector(problem, element, fvGeometry, elemVolVars, fluxVarCache, sigmaVector);
    }

    template<class Problem, class ElementVolumeVariables, class FluxVarsCache>
    static Tensor updatedEffectiveStressTensor(const Problem& problem,
                                               const Element& element,
                                               const SubControlVolumeFace& scvf,
                                               const FVElementGeometry& fvGeometry,
                                               const ElementVolumeVariables& elemVolVars,
                                               FluxVarsCache& fluxVarCache)
    {
      auto stress = fluxVarCache.stressTensorAtFace();
      const auto dstress = ParentType::stressTensor(problem,element,scvf,fvGeometry,elemVolVars,fluxVarCache);
      stress += dstress;
      return stress;
    }

};
} // end namespace Dumux

#endif
