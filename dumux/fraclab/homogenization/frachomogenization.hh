// -*- mode: C++; tab-width: 4; indent-tabs-mode: nil; c-basic-offset: 4 -*-
// vi: set et ts=4 sw=4 sts=4:
/*****************************************************************************
 *   See the file COPYING for full copying permissions.                      *
 *                                                                           *
 *   This program is free software: you can redistribute it and/or modify    *
 *   it under the terms of the GNU General Public License as published by    *
 *   the Free Software Foundation, either version 3 of the License, or       *
 *   (at your option) any later version.                                     *
 *                                                                           *
 *   This program is distributed in the hope that it will be useful,         *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of          *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the            *
 *   GNU General Public License for more details.                            *
 *                                                                           *
 *   You should have received a copy of the GNU General Public License       *
 *   along with this program.  If not, see <http://www.gnu.org/licenses/>.   *
 *****************************************************************************/

#ifndef DUMUX_FRACLAB_FACET_MEDIA_HOMOGENIZATION_HH
#define DUMUX_FRACLAB_FACET_MEDIA_HOMOGENIZATION_HH

#include "homogenization.hh"
// for box model only 

namespace Dumux {

template<class BulkTypeTag, class MDTraits, std::size_t bulkDomainId = 0, std:: size_t lowDimDomainId = 1>
class MultidomainFacetHomogenization
: public Homogenization<BulkTypeTag> 
{
    using Parent = Homogenization<BulkTypeTag>;
    
    using BulkIdType = typename MDTraits::template SubDomain<bulkDomainId>::Index;
    using LowDimIdType = typename MDTraits::template SubDomain<lowDimDomainId>::Index;
    static constexpr auto bulkId = BulkIdType();
    static constexpr auto lowDimId = LowDimIdType();

    // the sub-domain type tags
    template<std::size_t id> using SubDomainTypeTag = typename MDTraits::template SubDomain<id>::TypeTag;

    template<std::size_t id> using GridGeometry = typename MDTraits::template SubDomain<id>::GridGeometry;
    template<std::size_t id> using FVElementGeometry = typename GridGeometry<id>::LocalView;
    template<std::size_t id> using ElementBoundaryTypes = GetPropType<SubDomainTypeTag<id>, Properties::ElementBoundaryTypes>;
    template<std::size_t id> using NumEqVector = GetPropType<SubDomainTypeTag<id>, Properties::NumEqVector>;
    template<std::size_t id> using ElementResidualVector = ReservedBlockVector<NumEqVector<id>, FVElementGeometry<id>::maxNumElementScvs>;
    using GradientMatrix = typename  Parent::GradientMatrix;

public:

    using GridGeometryTuple = typename MDTraits::template TupleOfSharedPtrConst<GridGeometry>;
    using SolutionVector = typename MDTraits::SolutionVector;

    // only bulk grid is passed to parent because it to be used to compute boundaries and domain volume
    MultidomainFacetHomogenization(GridGeometryTuple&& gridGeometry)
    : Parent(std::get<bulkId>(gridGeometry))
    , gridGeometryTuple_(gridGeometry)
    {}
    
    // what changes from parent class for one-dimensional media is the way to average fluxes
    // we need to account fluxes in both domains (bulk and facet)
    // see Pouya and Fouché( 2009) for details 

    template<class Assembler, class CouplingManager>
    const GradientMatrix getHomogenizedFluxFromBoundary(Assembler& assembler,
                                                        CouplingManager& couplingManager,
                                                        const SolutionVector& x)
    {
        SolutionVector res;
        res[bulkId].resize(gridGeometry(bulkId).numDofs());
        res[lowDimId].resize(gridGeometry(lowDimId).numDofs());

        // need to correct boundary nodes with neumann bcs
        // otherwise they will have zero residual (zero flux)
        
        forEach(std::make_index_sequence<SolutionVector::N()>(), [&](const auto domainId)
        {
            const auto& problem = assembler.problem(domainId);
            const auto& gridVariables = assembler.gridVariables(domainId);
            const auto& gridGeometry_ = gridGeometry(domainId);
            const auto& localResidual = assembler.localResidual(domainId);

            for(const auto& element : elements(gridGeometry_.gridView()))
            {
                couplingManager.bindCouplingContext(domainId, element, assembler);
                // to do: if not boundary element, continue

                ElementBoundaryTypes<domainId> elemBcTypes;

                auto elemVolVars = localView(gridVariables.curGridVolVars());
                auto elemFluxVars = localView(gridVariables.gridFluxVarsCache());

                auto fvGeometry = localView(gridGeometry_);
                fvGeometry.bind(element);
                elemBcTypes.update(problem,element,fvGeometry);
                elemVolVars.bind(element, fvGeometry, x[domainId]);                                                                                                      
                elemFluxVars.bind(element, fvGeometry, elemVolVars);

                ElementResidualVector<domainId> elemRes(fvGeometry.numScv());
                //std::vector<bool> hasNeumann(fvGeometry.numScv(), false);

                for(const auto& scvf: scvfs(fvGeometry))
                {
                    if(this->onBoundary_(scvf.center())) continue;
                    localResidual.evalFlux(elemRes, problem, element, fvGeometry,elemVolVars,elemBcTypes,elemFluxVars,scvf);
                    /*
                    const auto scvIdx = scvf.insideScvIdx();
                    if (elemBcTypes[scvIdx].hasNeumann()){
                        hasNeumann[scvIdx] = true;
                    }
                    */
                    // if non stationarity is to be considered, need to add evalStorage function 
                }
                    
                for(const auto& scv : scvs(fvGeometry)){
                    //const auto idx = scv.indexInElement();
                    //if(hasNeumann[idx]) continue;
                    localResidual.evalSource(elemRes, problem, element, fvGeometry, elemVolVars, scv);
                }

                for(const auto& scv : scvs(fvGeometry))
                    res[domainId][scv.dofIndex()] += elemRes[scv.indexInElement()];
                    
            }
        });
       
        GradientMatrix hmgFlux(0.0);
        
        forEach(std::make_index_sequence<SolutionVector::N()>(), [&](const auto domainId)
        {  hmgFlux += this->getHomogenizedFluxFromBoundary_(gridGeometry(domainId), res[domainId]);});

        return hmgFlux;

    }

    //! the finite volume grid geometry of domain i
    template<std::size_t i>
    const auto& gridGeometry(Dune::index_constant<i> domainId) const
    { return *std::get<domainId>(gridGeometryTuple_); }

private:

    GridGeometryTuple gridGeometryTuple_;
};
}
#endif