// -*- mode: C++; tab-width: 4; indent-tabs-mode: nil; c-basic-offset: 4 -*-
// vi: set et ts=4 sw=4 sts=4:
/*****************************************************************************
 *   See the file COPYING for full copying permissions.                      *
 *                                                                           *
 *   This program is free software: you can redistribute it and/or modify    *
 *   it under the terms of the GNU General Public License as published by    *
 *   the Free Software Foundation, either version 3 of the License, or       *
 *   (at your option) any later version.                                     *
 *                                                                           *
 *   This program is distributed in the hope that it will be useful,         *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of          *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the            *
 *   GNU General Public License for more details.                            *
 *                                                                           *
 *   You should have received a copy of the GNU General Public License       *
 *   along with this program.  If not, see <http://www.gnu.org/licenses/>.   *
 *****************************************************************************/

#ifndef DUMUX_FRACLAB_HOMOGENIZATION_HH
#define DUMUX_FRACLAB_HOMOGENIZATION_HH

// for box model only 

namespace Dumux {

template<class TypeTag>
class Homogenization
{
    using GridGeometry = GetPropType<TypeTag, Properties::GridGeometry>;
    using Scalar = GetPropType<TypeTag, Properties::Scalar>;
    using StressTensor = Dune::FieldMatrix<Scalar,3,3>;
    using ScvStressVector = std::vector<std::vector<StressTensor>>;
    using GridView = typename GridGeometry::GridView;       
    using Element = typename GridView::template Codim<0>::Entity;
    using GlobalPosition = typename Element::Geometry::GlobalCoordinate;
    using FVElementGeometry = typename GridGeometry::LocalView;
    using ElementBoundaryTypes = GetPropType<TypeTag, Properties::ElementBoundaryTypes>;
    using NumEqVector = GetPropType<TypeTag, Properties::NumEqVector>;
    using ElementResidualVector = ReservedBlockVector<NumEqVector, FVElementGeometry::maxNumElementScvs>;
    static constexpr auto numEq = GetPropType<TypeTag, Properties::ModelTraits>::numEq();
    static constexpr int dimWorld = GridView::dimensionworld;

    using PrimaryVariables = GetPropType<TypeTag, Properties::PrimaryVariables>;
    using SolutionVector = GetPropType<TypeTag, Properties::SolutionVector>;

public:
    using GradientMatrix = Dune::FieldMatrix<Scalar, numEq, dimWorld>;

    Homogenization(std::shared_ptr<const GridGeometry> gridGeometry)
    : gridGeometry_(gridGeometry)
    {
        minCoor = gridGeometry -> bBoxMin();
        maxCoor = gridGeometry -> bBoxMax();
        // compute domain's volume
        for(const auto& element : elements(gridGeometry->gridView()))
        {
            auto fvGeometry = localView(*gridGeometry);
            fvGeometry.bind(element);
            for(const auto & scv : scvs(fvGeometry))
                volume += scv.volume();
        }
    }
    
    // returns homogenized variable by averaging the stresses 
    // stored at sub control volumes. Requires a multidomain
    // array or vector with number of lines aqual to number of elements
    // and number of colums equal to number os scvs that stores the variable 
    // to be homognized 

    template<class ScvStorage>
    const auto getHomogenizedVariableFromScvs(ScvStorage& scvVariables)
    {   
        //TO DO: verify if scvVariables have proper dimensions

        const auto& gridGeometry = *gridGeometry_;
        auto hmgVar = scvVariables[0][0];
        hmgVar = 0.0;

        for(const auto& element : elements(gridGeometry.gridView()))
        {
            auto fvGeometry = localView(gridGeometry);
            fvGeometry.bind(element);
            const auto eIdx = gridGeometry.elementMapper().index(element);
            
            for(const auto & scv : scvs(fvGeometry))
            {
                const auto scvVolume = scv.volume();
                auto scvVar = scvVariables[eIdx][scv.indexInElement()];
                scvVar *= scvVolume;
                hmgVar += scvVar;
            }
        }

        hmgVar /= volume;
        return hmgVar;
    }

    // returns homogenized stress tensor by averaging the stresses 
    // stored at sub control volumes
    const StressTensor getHomogenizedStressFromScvs(const ScvStressVector& scvStressVector)
    {
        StressTensor hmgStress(0.0);
        const auto& gridGeometry = *gridGeometry_;
        for(const auto& element : elements(gridGeometry.gridView()))
        {
            auto fvGeometry = localView(gridGeometry);
            fvGeometry.bind(element);
            const auto eIdx = gridGeometry.elementMapper().index(element);
            
            for(const auto & scv : scvs(fvGeometry))
            {
                const auto scvVolume = scv.volume();
                auto scvStress = scvStressVector[eIdx][scv.indexInElement()];
                scvStress *= scvVolume;
                hmgStress += scvStress;
            }
        }

        hmgStress /= volume;
        return hmgStress;
    }

    // returns matrix of gradients of primary variables using their values at the domain's boundaries
    // Each position ij contains gradient of variable i with respect to direction j = x,y,z
    template<class GridVariables, class SolVector>
    const GradientMatrix getHomogenizedGradient(const GridVariables& gridVariables,
                                                const SolVector& x)
    {
        return getHomogenizedGradient(*gridGeometry_,gridVariables,x);
    }

    template<class GG, class GridVariables, class SolVector>
    const GradientMatrix getHomogenizedGradient(const GG& gridGeometry,
                                                const GridVariables& gridVariables,
                                                const SolVector& x)
    {
        GradientMatrix gradX(0.0);

        for (const auto& element : elements(gridGeometry.gridView()))
        {
            auto fvGeometry = localView(gridGeometry);
            auto elemVolVars = localView(gridVariables.curGridVolVars());
            auto elemFluxVars = localView(gridVariables.gridFluxVarsCache());
            fvGeometry.bind(element);
            elemVolVars.bind(element, fvGeometry, x);
            elemFluxVars.bind(element, fvGeometry, elemVolVars);

            for (const auto& scvf : scvfs(fvGeometry))
            {
                auto scvfCenter = scvf.center();
                if(scvf.area() < eps_) continue;
                if(!this->onBoundary_(scvfCenter)) continue;

                const auto& fluxVarCache = elemFluxVars[scvf];
                const auto& shapeValues = fluxVarCache.shapeValues();
                // interpolate value of x at face's center

                PrimaryVariables xf(0.0);

                for (const auto& scv : scvs(fvGeometry))
                    xf += x[scv.dofIndex()] * shapeValues[scv.indexInElement()][0];
                
                const auto normal = scvf.unitOuterNormal();
                for(int i = 0; i < numEq; i++)
                    for(int dir = 0; dir < dimWorld; dir++)
                        gradX[i][dir] += xf[i]*normal[dir]*scvf.area();
            }
        }

        gradX /= volume;
        return gradX;
    }

    template<class Assembler>
    const auto getHomogenizedFluxFromBoundary(const Assembler& assembler,
                                              const SolutionVector& x)
    {
        SolutionVector res;
        res.resize(gridGeometry_->numDofs());

        // obtain flux or force residuals on nodes 

        assembler.assembleResidual(res,x);

        // TO DO: use local residual to compute fluxes only at boundary elements
        // so far this function works only for geomech and flow stationary problems
         
        const auto& problem = assembler.problem();
        const auto& gridVariables = assembler.gridVariables();
        const auto& localResidual = assembler.localResidual();

        for(const auto& element : elements(gridGeometry_->gridView()))
        {
            // to do: if not boundary element, continue

            ElementBoundaryTypes bcTypes;
            
            auto&& elemVolVars = localView(gridVariables.curGridVolVars());
            auto&& elemFluxVars = localView(gridVariables.gridFluxVarsCache());

            auto fvGeometry = localView(*gridGeometry_);
            fvGeometry.bind(element);
            bcTypes.update(problem,element,fvGeometry);
            elemVolVars.bind(element, fvGeometry, x);                                                                                                      
            elemFluxVars.bind(element, fvGeometry, elemVolVars);

            ElementResidualVector elemRes(fvGeometry.numScv());

            for(const auto& scvf: scvfs(fvGeometry))
            {
                if(onBoundary_(scvf.center())) continue;
                localResidual.evalFlux(elemRes, problem, element, fvGeometry,elemVolVars,bcTypes,elemFluxVars,scvf);
                // if non stationarity is to be considered, need to add evalStorage function 
            }
            
            for(const auto& scv : scvs(fvGeometry))
                localResidual.evalSource(elemRes, problem, element, fvGeometry, elemVolVars, scv);

            for(const auto& scv : scvs(fvGeometry))
                res[scv.dofIndex()] += elemRes[scv.indexInElement()];
        }
    
        return getHomogenizedFluxFromBoundary_(*gridGeometry_,res);
    }


    // res is in mass/time for flow and in force unit for mechanics
    template<class SolVector, class GG>
    const GradientMatrix getHomogenizedFluxFromBoundary_(const GG& gridGeometry,
                                                         const SolVector& res)
    {
        // Consult this formulation in Pouya and Fouché (2009)

        std::vector<bool> isAssigned(gridGeometry.numDofs(),0.0);
        GradientMatrix boundFlux (0.0);

        for(const auto& element : elements(gridGeometry.gridView()))
        {
            auto fvGeometry = localView(gridGeometry);
            fvGeometry.bind(element);
            for(const auto& scv : scvs(fvGeometry)){

                const auto coord = scv.dofPosition();
                const auto dofIdx = scv.dofIndex();

                if(!onBoundary_(coord)) continue;
                if(isAssigned[dofIdx]) continue;

                for(int eq = 0; eq < numEq; eq++)
                    for(int dir = 0; dir < dimWorld; dir++)
                        boundFlux[eq][dir] -= res[dofIdx][eq]*coord[dir];
                
                isAssigned[dofIdx] = true;
            }
        }

        boundFlux /= volume;

        return boundFlux;
    }

    bool onMinBoundary_(int dir, const GlobalPosition& globalPos) const
    {
        return globalPos[dir] - minCoor[dir] < eps_;
    }

    bool onMaxBoundary_(int dir, const GlobalPosition& globalPos) const
    {
        return maxCoor[dir] - globalPos[dir] < eps_;
    }
    
    bool onBoundary_(const GlobalPosition& globalPos) const
    {
        for(int dir = 0; dir < dimWorld; dir++)
            if(onMinBoundary_(dir,globalPos) or onMaxBoundary_(dir,globalPos))
                return true;
        return false; ///
    }

    void setEps_(const Scalar eps)
    {eps_ = eps;}

    const Scalar domainVolume()
    {return volume;}

protected:

    GlobalPosition minCoor;
    GlobalPosition maxCoor;
    Scalar volume = 0.0;
    Scalar eps_ = 1e-6;
private:
    std::shared_ptr<const GridGeometry> gridGeometry_;
};
}
#endif