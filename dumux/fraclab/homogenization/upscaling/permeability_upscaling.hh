// -*- mode: C++; tab-width: 4; indent-tabs-mode: nil; c-basic-offset: 4 -*-
// vi: set et ts=4 sw=4 sts=4:
/*****************************************************************************
 *   See the file COPYING for full copying permissions.                      *
 *                                                                           *
 *   This program is free software: you can redistribute it and/or modify    *
 *   it under the terms of the GNU General Public License as published by    *
 *   the Free Software Foundation, either version 2 of the License, or       *
 *   (at your option) any later version.                                     *
 *                                                                           *
 *   This program is distributed in the hope that it will be useful,         *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of          *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the            *
 *   GNU General Public License for more details.                            *
 *                                                                           *
 *   You should have received a copy of the GNU General Public License       *
 *   along with this program.  If not, see <http://www.gnu.org/licenses/>.   *
 *****************************************************************************/
/*!
 * \file
 * \brief file of upscaling funcion of media without interface elements
 */

#include <config.h>
#include <string.h>
#include <iostream>
#include <type_traits>
#include <tuple>

#include <dune/common/parallel/mpihelper.hh>
#include <dune/istl/matrixmarket.hh>

#include "properties.hh"
#include <dumux/fraclab/homogenization/homogenization.hh>
#include <dumux/discretization/evalgradients.hh>
#include <dumux/linear/seqsolverbackend.hh>
#include <dumux/linear/matrixconverter.hh>
#include <dumux/io/vtkoutputmodule.hh>
#include <dumux/assembly/fvassembler.hh>
#include <dumux/nonlinear/newtonsolver.hh>

namespace Dumux
{
    template <class TypeTag>
    class PermeabilityUpscaling
    {
        using Scalar = GetPropType<TypeTag, Dumux::Properties::Scalar>;
        using GridGeometry = GetPropType<MatrixTypeTag, Dumux::Properties::GridGeometry>;
        using SolutionVector = GetPropType<MatrixTypeTag, Dumux::Properties::SolutionVector>;
        using Problem = GetPropType<MatrixTypeTag, Dumux::Properties::Problem>;
        using SpatialParams = GetPropType<MatrixTypeTag, Dumux::Properties::SpatialParams>;
        using GridVariables = GetPropType<MatrixTypeTag, Dumux::Properties::GridVariables>;
        static constexpr int dimWorld = GridGeometry::GridView::dimensionworld;    
        //using PeriodicCouplingManager = FlowInterfaceMortarPeriodicCouplingManager<MDTraits, MatrixTypeTag, FractureTypeTag, CouplingManager, CouplingMapper>;
        using Homogenization = Homogenization<TypeTag>;

        using PermeabilityTensor = Dune::FieldMatrix<Scalar, dimWorld, dimWorld>;

    public:
        PermeabilityUpscaling(std::shared_ptr<GridGeometry> gridGeometry)
        : gridGeometry_(bulkGridGeometry)
        {
            spatialParams_ = std::make_shared<SpatialParams>(gridGeometry);
            problem_ = std::make_shared<Problem>(gridGeometry, spatialParams, "Upscaling");

            /*
            if (problem_->boundaryConditionType() == periodic)
                periodicCouplingManager_ = std::make_shared<PeriodicCouplingManager>(bulkGridGeometry,
                                                                                     fracGridGeometry,
                                                                                     couplingManager_,
                                                                                     couplingMapper_);
            else periodicCouplingManager_ = nullptr;
            if (periodicCouplingManager_)
                periodicCouplingManager_->setPeriodicDofMap();
            */
        }

        const PermeabilityTensor getUpscaledPermeability(bool writeVtk = true)
        {
            PermeabilityTensor permeability(0.0);

            auto homogenization = std::make_shared<Homogenization>(gridGeometry_);   

            for (int step = 0; step < dimWorld; step++)
            {
                
                /*
                if (periodicCouplingManager_)
                {
                    const auto gradP = problem->appliedPressureGradient();
                    periodicCouplingManager_->setPrimaryVariablesGradient(gradP);
                }
                */
                // the solution vector
                SolutionVector x;
                x.resize(gridGeometry_->numDofs());
            
                problem_->applyInitialSolution(x);
   
                // the grid variables 

                auto gridVars = std::make_shared<GridVariables>(problem_, gridGeometry_);  
                gridVars->init(x);

                // intialize the vtk output module
                std::string fileName = "perm_upscaling_step_" + std::to_string(step);
                VtkOutputModule<GridVariables, SolutionVector> vtkWriter(*gridVars, x, fileName);

                // add model-specific output fields to the writer
                using IOFields = GetPropType<TypeTag, Properties::IOFields>;
                IOFields::initOutputModule(vtkWriter);

                // add velocity output facility
                using VelocityOutput = GetPropType<TypeTag, Properties::VelocityOutput>;
                vtkWriter.addVelocityOutput(std::make_shared<VelocityOutput>(*gridVars));

                // the assembler
                using Assembler = FVAssembler<TypeTag, DiffMethod::numeric, true>;
                auto assembler = std::make_shared<Assembler>(problem_, gridGeometry_, gridVars);

                // the linear solver
                using LinearSolver = UMFPackBackend;
                auto linearSolver = std::make_shared<LinearSolver>();

                // the non-linear solver
                using NewtonSolver = Dumux::NewtonSolver<Assembler, LinearSolver>;
                auto newtonSolver = std::make_shared<NewtonSolver>(assembler, linearSolver);

                // linearize & solve
                newtonSolver->solve(x);

                // update grid variables for output
                gridVars->update(x);

                // Dune::FieldVector<Scalar,dimWorld> hmg_boundFlux(0.0);
                const auto hmg_gradP = homogenization->getHomogenizedGradient(*gridVars, x);
                const auto hmg_boundFlux = homogenization->getHomogenizedFluxFromBoundary(*assembler, x);

                if(writeVtk) vtkWriter.write(0.0);
                
                for (int dir = 0; dir < dimWorld; dir++)
                    permeability[step][dir] = -hmg_boundFlux[0][dir] / hmg_gradP[0][step];

                problem[bulkId].advanceUpscalingStep();
                problem[fractureId].advanceUpscalingStep();

            }

            return permeability;
        }

    private:
        std::shared_ptr<GridGeometry> gridGeometry_;
        std::shared_ptr<Problem> problem_;
        std::shared_ptr<SpatialParams> spatialParams_;
        //std::shared_ptr<PeriodicCouplingManager> periodicCouplingManager_;
    };
}