#ifndef DUMUX_FRACLAB_FLOW_UPSCALING_PROPERTIES_HH
#define DUMUX_FRACLAB_FLOW_UPSCALING_PROPERTIES_HH

#if HAVE_DUNE_MMESH
#include <dune/mmesh/mmesh.hh>
#endif

#include <dune/foamgrid/foamgrid.hh>
#include <dune/alugrid/grid.hh>
#include <dumux/discretization/box.hh>
#include <dumux/fraclab/multidomain/facet/box/elementboundarytypes.hh>
#include <dumux/multidomain/facet/box/properties.hh>
#include <dumux/porousmediumflow/1p/model.hh>
#include <dumux/material/fluidsystems/1pliquid.hh>
#include <dumux/material/components/constant.hh>
#include <dumux/fraclab/multidomain/facet/box/properties.hh>
#include <dumux/multidomain/facet/couplingmapper.hh>
#include <dumux/multidomain/facet/couplingmanager.hh>
#include <dumux/multidomain/traits.hh>

#include "problem_1p_fracture.hh"
#include "spatialparams_1p_fracture.hh"
#include "spatialparams_1p_matrix.hh"

namespace Dumux
{
    template<class MatrixTypeTag, class FractureTypeTag>
    class FracLabUpscalingTraits
    {
        using MatrixFVGridGeometry = Dumux::GetPropType<MatrixTypeTag, Dumux::Properties::GridGeometry>;
        using FractureFVGridGeometry = Dumux::GetPropType<FractureTypeTag, Dumux::Properties::GridGeometry>;
    public:
        using MDTraits = Dumux::MultiDomainTraits<MatrixTypeTag, FractureTypeTag>;
        using CouplingMapper = Dumux::FacetCouplingMapper<MatrixFVGridGeometry, FractureFVGridGeometry>;
        using CouplingManager = Dumux::FacetCouplingManager<MDTraits, CouplingMapper>;
    };

    namespace Properties
    {

        //! Type tag for the box scheme with coupling to
        //! another sub-domain living on the grid facets.
        // Create new type tags
        namespace TTag
        {
            struct UpscalingOnePMatrixBox{ using InheritsFrom = std::tuple<BoxFacetCouplingModel, OneP>;};
            struct UpscalingOnePMatrixBox2D{ using InheritsFrom = std::tuple<UpscalingOnePMatrixBox>;};
            struct UpscalingOnePMatrixBox3D{ using InheritsFrom = std::tuple<UpscalingOnePMatrixBox>;};
            struct UpscalingOnePFractureBox{using InheritsFrom = std::tuple<OneP, BoxModel>;};
            struct UpscalingOnePFractureBox2D{ using InheritsFrom = std::tuple<UpscalingOnePFractureBox>;};
            struct UpscalingOnePFractureBox3D{ using InheritsFrom = std::tuple<UpscalingOnePFractureBox>;};
        } // end namespace TTag

        // Properties for matrix problem

        // Set the grid type

        template <class TypeTag>
        struct Grid<TypeTag, TTag::UpscalingOnePMatrixBox2D>
        {
            using type = Dune::ALUGrid<2, 2, Dune::simplex, Dune::conforming>;
        };
        
        template <class TypeTag>
        struct Grid<TypeTag, TTag::UpscalingOnePMatrixBox3D>
        {
            using type = Dune::ALUGrid<3, 3, Dune::simplex, Dune::conforming>;
        };

        // Set the problem type
        template <class TypeTag>
        struct Problem<TypeTag, TTag::UpscalingOnePMatrixBox>
        {
            using type = UpscalingMatrixFlowProblem<TypeTag>;
        };

        // set the spatial params
        template <class TypeTag>
        struct SpatialParams<TypeTag, TTag::UpscalingOnePMatrixBox>
        {
            using type = UpscalingSpatialParamsOnePMatrix<GetPropType<TypeTag, Properties::GridGeometry>,
                                                 GetPropType<TypeTag, Properties::Scalar>>;
        };

        // the fluid system
        template <class TypeTag>
        struct FluidSystem<TypeTag, TTag::UpscalingOnePMatrixBox>
        {
        private:
            using Scalar = GetPropType<TypeTag, Properties::Scalar>;

        public:
            using type = FluidSystems::OnePLiquid<Scalar, Components::Constant</*id*/ 0, Scalar>>;
        };

        // solution-independent advection
        template <class TypeTag>
        struct SolutionDependentAdvection<TypeTag, TTag::UpscalingOnePMatrixBox>
        {
            static constexpr bool value = false;
        };

        template <class TypeTag>
        struct ElementBoundaryTypes<TypeTag, TTag::UpscalingOnePMatrixBox>
        {
            using type = FracLabBoxFacetCouplingElementBoundaryTypes<GetPropType<TypeTag, Properties::BoundaryTypes>>;
        };

        // Properties for fracture problem

        // Set the grid type

        template <class TypeTag>
        struct Grid<TypeTag, TTag::UpscalingOnePFractureBox2D>
        {
            using type = Dune::FoamGrid<1, 2>;
        };

        template <class TypeTag>
        struct Grid<TypeTag, TTag::UpscalingOnePFractureBox3D>
        {
            using type = Dune::FoamGrid<2, 3>;
        };

        // Set the problem type
        template <class TypeTag>
        struct Problem<TypeTag, TTag::UpscalingOnePFractureBox>
        {
            using type = UpscalingFractureFlowProblem<TypeTag>;
        };

        // set the spatial params
        template <class TypeTag>
        struct SpatialParams<TypeTag, TTag::UpscalingOnePFractureBox>
        {
            using type = UpscalingSpatialParamsOnePFracture<GetPropType<TypeTag, Properties::GridGeometry>,
                                                   GetPropType<TypeTag, Properties::Scalar>>;
        };

        // the fluid system
        template <class TypeTag>
        struct FluidSystem<TypeTag, TTag::UpscalingOnePFractureBox>
        {
        private:
            using Scalar = GetPropType<TypeTag, Properties::Scalar>;

        public:
            using type = FluidSystems::OnePLiquid<Scalar, Components::Constant</*id*/ 0, Scalar>>;
        };

        // solution-independent advection
        template <class TypeTag>
        struct SolutionDependentAdvection<TypeTag, TTag::UpscalingOnePFractureBox>
        {
            static constexpr bool value = false;
        };

        using DefaultTraits2D = FracLabUpscalingTraits<TTag::UpscalingOnePMatrixBox2D, TTag::UpscalingOnePFractureBox2D>;
        using DefaultTraits3D = FracLabUpscalingTraits<TTag::UpscalingOnePMatrixBox3D, TTag::UpscalingOnePFractureBox3D>;
        template<class TypeTag> struct CouplingManager<TypeTag, TTag::UpscalingOnePMatrixBox2D> { using type = typename DefaultTraits2D::CouplingManager; };
        template<class TypeTag> struct CouplingManager<TypeTag, TTag::UpscalingOnePFractureBox2D> { using type = typename DefaultTraits2D::CouplingManager; };
        template<class TypeTag> struct CouplingManager<TypeTag, TTag::UpscalingOnePMatrixBox3D> { using type = typename DefaultTraits3D::CouplingManager; };
        template<class TypeTag> struct CouplingManager<TypeTag, TTag::UpscalingOnePFractureBox3D> { using type = typename DefaultTraits3D::CouplingManager; };

    }
}

#endif