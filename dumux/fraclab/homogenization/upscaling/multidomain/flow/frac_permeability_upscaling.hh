// -*- mode: C++; tab-width: 4; indent-tabs-mode: nil; c-basic-offset: 4 -*-
// vi: set et ts=4 sw=4 sts=4:
/*****************************************************************************
 *   See the file COPYING for full copying permissions.                      *
 *                                                                           *
 *   This program is free software: you can redistribute it and/or modify    *
 *   it under the terms of the GNU General Public License as published by    *
 *   the Free Software Foundation, either version 2 of the License, or       *
 *   (at your option) any later version.                                     *
 *                                                                           *
 *   This program is distributed in the hope that it will be useful,         *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of          *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the            *
 *   GNU General Public License for more details.                            *
 *                                                                           *
 *   You should have received a copy of the GNU General Public License       *
 *   along with this program.  If not, see <http://www.gnu.org/licenses/>.   *
 *****************************************************************************/
/*!
 * \file
 * \brief file of upscaling funcion of frac media
 */

#include <config.h>
#include <string.h>
#include <iostream>
#include <type_traits>
#include <tuple>

#include <dune/common/parallel/mpihelper.hh>
#include <dune/istl/matrixmarket.hh>

#include "properties.hh"
// include the mortar periodic coupling manager for 3node interfaces
#include <dumux/fraclab/assembly/interface3nodemortarpcm.hh>
// include the assembler for periodic bcs in Dumux fraclab
#include <dumux/fraclab/multidomain/fvperiodicassembler.hh>
#include <dumux/fraclab/homogenization/frachomogenization.hh>
#include <dumux/discretization/evalgradients.hh>
#include <dumux/linear/seqsolverbackend.hh>
#include <dumux/linear/matrixconverter.hh>
#include <dumux/multidomain/newtonsolver.hh>
#include <dumux/multidomain/fvgridgeometry.hh>
#include <dumux/multidomain/fvproblem.hh>
#include <dumux/multidomain/fvgridvariables.hh>
#include <dumux/multidomain/io/vtkoutputmodule.hh>
#include <dumux/multidomain/facet/gridmanager.hh>

namespace Dumux
{
    template < class Traits, std::size_t bulkDomainId = 0, std::size_t lowDimDomainId = 1>
    class FracMediaPermeabilityUpscaling
    {

        using MDTraits = typename Traits::MDTraits;
        using CouplingMapper = typename Traits::CouplingMapper;

        using BulkIdType = typename MDTraits::template SubDomain<bulkDomainId>::Index;
        using LowDimIdType = typename MDTraits::template SubDomain<lowDimDomainId>::Index;
        static constexpr auto bulkId = BulkIdType();
        static constexpr auto fractureId = LowDimIdType();

        // the sub-domain type tags
        template <std::size_t id>using SubDomainTypeTag = typename MDTraits::template SubDomain<id>::TypeTag;
        using MatrixTypeTag = SubDomainTypeTag<bulkId>;
        using FractureTypeTag = SubDomainTypeTag<fractureId>;

        using Scalar = GetPropType<MatrixTypeTag, Dumux::Properties::Scalar>;
    
        template<std::size_t id> using GridGeometry = typename MDTraits::template SubDomain<id>::GridGeometry;
        using MDProblem = MultiDomainFVProblem<MDTraits>;
        using MDGridGeometry = MultiDomainFVGridGeometry<MDTraits>;
        using MatrixSpatialParams = GetPropType<MatrixTypeTag, Dumux::Properties::SpatialParams>;
        using FractureSpatialParams = GetPropType<FractureTypeTag, Dumux::Properties::SpatialParams>;
        using MatrixProblem = GetPropType<MatrixTypeTag, Dumux::Properties::Problem>;
        using FractureProblem = GetPropType<FractureTypeTag, Dumux::Properties::Problem>;
        using CouplingManager = typename Traits::CouplingManager;
        using PeriodicCouplingManager = FlowInterfaceMortarPeriodicCouplingManager<MDTraits, MatrixTypeTag, FractureTypeTag, CouplingManager, CouplingMapper>;
        using Homogenization = MultidomainFacetHomogenization<MatrixTypeTag, MDTraits>;
        using GridGeometryTuple = typename MDTraits::template TupleOfSharedPtrConst<GridGeometry>;

        static constexpr int dimWorld = GridGeometry<bulkId>::GridView::dimensionworld;

        using PermeabilityTensor = Dune::FieldMatrix<Scalar, dimWorld, dimWorld>;

    public:
        FracMediaPermeabilityUpscaling(std::shared_ptr<GridGeometry<bulkId>> bulkGridGeometry,
                                       std::shared_ptr<GridGeometry<fractureId>> fracGridGeometry,
                                       std::shared_ptr<CouplingMapper> couplingMapper)
        : bulkGridGeometry_(bulkGridGeometry)
        ,fracGridGeometry_(fracGridGeometry)
        ,couplingMapper_(couplingMapper)
        {
            gridGeometry_ = std::make_tuple(bulkGridGeometry, fracGridGeometry);
            //elementAt(gridGeometry_.getTuple(), bulkId) = bulkGridGeometry;
            //elementAt(gridGeometry_.getTuple(), fractureId) = fracGridGeometry;
   
            couplingManager_ = std::make_shared<CouplingManager>();
            
            //elementAt(gridGeometry_.getTuple(), bulkId) = std::make_shared<GridGeometry<bulkId>>(std::get<bulkId>(gridGeometry)->gridView());
            //elementAt(gridGeometry_.getTuple(), fractureId) = std::make_shared<GridGeometry<fractureId>>(std::get<fractureId>(gridGeometry)->gridView());

            matrixSpatialParams_ = std::make_shared<MatrixSpatialParams>(bulkGridGeometry);
            fractureSpatialParams_ = std::make_shared<typename FractureProblem::SpatialParams>(fracGridGeometry);
            
            problem.set(std::make_shared<MatrixProblem>(bulkGridGeometry, matrixSpatialParams_, couplingManager_, "Upscaling"), bulkId);
            problem.set(std::make_shared<FractureProblem>(fracGridGeometry, fractureSpatialParams_, couplingManager_, *bulkGridGeometry, "Upscaling"), fractureId);
           
            if (problem[bulkId].boundaryConditionType() == periodic)
                periodicCouplingManager_ = std::make_shared<PeriodicCouplingManager>(bulkGridGeometry,
                                                                                     fracGridGeometry,
                                                                                     couplingManager_,
                                                                                     couplingMapper_);
            else periodicCouplingManager_ = nullptr;
            if (periodicCouplingManager_)
                periodicCouplingManager_->setPeriodicDofMap();

        }

        FracMediaPermeabilityUpscaling(std::shared_ptr<GridGeometry<bulkId>> bulkGridGeometry,
                                       std::shared_ptr<GridGeometry<fractureId>> fracGridGeometry,
                                       std::shared_ptr<CouplingMapper> couplingMapper,
                                       std::shared_ptr<MatrixSpatialParams> matrixSpatialParams,
                                       std::shared_ptr<FractureSpatialParams> fractureSpatialParams)
        : bulkGridGeometry_(bulkGridGeometry)
        , fracGridGeometry_(fracGridGeometry)
        , couplingMapper_(couplingMapper)  
        , matrixSpatialParams_(matrixSpatialParams)  
        , fractureSpatialParams_(fractureSpatialParams)                             
        {
            
            gridGeometry_ = std::make_tuple(bulkGridGeometry, fracGridGeometry);
            couplingManager_ = std::make_shared<CouplingManager>();
        
            problem.set(std::make_shared<MatrixProblem>(bulkGridGeometry, matrixSpatialParams_, couplingManager_, "Upscaling"), bulkId);
            problem.set(std::make_shared<FractureProblem>(fracGridGeometry, fractureSpatialParams_, couplingManager_, *bulkGridGeometry, "Upscaling"), fractureId);

            if (problem[bulkId].boundaryConditionType() == periodic)
                periodicCouplingManager_ = std::make_shared<PeriodicCouplingManager>(bulkGridGeometry,
                                                                                     fracGridGeometry,
                                                                                     couplingManager_,
                                                                                     couplingMapper_);
            else periodicCouplingManager_ = nullptr;
            if (periodicCouplingManager_)
                periodicCouplingManager_->setPeriodicDofMap();

        }

        const PermeabilityTensor getUpscaledPermeability(bool writeVtk = true, std::string label = "perm_upscaling", Scalar time = 0.0)
        {
            PermeabilityTensor permeability(0.0);

            const auto bcType = problem[bulkId].boundaryConditionType();
            auto homogenization = std::make_shared<Homogenization>(std::make_tuple(bulkGridGeometry_, fracGridGeometry_));   

            for (int step = 0; step < dimWorld; step++)
            {
     
                if (periodicCouplingManager_)
                {
                    const auto gradP = problem[bulkId].appliedPressureGradient();
                    periodicCouplingManager_->setPrimaryVariablesGradient(gradP);
                }
   
                // the solution vector
                typename MDTraits::SolutionVector x;
                x[bulkId].resize(bulkGridGeometry_->numDofs());
                x[fractureId].resize(fracGridGeometry_->numDofs());

                problem[bulkId].applyInitialSolution(x[bulkId]);
                problem[fractureId].applyInitialSolution(x[fractureId]);

                // initialize the coupling manager
                couplingManager_->init(problem.get(bulkId), problem.get(fractureId), couplingMapper_, x);

                // the grid variables 
     
                using GridVariables = MultiDomainFVGridVariables<MDTraits>;
                GridVariables gridVars(std::make_tuple(bulkGridGeometry_, fracGridGeometry_), problem.getTuple());
                       
                gridVars.init(x);

                // intialize the vtk output module
                using VtkOutputModule = MultiDomainVtkOutputModule<MDTraits>;
                using MatrixOutputModule = typename VtkOutputModule::Type<bulkId>;
                using FractureOutputModule = typename VtkOutputModule::Type<fractureId>;

                VtkOutputModule vtkWriter;
                std::string matrixFile = label + "_matrix" + std::to_string(step);
                std::string fractureFile = label + "_fracture" + std::to_string(step);

                const auto dm = Dune::VTK::nonconforming;
                vtkWriter.set(std::make_shared<MatrixOutputModule>(gridVars[bulkId], x[bulkId], matrixFile, "Matrix", dm), bulkId);
                vtkWriter.set(std::make_shared<FractureOutputModule>(gridVars[fractureId], x[fractureId], fractureFile, "Fracture"), fractureId);
                vtkWriter.initDefaultOutputFields();

                // the assembler
                using Assembler = MultiDomainPeriodicFVAssembler<MDTraits, CouplingManager, PeriodicCouplingManager, DiffMethod::numeric, true>;
                auto assembler = std::make_shared<Assembler>(problem.getTuple(),
                                                             std::make_tuple(bulkGridGeometry_, fracGridGeometry_),
                                                             gridVars.getTuple(),
                                                             couplingManager_,
                                                             periodicCouplingManager_);

                // the linear solver
                using LinearSolver = UMFPackBackend;
                auto linearSolver = std::make_shared<LinearSolver>();

                // the non-linear solver
                using NewtonSolver = Dumux::MultiDomainNewtonSolver<Assembler, LinearSolver, CouplingManager>;
                auto newtonSolver = std::make_shared<NewtonSolver>(assembler, linearSolver, couplingManager_);

                // linearize & solve
                newtonSolver->solve(x);

                // update grid variables for output
                gridVars.update(x);

                // Dune::FieldVector<Scalar,dimWorld> hmg_boundFlux(0.0);
                const auto hmg_gradP = homogenization->getHomogenizedGradient(gridVars[bulkId], x[bulkId]);

                vtkWriter.write(time);
                
                if(bcType == no_flow){
                    const auto hmg_boundFlux = problem[bulkId].appliedFluxVector();
                    for (int dir = 0; dir < dimWorld; dir++)
                        permeability[step][dir] = hmg_boundFlux[0][step] / hmg_gradP[0][dir];

                } else {
                    
                    const auto hmg_boundFlux = homogenization->getHomogenizedFluxFromBoundary(*assembler, *couplingManager_, x);
                    for (int dir = 0; dir < dimWorld; dir++)
                        permeability[step][dir] = -hmg_boundFlux[0][dir] / hmg_gradP[0][step];
                }

                problem[bulkId].advanceUpscalingStep();
                problem[fractureId].advanceUpscalingStep();

            }

            // reset count of steps for future permeability computations

            problem[bulkId].resetStepCount();
            problem[fractureId].resetStepCount();
            return permeability;
        }

    private:
        GridGeometryTuple gridGeometry_;
        std::shared_ptr<GridGeometry<bulkId>> bulkGridGeometry_;
        std::shared_ptr<GridGeometry<fractureId>> fracGridGeometry_;
        //std::shared_ptr<MDGridGeometry> gridGeometry_;
        MDProblem problem;
        std::shared_ptr<CouplingManager> couplingManager_;
        std::shared_ptr<CouplingMapper> couplingMapper_;
        std::shared_ptr<MatrixSpatialParams> matrixSpatialParams_;
        std::shared_ptr<FractureSpatialParams> fractureSpatialParams_;
        std::shared_ptr<PeriodicCouplingManager> periodicCouplingManager_;
    };

}