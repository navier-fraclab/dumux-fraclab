// -*- mode: C++; tab-width: 4; indent-tabs-mode: nil; c-basic-offset: 4 -*-
// vi: set et ts=4 sw=4 sts=4:
/*****************************************************************************
 *   See the file COPYING for full copying permissions.                      *
 *                                                                           *
 *   This program is free software: you can redistribute it and/or modify    *
 *   it under the terms of the GNU General Public License as published by    *
 *   the Free Software Foundation, either version 2 of the License, or       *
 *   (at your option) any later version.                                     *
 *                                                                           *
 *   This program is distributed in the hope that it will be useful,         *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of          *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the            *
 *   GNU General Public License for more details.                            *
 *                                                                           *
 *   You should have received a copy of the GNU General Public License       *
 *   along with this program.  If not, see <http://www.gnu.org/licenses/>.   *
 *****************************************************************************/
/*!
Problem class for upscaling permeability in rectangular, parallelepided shaped
fractured media (porous matrix domain)
 */
#ifndef DUMUX_FRACLAB_FLOw_UPSCALING_PROBLEM_MATRIX_HH
#define DUMUX_FRACLAB_FLOw_UPSCALING_PROBLEM_MATRIX_HH

#include <dumux/porousmediumflow/problem.hh>

namespace Dumux {

enum FlowBoundaryConditionType{lin_press, no_flow, periodic};

template<class TypeTag>
class UpscalingMatrixFlowProblem : public PorousMediumFlowProblem<TypeTag>
{
    using ParentType = PorousMediumFlowProblem<TypeTag>;
    using CouplingManager = GetPropType<TypeTag, Properties::CouplingManager>;
    using FVGridGeometry = GetPropType<TypeTag, Properties::GridGeometry>;
    using FVElementGeometry = typename FVGridGeometry::LocalView;
    using SubControlVolume = typename FVElementGeometry::SubControlVolume;
    using SubControlVolumeFace = typename FVElementGeometry::SubControlVolumeFace;
    using GridView = typename FVGridGeometry::GridView;
    using GridIndexType = typename GridView::IndexSet::IndexType;
    using Element = typename GridView::template Codim<0>::Entity;
    using GlobalPosition = typename Element::Geometry::GlobalCoordinate;

    using Scalar = GetPropType<TypeTag, Properties::Scalar>;
    using PrimaryVariables = GetPropType<TypeTag, Properties::PrimaryVariables>;
    using NumEqVector = GetPropType<TypeTag, Properties::NumEqVector>;
    using BoundaryTypes = Dumux::BoundaryTypes</*numEq*/1>;
    enum { numEq = GetPropType<TypeTag, Properties::ModelTraits>::numEq() };
    static constexpr int dimWorld = GridView::dimensionworld;
    using GradientPV = Dune::FieldVector<GlobalPosition, 1>;
    
    using GridIndexTypeScalarPair = std::pair<GridIndexType, Scalar>;
    using GridIndexTypeScalarMapMultiMap = std::multimap<GridIndexType, GridIndexTypeScalarPair>;

public:
    //! The constructor
    UpscalingMatrixFlowProblem(std::shared_ptr<const FVGridGeometry> fvGridGeometry,
                                                std::shared_ptr<typename ParentType::SpatialParams> spatialParams,
                                                std::shared_ptr<CouplingManager> couplingManager,
                                                const std::string& paramGroup = "Upscaling")
    : ParentType(fvGridGeometry, spatialParams, paramGroup)
    , couplingManagerPtr_(couplingManager)
    {
        problemName_ = getParamFromGroup<std::string>(this->paramGroup(), "Problem.Name");
        
         const auto bc = getParamFromGroup<std::string>(this->paramGroup(), "Problem.BoundaryConditions");

        if(bc == "Dirichlet")
            bcType = lin_press;
        else if(bc == "Neumann")
            bcType = no_flow;
        else if(bc == "Periodic")
            bcType == periodic;
        else
            DUNE_THROW(Dune::InvalidStateException, 
            "Type of Boundary condition for upscaling is invalid. Options are: Dirichlet or Neumann");

        // Periodic and displacement boundary conditions apply spress gradient, neumann bcs apply flux
        if(bcType == no_flow)
            appliedFlux = getParamFromGroup<Scalar>("Problem", "AppliedFlux");    
        else 
            appliedGradP = getParamFromGroup<Scalar>("Problem", "AppliedPressureGradient");  
                  
        minCoor = fvGridGeometry -> bBoxMin();
        maxCoor = fvGridGeometry -> bBoxMax();
    }

    //! Return the problem name
    const std::string& name() const
    { return problemName_; }

    //! Return the (constant) temperature in the domain
    Scalar temperature() const
    { return 283.15; }

    //! Evaluate the source term at a given position
    PrimaryVariables sourceAtPos(const GlobalPosition& globalPos) const
    { return PrimaryVariables(0.0); }

    //! Evaluate the initial conditions at a given position
    PrimaryVariables initialAtPos(const GlobalPosition& globalPos) const
    {
         return dirichletAtPos(globalPos);
    }

    //! Evaluate the Neumann boundary conditions
    template<class ElementVolumeVariables, class ElemFluxVarsCache>
    NumEqVector neumann(const Element& element,
                        const FVElementGeometry& fvGeometry,
                        const ElementVolumeVariables& elemVolVars,
                        const ElemFluxVarsCache& elemFluxVarsCache,
                        const SubControlVolumeFace& scvf) const
    { 
        NumEqVector flux(0.0);
        const auto scvfCenter = scvf.center();
        if(bcType == no_flow and onMinBoundary_(step, scvfCenter))
            flux[step] = appliedFlux;
        return flux;
    }

    //! Specifies the type of interior boundary condition at a given position
    BoundaryTypes interiorBoundaryTypes(const Element& element, const SubControlVolumeFace& scvf) const
    {
        BoundaryTypes values;

        static const auto useDirichlet = getParamFromGroup<Scalar>("Bulk", "Problem.UseInteriorDirichletBCs", false);
        if (useDirichlet) values.setAllDirichlet();
        else values.setAllNeumann();

        return values;
    }

    //! Specifies the type of boundary condition at a given position
    BoundaryTypes boundaryTypesAtPos(const GlobalPosition& globalPos) const
    {
        BoundaryTypes values;
        values.setAllNeumann();
        
        if(bcType == lin_press){
           if(onBoundary_(globalPos))
                values.setAllDirichlet();
        }
        else if(bcType == no_flow){
            for(int bound = 0; bound < dimWorld; bound++){
                if(bound == step) continue;
                if(onBoundary_(bound,globalPos))
                    values.setAllDirichlet();
            }
        }

        return values;
    }

    //! Specify the Dirichlet BCs at a given position
    PrimaryVariables dirichletAtPos(const GlobalPosition& globalPos) const
    { 
        PrimaryVariables values(0.0);

        // if boundary conditions are dirichlet, impose linear displacement boundary conditions
        if(bcType == lin_press and onBoundary_(globalPos)){
            const auto gradP = appliedPressureGradient();
            values = gradP[0].dot(globalPos);
        }
    
        return values; 
    }

    //! Return reference to the coupling manager
    const CouplingManager& couplingManager() const
    { return *couplingManagerPtr_; }

    const GradientPV appliedPressureGradient() const
    {
        GradientPV gradP;
        gradP[0] = 0.0;
        gradP[0][step] = appliedGradP;

        return gradP;
    }

    void advanceUpscalingStep()
    {step++;}

    bool onMinBoundary_(int dir, const GlobalPosition& globalPos) const
    {return globalPos[dir] - minCoor[dir] < eps_;}

    bool onMaxBoundary_(int dir, const GlobalPosition& globalPos) const
    {return maxCoor[dir] - globalPos[dir] < eps_;}
    
    bool onBoundary_(const GlobalPosition& globalPos) const
    {
        for(int dir = 0; dir < dimWorld; dir++)
            if(onMinBoundary_(dir,globalPos) or onMaxBoundary_(dir,globalPos))
                return true;
        return false;
    }

    const FlowBoundaryConditionType boundaryConditionType() const
    {return bcType;}

    bool onBoundary_(int dir, const GlobalPosition& globalPos) const
    {
        return onMinBoundary_(dir,globalPos) or onMaxBoundary_(dir,globalPos);
    }

    void setEps_(Scalar eps)
    {eps_ = eps;}

private:
    std::shared_ptr<CouplingManager> couplingManagerPtr_;
    std::string problemName_;
    GlobalPosition minCoor;
    GlobalPosition maxCoor;
    Scalar appliedGradP;
    Scalar appliedFlux;
    FlowBoundaryConditionType bcType;
    Scalar eps_ = 1e-6;
    int step = 0;
    
};

} //end namespace Dumux

#endif
