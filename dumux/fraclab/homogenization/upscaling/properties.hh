#ifndef DUMUX_FLOW_UPSCALING_PROPERTIES_HH
#define DUMUX_FLOW_UPSCALING_PROPERTIES_HH

#if HAVE_DUNE_MMESH
#include <dune/mmesh/mmesh.hh>
#endif


#include <dumux/discretization/box.hh>
#include <dumux/porousmediumflow/1p/model.hh>
#include <dumux/material/fluidsystems/1pliquid.hh>
#include <dumux/material/components/constant.hh>

#include "problem_1p.hh"

namespace Dumux
{
    namespace Properties
    {
        // Create new type tags
        namespace TTag
        {
            struct UpscalingOnePBox{using InheritsFrom = std::tuple<OneP, BoxModel>;};
        } // end namespace TTag

        // Properties for new tag

        // Set the problem type
        template <class TypeTag>
        struct Problem<TypeTag, TTag::UpscalingOnePBox>
        {
            using type = UpscalingFlowProblem<TypeTag>;
        };

        // the fluid system
        template <class TypeTag>
        struct FluidSystem<TypeTag, TTag::UpscalingOnePBox>
        {
        private:
            using Scalar = GetPropType<TypeTag, Properties::Scalar>;

        public:
            using type = FluidSystems::OnePLiquid<Scalar, Components::Constant</*id*/ 0, Scalar>>;
        };

        // solution-independent advection
        template <class TypeTag>
        struct SolutionDependentAdvection<TypeTag, TTag::UpscalingOnePBox>
        {
            static constexpr bool value = false;
        };
    }
}

#endif