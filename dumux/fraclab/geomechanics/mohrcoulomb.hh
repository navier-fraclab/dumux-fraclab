// -*- mode: C++; tab-width: 4; indent-tabs-mode: nil; c-basic-offset: 4 -*-
// vi: set et ts=4 sw=4 sts=4:
/*****************************************************************************
 *   See the file COPYING for full copying permissions.                      *
 *                                                                           *
 *   This program is free software: you can redistribute it and/or modify    *
 *   it under the terms of the GNU General Public License as published by    *
 *   the Free Software Foundation, either version 3 of the License, or       *
 *   (at your option) any later version.                                     *
 *                                                                           *
 *   This program is distributed in the hope that it will be useful,         *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of          *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the            *
 *   GNU General Public License for more details.                            *
 *                                                                           *
 *   You should have received a copy of the GNU General Public License       *
 *   along with this program.  If not, see <http://www.gnu.org/licenses/>.   *
 *****************************************************************************/
/*!
 * \file
 * \ingroup Geomechanics
 * \brief \copydoc Dumux::MohrCoulomb
 */
#ifndef DUMUX_GEOMECHANICS_MOHRCOULOMB_HH
#define DUMUX_GEOMECHANICS_MOHRCOULOMB_HH

#include <dumux/fraclab/geomechanics/druckerprager.hh>

namespace Dumux {

/*!
 * \ingroup Geomechanics 
 */
template<class Scalar, int dim>
class MohrCoulomb
{
public:
    using StressTensor = Dune::FieldMatrix<Scalar, 3, 3>;
    using InvariantVector = Dune::FieldVector<Scalar, 3>;
    using FunctionDerivative = Dune::FieldVector<Scalar,3>;
    static constexpr int numStre = (dim == 2) ? 4:6;
    static constexpr int numStra = (dim == 2) ? 3:6; 
    using CamInvariants = CambridgeInvariants<Scalar,dim>;
    using DevInvariants = DeviatoricTensorInvariants<Scalar,dim>;

    using YieldNormal =  Dune::FieldVector<Scalar,numStre>;
    using DerivativeMatrix = Dune::FieldMatrix<Scalar, 3, numStre>;
    using FlowNormal = Dune::FieldVector<Scalar,numStra>;
    using DruckerPragerOuter =  DruckerPrager<Scalar, dim,OUTER>;
    using DruckerPragerInner =  DruckerPrager<Scalar, dim,INNER>;
    //! Default constructor
    MohrCoulomb() = default;

    //! Constructor taking parameters directly
    MohrCoulomb(Scalar cohesion, Scalar frictionAngle, Scalar dilationAngle)
    : cohesion_(cohesion)
    {
        frictionAngle_ = frictionAngle*(M_PI/180.0);
        dilationAngle_ = dilationAngle*(M_PI/180.0);
    }

    //! Return cohesion
    Scalar cohesion() const
    { return cohesion_; }

    //! Return frictionAngle
    Scalar frictionAngle() const
    { return frictionAngle_; }

    //! Return dilationAngle
    Scalar dilationAngle() const
    { return dilationAngle_; }

    void setCohesion(Scalar cohesion)
    { cohesion_ = cohesion; }

    void setFrictionAngle(Scalar frictionAngle)
    { frictionAngle_ = frictionAngle*(M_PI/180.0);}

    void setDilationAngle(Scalar dilationAngle)
    { dilationAngle_ = dilationAngle*(M_PI/180.0);}

    Scalar yieldFunction(const StressTensor& sigma) const
    {
        Scalar p = CamInvariants::meanStress(sigma);
        Scalar q = CamInvariants::deviatoricStress(sigma);
        Scalar theta = CamInvariants::lodeAngle(sigma);
        Scalar yf = p*sin(frictionAngle_)
        + q*(cos(theta)-(1/sqrt(3.0))*sin(frictionAngle_)*sin(theta))
        - cohesion_*cos(frictionAngle_);

        return yf;
    }

    Scalar flowFunction(const StressTensor& sigma) const
    {
        Scalar p = CamInvariants::meanStress(sigma);
        Scalar q = CamInvariants::deviatoricStress(sigma);
        Scalar theta = CamInvariants::lodeAngle(sigma);
        Scalar gf = p*sin(dilationAngle_)
        + q*(cos(theta)-(1/sqrt(3.0))*sin(dilationAngle_)*sin(theta))
        - cohesion_*cos(dilationAngle_);

        return gf;
    }
    
    FunctionDerivative functionDerivative(const StressTensor& sigma, bool flowRule = false) const
    {
        FunctionDerivative df(0.0);
        
        Scalar angle = flowRule ? dilationAngle_ : frictionAngle_;
        Scalar q = CamInvariants::deviatoricStress(sigma);
        Scalar theta = CamInvariants::lodeAngle(sigma);
        Scalar thetaCorner = (30.0 / 180.0) * M_PI;

        // if stress not at corner, regular Mohr-Coulom equation is 
        // used
    
        if (abs(theta) != thetaCorner)
        {
            Scalar At = cos(theta) - (sin(theta)*sin(angle))/(sqrt(3.0));
            Scalar dAdt =  -sin(theta) - (cos(theta)*sin(angle))/(sqrt(3.0));

            df[0]= sin(angle)/3.0;
            df[1] = (0.5/q)*(At - tan(3.0*theta)*dAdt);
            df[2] = -0.5 * sqrt(3.0) * dAdt/(q*q*cos(3.0*theta));
        } else {
            DruckerPragerOuter dpOuter(cohesion_,frictionAngle_,dilationAngle_,false);
            DruckerPragerInner dpInner(cohesion_,frictionAngle_,dilationAngle_,false);
  
            // if at corner, use Drucker Prager derivative
            if(theta > 0)
                df = dpOuter.functionDerivative(sigma, flowRule);
            else
                df = dpInner.functionDerivative(sigma, flowRule);
        }
    
        return df;
    }

    const DruckerPragerOuter equivalentDruckerPragerOuter() const
    {
        DruckerPragerOuter dpOuter(cohesion_,frictionAngle_,dilationAngle_,false);
        return dpOuter;
    }

    const DruckerPragerInner equivalentDruckerPragerInner() const
    {
        DruckerPragerInner dpInner(cohesion_,frictionAngle_,dilationAngle_,false);
        return dpInner;
    }
    
    YieldNormal yieldFunctionNormal(const StressTensor& sigma) const
    {
        const auto dInv = DevInvariants::getDerivatives(sigma);
        const auto df = functionDerivative(sigma);
        YieldNormal af(0.0);
        dInv.mtv(df, af);
        return af;
    }

    YieldNormal flowFunctionNormal(const StressTensor& sigma) const
    {   
        const auto dInv = DevInvariants::getDerivatives(sigma);
        const auto df = functionDerivative(sigma, true);
        YieldNormal ag(0.0);
        dInv.mtv(df, ag);
        return ag;
    }
    
    Scalar hardeningParameter()
    {return 0.0;}
private:
    Scalar cohesion_;
    Scalar frictionAngle_;
    Scalar dilationAngle_ = 0.0;
};
} // end namespace Dumux
#endif
