// -*- mode: C++; tab-width: 4; indent-tabs-mode: nil; c-basic-offset: 4 -*-
// vi: set et ts=4 sw=4 sts=4:
/*****************************************************************************
 *   See the file COPYING for full copying permissions.                      *
 *                                                                           *
 *   This program is free software: you can redistribute it and/or modify    *
 *   it under the terms of the GNU General Public License as published by    *
 *   the Free Software Foundation, either version 3 of the License, or       *
 *   (at your option) any later version.                                     *
 *                                                                           *
 *   This program is distributed in the hope that it will be useful,         *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of          *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the            *
 *   GNU General Public License for more details.                            *
 *                                                                           *
 *   You should have received a copy of the GNU General Public License       *
 *   along with this program.  If not, see <http://www.gnu.org/licenses/>.   *
 *****************************************************************************/
/*!
 * \file
 * \ingroup Geomechanics
 */
#ifndef DUMUX_GEOMECHANICS_DRUCKERPRAGER_HH
#define DUMUX_GEOMECHANICS_DRUCKERPRAGER_HH

#include <dumux/fraclab/geomechanics/invariantscambridge.hh>

namespace Dumux {

  
typedef enum
{
    OUTER = 0,  // circumscribe outer corners of Mohr Coulomb surface
    INNER = 1,  // circumscribe inner corners of Mohr Coulomb surface
}eDruckerPragerType;

template<class Scalar, int dim, class Implementation>
class DruckerPragerBase
{
    static constexpr int numStre = (dim == 2) ? 4:6;
    static constexpr int numStra = (dim == 2) ? 3:6;
    
    using DevInvariants = DeviatoricTensorInvariants<Scalar,dim>;

    using StressTensor = Dune::FieldMatrix<Scalar, 3, 3>;
    using InvariantVector = Dune::FieldVector<Scalar, 3>;  
    using FunctionDerivative = Dune::FieldVector<Scalar,3>;
    using YieldNormal = Dune::FieldVector<Scalar,numStre>;
    using DerivativeMatrix = Dune::FieldMatrix<Scalar, 3, numStre>;
    using FlowNormal = Dune::FieldVector<Scalar,numStra>;

public:
    DruckerPragerBase() = default;
  
    DruckerPragerBase(Scalar cohesion, Scalar frictionAngle, Scalar dilationAngle, bool degree = true)
    : cohesion_(cohesion)
    {
        setFrictionAngle(frictionAngle, degree);
        setDilationAngle(dilationAngle,degree);
       
        asImp_().setD();
        asImp_().setSig0();

        isSet = true;
    }
  
    Scalar cohesion() const
    {return cohesion_; }

    Scalar frictionAngle() const
    { return frictionAngle_; }

    Scalar dilationAngle() const
    { return frictionAngle_; }

    void setCohesion(Scalar cohesion)
    { cohesion_ = cohesion; }

    void setFrictionAngle(Scalar frictionAngle, bool degree = true)
    { frictionAngle_ = degree ? frictionAngle*(M_PI/180.0) : frictionAngle;}

    void setDilationAngle(Scalar dilationAngle, bool degree = true)
    { dilationAngle_ = degree ? dilationAngle*(M_PI/180.0) : dilationAngle;}

    void setFunctionParams()
    {
        asImp_().setD();
        asImp_().setSig0();
        isSet = true;
    }
    
    Scalar yieldFunction(const StressTensor& sigma) const
    {
        Scalar I1 = DevInvariants::firstInvariant(sigma);
        Scalar J2 = DevInvariants::secondInvariant(sigma);
        Scalar yf = Dy_*I1 + sqrt(J2) - sig0_;

        return yf;
    }

    FunctionDerivative functionDerivative(const StressTensor& sigma, bool flowRule = false) const
    {
        FunctionDerivative df(0.0);
        Scalar J2 = DevInvariants::secondInvariant(sigma);
        df[0] = flowRule ? Df_ : Dy_;
        df[1] = 0.5*pow(J2, -0.5);

        return df;
    }

    YieldNormal yieldFunctionNormal(const StressTensor& sigma) const
    {
        auto dInv = DevInvariants::getDerivatives(sigma);
        auto df = functionDerivative(sigma);
        YieldNormal af(0.0);
        dInv.mtv(df, af);
        return af;
    }

    YieldNormal flowFunctionNormal(const StressTensor& sigma) const
    {
        auto dInv = DevInvariants::getDerivatives(sigma);
        auto df = functionDerivative(sigma, true);
        YieldNormal af(0.0);
        dInv.mtv(df, af);
        return af;
    }
    
protected:
    Implementation &asImp_()
    { return *static_cast<Implementation*>(this); }
  
    const Implementation &asImp_() const
    { return *static_cast<const Implementation*>(this); } 

    Scalar cohesion_;
    Scalar frictionAngle_;
    Scalar dilationAngle_;
    Scalar Dy_; // for yield function
    Scalar sig0_; // for yield function
    Scalar Df_; // for flow function
    bool isSet = false; //function params D and sig0 are set?
};

template<class Scalar, int dim, eDruckerPragerType type>
class DruckerPrager
{
};
  
template<class Scalar, int dim>
class DruckerPrager<Scalar, dim, OUTER>
:public DruckerPragerBase<Scalar, dim,DruckerPrager<Scalar, dim, OUTER> >
{
    using ThisType = DruckerPrager<Scalar, dim, OUTER>;
    using ParentType = DruckerPragerBase<Scalar, dim, ThisType>;
    using ParentType::sig0_;
    using ParentType::Dy_;
    using ParentType::Df_;
    using ParentType::cohesion_;
    using ParentType::frictionAngle_;
    using ParentType::dilationAngle_;

public:

    DruckerPrager() = default;

    DruckerPrager(Scalar cohesion, Scalar frictionAngle, Scalar dilationAngle, bool degree = true)
    :ParentType(cohesion, frictionAngle, dilationAngle, degree)
    {};

    void setD()
    {
        Dy_ = 2.0*sin(frictionAngle_)/(sqrt(3.0)*(3.0-sin(frictionAngle_)));
        Df_ = 2.0*sin(dilationAngle_)/(sqrt(3.0)*(3.0-sin(dilationAngle_)));
    }

    void setSig0()
    {    sig0_ = 6.0 * cohesion_ *  cos(frictionAngle_)/(sqrt(3.0)*(3.0 - sin(frictionAngle_)));}
  
};

      
template<class Scalar, int dim>
class DruckerPrager<Scalar, dim, INNER>
:public DruckerPragerBase<Scalar, dim,DruckerPrager<Scalar, dim, INNER> >
{
    using ThisType = DruckerPrager<Scalar, dim, INNER>;
    using ParentType = DruckerPragerBase<Scalar, dim, ThisType>;
    using ParentType::sig0_;
    using ParentType::Dy_;
    using ParentType::Df_;
    using ParentType::cohesion_;
    using ParentType::frictionAngle_;
    using ParentType::dilationAngle_;
  
public:

    DruckerPrager() = default;

    DruckerPrager(Scalar cohesion, Scalar frictionAngle, Scalar dilationAngle, bool degree = true)
    :ParentType(cohesion, frictionAngle, dilationAngle, degree)
    {};

    void setD()
    {
        Scalar sinfi = sin(frictionAngle_);
        Scalar sinpsi = sin(dilationAngle_);
        Dy_ = 2.0*sinfi/(sqrt(3.0)*(3.0+sinfi));
        Df_ = 2.0*sinpsi/(sqrt(3.0)*(3.0+sinpsi));
    }

    void setSig0()
    {
        sig0_ = 6.0 * cohesion_ *  cos(frictionAngle_)/(sqrt(3.0)*(3.0 + sin(frictionAngle_)));
    }
  
};
} // end namespace Dumux
#endif
