// -*- mode: C++; tab-width: 4; indent-tabs-mode: nil; c-basic-offset: 4 -*-
// vi: set et ts=4 sw=4 sts=4:
/*****************************************************************************
 *   See the file COPYING for full copying permissions.                      *
 *                                                                           *
 *   This program is free software: you can redistribute it and/or modify    *
 *   it under the terms of the GNU General Public License as published by    *
 *   the Free Software Foundation, either version 3 of the License, or       *
 *   (at your option) any later version.                                     *
 *                                                                           *
 *   This program is distributed in the hope that it will be useful,         *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of          *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the            *
 *   GNU General Public License for more details.                            *
 *                                                                           *
 *   You should have received a copy of the GNU General Public License       *
 *   along with this program.  If not, see <http://www.gnu.org/licenses/>.   *
 *****************************************************************************/

#ifndef DUMUX_GEOMECHANICS_STATEVARIABLES_CACHE_HH
#define DUMUX_GEOMECHANICS_STATEVARIABLES_CACHE_HH

#include <dumux/geomechanics/stressvariablescache.hh>

namespace Dumux {

template<class Scalar>
struct StateVariable
{
    std::string label;
    std::vector<Scalar> value;

    void setValue( std::vector<Scalar> val)
    {value = val;}

    void setVectorSize(unsigned int size)
    {value.resize(size);}

    void setLabel(std::string lab)
    {label = lab;}
};

template< class Scalar, class GridGeometry, DiscretizationMethod dm = GridGeometry::discMethod >
class StateVariablesCache;

//! geomechanics is available only with the box method
template< class Scalar, class GridGeometry >
class StateVariablesCache<Scalar, GridGeometry, DiscretizationMethod::box>
: public StressHistoryCache<Scalar, GridGeometry, DiscretizationMethod::box>
{
    using Parent = StressHistoryCache<Scalar, GridGeometry, DiscretizationMethod::box>;
    using FVElementGeometry = typename GridGeometry::LocalView;
    using SubControlVolumeFace = typename FVElementGeometry::SubControlVolumeFace;
    using GridView = typename GridGeometry::GridView;
    using Element = typename GridView::template Codim<0>::Entity;
    using GridIndexType = typename IndexTraits<GridView>::GridIndex;
    static constexpr int dim = GridView::dimension;
    static constexpr int dimWorld = GridView::dimensionworld;
    static constexpr int numStre = (dim == 2 && dimWorld == 2) ? 4 : 2*dim;

    using ScalarVector = std::vector<Scalar>;
    using ScalarVectorVector = std::vector<ScalarVector>;
    using StringVector = std::vector<std::string>;
    using StateVariableVector = std::vector<StateVariable<Scalar>>;
    
public:
    
    void init(const StateVariableVector stateVarVector)
    {
        stateVariables_ = stateVarVector;
    }

    void updateStateVariable(const std::string lab, const ScalarVector val)
    {
        for(unsigned int i = 0; i < stateVariables_.size(); i++){
            if(stateVariables_[i].label == lab){
                for(unsigned int j = 0; j < stateVariables_[i].value.size(); j++){
                    stateVariables_[i].value[j] = val[j];
                }
                return;
            }
        }
        /*
        for(auto& scalarVar : stateVariables_){
            if (scalarVar.label == lab){
                scalarVar.value = val;
                return;
            }
        }
        */
        DUNE_THROW(Dune::InvalidStateException, "State Variable with label " + lab + " not sotred in StateVariablesCache class. Call the function init to initliaze all the variables to be stored");
    }

    const ScalarVector getStateVariable(const std::string lab) const
    {
        for(const auto& scalarVar :stateVariables_){
            if (scalarVar.label == lab)
                return scalarVar.value;
        }

        DUNE_THROW(Dune::InvalidStateException, "State Variable with label " + lab + " not sotred in StateVariablesCache class. Call the function init to initliaze all the variables to be stored");
    }

    const Scalar getTest() const
    {return test_;}

    void setTest(Scalar test)
    {test_ = test;}

private:

    StateVariableVector stateVariables_; 
    Scalar test_ = 0.0;
};

// specialization for the cell centered tpfa method
template< class Scalar, class GridGeometry >
class StateVariablesCache<Scalar, GridGeometry, DiscretizationMethod::cctpfa>
: public StressHistoryCache<Scalar, GridGeometry, DiscretizationMethod::cctpfa>
{};

// specialization for the cell centered mpfa method
template< class Scalar, class GridGeometry >
class StateVariablesCache<Scalar, GridGeometry, DiscretizationMethod::ccmpfa>
: public StressHistoryCache<Scalar, GridGeometry, DiscretizationMethod::cctpfa>
{};

} // end namespace Dumux

#endif
