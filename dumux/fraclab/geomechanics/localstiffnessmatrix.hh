// -*- mode: C++; tab-width: 4; indent-tabs-mode: nil; c-basic-offset: 4 -*-
// vi: set et ts=4 sw=4 sts=4:
/*****************************************************************************
 *   See the file COPYING for full copying permissions.                      *
 *                                                                           *
 *   This program is free software: you can redistribute it and/or modify    *
 *   it under the terms of the GNU General Public License as published by    *
 *   the Free Software Foundation, either version 3 of the License, or       *
 *   (at your option) any later version.                                     *
 *                                                                           *
 *   This program is distributed in the hope that it will be useful,         *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of          *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the            *
 *   GNU General Public License for more details.                            *
 *                                                                           *
 *   You should have received a copy of the GNU General Public License       *
 *   along with this program.  If not, see <http://www.gnu.org/licenses/>.   *
 *****************************************************************************/
#include <dune/common/fmatrix.hh>

#ifndef DUMUX_FRACLAB_GEOMECHANICS_LOCALSTIFF_HH
#define DUMUX_FRACLAB_GEOMECHANICS_LOCALSTIFF_HH

// Class that computes the elastic tangent matrix

namespace Dumux {

template<class TypeTag>
class ElasticTangentMatrix
{   
    using Scalar = GetPropType<TypeTag, Properties::GridVariables>;
    using GridGeometry = GetPropType<TypeTag, Properties::GridVariables>;
    using FVElementGeometry = typename GridGeometry::LocalView;
    using SubControlVolumeFace = typename FVElementGeometry::SubControlVolumeFace;
    using GridView = typename GridGeometry::GridView;
    using Element = typename GridView::template Codim<0>::Entity;
    
    enum { numEq = GetPropType<TypeTag, Properties::ModelTraits>::numEq() };

    static const int dim = GridView::dimension;
    static const int dimWorld = GridView::dimensionworld;

public:

    template<class Problem, class ElementVolumeVariables, class FluxVarsCache>
    static const auto localTangentMatrix(const Problem& problem,
                                         const Element& element,
                                         const SubControlVolumeFace& scvf,
                                         const FVElementGeometry& fvGeometry,
                                         const ElementVolumeVariables& elemVolVars,
                                         const FluxVarsCache& fluxVarCache)
    {
        const auto numNodes = fvGeometry.numScv();
       
        constexpr int nrowB = 3*(dimWorld-1);
        constexpr int ncolB = numNodes*dimWorld;

        Dune::FieldMatrix<Scalar,nrowB,ncolB> B(0.0);

        //build matrix B

        int row = dimWorld;
        for (const auto& scv : scvs(fvGeometry))
        {
            const auto gradNi = fluxVarCache.gradN(scv.indexInElement());
            for(int i = 0; i < dimWorld; i++){
         
                int colI = scv.indexInElement()*dimWorld + i;
                B[i][colI] = gradNi[i];
                
                for(int j = i+1; j < dimWorld; j++){
                    int colJ = scv.indexInElement()*dimWorld + j;
                    B[row][colI] = gradNi[j];
                    B[row][colI] = gradNi[i];   
                    row++;
                }
            }
        }

        // build consitutive elastic stiffness matrix

        constexpr int sizeC = nrowB;
        Dune::FieldMatrix<Scalar, sizeC, sizeC> C(0.0);
        const auto& lameParams = problem.spatialParams().lameParams(element, fvGeometry, elemVolVars, fluxVarCache);

        for(int i = 0; i < dimWorld; i++){
            for(int j = 0; j < dimWorld; j++){
                C[i][j] = lameParams.lambda();
                C[i][i] += 2.0*lameParams.mu();
            }
        }
        
        for(int i = dimWorld; i < sizeC; i++)
            C[i][i] = lameParams.mu();

        // compute elastic tangent matrix 
        
        const auto CB  = Dumux::multiplyMatrices(C, B);
        //const auto elMat = Dumux::multiplyMatrices(Bt, CB);

        // matrix of normal components to obtain deriv of forces with respec to displ
        Dune::FieldMatrix<Scalar,dimWorld,sizeC> N(0.0);

        const normal = scvf.unitOuterNormal();

        int col = dimWorld;
        for(int i = 0; i < dimWorld; i++)
            N[i][i] = normal[i];

        normal[0][dimWorld] = normal[1];   
        normal[1][dimWorld] = normal[0];   

        if(dimWorld == 3)
        {
            normal[0][4] = normal[2];   
            normal[1][5] = normal[2];   
            normal[2][4] = normal[0]; 
            normal[2][5] = normal[1];     
        }

        const auto elMatrix = Dumux::multiplyMatrices(N, CB);
    
        // integration over face
        elMatrix *= scvf.area();
        return elMat;
    }
};


}

#endif