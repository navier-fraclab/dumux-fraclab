 // -*- mode: C++; tab-width: 4; indent-tabs-mode: nil; c-basic-offset: 4 -*-
 // vi: set et ts=4 sw=4 sts=4:
 /*****************************************************************************
  *   See the file COPYING for full copying permissions.                      *
  *                                                                           *
  *   This program is free software: you can redistribute it and/or modify    *
  *   it under the terms of the GNU General Public License as published by    *
  *   the Free Software Foundation, either version 3 of the License, or       *
  *   (at your option) any later version.                                     *
  *                                                                           *
  *   This program is distributed in the hope that it will be useful,         *
  *   but WITHOUT ANY WARRANTY; without even the implied warranty of          *
  *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the            *
  *   GNU General Public License for more details.                            *
  *                                                                           *
  *   You should have received a copy of the GNU General Public License       *
  *   along with this program.  If not, see <http://www.gnu.org/licenses/>.   *
  *****************************************************************************/

#ifndef DUMUX_GEOMECHANICS_INVARIANTSDEVIATORIC_HH
#define DUMUX_GEOMECHANICS_INVARIANTSDEVIATORIC_HH

#include <dumux/fraclab/geomechanics/invariants.hh>
#include <dune/common/math.hh>

namespace Dumux {

template<class Scalar, int dim>
class DeviatoricTensorInvariants : public InvariantsBase
<DeviatoricTensorInvariants<Scalar, dim>,Scalar, dim>
{
    using ThisType = DeviatoricTensorInvariants<Scalar, dim>;
    using ParentType = InvariantsBase<ThisType,Scalar,dim>;

    static constexpr int numStr = (dim == 2) ? 4:6;

    using StrTensor = typename ParentType::StressTensor;
    using StrVector = typename ParentType::StressVector;
    using InvariantVector = typename ParentType::InvariantVector;
    using DerivativeVector = typename ParentType::DerivativeVector;
    using SecondDerivative = typename ParentType::SecondDerivative;
    using Invariants = TensorInvariants<Scalar,numStr>;

public:
    
    static Scalar firstInvariant(StrTensor tensor)
    {
        Scalar I1 = trace(tensor);
        return I1;
    }

    static Scalar secondInvariant(StrTensor tensor)
    {
        Scalar I1 = firstInvariant(tensor);
        Scalar sx = tensor[0][0] - I1/3.0;
        Scalar sy = tensor[1][1] - I1/3.0;
        Scalar sz = tensor[2][2] - I1/3.0;
        Scalar Txy = tensor[0][1];
        Scalar Txz = tensor[0][2];
        Scalar Tyz = tensor [1][2];

        Scalar J2D = 0.5*(sx*sx + sy*sy +sz*sz) + Txy*Txy + Txz*Txz + Tyz*Tyz;
    
        return J2D;
    }

    static Scalar thirdInvariant(StrTensor tensor)
    {
        Scalar I1 = firstInvariant(tensor); 
        Scalar sx = tensor[0][0] - I1/3.0;
        Scalar sy = tensor[1][1] - I1/3.0;
        Scalar sz = tensor[2][2] - I1/3.0;

        StrTensor devTensor = tensor;

        devTensor[0][0] = sx;   
        devTensor[1][1] = sy;
        devTensor[2][2] = sz;
    
        Scalar J3D = devTensor.determinant(true);
    
        return J3D;
    } 

    const static DerivativeVector getInvarDerivative(int i, const StrTensor& tensor)
    {
        if(i == 0)
        {
            DerivativeVector dI1(0.0);
            for (int i = 0; i < 3; ++i) dI1[i] = 1.0; 

            return dI1;
        }

        if(i == 1)
        {
            DerivativeVector dI2(0.0);
            Scalar Sx = tensor[0][0];
            Scalar Sy = tensor[1][1];
            Scalar Sz = tensor[2][2];
        
            dI2[0] = (2.0/3.0) * Sx - (1.0/3.0) * (Sy + Sz);
            dI2[1] = (2.0/3.0) * Sy - (1.0/3.0) * (Sx + Sz);
            dI2[2] = (2.0/3.0) * Sz - (1.0/3.0) * (Sx + Sy);
            dI2[3] = 2.0*tensor[0][1];
            if(numStr == 6)
            {
                dI2[4] = 2.0*tensor[0][2];
                dI2[5] = 2.0*tensor[1][2];
            }   
            return dI2;
        }

        if(i == 2)
        {
            DerivativeVector dI3(0.0);
            Scalar I1 = firstInvariant(tensor);
            Scalar J2D = secondInvariant(tensor);
            Scalar sx = tensor[0][0] - I1/3.0;
            Scalar sy = tensor[1][1] - I1/3.0;
            Scalar sz = tensor[2][2] - I1/3.0;
            Scalar Txy = tensor[0][1];
            Scalar Txz = (numStr == 6) ? tensor[0][2] : 0.0;
            Scalar Tyz = (numStr == 6) ? tensor[1][2] : 0.0;

            dI3[0] = sy*sz - pow(Tyz,2.0) + J2D/3.0;
            dI3[1] = sx*sz - pow(Txz,2.0) + J2D/3.0;
            dI3[2] = sx*sy - pow(Txy,2.0) + J2D/3.0;
            dI3[3] = 2.0*(Tyz*Txz - sz*Txy);
            if(numStr == 6)
            {
                dI3[4] = 2.0*(Tyz*Txy - sy*Txz);
                dI3[5] = 2.0*(Txy*Txz - sx*Tyz);
            }
    
            return dI3;
        }
    }

    template<int id>
    const static DerivativeVector getInvariantDerivative(const StrTensor& tensor)
    {
        return getInvariantDerivative(std::integral_constant<int, id>{},tensor);
    }
    
    const static DerivativeVector getInvariantDerivative(std::integral_constant<int, 1>,const StrTensor& tensor)
    {
        DerivativeVector dI1(0.0);
        for (int i = 0; i < 3; ++i) dI1[i] = 1.0; 

        return dI1;
    }
    
    const static DerivativeVector getInvariantDerivative(std::integral_constant<int, 2>,
						 StrTensor tensor)
    {
        DerivativeVector dI2(0.0);
        Scalar Sx = tensor[0][0];
        Scalar Sy = tensor[1][1];
        Scalar Sz = tensor[2][2];
    
        dI2[0] = (2.0/3.0) * Sx - (1.0/3.0) * (Sy + Sz);
        dI2[1] = (2.0/3.0) * Sy - (1.0/3.0) * (Sx + Sz);
        dI2[2] = (2.0/3.0) * Sz - (1.0/3.0) * (Sx + Sy);
        dI2[3] = 2.0*tensor[0][1];
        if(numStr == 6)
        {
            dI2[4] = 2.0*tensor[0][2];
            dI2[5] = 2.0*tensor[1][2];
        }   
        return dI2;
    }

    const static DerivativeVector getInvariantDerivative(std::integral_constant<int, 3>, const StrTensor& tensor)
    {
        DerivativeVector dI3(0.0);
        Scalar I1 = firstInvariant(tensor);
        Scalar J2D = secondInvariant(tensor);
        Scalar sx = tensor[0][0] - I1/3.0;
        Scalar sy = tensor[1][1] - I1/3.0;
        Scalar sz = tensor[2][2] - I1/3.0;
        Scalar Txy = tensor[0][1];
        Scalar Txz = (numStr == 6) ? tensor[0][2] : 0.0;
        Scalar Tyz = (numStr == 6) ? tensor[1][2] : 0.0;

        dI3[0] = sy*sz - pow(Tyz,2.0) + J2D/3.0;
        dI3[1] = sx*sz - pow(Txz,2.0) + J2D/3.0;
        dI3[2] = sx*sy - pow(Txy,2.0) + J2D/3.0;
        dI3[3] = 2.0*(Tyz*Txz - sz*Txy);
        if(numStr == 6)
        {
            dI3[4] = 2.0*(Tyz*Txy - sy*Txz);
            dI3[5] = 2.0*(Txy*Txz - sx*Tyz);
        }
    
        return dI3;
    }

    template<int id>
    static SecondDerivative getInvariantSecondDerivative(const StrTensor& tensor)
    {
        return getInvariantSecondDerivative(std::integral_constant<int, id>{},tensor);
    }
    
    static SecondDerivative getInvariantSecondDerivative(std::integral_constant
						       <int, 1>,const StrTensor& tensor)
    {
        SecondDerivative ddI1(0.0);
        return ddI1;
    }
 
    static SecondDerivative getInvariantSecondDerivative(std::integral_constant
						       <int, 2>,const StrTensor& tensor)
    {
        SecondDerivative ddI2(0.0);
        return ddI2;
    }

    static SecondDerivative getInvariantSecondDerivative(std::integral_constant
						       <int, 3>,const StrTensor& tensor)
    {
        SecondDerivative ddI3(0.0);
        return ddI3;
    }
  
};
}
#endif
