 // -*- mode: C++; tab-width: 4; indent-tabs-mode: nil; c-basic-offset: 4 -*-
 // vi: set et ts=4 sw=4 sts=4:
 /*****************************************************************************
  *   See the file COPYING for full copying permissions.                      *
  *                                                                           *
  *   This program is free software: you can redistribute it and/or modify    *
  *   it under the terms of the GNU General Public License as published by    *
  *   the Free Software Foundation, either version 3 of the License, or       *
  *   (at your option) any later version.                                     *
  *                                                                           *
  *   This program is distributed in the hope that it will be useful,         *
  *   but WITHOUT ANY WARRANTY; without even the implied warranty of          *
  *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the            *
  *   GNU General Public License for more details.                            *
  *                                                                           *
  *   You should have received a copy of the GNU General Public License       *
  *   along with this program.  If not, see <http://www.gnu.org/licenses/>.   *
  *****************************************************************************/

#ifndef DUMUX_GEOMECHANICS_CAMBRIDGEINVARIANTS_HH
#define DUMUX_GEOMECHANICS_CAMBRIDGEINVARIANTS_HH

#include <dumux/fraclab/geomechanics/invariantsdeviatoric.hh>

namespace Dumux {

template<class Scalar, int dim>
class CambridgeInvariants : public InvariantsBase
<CambridgeInvariants<Scalar, dim>,Scalar, dim>
{
    using ThisType = CambridgeInvariants<Scalar, dim>;
    using ParentType = InvariantsBase<ThisType,Scalar,dim>;

    static constexpr int numStr = (dim == 2) ? 4:6;

    using StrTensor = typename ParentType::StressTensor;
    using StrVector = typename ParentType::StressVector;
    using InvariantVector = typename ParentType::InvariantVector;
    using DerivativeVector = typename ParentType::DerivativeVector;
    using SecondDerivative = typename ParentType::SecondDerivative;
    using DevInvariants = DeviatoricTensorInvariants<Scalar,numStr>;
  
public:  
    static Scalar meanStress(const StrTensor& tensor)
    {
        Scalar p = 0.0;
        for (int i = 0; i < 3; ++i) p += tensor[i][i]/3.0;
    
        return p;
    }

    static Scalar deviatoricStress(const StrTensor& tensor)
    {
        Scalar J2D = DevInvariants::secondInvariant(tensor);
        Scalar q = fmax(0.0,sqrt(J2D));
        return q;
    }

    static Scalar lodeAngle(const StrTensor& tensor, bool rad = true)
    {
        Scalar theta;
        Scalar senTheta;
        Scalar J2D = DevInvariants::secondInvariant(tensor);
        Scalar J3D = DevInvariants::thirdInvariant(tensor);
    
        senTheta = -sqrt(3.0)*J3D*1.5;
        senTheta /= pow(J2D, 1.5);
        theta = asin(senTheta)/3.0;
        if(!rad) theta *= 180.0/M_PI;

        if (abs(senTheta) >= 0.99999999) 
        {
            if (senTheta < 0.0) theta = rad ? -(30.0 / 180.0)*M_PI : -30.0;
            else  theta = rad ? (30.0 / 180.0)*M_PI : 30.0;
        }

        if (sqrt(J2D) < 1e-14) theta = rad ? (30.0 / 180.0)*M_PI : 30.0;
     
        return theta;
    }

    template<int id>
    static DerivativeVector getInvariantDerivative(const StrTensor& tensor)
    {
        return getInvariantDerivative(std::integral_constant<int, id>{},tensor);
    }
    
    static DerivativeVector getInvariantDerivative(std::integral_constant<int, 1>,const StrTensor& tensor)
    {
        DerivativeVector dp(0.0);
        for (int i = 0; i < 3; ++i) dp[i] = 1.0/3.0; 

        return dp;
    }
    
    static DerivativeVector getInvariantDerivative(std::integral_constant<int, 2>, const StrTensor& tensor)
    {
        DUNE_THROW(Dune::NotImplemented,
                        "Derivatives of invariant q not implemented.");
    }

    static DerivativeVector getInvariantDerivative(std::integral_constant<int, 3>,const StrTensor& tensor)
    {
        DUNE_THROW(Dune::NotImplemented,
                        "Derivatives of Lode angle  not implemented.");
    }

    static SecondDerivative getInvariantSecondDerivative(std::integral_constant<int, 1>,const StrTensor& tensor)
    {
        DUNE_THROW(Dune::NotImplemented,
                   "Second Derivatives of mean stress p not implemented.");
    }
 
    static SecondDerivative getInvariantSecondDerivative(std::integral_constant<int, 2>,const StrTensor& tensor)
    {
        DUNE_THROW(Dune::NotImplemented,
                   "Second Derivatives of invariant q not implemented.");
     }

    static SecondDerivative getInvariantSecondDerivative(std::integral_constant<int, 3>,const StrTensor& tensor)
    {
        DUNE_THROW(Dune::NotImplemented,
                   "Second Derivatives of Lode angle not implemented.");
    }
};
}

#endif
