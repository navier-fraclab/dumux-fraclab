// -*- mode: C++; tab-width: 4; indent-tabs-mode: nil; c-basic-offset: 4 -*-
// vi: set et ts=4 sw=4 sts=4:
/*****************************************************************************
 *   See the file COPYING for full copying permissions.                      *
 *                                                                           *
 *   This program is free software: you can redistribute it and/or modify    *
 *   it under the terms of the GNU General Public License as published by    *
 *   the Free Software Foundation, either version 3 of the License, or       *
 *   (at your option) any later version.                                     *
 *                                                                           *
 *   This program is distributed in the hope that it will be useful,         *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of          *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the            *
 *   GNU General Public License for more details.                            *
 *                                                                           *
 *   You should have received a copy of the GNU General Public License       *
 *   along with this program.  If not, see <http://www.gnu.org/licenses/>.   *
 *****************************************************************************/
/*!
 * \ingroup Geomechanics
 * \brief The stresshistory variables cache classes for elastoplastic models and other models that require the storage of stresses.
 *        Store stresses 
 */
#ifndef DUMUX_GEOMECHANICS_STRESSHISTORYVARIABLESCACHE_HH
#define DUMUX_GEOMECHANICS_STRESSHISTORYVARIABLESCACHE_HH

#include <dumux/geomechanics/stressvariablescache.hh>

namespace Dumux {

template< class Scalar, class GridGeometry, DiscretizationMethod dm = GridGeometry::discMethod >
class StressHistoryCache;

//! geomechanics is available only with the box method
template< class Scalar, class GridGeometry >
class StressHistoryCache<Scalar, GridGeometry, DiscretizationMethod::box>
: public StressVariablesCache<Scalar, GridGeometry, DiscretizationMethod::box>
{
    using FVElementGeometry = typename GridGeometry::LocalView;
    using SubControlVolumeFace = typename FVElementGeometry::SubControlVolumeFace;
    using GridView = typename GridGeometry::GridView;
    using Element = typename GridView::template Codim<0>::Entity;
    //using GridViewElement = GridViewGeometricEntitySet<GridView, 0>;
    using GridIndexType = typename IndexTraits<GridView>::GridIndex;
    static constexpr int dim = GridView::dimension;
    static constexpr int dimWorld = GridView::dimensionworld;
    static constexpr int numStre = (dim == 2 && dimWorld == 2) ? 4 : 2*dim;
    //static_assert(dim == dimWorld, "Hookes Law not implemented for network/surface grids");
    
public:
    using StressTensor = Dune::FieldMatrix<Scalar, 3, 3>;
    using StressVector = Dune::FieldVector<Scalar, numStre>;
    
    const StressVector stressVectorAtFace() const
    {       
        return stressAtFace;
    }

    const StressTensor stressTensorAtFace() const
    {
        return stressVectorToTensor(stressAtFace);
    }

    void updateStress(const StressTensor& currStress)
    {
        stressAtFace = stressTensorToVector(currStress);
    };

    void updateStress(const StressVector& currStress)
    {
        stressAtFace = currStress;
    };
    
    StressTensor stressVectorToTensor (const StressVector& stress) const
    {
        StressTensor tensor(0.0);
        for (int i = 0; i < 3; ++i) tensor[i][i] = stress[i];
        tensor[0][1] = stress[3];
        if(numStre == 6)
        {
            tensor[0][2] = stress[4];
            tensor[1][2] = stress[5];
        }
        for (int i = 1; i < 3; ++i) 
            for (int j = 0; j < i ; ++j)
                tensor[i][j] = tensor[j][i];

        return tensor;
    }

    template<class Problem, class ElemVolVars>
    void updateState(const Problem& problem,
                      const Element& element,
                      const FVElementGeometry& fvGeometry,
                      const ElemVolVars& elemVolVars,
                      const SubControlVolumeFace& scvf)
    {
        problem.updateState(element,fvGeometry,elemVolVars, *this, scvf);
    }
    
    StressVector stressTensorToVector (const StressTensor& stress) const
    {
        StressVector vector(0.0);
        for (int i = 0; i < 3; ++i) vector[i] = stress[i][i];
        vector[3] = stress[0][1];
        if(numStre == 6)
        {
            vector[4] = stress[0][2];
            vector[5] = stress[1][2];
        }
        
        return vector;
    }

    bool isAtApex() const
    {
        return apex;
    }

    void setAsApex()
    {
        apex = true;
    }

    void setAsYielded()
    {
        yielded = true;
    }

    void setAsElastic()
    {
        yielded = false;
    }

    bool isYielded() const
    {
        return yielded;
    }
    
protected:
    StressVector stressAtFace = StressVector(0.0);
    bool apex = false;
    bool yielded = false;

};

// specialization for the cell centered tpfa method
template< class Scalar, class GridGeometry >
class StressHistoryCache<Scalar, GridGeometry, DiscretizationMethod::cctpfa>
: public StressVariablesCache<Scalar, GridGeometry, DiscretizationMethod::cctpfa>
{};

// specialization for the cell centered mpfa method
template< class Scalar, class GridGeometry >
class StressHistoryCache<Scalar, GridGeometry, DiscretizationMethod::ccmpfa>
: public StressHistoryCache<Scalar, GridGeometry, DiscretizationMethod::cctpfa>
{};

} // end namespace Dumux

#endif
