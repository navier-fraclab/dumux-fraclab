 // -*- mode: C++; tab-width: 4; indent-tabs-mode: nil; c-basic-offset: 4 -*-
 // vi: set et ts=4 sw=4 sts=4:
 /*****************************************************************************
  *   See the file COPYING for full copying permissions.                      *
  *                                                                           *
  *   This program is free software: you can redistribute it and/or modify    *
  *   it under the terms of the GNU General Public License as published by    *
  *   the Free Software Foundation, either version 3 of the License, or       *
  *   (at your option) any later version.                                     *
  *                                                                           *
  *   This program is distributed in the hope that it will be useful,         *
  *   but WITHOUT ANY WARRANTY; without even the implied warranty of          *
  *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the            *
  *   GNU General Public License for more details.                            *
  *                                                                           *
  *   You should have received a copy of the GNU General Public License       *
  *   along with this program.  If not, see <http://www.gnu.org/licenses/>.   *
  *****************************************************************************/

#ifndef DUMUX_GEOMECHANICS_INVARIANTS_HH
#define DUMUX_GEOMECHANICS_INVARIANTS_HH

#include <dumux/fraclab/geomechanics/invariantsbase.hh>

namespace Dumux {
  
template<class Scalar, int dim>
class TensorInvariants : public InvariantsBase<TensorInvariants
					       <Scalar,dim>,Scalar, dim>
{
  using ThisType = TensorInvariants<Scalar,dim>;
  using ParentType = InvariantsBase<ThisType,Scalar,dim>;

  static constexpr int numStr = (dim == 2) ? 4:6;

  using StrTensor = typename ParentType::StressTensor;
  using StrVector = typename ParentType::StressVector;
  using InvariantVector = typename ParentType::InvariantVector;
  using DerivativeVector = typename ParentType::DerivativeVector;
  using SecondDerivative = typename ParentType::SecondDerivative;

public:  
  static Scalar firstInvariant(const StrTensor& tensor)
  {
    Scalar I1 = trace(tensor);

    return I1;
  }
  
  static Scalar secondInvariant(const StrTensor& tensor)
  {
    Scalar I2 = 0.0;
    for (int i = 0; i < 3; ++i) {
      for (int j = i+1; j < 3; ++j) {
          I2 += tensor[i][i]*tensor[j][j];
          I2 -= tensor[i][j]*tensor[j][i];
      }  
    }
    
    return I2;
  }

  static Scalar thirdInvariant(const StrTensor& tensor)
  {
    Scalar I3 = tensor.determinant(true); 
    
    return I3;
  }

  template<int id>
  static DerivativeVector getInvariantDerivative(const StrTensor& tensor)
  {
    return getInvariantDerivative(std::integral_constant<int, id>{},tensor);
  }
    
  static DerivativeVector getInvariantDerivative(std::integral_constant<int, 1>,
						 StrTensor tensor)
  {
    //derivatives of I1 with respect to strain/stresses
    DerivativeVector dI1(0.0);
    for (int i = 0; i < 3; ++i) dI1[i] = 1.0; 

    return dI1;
  }

  static DerivativeVector getInvariantDerivative(std::integral_constant<int, 2>,
						 StrTensor tensor)
  {
    //derivatives of I2 with respect to strain/stresses
    DerivativeVector dI2(0.0);
    dI2[0] = tensor[1][1] + tensor[2][2];
    dI2[1] = tensor[0][0] + tensor[2][2];
    dI2[2] = tensor[0][0] + tensor[1][1];
    dI2[3] = -2.0*tensor[0][1];
    if(numStr == 6)
    {
      dI2[4] = -2.0*tensor[0][2];
      dI2[5] = -2.0*tensor[1][2];
    }
    
    return dI2;
  }

  static DerivativeVector getInvariantDerivative(std::integral_constant<int, 3>,
						 StrTensor tensor)
  {
    //derivatives of I2 with respect to strain/stresses
    DerivativeVector dI3(0.0);
    Scalar Sx = tensor[0][0];
    Scalar Sy = tensor[1][1];
    Scalar Sz = tensor[2][2];
    Scalar Txy = tensor[0][1];
    Scalar Txz = (numStr == 6) ? tensor[0][2] : 0.0;
    Scalar Tyz = (numStr == 6) ? tensor[1][2] : 0.0;

    dI3[0] = Sy * Sz - Tyz * Tyz;
    dI3[1] = Sx * Sz - Txz * Txz;
    dI3[2] = Sx * Sy - Txy * Txy;
    dI3[3] = 2.0 * (Txz * Tyz - Txy * Sz);
    if(numStr == 6)
    {
      dI3[4] = 2.0 * (Txy * Tyz - Txz * Sy);
      dI3[5] = 2.0 * (Txy * Txz - Tyz * Sx);
    }
    
    
    return dI3;
  }

  static SecondDerivative getInvariantSecondDerivative(std::integral_constant
						       <int, 1>,StrTensor tensor)
  {
    DUNE_THROW(Dune::NotImplemented,
               "Second Derivatives of first invariant I1 not implemented.");
  }
 
  static SecondDerivative getInvariantSecondDerivative(std::integral_constant
						       <int, 2>,StrTensor tensor)
  {
    DUNE_THROW(Dune::NotImplemented,
               "Second Derivatives of second invariant I2 not implemented.");
  }

    static SecondDerivative getInvariantSecondDerivative(std::integral_constant
						       <int, 3>,StrTensor tensor)
  {
    DUNE_THROW(Dune::NotImplemented,
               "Second Derivatives of third invariant I3 not implemented.");
  }
  
};
}
#endif
