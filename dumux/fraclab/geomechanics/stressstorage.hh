// -*- mode: C++; tab-width: 4; indent-tabs-mode: nil; c-basic-offset: 4 -*-
// vi: set et ts=4 sw=4 sts=4:
/*****************************************************************************
 *   See the file COPYING for full copying permissions.                      *
 *                                                                           *
 *   This program is free software: you can redistribute it and/or modify    *
 *   it under the terms of the GNU General Public License as published by    *
 *   the Free Software Foundation, either version 3 of the License, or       *
 *   (at your option) any later version.                                     *
 *                                                                           *
 *   This program is distributed in the hope that it will be useful,         *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of          *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the            *
 *   GNU General Public License for more details.                            *
 *                                                                           *
 *   You should have received a copy of the GNU General Public License       *
 *   along with this program.  If not, see <http://www.gnu.org/licenses/>.   *
 *****************************************************************************/
/*!
 * \file
 * \ingroup SpatialParameters
 * \brief The class for managing the storage of stresses at nodes and scvs in vectors
 */
#ifndef DUMUX_FRACLAB_STRESSSTORAGE_HH
#define DUMUX_FRACLAB_STRESSSTORAGE_HH

#include <dune/common/densematrix.hh>
#include "invariantscambridge.hh"
#include "stresshistorycache.hh"

namespace Dumux {
//works for box model only: add assert
template<class TypeTag, class BulkStressType>
class StressStorage
{
    using Scalar = GetPropType<TypeTag, Properties::Scalar>;
    using Problem = GetPropType<TypeTag, Properties::Problem>;
    using GridGeometry = GetPropType<TypeTag, Properties::GridGeometry>;
    using FVElementGeometry = typename GridGeometry::LocalView;
    using SubControlVolume = typename GridGeometry::SubControlVolume;
    using SubControlVolumeFace = typename GridGeometry::SubControlVolumeFace;
    using StressType = GetPropType<TypeTag, Properties::StressType>;
    using GridView = typename GetPropType<TypeTag, Properties::GridGeometry>::GridView;
    using GridIndexType = typename GridView::IndexSet::IndexType;
    using Element = typename GridView::template Codim<0>::Entity;
    using GlobalPosition = typename Element::Geometry::GlobalCoordinate;
    using SolutionVector = GetPropType<TypeTag, Properties::SolutionVector>;
    using GridVariables = GetPropType<TypeTag, Properties::GridVariables>;
    //using FluxVarsCache = typename GridVariables::GridFluxVariablesCache::FluxVariablesCache;

    static constexpr int dim = GridView::dimension;
    static constexpr int dimWorld = GridView::dimensionworld;
    static_assert(dim == dimWorld, "Stress storage not implemented for network/surface grids");

    using FluxVarsCache = Dumux::StressHistoryCache<Scalar,GridGeometry>;
    using StressTensor = Dune::FieldMatrix<Scalar,3,3>;
    using ForceVector = Dune::FieldVector<Scalar,3>;
    using InvariantVector = Dune::FieldVector<Scalar,3>;
    using CambridgeInvar = Dumux::CambridgeInvariants<Scalar,dim>;
    using ScvStressVector = std::vector<std::vector<StressTensor>>;
    using NodalStressArray = std::array< std::vector<ForceVector>, 3 >;
    using ScalarVector = std::vector<Scalar>;
    using PrincipalStressVector = std::array< std::vector<Scalar>, 3 >;

public:
    //! The constructor
    StressStorage(std::shared_ptr<const GridGeometry> gridGeometry, 
                                  bool effStressAnalysis = false) // is it effective stress or purely mechanical problem ?
    : gridGeometry_(gridGeometry)
    {   
        effectiveStressLaw_ = effStressAnalysis;
        const auto& gridView = gridGeometry_->gridView();
        const auto numNodes = gridGeometry_->numDofs();
        const auto numCells = gridGeometry_->gridView().size(0);

        nodalVolumes_.resize(numNodes,0.0);
        scvStresses_.resize(numCells);

        for(const auto& element : elements(gridView))
        {
            auto fvGeometry = localView(*gridGeometry_);
            fvGeometry.bind(element);
            const auto eIdx = gridGeometry_->elementMapper().index(element);
            scvStresses_[eIdx].resize(fvGeometry.numScv(), 0.0);

            for (const auto& scv : scvs(fvGeometry))
            {
                const auto dofIdx = scv.dofIndex(); //??
                const auto scvVolume = scv.volume();
                nodalVolumes_[dofIdx] += scvVolume;
            }
        }   

        scvEffectiveStresses_ = scvStresses_;

        nodalEffectiveMeanStresses_.resize(numNodes,0.0);
        nodalVonMises_.resize(numNodes,0.0);

        std::for_each(nodalStresses_.begin(), nodalStresses_.end(), [numNodes] (auto& sigma) { sigma.resize(numNodes);});
        std::for_each(nodalPrincipalStress_.begin(), nodalPrincipalStress_.end(), [numNodes] (auto& sigma) { sigma.resize(numNodes);});
        std::for_each(nodalEffectiveStresses_.begin(), nodalEffectiveStresses_.end(), [numNodes] (auto& sigma) { sigma.resize(numNodes);});
        std::for_each(nodalStresses_.begin(), nodalStresses_.end(), [numNodes] (auto& sigma) { sigma.resize(numNodes);});
        std::for_each(prevNodalEffectiveStresses_.begin(), prevNodalEffectiveStresses_.end(), [numNodes] (auto& sigma) { sigma.resize(numNodes);});
        
    }

    void updateScvStresses(const GridVariables& gridVariables,
                           const SolutionVector& x,
                           const Problem& problem, 
                           bool elastoplastic = false)
    {

        auto gridView = gridGeometry_->gridView();
        for (const auto& element : elements(gridView))
        {   
            const auto eIdx = gridGeometry_->elementMapper().index(element);
            auto fvGeometry = localView(*gridGeometry_);
            auto&& elemVolVars = localView(gridVariables.curGridVolVars());
            fvGeometry.bind(element);
            elemVolVars.bind(element, fvGeometry, x);
            
            for (const auto& scv : scvs(fvGeometry))
            {
                FluxVarsCache fluxVarsCache;
                fluxVarsCache.update(problem, element, fvGeometry, elemVolVars, scv.center());

                auto& scvStress = scvStresses_[eIdx][scv.indexInElement()];
                const auto currSigma = scvStress;
                fluxVarsCache.updateStress(currSigma);   
                auto dstress = BulkStressType::stressTensor(problem, element,fvGeometry,elemVolVars, fluxVarsCache);
                
                for(int dirI = 0; dirI < dim; dirI++)
                    for(int dirJ = 0; dirJ < dim; dirJ++)
                        scvStress[dirI][dirJ] += dstress[dirI][dirJ];

                auto& scvEffStress = scvEffectiveStresses_[eIdx][scv.indexInElement()];
                if(!effectiveStressLaw_) scvEffStress = scvStress;
                // else call eff stress method

                // if 2d, use plane strain assumption to compute stress in z-direction
                if(dim == 2){
                    const auto& lameParams = problem.spatialParams().lameParams(element,fvGeometry,
                                                                                elemVolVars,fluxVarsCache);
                    auto poisson = 0.5*lameParams.lambda()/(lameParams.mu()+lameParams.lambda());
                    scvEffStress[2][2] = poisson*(scvEffStress[0][0] + scvEffStress[1][1]);

                    if(!effectiveStressLaw_) scvStress[2][2] = scvEffStress[2][2];
                    // else compute pore pressure to add 
                }
            }
        }            
    }

    void updateScvEffectiveStresses(const GridVariables& gridVariables,
                                    const SolutionVector& x,
                                    const Problem& problem)
    {
        if(!effectiveStressLaw_)
        {
            updateScvStresses(gridVariables,x,problem);
            return;
        }

        auto gridView = gridGeometry_->gridView();
        for (const auto& element : elements(gridView))
        {   
            const auto eIdx = gridGeometry_->elementMapper().index(element);
            auto fvGeometry = localView(*gridGeometry_);
            auto&& elemVolVars = localView(gridVariables.curGridVolVars());
            fvGeometry.bind(element);
            elemVolVars.bind(element, fvGeometry, x);
            
            for (const auto& scv : scvs(fvGeometry))
            {
                FluxVarsCache fluxVarsCache;
                fluxVarsCache.update(problem, element, fvGeometry, elemVolVars, scv.center());

                auto& scvEffStress = scvEffectiveStresses_[eIdx][scv.indexInElement()];
                const auto currSigma = scvEffStress;
                fluxVarsCache.updateStress(currSigma);   
                auto dstress = BulkStressType::effectiveStressTensor(problem, element,fvGeometry,elemVolVars, fluxVarsCache);
                
                for(int dirI = 0; dirI < dim; dirI++)
                    for(int dirJ = 0; dirJ < dim; dirJ++)
                        scvEffStress[dirI][dirJ] += dstress[dirI][dirJ];

                // if 2d, use plane strain assumption to compute stress in z-direction
                if(dim == 2){
                    const auto& lameParams = problem.spatialParams().lameParams(element,fvGeometry,elemVolVars,fluxVarsCache);
                    auto poisson = 0.5*lameParams.lambda()/(lameParams.mu()+lameParams.lambda());
                    scvEffStress[2][2] = poisson*(scvEffStress[0][0] + scvEffStress[1][1]);
                }
            }
        }            
    }

    // compute nodal stresses as volume average of surrounding scvs of the node

    void updateNodalStresses()
    {
        for(auto& dirNodalStresses : nodalStresses_)
            for(auto& stress : dirNodalStresses)
                stress = 0.0;
        nodalEffectiveStresses_ = nodalStresses_;
        const auto& gridView = gridGeometry_->gridView();

        for (const auto& element : elements(gridView))
        {
            const auto eIdx = gridGeometry_->elementMapper().index(element);
            auto fvGeometry = localView(*gridGeometry_);
            fvGeometry.bind(element);
        
            for (const auto& scv : scvs(fvGeometry))
            {
                const auto dofIdx = scv.dofIndex(); 
                const auto scvVolume = scv.volume();

                auto scvStress = scvStresses_[eIdx][scv.indexInElement()];
                auto scvEffStress = scvEffectiveStresses_[eIdx][scv.indexInElement()];
                
                scvStress *= scvVolume;
                scvEffStress *= scvVolume;

                for(int dir = 0; dir < 3; dir++){
                    nodalStresses_[dir][dofIdx] += scvStress[dir];
                    nodalEffectiveStresses_[dir][dofIdx] += scvEffStress[dir];
                }
            }
        }

        const auto numNodes = gridGeometry_->numDofs();
        for(int in = 0; in < numNodes; in++){
            for(int dir = 0; dir < 3; dir++){
                nodalStresses_[dir][in] /= nodalVolumes_[in];
                nodalEffectiveStresses_[dir][in] /= nodalVolumes_[in];
            }
        }
    }
    
    void updateStresses(const GridVariables& gridVariables,
                        const SolutionVector& x,
                        const Problem& problem, 
                        bool elastoplastic = false)
    {
        updateScvStresses(gridVariables,x,problem,elastoplastic);
        updateNodalStresses();
    }

    void updateNodalStressInvariants()
    {
        const auto numNodes = gridGeometry_->numDofs();
        std::vector<Scalar> nodalPrincipal(numNodes);

        nodalEffectiveMeanStresses_.resize(numNodes,0.0);
        nodalVonMises_.resize(numNodes,0.0);
        
        for(auto& princVector : nodalPrincipalStress_)
            for(auto& prinSigma : princVector)
                prinSigma = 0.0;

        const auto& gridView = gridGeometry_->gridView();

        for (const auto& element : elements(gridView))
        {
            const auto eIdx = gridGeometry_->elementMapper().index(element);
            auto fvGeometry = localView(*gridGeometry_);
            fvGeometry.bind(element);
        
            for (const auto& scv : scvs(fvGeometry))
            {
                Dune::FieldMatrix<Scalar,3,3> sigma(0.0);
                const auto dofIdx = scv.dofIndex(); 
                const auto scvVolume = scv.volume();

                auto scvStress = scvEffectiveStresses_[eIdx][scv.indexInElement()];
                auto p = CambridgeInvar::meanStress(scvStress);
                auto q = CambridgeInvar::deviatoricStress(scvStress);
                auto scvPrincipalStress = CambridgeInvar::principalStresses(scvStress);
                auto vonMises = q*q;
                
                p *= scvVolume;
                vonMises *= scvVolume;
                scvPrincipalStress *= scvVolume;

                nodalEffectiveMeanStresses_[dofIdx]  += p;
                nodalVonMises_[dofIdx]  += vonMises;
                for(int i = 0; i <3;i++)
                    nodalPrincipalStress_[i][dofIdx] += scvPrincipalStress[i];              
            }
        }

        for(int in = 0; in < numNodes; in++){
            const auto vol = nodalVolumes_[in];
            nodalEffectiveMeanStresses_[in] /= vol;
            nodalVonMises_[in] /= vol;
            for(int i = 0; i <3;i++)
                nodalPrincipalStress_[i][in] /= vol;
        }
            
    }

    void resetStressStorage()
    {
        std::for_each(nodalStresses_.begin(), nodalStresses_.end(), [] (auto& sigma) { sigma.clear();});
        std::for_each(nodalEffectiveStresses_.begin(), nodalEffectiveStresses_.end(), [] (auto& sigma) { sigma.clear();});
        scvStresses_.clear();
        nodalEffectiveMeanStresses_.clear();
        nodalVonMises_.clear();
        scvEffectiveStresses_.clear();
        nodalPrincipalStress_.clear();
    }

    const ScvStressVector& getScvStressStorage()
    {return scvStresses_;}

    const ScvStressVector& getScvEffectiveStressStorage()
    {return scvEffectiveStresses_;}

    const NodalStressArray& getNodalStressStorage()
    {return nodalStresses_;}

    const NodalStressArray& getNodalEffectiveStressStorage()
    {return nodalEffectiveStresses_;}

    const PrincipalStressVector& getNodalPrincipalStressStorage()
    {return nodalPrincipalStress_;}

    const ScalarVector& getNodalEffectiveMeanStressStorage()
    {return nodalEffectiveMeanStresses_;}

    const ScalarVector& getNodalVonMisesStressStorage()
    {return nodalVonMises_;}

    const StressTensor& scvEffectiveStress(GridIndexType eIdx, GridIndexType scvIdx)
    {return scvEffectiveStresses_[eIdx][scvIdx];}

    const StressTensor nodalEffectiveStress(GridIndexType vIdx)
    {
        StressTensor sigma(0.0);
        for(int i = 0;i <3;i++)
            sigma[i] = nodalEffectiveStresses_[i][vIdx];
        return sigma; 
    }

    const StressTensor prevNodalEffectiveStress(GridIndexType vIdx)
    {
        StressTensor sigma(0.0);
        for(int i = 0;i <3;i++)
            sigma[i] = prevNodalEffectiveStresses_[i][vIdx];
        return sigma; 
    }

    const ScvStressVector scvEffectiveStresses()
    {return scvEffectiveStresses_;}

    void setScvEffectiveStresses(const ScvStressVector& scvEffStr)
    {scvEffectiveStresses_ = scvEffStr;}

    void advanceNodalEffectiveStresses()
    {prevNodalEffectiveStresses_ = nodalEffectiveStresses_;}
private:

    ScvStressVector scvStresses_;
    ScvStressVector scvEffectiveStresses_;
    NodalStressArray nodalStresses_;
    NodalStressArray nodalEffectiveStresses_;
    NodalStressArray prevNodalEffectiveStresses_;
    ScalarVector nodalEffectiveMeanStresses_;
    ScalarVector nodalVonMises_;
    PrincipalStressVector nodalPrincipalStress_;
    ScalarVector nodalVolumes_;
    bool effectiveStressLaw_;
    std::shared_ptr<const GridGeometry> gridGeometry_;
};

} //end namespace Dumux
#endif