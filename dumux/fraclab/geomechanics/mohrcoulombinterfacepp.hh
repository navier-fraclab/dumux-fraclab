// -*- mode: C++; tab-width: 4; indent-tabs-mode: nil; c-basic-offset: 4 -*-
// vi: set et ts=4 sw=4 sts=4:
/*****************************************************************************
 *   See the file COPYING for full copying permissions.                      *
 *                                                                           *
 *   This program is free software: you can redistribute it and/or modify    *
 *   it under the terms of the GNU General Public License as published by    *
 *   the Free Software Foundation, either version 3 of the License, or       *
 *   (at your option) any later version.                                     *
 *                                                                           *
 *   This program is distributed in the hope that it will be useful,         *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of          *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the            *
 *   GNU General Public License for more details.                            *
 *                                                                           *
 *   You should have received a copy of the GNU General Public License       *
 *   along with this program.  If not, see <http://www.gnu.org/licenses/>.   *
 *****************************************************************************/

// class to define the perfectly plastic MohrCoulomb model for interfaces/lower-dimensional elements
/*!
 * \file
 * \ingroup Geomechanics
 * \brief \copydoc Dumux::MohrCoulombInterface
 */
#ifndef DUMUX_GEOMECHANICS_INTERFACE_PERFECTPLASTIC_MOHRCOULOMB_HH
#define DUMUX_GEOMECHANICS_INTERFACE_PERFECTPLASTIC_MOHRCOULOMB_HH

#include <dumux/fraclab/geomechanics/mohrcoulomb.hh>

namespace Dumux {

/*!
 * \ingroup Geomechanics 
 */
template<class Scalar, int dim>
class PerfectlyPlasticMohrCoulombInterface
{
public:
    using StressTensor = Dune::FieldMatrix<Scalar, dim, dim>;
    using StressVector = Dune::FieldVector<Scalar, dim>;
    using FunctionDerivative = Dune::FieldVector<Scalar,dim>;
    static constexpr int numStre = (dim == 2) ? 2:3;
    static constexpr int numStra = (dim == 2) ? 2:3; 
    using YieldNormal =  Dune::FieldVector<Scalar,dim>;
    using FlowNormal = Dune::FieldVector<Scalar,dim>;
    //! Default constructor
    PerfectlyPlasticMohrCoulombInterface() = default;

    //! Constructor taking parameters directly
    PerfectlyPlasticMohrCoulombInterface(Scalar cohesion, Scalar frictionAngle, Scalar dilationAngle = 0.0)
    : cohesion_(cohesion)
    {
        frictionAngle_ = frictionAngle*(M_PI/180.0);
    }

    //! Return cohesion
    Scalar cohesion() const
    { return cohesion_; }

    //! Return frictionAngle
    Scalar frictionAngle() const
    { return frictionAngle_; }

    //! Return dilationAngle
    Scalar dilationAngle() const
    { return dilationAngle_;}

    void setCohesion(Scalar cohesion)
    { cohesion_ = cohesion; }

    void setFrictionAngle(Scalar frictionAngle)
    { frictionAngle_ = frictionAngle*(M_PI/180.0);}

    void setDilationAngle(Scalar dilationAngle)
    { dilationAngle_ = dilationAngle*(M_PI/180.0);}

    Scalar yieldFunction(StressVector sigma) const
    {
        Scalar normalStress = sigma[0];

        Scalar shearStress(0.0);
        for(int i = 1; i <dim;i++)
            shearStress += std::pow(sigma[i],2.0);
        shearStress = std::pow(shearStress,0.5);

        Scalar yf = abs(shearStress) + normalStress*tan(frictionAngle_) - cohesion_;

        return yf;
    }

    Scalar flowFunction(StressVector sigma) const
    {

        Scalar normalStress = sigma[0];
        Scalar shearStress(0.0);
        for(int i = 1; i <dim;i++)
            shearStress += std::pow(sigma[i],2.0);
        shearStress = std::pow(shearStress,0.5);

        Scalar gf = abs(shearStress) + normalStress*tan(dilationAngle_) + cohesion_;
        return gf;
    }

    YieldNormal yieldFunctionNormal(StressVector sigma) const
    {
        Scalar shearStress(0.0);
        for(int i = 1; i <dim;i++)
            shearStress += std::pow(sigma[i],2.0);
        shearStress = std::pow(shearStress,0.5);
        
        YieldNormal af(0.0);
        af[0] = tan(frictionAngle_);
        for(int i = 1; i <dim;i++)
            af[i] = sigma[i]/shearStress;

        Scalar stressApex = cohesion_/tan(frictionAngle_);
        if(abs(af[0]-stressApex) < 1e-15){
            for(int i = 1; i <dim;i++)
                af[i] = 0.0;
        }
        return af;
    }

    YieldNormal flowFunctionNormal(StressVector sigma) const
    {
        Scalar shearStress(0.0);
        for(int i = 1; i <dim;i++)
            shearStress += std::pow(sigma[i],2.0);
        shearStress = std::pow(shearStress,0.5);

        YieldNormal af(0.0);
        af[0] = tan(dilationAngle_);
        for(int i = 1; i <dim;i++)
            af[i] = sigma[i]/shearStress;

        Scalar stressApex = cohesion_/tan(frictionAngle_);
        if(abs(af[0]-stressApex) < 1e-15){
            for(int i = 1; i <dim;i++)
                af[i] = 0.0;
        }
        
        return af;
    }

    const Scalar hardeningParameter() const
    {return 0.0;}

protected:
    Scalar cohesion_;
    Scalar frictionAngle_;
    Scalar dilationAngle_;
};
} // end namespace Dumux
#endif
