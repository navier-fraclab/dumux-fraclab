// -*- mode: C++; tab-width: 4; indent-tabs-mode: nil; c-basic-offset: 4 -*-
// vi: set et ts=4 sw=4 sts=4:
/*****************************************************************************
 *   See the file COPYING for full copying permissions.                      *
 *                                                                           *
 *   This program is free software: you can redistribute it and/or modify    *
 *   it under the terms of the GNU General Public License as published by    *
 *   the Free Software Foundation, either version 3 of the License, or       *
 *   (at your option) any later version.                                     *
 *                                                                           *
 *   This program is distributed in the hope that it will be useful,         *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of          *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the            *
 *   GNU General Public License for more details.                            *
 *                                                                           *
 *   You should have received a copy of the GNU General Public License       *
 *   along with this program.  If not, see <http://www.gnu.org/licenses/>.   *
 *****************************************************************************/
/*!
 * \file
 * \ingroup Geomechanics
 * \brief \copydoc Dumux::InterfaceStiffnessParams
 */
#ifndef DUMUX_GEOMECHANICS_INT_STIFF_PARAMS_HH
#define DUMUX_GEOMECHANICS_INT_STIFF_PARAMS_HH

namespace Dumux {

/*!
 * \ingroup Geomechanics
 * \brief Structure encapsulating the stiffness parameters of discontinuities represented by interface/lower-dimensional elements
 */
template<class Scalar>
struct InterfaceStiffnessParams
{
    //! Default constructor
    InterfaceStiffnessParams() = default;

    //! Constructor taking stiffnesses
    InterfaceStiffnessParams(Scalar Kn, Scalar Kt, Scalar Knt = 0)
      : Kn_(Kn) , Kt_(Kt), Knt_(Knt)
    {}

    Scalar normalStiffness() const
    { return Kn_; }

    Scalar tangentStiffness() const
    { return Kt_; }

    Scalar dilatantStiffness() const
    {return Knt_;}

    void setNormalStiffness(Scalar Kn)
    { Kn_ = Kn; }

    void setTangentStiffness(Scalar Kt)
    {Kt_ = Kt;}

    void setDilatantStiffness(Scalar Knt)
    {Knt_ = Knt;}
private:
    Scalar Kn_;
    Scalar Kt_;
    Scalar Knt_ = 0;
};
} // end namespace Dumux
#endif
