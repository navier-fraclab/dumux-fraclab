 // -*- mode: C++; tab-width: 4; indent-tabs-mode: nil; c-basic-offset: 4 -*-
 // vi: set et ts=4 sw=4 sts=4:
 /*****************************************************************************
  *   See the file COPYING for full copying permissions.                      *
  *                                                                           *
  *   This program is free software: you can redistribute it and/or modify    *
  *   it under the terms of the GNU General Public License as published by    *
  *   the Free Software Foundation, either version 3 of the License, or       *
  *   (at your option) any later version.                                     *
  *                                                                           *
  *   This program is distributed in the hope that it will be useful,         *
  *   but WITHOUT ANY WARRANTY; without even the implied warranty of          *
  *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the            *
  *   GNU General Public License for more details.                            *
  *                                                                           *
  *   You should have received a copy of the GNU General Public License       *
  *   along with this program.  If not, see <http://www.gnu.org/licenses/>.   *
  *****************************************************************************/

#ifndef DUMUX_GEOMECHANICS_INVARIANTSBASE_HH
#define DUMUX_GEOMECHANICS_INVARIANTSBASE_HH

#include <dune/common/densematrix.hh>

namespace Dumux {
// this class is the base class to compute different types of stress tensor invariants

template<class Impl, class Scalar, int dim>
class InvariantsBase
{
protected:
    
    static constexpr int numStr = (dim == 2) ? 4:6;
    using StressTensor = Dune::FieldMatrix<Scalar, 3, 3>;
    using StressVector = Dune::FieldVector<Scalar, numStr>;
    using InvariantVector = Dune::FieldVector<Scalar, 3>;
    using DerivativeVector =  Dune::FieldVector<Scalar, numStr>;
    using DerivativeMatrix = Dune::FieldMatrix<Scalar, 3, numStr>;
    using SecondDerivative = Dune::FieldMatrix<Scalar, numStr, numStr>;

public:
    
    const static DerivativeMatrix getDerivatives(const StressTensor& tensor)
    {
        DerivativeMatrix dInv(0.0);

        //for(int i = 0; i< 3; i++)
          //  dInv[i] = Impl::getInvarDerivative(i, tensor);

        dInv[0] = Impl::getInvariantDerivative(std::integral_constant<int, 1>{},tensor);
        dInv[1] = Impl::getInvariantDerivative(std::integral_constant<int, 2>{},tensor);
        dInv[2] = Impl::getInvariantDerivative(std::integral_constant<int, 3>{},tensor);

        return dInv;
   }

   // obtain minor, intermediary and major principal stresses
   static const InvariantVector principalStresses(const StressTensor& sigma)
   {
        InvariantVector principalSigma;
        Dune::FMatrixHelp::eigenValues(sigma,principalSigma);
        return principalSigma;
   }
  
};
} // end namespace Dumux
#endif
