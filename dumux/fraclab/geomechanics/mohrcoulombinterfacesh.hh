// -*- mode: C++; tab-width: 4; indent-tabs-mode: nil; c-basic-offset: 4 -*-
// vi: set et ts=4 sw=4 sts=4:
/*****************************************************************************
 *   See the file COPYING for full copying permissions.                      *
 *                                                                           *
 *   This program is free software: you can redistribute it and/or modify    *
 *   it under the terms of the GNU General Public License as published by    *
 *   the Free Software Foundation, either version 3 of the License, or       *
 *   (at your option) any later version.                                     *
 *                                                                           *
 *   This program is distributed in the hope that it will be useful,         *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of          *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the            *
 *   GNU General Public License for more details.                            *
 *                                                                           *
 *   You should have received a copy of the GNU General Public License       *
 *   along with this program.  If not, see <http://www.gnu.org/licenses/>.   *
 *****************************************************************************/

// class to define the perfectly plastic MohrCoulomb model for interfaces/lower-dimensional elements
/*!
 * \file
 * \ingroup Geomechanics
 * \brief \copydoc Dumux::MohrCoulombInterface
 */
#ifndef DUMUX_GEOMECHANICS_INTERFACE_STRAINHARDENING_MOHRCOULOMB_HH
#define DUMUX_GEOMECHANICS_INTERFACE_STRAINHARDENING_MOHRCOULOMB_HH

#include "mohrcoulombinterfacepp.hh"

namespace Dumux {

/*!
 * \ingroup Geomechanics 
 */
template<class Scalar, int dim>
class StrainHardeningMohrCoulombInterface
: public PerfectlyPlasticMohrCoulombInterface<Scalar,dim>
{
    using StressTensor = Dune::FieldMatrix<Scalar, dim, dim>;
    using StressVector = Dune::FieldVector<Scalar, dim>;
    using FunctionDerivative = Dune::FieldVector<Scalar,dim>;
    static constexpr int numStre = (dim == 2) ? 2:3;
    static constexpr int numStra = (dim == 2) ? 2:3; 
    using YieldNormal =  Dune::FieldVector<Scalar,dim>;
    using FlowNormal = Dune::FieldVector<Scalar,dim>;
    using ScalarVector = std::vector<Scalar>;
 public:   
    //! Default constructor
    StrainHardeningMohrCoulombInterface()
    {
    };

    StrainHardeningMohrCoulombInterface(ScalarVector hPoints, ScalarVector fiPoints, ScalarVector coPoints, ScalarVector psiPoints)
    : StrainHardeningMohrCoulombInterface()
    {
        setHardeningFunction(hPoints,fiPoints,coPoints,psiPoints);
    };

    void setHardeningFunction(ScalarVector hPoints, ScalarVector fiPoints, ScalarVector coPoints, ScalarVector psiPoints)
    {
        npoints = hPoints.size();
        hardeningVariablePoints_ = hPoints;
        frictionAnglePoints_ = fiPoints;
        cohesionPoints_ = coPoints;
        dilationAnglePoints_ = psiPoints;

        for(auto& fi : frictionAnglePoints_)
            fi *= (M_PI/180.0);
        for(auto& psi : dilationAnglePoints_)
            psi *= (M_PI/180.0);
    }

    const Scalar hardeningParameter() const
    {return hardParam_;}

    Scalar derivHardeningLambda()
    {}
    
    Scalar derivHardeningVarLambda(Scalar hardVar)
    { return 1.0;}

    void updateMohrCoulomParams(Scalar hardVar)
    {
        int up_pos = npoints-1;
        for(int pos = 0; pos < hardeningVariablePoints_.size(); pos++){
            if (hardeningVariablePoints_[pos] > hardVar){
                up_pos = pos;
                break;
            }
        }
        if(up_pos == 0 or hardVar > hardeningVariablePoints_[npoints-1])
        {
            this->frictionAngle_ = frictionAnglePoints_[up_pos];
            this->dilationAngle_ = dilationAnglePoints_[up_pos];
            this->cohesion_ = cohesionPoints_[up_pos];
            hardParam_ = 0.0;

        } else {

            Scalar dHVar = hardeningVariablePoints_[up_pos]-hardeningVariablePoints_[up_pos-1];
            Scalar dFi = (frictionAnglePoints_[up_pos]-frictionAnglePoints_[up_pos-1]);
            Scalar dPsi = (dilationAnglePoints_[up_pos]-dilationAnglePoints_[up_pos-1]);
            Scalar dCo = cohesionPoints_[up_pos]-cohesionPoints_[up_pos-1];

            Scalar coef = (hardVar - hardeningVariablePoints_[up_pos-1])/ (dHVar);
            this->frictionAngle_ = frictionAnglePoints_[up_pos-1] + coef*dFi;
            this->dilationAngle_ =  dilationAnglePoints_[up_pos-1] + coef*dPsi;
            this->cohesion_ = cohesionPoints_[up_pos-1] + coef*dCo;

            Scalar dkdlamb = derivHardeningVarLambda(hardVar);
            Scalar cosfi = cos(this->frictionAngle_);
            Scalar dfdfi = 1/std::pow(cosfi,2.0);
            Scalar dfdc = -1.0;
            Scalar dfidk = dFi/dHVar;
            Scalar dcodk = dCo/dHVar;
            Scalar dfdk = dfdfi*dfidk + dfdc*dcodk;
            hardParam_ = dfdk*dkdlamb;
        }

    }

private:

    int npoints;
    Scalar hardParam_; // hardening Parameter
    ScalarVector hardeningVariablePoints_;
    ScalarVector frictionAnglePoints_;
    ScalarVector cohesionPoints_;
    ScalarVector dilationAnglePoints_;
    
};
} // end namespace Dumux
#endif
