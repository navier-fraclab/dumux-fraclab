// -*- mode: C++; tab-width: 4; indent-tabs-mode: nil; c-basic-offset: 4 -*-
// vi: set et ts=4 sw=4 sts=4:
/*****************************************************************************
 *   See the file COPYING for full copying permissions.                      *
 *                                                                           *
 *   This program is free software: you can redistribute it and/or modify    *
 *   it under the terms of the GNU General Public License as published by    *
 *   the Free Software Foundation, either version 3 of the License, or       *
 *   (at your option) any later version.                                     *
 *                                                                           *
 *   This program is distributed in the hope that it will be useful,         *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of          *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the            *
 *   GNU General Public License for more details.                            *
 *                                                                           *
 *   You should have received a copy of the GNU General Public License       *
 *   along with this program.  If not, see <http://www.gnu.org/licenses/>.   *
 *****************************************************************************/
/*!
 * \file
 * \ingroup Geomechanics
 * \brief Defines a type tag and some properties for geomechanical DuMuX models.
 */

#ifndef DUMUX_FRACLAB_GEOMECHANICS_PROPERTIES_HH
#define DUMUX_FRACLAB_GEOMECHANICS_PROPERTIES_HH

#include <dumux/common/properties/model.hh>
#include <dumux/fraclab/common/properties.hh>
#include <dumux/geomechanics/elastic/model.hh>
#include <dumux/geomechanics/poroelastic/model.hh>
#include "localstiffnessmatrix.hh"

namespace Dumux {
namespace Properties {

template<class TypeTag>
struct LocalTangentMatrix <TypeTag, TTag::Elastic> 
{ using type = ElasticTangentMatrix<TypeTag>; };

template<class TypeTag>
struct LocalTangentMatrix <TypeTag, TTag::Poroelastic> 
{ using type = ElasticTangentMatrix<TypeTag>; };

} // namespace Properties
} // namespace Dumux

 #endif
