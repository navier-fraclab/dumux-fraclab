// -*- mode: C++; tab-width: 4; indent-tabs-mode: nil; c-basic-offset: 4 -*-
// vi: set et ts=4 sw=4 sts=4:
/*****************************************************************************
 *   See the file COPYING for full copying permissions.                      *
 *                                                                           *
 *   This program is free software: you can redistribute it and/or modify    *
 *   it under the terms of the GNU General Public License as published by    *
 *   the Free Software Foundation, either version 3 of the License, or       *
 *   (at your option) any later version.                                     *
 *                                                                           *
 *   This program is distributed in the hope that it will be useful,         *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of          *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the            *
 *   GNU General Public License for more details.                            *
 *                                                                           *
 *   You should have received a copy of the GNU General Public License       *
 *   along with this program.  If not, see <http://www.gnu.org/licenses/>.   *
 *****************************************************************************/
/*!
 * \file
 * \ingroup MultiDomain
 * \brief Traits for geomechanical multidomain problems with facets
 */

#ifndef DUMUX_FRACLAB_MULTIDOMAIN_TRAITS_HH
#define DUMUX_FRACLAB_MULTIDOMAIN_TRAITS_HH

#include <dumux/multidomain/traits.hh>

using namespace Dumux::Detail;

namespace Dumux {

// this class is just a copy of original MultiDOmain Traits without 
// definition of GridVariables in MultiDomain
// so we can avoid compile errors when spaial params class depend on
// a coupling manager or smt else that takes MDTraits as input

template<typename... SubDomainTypeTags>
struct FracLabMultiDomainTraits
{
    //! the number of subdomains
    static constexpr std::size_t numSubDomains = sizeof...(SubDomainTypeTags);

private:

    //! the type tag of a sub domain problem
    template<std::size_t id>
    using SubDomainTypeTag = typename std::tuple_element_t<id, std::tuple<SubDomainTypeTags...>>;

    //! helper alias to construct derived multidomain types like tuples
    using Indices = std::make_index_sequence<numSubDomains>;

    //! the scalar type of each sub domain
    template<std::size_t id>
    using SubDomainScalar = GetPropType<SubDomainTypeTag<id>, Properties::Scalar>;

    //! the jacobian type of each sub domain
    template<std::size_t id>
    using SubDomainJacobianMatrix = GetPropType<SubDomainTypeTag<id>, Properties::JacobianMatrix>;

    //! the solution type of each sub domain
    template<std::size_t id>
    using SubDomainSolutionVector = GetPropType<SubDomainTypeTag<id>, Properties::SolutionVector>;

public:

    /*
     * \brief sub domain types
     */
    //\{

    template<std::size_t id>
    struct SubDomain
    {
        using Index = Dune::index_constant<id>;
        using TypeTag = SubDomainTypeTag<id>;
        using Grid = GetPropType<SubDomainTypeTag<id>, Properties::Grid>;
        using GridGeometry = GetPropType<SubDomainTypeTag<id>, Properties::GridGeometry>;
        using Problem = GetPropType<SubDomainTypeTag<id>, Properties::Problem>;
        //using GridVariables =GetPropType<SubDomainTypeTag<id>, Properties::GridVariables>;
        using IOFields = GetPropType<SubDomainTypeTag<id>, Properties::IOFields>;
        using SolutionVector = GetPropType<SubDomainTypeTag<id>, Properties::SolutionVector>;
    };

    //\}

    /*
     * \brief multi domain types
     */
    //\{

    //! the scalar type
    using Scalar = typename makeFromIndexedType<std::common_type_t, SubDomainScalar, Indices>::type;

    //! the solution vector type
    using SolutionVector = typename makeFromIndexedType<Dune::MultiTypeBlockVector, SubDomainSolutionVector, Indices>::type;

    //! the jacobian type
    using JacobianMatrix = typename Detail::MultiDomainMatrixType<SubDomainJacobianMatrix, Indices, Scalar>::type;

    //\}

    /*
     * \brief helper aliases to contruct derived tuple types
     */
    //\{

    //! helper alias to create tuple<...> from indexed type
    template<template<std::size_t> class T>
    using Tuple = typename makeFromIndexedType<std::tuple, T, Indices>::type;

    //! helper alias to create tuple<std::shared_ptr<...>> from indexed type
    template<template<std::size_t> class T>
    using TupleOfSharedPtr = typename Detail::MultiDomainTupleSharedPtr<T, Indices>::type;

    //! helper alias to create tuple<std::shared_ptr<const ...>> from indexed type
    template<template<std::size_t> class T>
    using TupleOfSharedPtrConst = typename Detail::MultiDomainTupleSharedPtrConst<T, Indices>::type;

    //\}
};

template<typename... SubDomainTypeTags>
struct GeomechanicsBaseMultiDomainTraits
{
    static constexpr std::size_t bulkId = 0;
    //! the number of subdomains
    static constexpr std::size_t numSubDomains = sizeof...(SubDomainTypeTags);

private:

    //! the type tag of a sub domain problem
    template<std::size_t id>
    using SubDomainTypeTag = typename std::tuple_element_t<id, std::tuple<SubDomainTypeTags...>>;

    //! helper alias to construct derived multidomain types like tuples
    using Indices = std::make_index_sequence<numSubDomains>;

    //! the scalar type of each sub domain
    template<std::size_t id>
    using SubDomainScalar = GetPropType<SubDomainTypeTag<id>, Properties::Scalar>;

    //! the jacobian type of each sub domain
    template<std::size_t id>
    using SubDomainJacobianMatrix = GetPropType<SubDomainTypeTag<id>, Properties::JacobianMatrix>;

    //! the solution type of each sub domain
    template<std::size_t id>
    using SubDomainSolutionVector = GetPropType<SubDomainTypeTag<id>, Properties::SolutionVector>;

public:

    static void setBulkId(std::size_t id){bulkId = id;}
    /*
     * \brief sub domain types
     */
    //\{

    template<std::size_t id>
    struct SubDomain
    {
        using Index = Dune::index_constant<id>;
        using TypeTag = SubDomainTypeTag<id>;
        using Grid = GetPropType<SubDomainTypeTag<id>, Properties::Grid>;
        using GridGeometry = GetPropType<SubDomainTypeTag<id>, Properties::GridGeometry>;
        //using Problem = GetPropType<SubDomainTypeTag<id>, Properties::Problem>;
        //using GridVariables =GetPropType<SubDomainTypeTag<id>, Properties::GridVariables>;
        using IOFields = GetPropType<SubDomainTypeTag<id>, Properties::IOFields>;
        using SolutionVector = GetPropType<SubDomainTypeTag<id>, Properties::SolutionVector>;
    };

    //\}

    /*
     * \brief multi domain types
     */
    //\{

    //! the scalar type
    using Scalar = typename makeFromIndexedType<std::common_type_t, SubDomainScalar, Indices>::type;

    //! the solution vector type
    using SolutionVector = SubDomainSolutionVector<bulkId>;

    //! the jacobian type
    using JacobianMatrix = SubDomainJacobianMatrix<bulkId>;
    //\}

    using MultiDomainSolutionVector = typename makeFromIndexedType<Dune::MultiTypeBlockVector, SubDomainSolutionVector, Indices>::type;
    /*
     * \brief helper aliases to contruct derived tuple types
     */
    //\{

    //! helper alias to create tuple<...> from indexed type
    template<template<std::size_t> class T>
    using Tuple = typename makeFromIndexedType<std::tuple, T, Indices>::type;

    //! helper alias to create tuple<std::shared_ptr<...>> from indexed type
    template<template<std::size_t> class T>
    using TupleOfSharedPtr = typename Detail::MultiDomainTupleSharedPtr<T, Indices>::type;

    //! helper alias to create tuple<std::shared_ptr<const ...>> from indexed type
    template<template<std::size_t> class T>
    using TupleOfSharedPtrConst = typename Detail::MultiDomainTupleSharedPtrConst<T, Indices>::type;

    //\}
};

template<typename... SubDomainTypeTags>
struct GeomechanicsMultiDomainTraits
{
    static constexpr std::size_t bulkId = 0;
    //! the number of subdomains
    static constexpr std::size_t numSubDomains = sizeof...(SubDomainTypeTags);

private:

    //! the type tag of a sub domain problem
    template<std::size_t id>
    using SubDomainTypeTag = typename std::tuple_element_t<id, std::tuple<SubDomainTypeTags...>>;

    //! helper alias to construct derived multidomain types like tuples
    using Indices = std::make_index_sequence<numSubDomains>;

    //! the scalar type of each sub domain
    template<std::size_t id>
    using SubDomainScalar = GetPropType<SubDomainTypeTag<id>, Properties::Scalar>;

    //! the jacobian type of each sub domain
    template<std::size_t id>
    using SubDomainJacobianMatrix = GetPropType<SubDomainTypeTag<id>, Properties::JacobianMatrix>;

    //! the solution type of each sub domain
    template<std::size_t id>
    using SubDomainSolutionVector = GetPropType<SubDomainTypeTag<id>, Properties::SolutionVector>;

public:

    static void setBulkId(std::size_t id){bulkId = id;}
    /*
     * \brief sub domain types
     */
    //\{

    template<std::size_t id>
    struct SubDomain
    {
        using Index = Dune::index_constant<id>;
        using TypeTag = SubDomainTypeTag<id>;
        using Grid = GetPropType<SubDomainTypeTag<id>, Properties::Grid>;
        using GridGeometry = GetPropType<SubDomainTypeTag<id>, Properties::GridGeometry>;
        using Problem = GetPropType<SubDomainTypeTag<id>, Properties::Problem>;
        using GridVariables =GetPropType<SubDomainTypeTag<id>, Properties::GridVariables>;
        using IOFields = GetPropType<SubDomainTypeTag<id>, Properties::IOFields>;
        using SolutionVector = GetPropType<SubDomainTypeTag<id>, Properties::SolutionVector>;
    };

    //\}

    /*
     * \brief multi domain types
     */
    //\{

    //! the scalar type
    using Scalar = typename makeFromIndexedType<std::common_type_t, SubDomainScalar, Indices>::type;

    //! the solution vector type
    using SolutionVector = SubDomainSolutionVector<bulkId>;

    //! the jacobian type
    using JacobianMatrix = SubDomainJacobianMatrix<bulkId>;
    //\}

    using MultiDomainSolutionVector = typename makeFromIndexedType<Dune::MultiTypeBlockVector, SubDomainSolutionVector, Indices>::type;
    /*
     * \brief helper aliases to contruct derived tuple types
     */
    //\{

    //! helper alias to create tuple<...> from indexed type
    template<template<std::size_t> class T>
    using Tuple = typename makeFromIndexedType<std::tuple, T, Indices>::type;

    //! helper alias to create tuple<std::shared_ptr<...>> from indexed type
    template<template<std::size_t> class T>
    using TupleOfSharedPtr = typename Detail::MultiDomainTupleSharedPtr<T, Indices>::type;

    //! helper alias to create tuple<std::shared_ptr<const ...>> from indexed type
    template<template<std::size_t> class T>
    using TupleOfSharedPtrConst = typename Detail::MultiDomainTupleSharedPtrConst<T, Indices>::type;

    //\}
};


} //end namespace Dumux

#endif
