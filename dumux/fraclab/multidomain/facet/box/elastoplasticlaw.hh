/*!
 * \filey
 * \ingroup BoxFlux
 * \brief Specialization of Hooke's law for the box scheme. This computes
 *        the stress tensor and surface forces resulting from mechanical deformation.
 */
#ifndef DUMUX_DISCRETIZATION_BOX_FACET_COUPLING_ELASTOPLASTIC_LAW_HH
#define DUMUX_DISCRETIZATION_BOX_FACET_COUPLING_ELASTOPLASTIC_LAW_HH

#include <dumux/flux/box/hookeslaw.hh>
#include <dumux/fraclab/common/properties.hh>
#include "plasticcorrection.hh"
#include "elasticlaw.hh"

namespace Dumux{
  
template<class TypeTag, class Scalar, class GridGeometry>
class BoxFacetCouplingElastoplasticLaw
: public BoxFacetCouplingElasticLaw<Scalar,GridGeometry>
{
    using FVElementGeometry = typename GridGeometry::LocalView;
    using SubControlVolumeFace = typename FVElementGeometry::SubControlVolumeFace;
    using GridView = typename GridGeometry::GridView;
    using Element = typename GridView::template Codim<0>::Entity;
  
public:

    using ParentType = BoxFacetCouplingElasticLaw<Scalar,GridGeometry>;
    using FacetReturn = GetPropType<TypeTag, Properties::InterfaceReturnAlgorithm>;
    using ElasticStressType = HookesLaw<Scalar, GridGeometry, DiscretizationMethod::box>;
    using StressLaw = PlasticCorrection<TypeTag, ElasticStressType>;
    using StressTensor = typename StressLaw::StressTensor;
    static constexpr int nrow = ElasticStressType::nrow;
    static constexpr int dimWorld = GridView::dimensionworld;
  
    using RotationMatrix = Dune::FieldMatrix<Scalar, 2, dimWorld>;
  
    using FacetTraction = Dune::FieldVector<Scalar, dimWorld>;
    //! export the type used for force vectors
    using ForceVector = Dune::FieldVector<Scalar, dimWorld>;

    template<class Problem, class ElementVolumeVariables, class ElementFluxVarsCache>
    static ForceVector force(const Problem& problem,
                             const Element& element,
                             const FVElementGeometry& fvGeometry,
                             const ElementVolumeVariables& elemVolVars,
                             const SubControlVolumeFace& scvf,
                             const ElementFluxVarsCache& elemFluxVarCache)
                             /**/
    {
         // if face is not at zero-thickness interface, stress law for bulk material is called

        if(!scvf.interiorBoundary())
            return StressLaw::force(problem, element, fvGeometry, elemVolVars, scvf, elemFluxVarCache);

        ForceVector facetForce(0.0);
        // obtain cohesive traction in interface element
        const auto traction = interfaceTraction(problem, element, fvGeometry, elemVolVars, elemFluxVarCache[scvf], scvf);
        // rotate stresses to global coord system
        ParentType::rotationMatrix.mtv(traction, facetForce);
        //integrate over face
        facetForce *= scvf.area();

        return facetForce;
	
    }
    
    template<class Problem, class ElementVolumeVariables, class FluxVarsCache>
    static FacetTraction interfaceTraction(const Problem& problem,
					                        const Element& element,
					                        const FVElementGeometry& fvGeometry,
					                        const ElementVolumeVariables& elemVolVars,
					                        const FluxVarsCache& fluxVarCache,
					                        const SubControlVolumeFace& scvf)
    {

        //calculate normal and tangential elastic increment stresses 
        auto traction = ParentType::interfaceTraction(problem, element, fvGeometry, elemVolVars, fluxVarCache, scvf); 

        const auto& spatialParams = problem.spatialParams();
        const auto& plasticModel = spatialParams.interfacePlasticModel(element, fvGeometry, elemVolVars, fluxVarCache);
	    const auto& stiffMatrix = ParentType::stiffnessMatrix; //spatialParams.interfaceElasticMatrix(element, fvGeometry, elemVolVars, fluxVarCache);

        // compute current elastic trial
        auto initTractionVector = fluxVarCache.stressVectorAtFace();
	    ForceVector initTraction(0.0);
	    for(int i = 0; i < dimWorld; i++)
	    {
	        traction[i] += initTractionVector[i];
	        initTraction[i] = initTractionVector[i];
	    }

        // check if yield surface is surpass and call return algorithm 
        const auto yf = plasticModel.yieldFunction(traction);
	    const auto tol = StressLaw::getYieldFunctionTolerance();

	    if(yf > tol)
	        FacetReturn::returnAlgorithm(element, fvGeometry,elemVolVars,fluxVarCache,plasticModel, stiffMatrix,initTraction, traction, yf);
	   
        // we need the stress increment 
        traction -= initTraction;

        return traction;
    }
   
    // initialize storage arrays
    static void init(const GridGeometry& gridGeometry)
    {
        const auto numElems = gridGeometry.gridView().size(0);
       
        facetEffTractions_.resize(numElems);
    
        for(const auto& element : elements(gridGeometry.gridView()))
        {
            const auto eIdx = gridGeometry.elementMapper().index(element);
            auto fvGeometry = localView(gridGeometry);
            fvGeometry.bind(element);
            facetEffTractions_[eIdx].resize(fvGeometry.numScvf());
        }

        initialized = true;

    }

    protected:
        static inline std::vector<std::vector<ForceVector>> facetEffTractions_;
        static inline bool initialized = false;
        static inline RotationMatrix rotationMatrix;    
};
}
#endif
