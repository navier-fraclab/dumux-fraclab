/*!
 * \filey
 * \ingroup BoxFlux
 * \brief Specialization of Hooke's law for the box scheme. This computes
 *        the stress tensor and surface forces resulting from mechanical deformation.
 */
#ifndef DUMUX_DISCRETIZATION_BOX_FACET_COUPLING_POROELASTIC_LAW_HH
#define DUMUX_DISCRETIZATION_BOX_FACET_COUPLING_POROELASTIC_LAW_HH

#include <dumux/flux/box/hookeslaw.hh>
#include <dumux/flux/box/effectivestresslaw.hh>					   

namespace Dumux{
  
template<class Scalar, class GridGeometry>
class BoxFacetCouplingPoroElasticLaw
:public BoxFacetCouplingElasticLaw<Scalar,GridGeometry>
{
    using FVElementGeometry = typename GridGeometry::LocalView;
    using SubControlVolumeFace = typename FVElementGeometry::SubControlVolumeFace;
    using GridView = typename GridGeometry::GridView;
    using Element = typename GridView::template Codim<0>::Entity;
  
public:

    using ParentType = BoxFacetCouplingElasticLaw<Scalar,GridGeometry>;
    using EffectiveStressType = HookesLaw<Scalar, GridGeometry, DiscretizationMethod::box>;
    using DefaultBoxElasticLaw = EffectiveStressLaw<EffectiveStressType, GridGeometry>;
    using StressTensor = typename DefaultBoxElasticLaw::StressTensor;
    static constexpr int nrow = DefaultBoxElasticLaw::nrow;
    static constexpr int dimWorld = GridView::dimensionworld;
  
    using RotationMatrix = Dune::FieldMatrix<Scalar, 2, dimWorld>;
  
    using FacetTraction = Dune::FieldVector<Scalar, dimWorld>;
    //! export the type used for force vectors
    using ForceVector = Dune::FieldVector<Scalar, dimWorld>;

    template<class Problem, class ElementVolumeVariables, class ElementFluxVarsCache>
    static ForceVector force(const Problem& problem,
                             const Element& element,
                             const FVElementGeometry& fvGeometry,
                             const ElementVolumeVariables& elemVolVars,
                             const SubControlVolumeFace& scvf,
                             const ElementFluxVarsCache& elemFluxVarCache)
  {
        if(!scvf.interiorBoundary())
            return DefaultBoxElasticLaw::force(problem, element, fvGeometry, elemVolVars, scvf, elemFluxVarCache);

        ForceVector facetForce(0.0);

        // obtain cohesive traction in interface element
        auto traction = interfaceTraction(problem, element, fvGeometry, elemVolVars, elemFluxVarCache[scvf], scvf);
        // rotate stresses to global coord system
        ParentType::rotationMatrix.mtv(traction, facetForce);
        //integrate over face
        facetForce *= scvf.area();

        return facetForce;
    }

    template<class Problem, class ElementVolumeVariables, class FluxVarsCache>
    static FacetTraction effectiveInterfaceTraction(const Problem& problem,
                                                    const Element& element,
                                                    const FVElementGeometry& fvGeometry,
                                                    const ElementVolumeVariables& elemVolVars,
                                                    const FluxVarsCache& fluxVarCache,
                                                    const SubControlVolumeFace& scvf)
    {
        return ParentType::interfaceTraction(problem, element, fvGeometry, elemVolVars, fluxVarCache, scvf);    
    }

    template<class Problem, class ElementVolumeVariables, class FluxVarsCache>
    static FacetTraction interfaceTraction(const Problem& problem,
                                           const Element& element,
                                           const FVElementGeometry& fvGeometry,
                                           const ElementVolumeVariables& elemVolVars,
                                           const FluxVarsCache& fluxVarCache,
                                           const SubControlVolumeFace& scvf)
    {
        //compute eff stresses in local coor system 
        auto traction = effectiveInterfaceTraction(problem, element, fvGeometry, elemVolVars, fluxVarCache, scvf);

        // obtain pore pressures and compute total stress

        const auto& elementIdx = problem.gridGeometry().elementMapper().index(element);
        const auto lowDimElemIdx = problem.couplingManager().getLowDimElementIndex(elementIdx, scvf);
        const auto lowDimScvIdx = problem.couplingManager().getCoupledLowDimScvIdx(elementIdx, scvf);

	    const auto biotCoeff = problem.spatialParams().interfaceBiotCoefficient(lowDimElemIdx, lowDimScvIdx);
	    const auto effPress = problem.interfaceEffectivePorePressure(lowDimElemIdx, lowDimScvIdx);
	    const auto bcp = biotCoeff*effPress;
        
        traction[0] -= bcp; //porepressure affects normal stress only
    
        return traction;
    }
};
}
#endif
