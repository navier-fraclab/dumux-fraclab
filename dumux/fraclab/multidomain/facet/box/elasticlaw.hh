// -*- mode: C++; tab-width: 4; indent-tabs-mode: nil; c-basic-offset: 4 -*-
// vi: set et ts=4 sw=4 sts=4:
/*****************************************************************************
 *   See the file COPYING for full copying permissions.                      *
 *                                                                           *
 *   This program is free software: you can redistribute it and/or modify    *
 *   it under the terms of the GNU General Public License as published by    *
 *   the Free Software Foundation, either version 3 of the License, or       *
 *   (at your option) any later version.                                     *
 *                                                                           *
 *   This program is distributed in the hope that it will be useful,         *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of          *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the            *
 *   GNU General Public License for more details.                            *
 *                                                                           *
 *   You should have received a copy of the GNU General Public License       *
 *   along with this program.  If not, see <http://www.gnu.org/licenses/>.   *
 *****************************************************************************/
 /*!
 * \filey
 * \ingroup BoxFlux
 * \brief Specialization of Hooke's law for the box scheme. This computes
 *        the stress tensor and surface forces resulting from mechanical deformation.
 */
#ifndef DUMUX_DISCRETIZATION_BOX_FACET_COUPLING_ELASTIC_LAW_HH
#define DUMUX_DISCRETIZATION_BOX_FACET_COUPLING_ELASTIC_LAW_HH

#include <dumux/common/parameters.hh>
#include <dumux/common/properties.hh>
#include <dune/common/fmatrix.hh>
#include <dumux/discretization/method.hh>
#include <dumux/common/math.hh>
#include <dumux/flux/box/hookeslaw.hh>

namespace Dumux{
  
template<class Scalar, class GridGeometry>
class BoxFacetCouplingElasticLaw
{
    using DefaultBoxElasticLaw = HookesLaw<Scalar, GridGeometry, DiscretizationMethod::box>;
    using FVElementGeometry = typename GridGeometry::LocalView;
    using SubControlVolumeFace = typename FVElementGeometry::SubControlVolumeFace;
    using GridView = typename GridGeometry::GridView;
    using Element = typename GridView::template Codim<0>::Entity;
  
    static constexpr int nrow = DefaultBoxElasticLaw::nrow;
    static constexpr int dimWorld = GridView::dimensionworld;

public:

    using StressTensor = typename DefaultBoxElasticLaw::StressTensor;
    using RotationMatrix = Dune::FieldMatrix<Scalar, dimWorld, dimWorld>; 
    using StiffnessMatrix = Dune::FieldMatrix<Scalar, dimWorld, dimWorld>; 
    using FacetTraction = Dune::FieldVector<Scalar, dimWorld>;
    //! export the type used for force vectors
    using ForceVector = Dune::FieldVector<Scalar, dimWorld>;

    template<class Problem, class ElementVolumeVariables, class ElementFluxVarsCache>
    static ForceVector force(const Problem& problem,
                             const Element& element,
                             const FVElementGeometry& fvGeometry,
                             const ElementVolumeVariables& elemVolVars,
                             const SubControlVolumeFace& scvf,
                             const ElementFluxVarsCache& elemFluxVarCache)
    {
        // if face is not at zero-thickness interface, elastic law for bulk material is called
        if(!scvf.interiorBoundary())
            return DefaultBoxElasticLaw::force(problem, element, fvGeometry, elemVolVars, scvf, elemFluxVarCache);

        // otherwise, elastic law for the interface is used  
        // obtain cohesive normal/tangent tractions in interface element

        auto traction = interfaceTraction(problem, element, fvGeometry, elemVolVars, elemFluxVarCache[scvf], scvf); 
        
        ForceVector force(0.0);
        // rotate stresses to global coord system
        rotationMatrix.mtv(traction, force);
   
        //integrate over face and change sign to adapt to positive traction convention
        force *= scvf.area();

        return force;
    }

    // computes interface stresses with respect to local coordinate system (normal and tangent vectors to interface)
    template<class Problem, class ElementVolumeVariables, class FluxVarsCache>
    static FacetTraction interfaceTraction(const Problem& problem,
                                           const Element& element,
                                           const FVElementGeometry& fvGeometry,
                                           const ElementVolumeVariables& elemVolVars,
                                           const FluxVarsCache& fluxVarCache,
                                           const SubControlVolumeFace& scvf)
    {
        //calculate normal and tangential stresses 
        FacetTraction traction(0.0);
        ForceVector displ(0.0);

        const auto& shapeValues = fluxVarCache.shapeValues();
        const auto& elementIdx = problem.gridGeometry().elementMapper().index(element);
        for (int dir = 0; dir < dimWorld; ++dir)
            for (const auto& scv : scvs(fvGeometry))
                displ[dir] += shapeValues[scv.indexInElement()]*elemVolVars[scv.indexInElement()].displacement(dir);
        
        const auto& lowDimStencil = problem.couplingManager().couplingElementStencil(elementIdx);
        const auto lowDimElemIdx = problem.couplingManager().getLowDimElementIndex(elementIdx, scvf);
      
        auto it = std::find(lowDimStencil.begin(), lowDimStencil.end(), lowDimElemIdx);
        const auto lowDimPosition = std::distance(lowDimStencil.begin(), it);
        const auto pairBulkIdx = problem.couplingManager().bulkPairElementIdx(elementIdx, lowDimPosition);
        const auto lowDimScvIdx = problem.couplingManager().getCoupledLowDimScvIdx(elementIdx, scvf);
        const auto scvfIndices = problem.couplingManager().getCoincidingBulkScvfs(lowDimElemIdx, pairBulkIdx);
        const auto pairScvfIdx = scvfIndices[lowDimScvIdx];
 
        const auto& pairVolVars = problem.couplingManager().getPairBulkVolVars(elementIdx, lowDimPosition);
        const auto& pairFluxVars = problem.couplingManager().getPairBulkFluxVarsCache(elementIdx, lowDimPosition);
        const auto& pairFvGeometry = problem.couplingManager().getPairFvGeometry(elementIdx, lowDimPosition);
        const auto& pairScvf = pairFvGeometry.scvf(pairScvfIdx);
        const auto& pairFluxVarsCache = pairFluxVars[pairScvf];
        const auto& pairShapeValues = pairFluxVarsCache.shapeValues();
        // pairFvGeometry.bind(pairBulk);

        // obtain displacements at the opposite side of the interface element 
        ForceVector pairDispl(0.0);
        for (int dir = 0; dir < dimWorld; ++dir)
            for (const auto& scv : scvs(pairFvGeometry))
                pairDispl[dir] += pairShapeValues[scv.indexInElement()]*pairVolVars[scv.indexInElement()].displacement(dir);
        
        // compute displacement jumps in the x-y-z coord system
        displ -= pairDispl;
        
        //obtain normal and tangent displ. jumps
        rotationMatrix = problem.couplingManager().interfaceRotationMatrix(scvf,lowDimElemIdx,lowDimScvIdx);
        ForceVector rdispl(0.0);
        rotationMatrix.mv(displ, rdispl);

        const auto& stiffParams = problem.spatialParams().interfaceStiffnessParams(element, scvf, fvGeometry, elemVolVars, fluxVarCache, rdispl);
        auto Kn = stiffParams.normalStiffness();
        auto Kt = stiffParams.tangentStiffness();
        auto Knt = stiffParams.dilatantStiffness();

        stiffnessMatrix[0][0] = Kn;
        for(int i = 1; i < dimWorld; i++){
            stiffnessMatrix[i][i] = Kt;
            stiffnessMatrix[i][0] = Knt;
            stiffnessMatrix[0][i] = Knt;
        }

        // obtain normal and tangent stresses at the interface 
        traction[0] = Kn*rdispl[0];

        for (int i = 1; i < dimWorld; ++i) {
            traction[0] += Knt*rdispl[i];
            traction[i] = Knt*rdispl[0] + Kt*rdispl[i];
        }
        
        // change sign so traction is positive
        traction *= -1.0;

        return traction;
    }

    // computes interface stresses with respect to local coordinate system (normal and tangent vectors to interface)
    template<class Problem, class ElementVolumeVariables, class FluxVarsCache>
    static ForceVector interfaceDisplacementJump(const Problem& problem,
                                                 const Element& element,
                                                 const FVElementGeometry& fvGeometry,
                                                 const ElementVolumeVariables& elemVolVars,
                                                 const FluxVarsCache& fluxVarCache,
                                                 const SubControlVolumeFace& scvf)
    {
        //calculate normal and tangential stresses 
        ForceVector displ(0.0);

        const auto& shapeValues = fluxVarCache.shapeValues();
        const auto& elementIdx = problem.gridGeometry().elementMapper().index(element);
        for (int dir = 0; dir < dimWorld; ++dir)
            for (const auto& scv : scvs(fvGeometry))
                displ[dir] += shapeValues[scv.indexInElement()]*elemVolVars[scv.indexInElement()].displacement(dir);
        
        const auto& lowDimStencil = problem.couplingManager().couplingElementStencil(elementIdx);
        const auto lowDimElemIdx = problem.couplingManager().getLowDimElementIndex(elementIdx, scvf);
      
        auto it = std::find(lowDimStencil.begin(), lowDimStencil.end(), lowDimElemIdx);
        const auto lowDimPosition = std::distance(lowDimStencil.begin(), it);
        const auto pairBulkIdx = problem.couplingManager().bulkPairElementIdx(elementIdx, lowDimPosition);
        const auto lowDimScvIdx = problem.couplingManager().getCoupledLowDimScvIdx(elementIdx, scvf);
        const auto scvfIndices = problem.couplingManager().getCoincidingBulkScvfs(lowDimElemIdx, pairBulkIdx);
        const auto pairScvfIdx = scvfIndices[lowDimScvIdx];
 
        const auto& pairVolVars = problem.couplingManager().getPairBulkVolVars(elementIdx, lowDimPosition);
        const auto& pairFluxVars = problem.couplingManager().getPairBulkFluxVarsCache(elementIdx, lowDimPosition);
        const auto& pairFvGeometry = problem.couplingManager().getPairFvGeometry(elementIdx, lowDimPosition);
        const auto& pairScvf = pairFvGeometry.scvf(pairScvfIdx);
        const auto& pairFluxVarsCache = pairFluxVars[pairScvf];
        const auto& pairShapeValues = pairFluxVarsCache.shapeValues();
        // pairFvGeometry.bind(pairBulk);

        // obtain displacements at the opposite side of the interface element 
        ForceVector pairDispl(0.0);
        for (int dir = 0; dir < dimWorld; ++dir)
            for (const auto& scv : scvs(pairFvGeometry))
                pairDispl[dir] += pairShapeValues[scv.indexInElement()]*pairVolVars[scv.indexInElement()].displacement(dir);
        
        // compute displacement jumps in the x-y-z coord system
        displ -= pairDispl;
        
        //obtain normal and tangent displ. jumps
        rotationMatrix = problem.couplingManager().interfaceRotationMatrix(scvf,lowDimElemIdx,lowDimScvIdx);
        ForceVector rdispl(0.0);
        rotationMatrix.mv(displ, rdispl);

        rdispl *= -1.0;

        return rdispl;
    }

    template<class Problem>
    static void updateRotationMatrix(const Problem& problem,
                                     const Element& element,
                                     const SubControlVolumeFace& scvf)
    {
        const auto& couplingManager = problem.couplingManager();
        const auto& elementIdx = problem.gridGeometry().elementMapper().index(element);
        const auto lowDimElemIdx = couplingManager.getLowDimElementIndex(elementIdx, scvf);
        const auto lowDimScvIdx = couplingManager.getCoupledLowDimScvIdx(elementIdx, scvf);
        rotationMatrix = problem.couplingManager().interfaceRotationMatrix(scvf,lowDimElemIdx,lowDimScvIdx);
    }

    static const StiffnessMatrix& elasticStiffnessMatrix()
    {return stiffnessMatrix;}
protected:
    static inline RotationMatrix rotationMatrix;
    static inline StiffnessMatrix stiffnessMatrix;
};
}
#endif
