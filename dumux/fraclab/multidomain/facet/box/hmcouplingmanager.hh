// -*- mode: C++; tab-width: 4; indent-tabs-mode: nil; c-basic-offset: 4 -*-
// vi: set et ts=4 sw=4 sts=4:
/*****************************************************************************
 *   See the file COPYING for full copying permissions.                      *
 *                                                                           *
 *   This program is free software: you can redistribute it and/or modify    *
 *   it under the terms of the GNU General Public License as published by    *
 *   the Free Software Foundation, either version 3 of the License, or       *
 *   (at your option) any later version.                                     *
 *                                                                           *
 *   This program is distributed in the hope that it will be useful,         *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of          *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the            *
 *   GNU General Public License for more details.                            *
 *                                                                           *
 *   You should have received a copy of the GNU General Public License       *
 *   along with this program.  If not, see <http://www.gnu.org/licenses/>.   *
 *****************************************************************************/
/*!
 * \file
 * \ingroup FacetCoupling
 * coupling manager for staggered hydromechanical coupling in multidomain facet problems 
 */
#ifndef DUMUX_BOX_FACETHMCOUPLING_MANAGER_HH
#define DUMUX_BOX_FACETHMCOUPLING_MANAGER_HH

#include <dumux/multidomain/facet/box/couplingmanager.hh>
#include <dumux/discretization/box/boxgeometryhelper.hh>

namespace Dumux
{

template<class HMTraits, class CouplingMapper,class FlowAssembler, class MechanicsAssembler, std::size_t bulkDomainId = 0, std:: size_t lowDimDomainId = 1>
  class MultidomainFacetBoxHMCouplingManager
  {
    using MechanicsCouplingManager = typename HMTraits::MechanicsCouplingManager;
    using FlowCouplingManager = typename HMTraits::FluxCouplingManager;
    using FMDTraits = typename HMTraits::FluxMultiDomainTraits;
    using MMDTraits = typename HMTraits::MechanicsMultiDomainTraits;
    using MechanicsSolutionVector = typename MMDTraits::SolutionVector;
    using FluxSolutionVector = typename FMDTraits::SolutionVector;
    // randomly choosing Flux Multidomain Traits (FMD) but domain Ids should be the same for both (verify this)
    using BulkIdType = typename FMDTraits::template SubDomain<bulkDomainId>::Index;
    using LowDimIdType = typename FMDTraits::template SubDomain<lowDimDomainId>::Index;
    static constexpr auto bulkId = BulkIdType();
    static constexpr auto lowDimId = LowDimIdType();
    // the sub-domain type tags
    template<std::size_t id> using FluxSubDomainTypeTag = typename FMDTraits::template SubDomain<id>::TypeTag;
    template<std::size_t id> using MechanicsSubDomainTypeTag = typename MMDTraits::template SubDomain<id>::TypeTag;   
    template<std::size_t id> using GridGeometry = GetPropType<FluxSubDomainTypeTag<id>, Properties::GridGeometry>;
    template<std::size_t id> using FVElementGeometry = typename GridGeometry<id>::LocalView;
    template<std::size_t id> using SubControlVolume = typename GridGeometry<id>::SubControlVolume;
    template<std::size_t id> using SubControlVolumeFace = typename GridGeometry<id>::SubControlVolumeFace;
    template<std::size_t id> using GridView = typename GridGeometry<id>::GridView;
    template<std::size_t id> using Element = typename GridView<id>::template Codim<0>::Entity;
    template<std::size_t id> using GridIndexType = typename GridView<id>::IndexSet::IndexType;
    template<std::size_t id> using Scalar = GetPropType<FluxSubDomainTypeTag<id>, Properties::Scalar>;      
    template<std::size_t id> using MechanicsGridVariables = GetPropType<MechanicsSubDomainTypeTag<id>, Properties::GridVariables>;
    template<std::size_t id> using MechanicsGridFluxVariablesCache = typename MechanicsGridVariables<id>::GridFluxVariablesCache;
    template<std::size_t id> using MechanicsFluxVariablesCache = typename MechanicsGridFluxVariablesCache<id>::FluxVariablesCache;  
    template<std::size_t id> using MechanicsElementFluxVariablesCache = typename MechanicsGridFluxVariablesCache<id>::LocalView;
    template<std::size_t id> using MechanicsGridVolumeVariables = typename MechanicsGridVariables<id>::GridVolumeVariables;
    template<std::size_t id> using MechanicsElementVolumeVariables = typename MechanicsGridVolumeVariables<id>::LocalView;
    template<std::size_t id> using FlowGridVariables = GetPropType<FluxSubDomainTypeTag<id>, Properties::GridVariables>;
    template<std::size_t id> using FlowGridFluxVariablesCache = typename FlowGridVariables<id>::GridFluxVariablesCache;
    template<std::size_t id> using FlowFluxVariablesCache = typename FlowGridFluxVariablesCache<id>::FluxVariablesCache;  
    template<std::size_t id> using FlowElementFluxVariablesCache = typename FlowGridFluxVariablesCache<id>::LocalView;

    static constexpr int dimWorld = GridView<bulkId>::dimensionworld;
    static constexpr int bulkDim = GridView<bulkId>::dimension;
    static constexpr int lowDim = GridView<lowDimId>::dimension;
      
  public:

      
    MultidomainFacetBoxHMCouplingManager(std::shared_ptr<const GridGeometry<bulkId>> bulkGridGeometry,
                                         std::shared_ptr<const GridGeometry<lowDimId>> lowDimGridGeometry,
                                         Scalar<bulkId> initPorosity,
                                         Scalar<lowDimId> initAperture)
        : bulkGridGeometry_(bulkGridGeometry)
        , lowDimGridGeometry_(lowDimGridGeometry)
    {
        setStorageVectors();
        bulkPorosities_.resize(bulkGridGeometry_->numDofs(), initPorosity);   
        facetApertures_.resize(lowDimGridGeometry_->numDofs(), initAperture);      
    }

    MultidomainFacetBoxHMCouplingManager(std::shared_ptr<const GridGeometry<bulkId>> bulkGridGeometry,
                                         std::shared_ptr<const GridGeometry<lowDimId>> lowDimGridGeometry,
                                         std::vector<Scalar<bulkId>> initPorosities,
                                         std::vector<Scalar<lowDimId>> initApertures)
        : bulkGridGeometry_(bulkGridGeometry)
        , lowDimGridGeometry_(lowDimGridGeometry)
    {
        setStorageVectors();
        //assert if sizes are correct
        bulkPorosities_ = initPorosities;
        facetApertures_ = initApertures;
    }

    void setStorageVectors()
    {        
        boxVolumes_.resize(bulkGridGeometry_->numDofs(), 0.0);
        facetAreas_.resize(lowDimGridGeometry_->numDofs(), 0.0);
        volStrainVar_.resize(bulkGridGeometry_->numDofs());
        normalDisplJumpVar_.resize(lowDimGridGeometry_->numDofs());

        for(const auto& element : elements(bulkGridGeometry_->gridView()))
        {
            auto fvGeometry = localView(*bulkGridGeometry_);
            fvGeometry.bind(element);
            for(const auto& scv : scvs(fvGeometry))
                boxVolumes_[scv.dofIndex()] += scv.volume();
        }
        
        for(const auto& element : elements(lowDimGridGeometry_->gridView()))
        {
            auto fvGeometry = localView(*lowDimGridGeometry_);
            fvGeometry.bind(element);
            for(const auto& scv : scvs(fvGeometry))
                facetAreas_[scv.dofIndex()] += scv.volume();
        }
    }

    void init(std::shared_ptr<CouplingMapper> couplingMapper,
              std::shared_ptr<FlowCouplingManager> flowCouplingManager,
              std::shared_ptr<MechanicsCouplingManager> mechanicsCouplingManager,
              std::shared_ptr<FlowAssembler> flowAssembler,
              std::shared_ptr<MechanicsAssembler> mechanicsAssembler,
              FluxSolutionVector& fluxSol,
              FluxSolutionVector& prevIterFluxSol,
              MechanicsSolutionVector& mechSol)
    {
        couplingMapper_ = couplingMapper;
        flowCouplingManager_ = flowCouplingManager;
        mechanicsCouplingManager_ = mechanicsCouplingManager;
        flowAssembler_ = flowAssembler;
        mechanicsAssembler_ = mechanicsAssembler;
        setFluxCurrentSolution(fluxSol);
        setFluxPrevIterSolution(prevIterFluxSol);
        setMechanicsCurrentSolution(mechSol);
        initialized = true;
        //updateStrains();
    }

    void updateStrains()
    {
        setBulkVolumetricStrains();
        updateBulkPorositiesFromStrains();
        setFacetNormalDisplacementJumps();
        updateFacetAperturesFromStrains();        
    }
    
    Scalar<lowDimId> coupledFacetTotalEffectivePorePressure(const Element<bulkId>& element,
                                                            const SubControlVolumeFace<bulkId>& scvf) const
    {
      flowCouplingManager_->bindCouplingContext(bulkId, element, *flowAssembler_);
      const auto& facetElemVolVars = flowCouplingManager_->getLowDimVolVars(element, scvf);
      //for now assume one-phase flow
      return facetElemVolVars.pressure(0);
    }

    Scalar<lowDimId> coupledFacetEffectivePorePressure(const GridIndexType<lowDimId> eIdx,
                                                       const GridIndexType<bulkId> scvIdx) const
    {
        const auto& element = lowDimGridGeometry_->element(eIdx);
        const auto& curSol = (*curFluxSol)[lowDimId];
        const auto& prevSol = flowAssembler_->prevSol()[lowDimId];

        auto curElemVolVars = localView(flowAssembler_->gridVariables(lowDimId).curGridVolVars());
        auto fvGeometry = localView(*lowDimGridGeometry_);
        fvGeometry.bind(element);
        curElemVolVars.bind(element, fvGeometry, curSol);
        FlowFluxVariablesCache<lowDimId> fluxVarCache;
        const auto& scv = fvGeometry.scv(scvIdx);
        fluxVarCache.update(flowCouplingManager_->problem(lowDimId), element, fvGeometry, curElemVolVars, scv.geometry().center());
        const auto shapeValues = fluxVarCache.shapeValues();
        Scalar<lowDimId> effPress(0.0);

        for (int dir = 0; dir < dimWorld; ++dir)
            for (const auto& scvK : scvs(fvGeometry))
                effPress += shapeValues[dir]* (curSol[scvK.dofIndex()][0] - prevSol[scvK.dofIndex()][0]);
        
        return effPress;
    }

    Scalar<bulkId> bulkEffectivePorePressure(const Element<bulkId>& element,
                                             const MechanicsFluxVariablesCache<bulkId>& fluxVarCache) const
    {
      const auto& curSol = (*curFluxSol)[bulkId];
      const auto& prevSol = flowAssembler_->prevSol()[bulkId];
      
      auto bulkFvGeometry = localView(*bulkGridGeometry_);
      auto curElemVolVars = localView(flowAssembler_->gridVariables(bulkId).curGridVolVars());
      auto fvGeometry = localView(*bulkGridGeometry_);
      fvGeometry.bind(element);

      curElemVolVars.bind(element, fvGeometry, curSol);
      const auto& shapeValues = fluxVarCache.shapeValues();

      Scalar<bulkId> effPress(0.0);
      for (auto&& scv : scvs(fvGeometry))
          effPress += shapeValues[scv.indexInElement()]*(curSol[scv.dofIndex()][0]-prevSol[scv.dofIndex()][0]);
      return effPress;   
    }

    Dune::FieldVector<Scalar<bulkId>, dimWorld> facetDisplacementJump(const Element<lowDimId>& element,
                                                                        const SubControlVolume<lowDimId>& scv) const
     {
         const auto lowDimElemIdx = flowCouplingManager_->problem(lowDimId).gridGeometry().elementMapper().index(element);
         const auto& map = couplingMapper_->couplingMap(lowDimId, bulkId);
         auto it = map.find(lowDimElemIdx);
         const auto& embedments = it->second.embedments;
         const auto& bulkElemIdx1 = embedments[0].first;
         const auto& coincidingScvfs1 = embedments[0].second;
         const auto& bulkScvfIdx1 =  coincidingScvfs1[scv.localDofIndex()];
         const auto& bulkElemIdx2 = embedments[1].first;
         const auto& coincidingScvfs2 = embedments[1].second;
         const auto& bulkScvfIdx2 = coincidingScvfs2[scv.localDofIndex()];
         const auto& bulkMechSol = *curMechSol;
         const auto& bulkMechPrevSol = mechanicsAssembler_->prevSol();
         const auto& bulkGridGeometry = *bulkGridGeometry_;
         const auto& bulkElem1 = bulkGridGeometry.element(bulkElemIdx1);
         const auto& bulkElem2 = bulkGridGeometry.element(bulkElemIdx2);

         auto bulkFvGeometry1 = localView(bulkGridGeometry);
         auto bulkFvGeometry2 = localView(bulkGridGeometry);
         auto bulkCurElemVolVars1 = localView(mechanicsAssembler_->gridVariables().curGridVolVars());
         auto bulkPrevElemVolVars1 = localView(mechanicsAssembler_->gridVariables().prevGridVolVars());
         auto bulkCurElemVolVars2 = localView(mechanicsAssembler_->gridVariables().curGridVolVars());
         auto bulkPrevElemVolVars2 = localView(mechanicsAssembler_->gridVariables().prevGridVolVars());
         auto bulkElemFluxVarsCache1 = localView(mechanicsAssembler_->gridVariables().gridFluxVarsCache());
         auto bulkElemFluxVarsCache2 = localView(mechanicsAssembler_->gridVariables().gridFluxVarsCache());
         
         bulkFvGeometry1.bind(bulkElem1);
         bulkCurElemVolVars1.bind(bulkElem1, bulkFvGeometry1, bulkMechSol);
         bulkPrevElemVolVars1.bind(bulkElem1, bulkFvGeometry1, bulkMechPrevSol);
         bulkElemFluxVarsCache1.bind(bulkElem1, bulkFvGeometry1, bulkCurElemVolVars1);

         bulkFvGeometry2.bind(bulkElem1);
         bulkCurElemVolVars2.bind(bulkElem1, bulkFvGeometry2, bulkMechSol);
         bulkPrevElemVolVars2.bind(bulkElem1, bulkFvGeometry2, bulkMechPrevSol);
         bulkElemFluxVarsCache2.bind(bulkElem1, bulkFvGeometry2, bulkCurElemVolVars2);

         const auto& scvf1 = bulkFvGeometry1.scvf(bulkScvfIdx1);
         const auto& scvf2 = bulkFvGeometry1.scvf(bulkScvfIdx2);
         const auto& fluxVarCache1 = bulkElemFluxVarsCache1[scvf1];
         const auto& fluxVarCache2 = bulkElemFluxVarsCache2[scvf2];
         const auto& shapeValues1 = fluxVarCache1.shapeValues();
         const auto& shapeValues2 = fluxVarCache2.shapeValues();
         
         Dune::FieldVector<Scalar<bulkId>, dimWorld> displ(0.0), rdispl(0.0);
         for (int dir = 0; dir < dimWorld; ++dir)
         {
            for (const auto& scvI : scvs(bulkFvGeometry1))
                displ[dir] += shapeValues1[scvI.indexInElement()]*(bulkCurElemVolVars1[scvI.indexInElement()].displacement(dir)
                                                                   - bulkPrevElemVolVars1[scvI.indexInElement()].displacement(dir)) ;
            for (const auto& scvJ : scvs(bulkFvGeometry2))
                displ[dir] -= shapeValues2[scvJ.indexInElement()]*(bulkCurElemVolVars2[scvJ.indexInElement()].displacement(dir)
                                                                   - bulkPrevElemVolVars2[scvJ.indexInElement()].displacement(dir));
         }

         auto normalToFacet = scvf1.unitOuterNormal();
         Dune::FieldMatrix<Scalar<bulkId>, 2, dimWorld> rotationMatrix(0.0);
         rotationMatrix[0] = normalToFacet;
         rotationMatrix[1][0] = normalToFacet[1];
         rotationMatrix[1][1] = -normalToFacet[0];
         rotationMatrix.mv(displ, rdispl);
        
         return rdispl;
     }

      Scalar<bulkId> updateFluxBulkPorosity(const Element<bulkId>& element,
                                            const SubControlVolume<bulkId>& scv) const
      {
          if(!initialized) return 0.0;

          const auto& prevSol = (*prevIterFluxSol)[bulkId];
          const auto& curSol = (*curFluxSol)[bulkId];
          const auto prevEffPressure = prevSol[scv.dofIndex()][0];
          const auto curEffPressure = curSol[scv.dofIndex()][0];       
          const auto dpress = curEffPressure - prevEffPressure;
          const auto comp = flowAssembler_->problem(bulkId).spatialParams().volumetricCompressibility(element,scv);
          return  comp*dpress;
      }

      Scalar<lowDimId> updateFluxFacetAperture(const Element<lowDimId>& element,
                                               const SubControlVolume<lowDimId>& scv) const
      {
          if(!initialized) return 0.0;

          const auto& prevSol = (*prevIterFluxSol)[lowDimId];
          const auto& curSol = (*curFluxSol)[lowDimId];
          const auto prevEffPressure = prevSol[scv.dofIndex()][0];
          const auto curEffPressure = curSol[scv.dofIndex()][0];       
          const auto dpress = curEffPressure - prevEffPressure;
          const auto comp = flowAssembler_->problem(lowDimId).spatialParams().apertureCompressibility(element,scv);
          return  comp*dpress;
      }
     
      void setBulkVolumetricStrains()
      {
          for(int i = 0; i < volStrainVar_.size(); i++) volStrainVar_[i] = 0.0;
            
          const auto& prevMechSol = mechanicsAssembler_->prevSol();
          for(const auto& element : elements(bulkGridGeometry_->gridView()))
          {
              auto elemFluxVars = localView(mechanicsAssembler_->gridVariables().gridFluxVarsCache());
              auto elemVolVars = localView(mechanicsAssembler_->gridVariables().curGridVolVars());       
              auto fvGeometry = localView(*bulkGridGeometry_);
              fvGeometry.bind(element);
              elemVolVars.bind(element, fvGeometry, *curMechSol);                                                                                               
              elemFluxVars.bind(element, fvGeometry, elemVolVars);
              
              for(const auto& scvf : scvfs(fvGeometry))
              {
                  Dune::FieldVector<Scalar<bulkId>, bulkDim> displ(0.0);
                  const auto shapeValues = elemFluxVars[scvf].shapeValues();
                  const auto normal = scvf.unitOuterNormal();
                  const auto& insideScv = fvGeometry.scv(scvf.insideScvIdx());
                  const auto& outsideScv = fvGeometry.scv(scvf.outsideScvIdx());
                
                  for (int dir = 0; dir < dimWorld; ++dir){
                      for (const auto& scvK : scvs(fvGeometry)){
                          displ[dir] += shapeValues[scvK.indexInElement()]*
                              ((*curMechSol)[scvK.dofIndex()][dir] -
                               prevMechSol[scvK.dofIndex()][dir]);
                      }
                      volStrainVar_[insideScv.dofIndex()] += displ[dir]*normal[dir]*scvf.area();
                      if(!scvf.boundary())
                          volStrainVar_[outsideScv.dofIndex()] -= displ[dir]*normal[dir]*scvf.area();
                  }
              }
          }
          
          for(int i = 0; i < volStrainVar_.size(); i++)
            volStrainVar_[i] /= boxVolumes_[i];   
      }

      void updateBulkPorositiesFromStrains()
      {
          std::set<GridIndexType<bulkId>> assignedDofs;

          for(const auto& element : elements(bulkGridGeometry_->gridView()))
          {
              auto fvGeometry = localView(*bulkGridGeometry_);
              fvGeometry.bind(element);
              for(const auto& scv : scvs(fvGeometry)){
                  if(assignedDofs.find(scv.dofIndex()) != assignedDofs.end()) continue;
                  bulkPorosities_[scv.dofIndex()] = getUpdatedPorosityFromStrains(element, scv);
                  assignedDofs.insert(scv.dofIndex());
              }
          }
          
          assignedDofs.clear();
      }

      Scalar<bulkId> getUpdatedPorosityFromStrains(const Element<bulkId>& element,
                                                   const SubControlVolume<bulkId>& scv)
      {
          const auto prevGridVolVars = flowAssembler_->gridVariables(bulkId).prevGridVolVars();
          const auto curGridVolVars = flowAssembler_->gridVariables(bulkId).curGridVolVars();
          const auto prevFluxSol = flowAssembler_->prevSol()[bulkId];
          const auto& curSol = (*curFluxSol)[bulkId];
          
          auto prevElemVolVars = localView(prevGridVolVars);
          auto curElemVolVars = localView(curGridVolVars);
          auto elemFluxVars = localView(flowAssembler_->gridVariables(bulkId).gridFluxVarsCache());
          auto fvGeometry = localView(*bulkGridGeometry_);
          fvGeometry.bind(element);
          prevElemVolVars.bind(element, fvGeometry, prevFluxSol);
          curElemVolVars.bind(element, fvGeometry, curSol);
          elemFluxVars.bind(element, fvGeometry, curElemVolVars);
          
          const auto biot =  mechanicsAssembler_->problem(bulkId).spatialParams().biotCoefficient(element, fvGeometry, curElemVolVars, elemFluxVars);
          Scalar<bulkId> prevPorosity = prevElemVolVars[scv].porosity();
          Scalar<bulkId> ev = volStrainVar_[scv.dofIndex()];
          Scalar<bulkId> porosity = prevPorosity + biot * ev;
          return porosity;
      }

      void setFacetNormalDisplacementJumps()
      {
          for(int i = 0; i < volStrainVar_.size(); i++) normalDisplJumpVar_[i] = 0.0;

          for(const auto& element : elements(lowDimGridGeometry_->gridView()))
          {
              auto fvGeometry = localView(*lowDimGridGeometry_);
              fvGeometry.bind(element);
              for(const auto& scv : scvs(fvGeometry))
              {
                  const auto& un = facetDisplacementJump(element, scv)[0];
                  normalDisplJumpVar_[scv.dofIndex()] += un * scv.volume(); 
              }                          
          }

          for(int i = 0; i < volStrainVar_.size(); i++) normalDisplJumpVar_[i] /= facetAreas_[i];       
      }

      Scalar<bulkId> volumetricStrain(const Element<bulkId>& element,
                                      const SubControlVolume<bulkId>& scv) const
      {
          const auto& bulkGridGeometry = flowCouplingManager_->problem(bulkId).gridGeometry();        
          const auto eIdx = bulkGridGeometry.elementMapper().index(element);
          return volStrainVar_[scv.dofIndex()];
      }

      void updateFacetAperturesFromStrains()
      {
          std::set<GridIndexType<lowDimId>> assignedDofs;

          for(const auto& element : elements(lowDimGridGeometry_->gridView()))
          {
              auto fvGeometry = localView(*lowDimGridGeometry_);
              fvGeometry.bind(element);
              for(const auto& scv : scvs(fvGeometry)){
                  if(assignedDofs.find(scv.dofIndex()) != assignedDofs.end()) continue;
                  facetApertures_[scv.dofIndex()] = getUpdatedApertureFromStrains(element, scv);
                  assignedDofs.insert(scv.dofIndex());
              }
          }
          
          assignedDofs.clear();
      }

      Scalar<lowDimId> getUpdatedApertureFromStrains(const Element<lowDimId>& element,
                                                     const SubControlVolume<lowDimId>& scv) 
      {
          const auto prevGridVolVars = flowAssembler_->gridVariables(lowDimId).prevGridVolVars();
          const auto curGridVolVars = flowAssembler_->gridVariables(lowDimId).curGridVolVars();
          const auto prevFluxSol = flowAssembler_->prevSol()[lowDimId];
          const auto& curSol = (*curFluxSol)[lowDimId];
          
          auto prevElemVolVars = localView(prevGridVolVars);
          auto curElemVolVars = localView(curGridVolVars);
          auto elemFluxVars = localView(flowAssembler_->gridVariables(lowDimId).gridFluxVarsCache());
          auto fvGeometry = localView(*lowDimGridGeometry_);
          fvGeometry.bind(element);
          prevElemVolVars.bind(element, fvGeometry, prevFluxSol);
          curElemVolVars.bind(element, fvGeometry, curSol);
          elemFluxVars.bind(element, fvGeometry, curElemVolVars);
          
          auto prevAperture = prevElemVolVars[scv].extrusionFactor();
          const auto biot =  mechanicsAssembler_->problem(lowDimId).spatialParams().interfaceBiotCoefficient(element, fvGeometry, curElemVolVars, elemFluxVars);
          auto aperture = prevAperture + biot * normalDisplJumpVar_[scv.dofIndex()];
          return aperture;    
      }
                                           
      Scalar<lowDimId> normalDisplacementJump(const SubControlVolume<lowDimId>& scv) const
      {
          return normalDisplJumpVar_[scv.dofIndex()];
      }

      Scalar<lowDimId> fluxAperture(const Element<lowDimId>& element,
                                    const SubControlVolume<lowDimId>& scv) const
      {
          auto daperPress = updateFluxFacetAperture(element, scv);
          return facetApertures_[scv.dofIndex()] + daperPress;
      }
      
      Scalar<bulkId> fluxPorosity(const Element<bulkId>& element,
                                  const SubControlVolume<bulkId>& scv) const
      {
          auto dporPress = updateFluxBulkPorosity(element, scv);
          return bulkPorosities_[scv.dofIndex()] + dporPress;
      }

      Scalar<lowDimId> aperture(const SubControlVolume<lowDimId>& scv) const
      {
          return facetApertures_[scv.dofIndex()];
      }
      
      Scalar<bulkId> porosity(const SubControlVolume<bulkId>& scv) const
      {
          return bulkPorosities_[scv.dofIndex()];
      }

      void setPorosity(const SubControlVolume<bulkId>& scv,
                       const Scalar<bulkId> porosity)
      {
          bulkPorosities_[scv.dofIndex()] = porosity;
      }
      
      const FlowCouplingManager& flowCouplingManager() const
      {return *flowCouplingManager_;}

      const MechanicsCouplingManager& mechanicsCouplingManager() const
      {return *mechanicsCouplingManager_;}

      void setFluxPrevIterSolution(FluxSolutionVector& sol)
      {
          prevIterFluxSol = &sol;
      }
      
      void setFluxCurrentSolution(FluxSolutionVector& sol)
      {
          curFluxSol = &sol;
      }

      void setMechanicsCurrentSolution(MechanicsSolutionVector& sol)
      {
          curMechSol = &sol;
      }
          
  private:

    std::shared_ptr<FlowCouplingManager> flowCouplingManager_;
    std::shared_ptr<MechanicsCouplingManager> mechanicsCouplingManager_;
    std::shared_ptr<FlowAssembler> flowAssembler_;
    std::shared_ptr<MechanicsAssembler> mechanicsAssembler_;
    std::shared_ptr<const GridGeometry<bulkId>> bulkGridGeometry_;
    std::shared_ptr<const GridGeometry<lowDimId>> lowDimGridGeometry_; 
    std::shared_ptr<CouplingMapper> couplingMapper_;
    std::vector<Scalar<bulkId>> volStrainVar_;
    std::vector<Scalar<lowDimId>> normalDisplJumpVar_;
    std::vector<Scalar<bulkId>> bulkPorosities_;
    std::vector<Scalar<lowDimId>> facetApertures_;
    MechanicsSolutionVector * curMechSol;
    FluxSolutionVector * curFluxSol;
    FluxSolutionVector * prevIterFluxSol;

    bool initialized = false;
    std::vector<Scalar<bulkId>> boxVolumes_;
    std::vector<Scalar<lowDimId>> facetAreas_;
  };
  
}

#endif
