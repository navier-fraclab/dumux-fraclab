// -*- mode: C++; tab-width: 4; indent-tabs-mode: nil; c-basic-offset: 4 -*-
// vi: set et ts=4 sw=4 sts=4:
/*****************************************************************************
 *   See the file COPYING for full copying permissions.                      *
 *                                                                           *
 *   This program is free software: you can redistribute it and/or modify    *
 *   it under the terms of the GNU General Public License as published by    *
 *   the Free Software Foundation, either version 3 of the License, or       *
 *   (at your option) any later version.                                     *
 *                                                                           *
 *   This program is distributed in the hope that it will be useful,         *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of          *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the            *
 *   GNU General Public License for more details.                            *
 *                                                                           *
 *   You should have received a copy of the GNU General Public License       *
 *   along with this program.  If not, see <http://www.gnu.org/licenses/>.   *
 *****************************************************************************/
/*!
 * \file
 * \ingroup FacetCoupling
 * \brief Properties (and default properties) for all models using the box
 *        scheme together with coupling across the grid element facets
 * \note If n is the dimension of the lowest grid to be considered in the hierarchy,
 *       all problem type tags for the grids with the dimension m > n must inherit
 *       from these or other facet coupling properties (e.g. CCTpfaFacetCouplingModel).
 */

#ifndef DUMUX_FRACLAB_FACETCOUPLING_BOX_PROPERTIES_HH
#define DUMUX_FRACLAB_FACETCOUPLING_BOX_PROPERTIES_HH

#include <dune/common/deprecated.hh>
#include <dumux/common/properties.hh>
#include <dumux/multidomain/facet/box/properties.hh>
#include <dumux/discretization/box.hh>
#include <dumux/multidomain/facet/box/fvgridgeometry.hh>
#include <dumux/multidomain/facet/box/upwindscheme.hh>
#include <dumux/porousmediumflow/fluxvariables.hh>
#include <dumux/fraclab/geomechanics/stresshistorycache.hh>
#include <dumux/fraclab/flux/plasticcorrection.hh>

#include "elementboundarytypes.hh"
#include "elasticlaw.hh"
#include "elastoplasticlaw.hh"
#include "poroelasticlaw.hh"
#include "poroplasticlaw.hh"
#include "geomechanicslocalresidual.hh"

namespace Dumux {

namespace Properties {

//! Type tag for the box scheme with coupling to
//! another sub-domain living on the grid facets.
// Create new type tags
namespace TTag {
struct FracLabBoxFacetCouplingModel { using InheritsFrom = std::tuple<BoxFacetCouplingModel>;};
struct GeomechanicsBoxFacetCouplingModel { using InheritsFrom = std::tuple<FracLabBoxFacetCouplingModel>;};
struct ElastoplasticBoxFacetCouplingModel { using InheritsFrom = std::tuple<FracLabBoxFacetCouplingModel>;};
struct PoroelasticBoxFacetCouplingModel { using InheritsFrom = std::tuple<FracLabBoxFacetCouplingModel>;};
struct PoroplasticBoxFacetCouplingModel { using InheritsFrom = std::tuple<FracLabBoxFacetCouplingModel>;};   
} // end namespace TTag


//! Use the box local residual for models with facet coupling
//template<class TypeTag>
//struct BaseLocalResidual<TypeTag, TTag::BoxFacetCouplingModel> { using type = BoxFacetCouplingLocalResidual<TypeTag>; };

// local resisuals 
template<class TypeTag>
struct BaseLocalResidual<TypeTag, TTag::FracLabBoxFacetCouplingModel> { using type = GeomechanicsFacetCouplingLocalResidual<TypeTag>; };
/*
template<class TypeTag>
struct BaseLocalResidual<TypeTag, TTag::ElastoplasticFacetCouplingModel> { using type = GeomechanicsFacetCouplingLocalResidual<TypeTag>; };

template<class TypeTag>
struct BaseLocalResidual<TypeTag, TTag::PoromechanicsBoxFacetCouplingModel> { using type = GeomechanicsFacetCouplingLocalResidual<TypeTag>; };

template<class TypeTag>
struct BaseLocalResidual<TypeTag, TTag::PoroplasticBoxFacetCouplingModel> { using type = GeomechanicsFacetCouplingLocalResidual<TypeTag>; };
*/

// stress types 

template<class TypeTag>
struct StressType<TypeTag, TTag::GeomechanicsBoxFacetCouplingModel>
{
    using type = BoxFacetCouplingElasticLaw< GetPropType<TypeTag, Properties::Scalar>,
                                             GetPropType<TypeTag, Properties::GridGeometry> >;
};

template<class TypeTag>
struct StressType<TypeTag, TTag::PoroelasticBoxFacetCouplingModel>
{
    using type = BoxFacetCouplingPoroElasticLaw< GetPropType<TypeTag, Properties::Scalar>,
                                                 GetPropType<TypeTag, Properties::GridGeometry> >;
};

template<class TypeTag>
struct StressType<TypeTag, TTag::ElastoplasticBoxFacetCouplingModel>
{
private:
    using Scalar = GetPropType<TypeTag, Properties::Scalar>;
    using GridGeometry =  GetPropType<TypeTag, Properties::GridGeometry>;
    using ElasticStressType = HookesLaw< Scalar, GridGeometry >;
public:
    using type = BoxFacetCouplingElastoplasticLaw< TypeTag, Scalar, GridGeometry>;
};


template<class TypeTag>
struct StressType<TypeTag, TTag::PoroplasticBoxFacetCouplingModel>
{
private:
    using Scalar = GetPropType<TypeTag, Properties::Scalar>;
    using GridGeometry =  GetPropType<TypeTag, Properties::GridGeometry>;
    using ElasticStressType = HookesLaw< Scalar, GridGeometry >;
public:
    using type = BoxFacetCouplingPoroPlasticLaw< TypeTag, Scalar, GridGeometry>;
};


// flux variables cache

template<class TypeTag>
struct FluxVariablesCache<TypeTag, TTag::ElastoplasticBoxFacetCouplingModel>
{
    using type = StressHistoryCache< GetPropType<TypeTag, Properties::Scalar>,
                                     GetPropType<TypeTag, Properties::GridGeometry> >;
};

template<class TypeTag>
struct FluxVariablesCache<TypeTag, TTag::PoroplasticBoxFacetCouplingModel>
{
    using type = StressHistoryCache< GetPropType<TypeTag, Properties::Scalar>,
                                     GetPropType<TypeTag, Properties::GridGeometry> >;
};

//! Per default, use the porous medium flow flux variables with the modified upwind scheme
//template<class TypeTag>
//struct FluxVariables<TypeTag, TTag::BoxFacetCouplingModel>
//{
 //   using type = PorousMediumFluxVariables<TypeTag,
                                  // BoxFacetCouplingUpwindScheme<GetPropType<TypeTag, Properties::GridGeometry>>>;
//};

//! Per default, use the porous medium flow flux variables with the modified upwind scheme
template<class TypeTag>
struct ElementBoundaryTypes<TypeTag, TTag::FracLabBoxFacetCouplingModel>
{ using type = FracLabBoxFacetCouplingElementBoundaryTypes<GetPropType<TypeTag, Properties::BoundaryTypes>>; };

//! Set the default for the grid finite volume geometry
//template<class TypeTag>
//struct GridGeometry<TypeTag, TTag::BoxFacetCouplingModel>
//{
//private:
    //static constexpr bool enableCache = getPropValue<TypeTag, Properties::EnableGridGeometryCache>();
    //DUNE_NO_DEPRECATED_BEGIN
    //using GridView = GetPropType<TypeTag, Properties::GridView>;
    //DUNE_NO_DEPRECATED_END
    //using Scalar = GetPropType<TypeTag, Properties::Scalar>;
//public:
    //using type = BoxFacetCouplingFVGridGeometry<Scalar, GridView, enableCache>;
//};

} // namespace Properties
} // namespace Dumux

#endif
