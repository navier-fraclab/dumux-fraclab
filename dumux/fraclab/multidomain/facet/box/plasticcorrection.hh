#ifndef DUMUX_DISCRETIZATION_BOX_FACET_PLASTICCORRECTION_HH
#define DUMUX_DISCRETIZATION_BOX_FACET_PLASTICCORRECTION_HH

#include <dumux/fraclab/common/properties.hh>			   
			
namespace Dumux{
  
template<class Scalar, class GridGeometry>
class FacetReturnBase
{
    using FVElementGeometry = typename GridGeometry::LocalView;
    using SubControlVolumeFace = typename FVElementGeometry::SubControlVolumeFace;

    using GridView = typename GridGeometry::GridView;
    using Element = typename GridView::template Codim<0>::Entity;

    static constexpr int dim = GridView::dimension;
    static constexpr int dimWorld = GridView::dimensionworld;
    static constexpr int numStre = dimWorld;
   
  
public:

    using YieldNormal = Dune::FieldVector<Scalar,numStre>;
    using StressVector = Dune::FieldVector<Scalar,numStre>;
  
    template<class PlasticModel, class ElasticMatrix>
    static Scalar facetLambdaIncrement(const PlasticModel& plasticModel,
                                       const StressVector& stressVector,
                                       const Scalar yieldFunction, 
                                       const ElasticMatrix elasticMatrix,
                                       const YieldNormal af, 
                                       const YieldNormal ag)
    {
        YieldNormal eag;
        elasticMatrix.mv(ag, eag);
        Scalar aea = af.dot(eag);
        Scalar hard = plasticModel.hardeningParameter();
        aea -= hard;
        Scalar dlambda = yieldFunction/aea;

        return dlambda;
    }

    template<class PlasticModel, class ElasticMatrix, class FluxVarsCache>
    static Scalar facetLambdaIncrement(const PlasticModel& plasticModel,
                                       const StressVector& displJump,
                                       const ElasticMatrix& elasticMatrix,
                                       const FluxVarsCache& fluxVarCache)
    {
        StressVector trialStress(0.0);
        elasticMatrix.mv(displJump, trialStress);
        const auto initStress = fluxVarCache.stressVectorAtFace();
        for(int dir = 0; dir < dimWorld; dir++) trialStress[dir] += initStress[dir];
        Scalar yieldFunction = plasticModel.yieldFunction(trialStress);
        if (yieldFunction < 0.0) return 0.0;

        const auto ag = plasticModel.flowFunctionNormal(trialStress);
        const auto af = plasticModel.yieldFunctionNormal(trialStress);

        YieldNormal eag;
        elasticMatrix.mv(ag, eag);
        Scalar aea = af.dot(eag);
        Scalar hard = plasticModel.hardeningParameter();
        aea -= hard;
      
        Scalar dlambda = yieldFunction/aea;

        return dlambda;
    }

    template<class ElasticMatrix>
    static StressVector facetStressIncrement(const Scalar dlambda, const ElasticMatrix elasticMatrix,
					     const YieldNormal ag)
    {
        StressVector dstress;
        elasticMatrix.mv(ag, dstress);
        dstress *= (-dlambda);

        return dstress;
    }

    template<class ElementVolumeVariables, class FluxVarsCache, class PlasticModel, class ElasticMatrix>
    static void returnAlgorithm(const Element& element,
                                const FVElementGeometry& fvGeometry,
                                const ElementVolumeVariables& elemVolVars,
                                const FluxVarsCache& fluxVarCache,
                                const PlasticModel& plasticModel,
                                const ElasticMatrix& elasticMatrix,
                                const StressVector& initStress,
                                StressVector& stress,
                                Scalar yf
                                )
    {
      DUNE_THROW(Dune::NotImplemented, "Please define a valid return algorithm class for the InterfaceReturnAlgorithm property. This class needs to overload the function "
        " returnAlgorithm of class FacetReturnBase");

    }

};

template<class Scalar, class GridGeometry>
class InterfaceMohrCoulombReturn
: public FacetReturnBase<Scalar,GridGeometry>
{
    using FVElementGeometry = typename GridGeometry::LocalView;
    using SubControlVolumeFace = typename FVElementGeometry::SubControlVolumeFace;
    using Parent = FacetReturnBase<Scalar,GridGeometry>;
    using GridView = typename GridGeometry::GridView;
    using Element = typename GridView::template Codim<0>::Entity;

    static constexpr int dim = GridView::dimension;
    static constexpr int dimWorld = GridView::dimensionworld;
    static constexpr int numStre = dimWorld;
   
  
public:

    using YieldNormal = Dune::FieldVector<Scalar,numStre>;
    using StressVector = Dune::FieldVector<Scalar,numStre>;

    template<class ElementVolumeVariables, class FluxVarsCache, class PlasticModel, class ElasticMatrix>
    static void returnAlgorithm(const Element& element,
                                const FVElementGeometry& fvGeometry,
                                const ElementVolumeVariables& elemVolVars,
                                const FluxVarsCache& fluxVarCache,
                                const PlasticModel& plasticModel,
                                const ElasticMatrix& elasticMatrix,
                                const StressVector& initStress,
                                StressVector& stress,
                                Scalar yf
                                )
    {

        if (isAtApex(plasticModel,initStress))
        {
	        returnToApex(plasticModel,stress);
	        return;
        }
        //if at apex, return to apex
        
        const auto aft = plasticModel.yieldFunctionNormal(stress);
        const auto agt = plasticModel.flowFunctionNormal(stress);

        const auto dlambda = Parent::facetLambdaIncrement(plasticModel, stress, yf, elasticMatrix, aft, agt);
        const auto dstress = Parent::facetStressIncrement(dlambda, elasticMatrix, agt);

        stress += dstress;

        const auto af = plasticModel.yieldFunctionNormal(stress);
        auto a = af.dot(aft);
        auto normTrial = aft.two_norm();
        auto norm = af.two_norm();
        auto cosb = a/(normTrial*norm);

        yf = plasticModel.yieldFunction(stress);
        if(cosb < 0.0)
                returnToApex(plasticModel,stress);
    }

    template<class PlasticModel>
    static void returnToApex(const PlasticModel& model,
                             StressVector& stress
                            )
    {
        const auto& cohesion = model.cohesion();
        const auto& frictionAngle = model.frictionAngle();

        stress = 0.0;
        stress[0] = cohesion/tan(frictionAngle);
    }

    template<class PlasticModel>
    static bool isAtApex(const PlasticModel& model,
                         const StressVector& stress
                        )
    {
        const auto& cohesion = model.cohesion();
        const auto& frictionAngle = model.frictionAngle();

        return abs(stress[0]-cohesion/tan(frictionAngle))< 1e-15 and stress[1] == 0.0;
    }

};

}

  #endif
    
