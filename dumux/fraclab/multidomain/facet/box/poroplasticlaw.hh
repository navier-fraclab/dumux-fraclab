/*!
 * \filey
 * \ingroup BoxFlux
 * \brief Specialization of Hooke's law for the box scheme. This computes
 *        the stress tensor and surface forces resulting from mechanical deformation.
 */
#ifndef DUMUX_DISCRETIZATION_BOX_FACET_COUPLING_POROPLASTIC_LAW_HH
#define DUMUX_DISCRETIZATION_BOX_FACET_COUPLING_POROPLASTIC_LAW_HH

#include <dumux/flux/box/hookeslaw.hh>
#include <dumux/fraclab/common/properties.hh>
#include "elastoplasticlaw.hh"
#include "plasticcorrection.hh"

namespace Dumux{
  
template<class TypeTag, class Scalar, class GridGeometry>
class BoxFacetCouplingPoroPlasticLaw
: public BoxFacetCouplingElastoplasticLaw<TypeTag,Scalar,GridGeometry>
{
  using FVElementGeometry = typename GridGeometry::LocalView;
  using SubControlVolumeFace = typename FVElementGeometry::SubControlVolumeFace;
  using GridView = typename GridGeometry::GridView;
  using Element = typename GridView::template Codim<0>::Entity;
  using ParentType = BoxFacetCouplingElastoplasticLaw<TypeTag,Scalar,GridGeometry>;
public:

    //using FacetReturn = GetPropType<TypeTag, Properties::InterfaceReturnAlgorithm>;
    using ElasticStressType = HookesLaw<Scalar, GridGeometry, DiscretizationMethod::box>;
    using StressLaw = PoroPlasticCorrection<TypeTag, ElasticStressType>;
    using StressTensor = typename StressLaw::StressTensor;
    static constexpr int nrow = ElasticStressType::nrow;
    static constexpr int dimWorld = GridView::dimensionworld;
  
    using RotationMatrix = Dune::FieldMatrix<Scalar, 2, dimWorld>;
  
    using FacetTraction = Dune::FieldVector<Scalar, dimWorld>;
    //! export the type used for force vectors
    using ForceVector = Dune::FieldVector<Scalar, dimWorld>;

    template<class Problem, class ElementVolumeVariables, class ElementFluxVarsCache>
    static ForceVector force(const Problem& problem,
                             const Element& element,
                             const FVElementGeometry& fvGeometry,
                             const ElementVolumeVariables& elemVolVars,
                             const SubControlVolumeFace& scvf,
                             const ElementFluxVarsCache& elemFluxVarCache)
    {
        if(!scvf.interiorBoundary())
            return StressLaw::force(problem, element, fvGeometry, elemVolVars, scvf, elemFluxVarCache);
	
        ForceVector facetForce(0.0);
        // obtain cohesive traction in interface element
        const auto traction = interfaceTraction(problem, element, fvGeometry, elemVolVars, elemFluxVarCache[scvf], scvf);
        // rotate stresses to global coord system
        ParentType::ParentType::rotationMatrix.mtv(traction, facetForce);
        //integrate over face
        facetForce *= scvf.area();

        return facetForce;
	
    }

    template<class Problem, class ElementVolumeVariables, class FluxVarsCache>
    static FacetTraction effectiveInterfaceTraction(const Problem& problem,
                                                    const Element& element,
                                                    const FVElementGeometry& fvGeometry,
                                                    const ElementVolumeVariables& elemVolVars,
                                                    const FluxVarsCache& fluxVarCache,
                                                    const SubControlVolumeFace& scvf)
    {
        return ParentType::interfaceTraction(problem, element, fvGeometry, elemVolVars, fluxVarCache, scvf);  
    }


    template<class Problem, class ElementVolumeVariables, class FluxVarsCache>
    static FacetTraction interfaceTraction(const Problem& problem,
                                           const Element& element,
                                           const FVElementGeometry& fvGeometry,
                                           const ElementVolumeVariables& elemVolVars,
                                           const FluxVarsCache& fluxVarCache,
                                           const SubControlVolumeFace& scvf)
    {
        auto traction = effectiveInterfaceTraction(problem,element,fvGeometry,elemVolVars,fluxVarCache,scvf);
        // obtain pore pressures and compute total stress

        const auto elementIdx = problem.gridGeometry().elementMapper().index(element);
        const auto lowDimElemIdx = problem.couplingManager().getLowDimElementIndex(elementIdx, scvf);
        const auto lowDimScvIdx = problem.couplingManager().getCoupledLowDimScvIdx(elementIdx, scvf);

        const auto biotCoeff = problem.spatialParams().interfaceBiotCoefficient(lowDimElemIdx, lowDimScvIdx);
        const auto effPress = problem.interfaceEffectivePorePressure(lowDimElemIdx, lowDimScvIdx);
        const auto bcp = biotCoeff*effPress;
        
        traction[0] -= bcp; //porepressure affects normal stress only
    
        return traction;   
    }

    template<class Problem, class ElementVolumeVariables, class FluxVarsCache>
    static FacetTraction facetCohesiveTraction(const Problem& problem,
                                                const Element& element,
                                                const FVElementGeometry& fvGeometry,
                                                const ElementVolumeVariables& elemVolVars,
                                                const FluxVarsCache& fluxVarCache,
                                                const SubControlVolumeFace& scvf,
                                                RotationMatrix& rotationMatrix,
                                                bool totalStress = true)
    {
        using FacetReturn = GetPropType<TypeTag, Properties::InterfaceReturnAlgorithm>;
        //calculate normal and tangential stresses 
        FacetTraction traction(0.0);
        ForceVector displ(0.0);

        const auto& shapeValues = fluxVarCache.shapeValues();
        const auto& elementIdx = problem.gridGeometry().elementMapper().index(element);
        for (int dir = 0; dir < dimWorld; ++dir)
            for (const auto& scv : scvs(fvGeometry))
                displ[dir] += shapeValues[scv.indexInElement()]*elemVolVars[scv.indexInElement()].displacement(dir);
        
        const auto& lowDimStencil = problem.couplingManager().couplingElementStencil(elementIdx);
        const auto lowDimElemIdx = problem.couplingManager().getLowDimElementIndex(elementIdx, scvf);
        auto it = std::find(lowDimStencil.begin(), lowDimStencil.end(), lowDimElemIdx);
        const auto lowDimPosition = std::distance(lowDimStencil.begin(), it);

        const auto pairBulkIdx = problem.couplingManager().bulkPairElementIdx(elementIdx, lowDimPosition);
        const auto lowDimScvIdx = problem.couplingManager().getCoupledLowDimScvIdx(elementIdx, scvf);
        const auto scvfIndices = problem.couplingManager().getCoincidingBulkScvfs(lowDimElemIdx, pairBulkIdx);
        const auto pairScvfIdx = scvfIndices[lowDimScvIdx];        
        const auto& pairVolVars = problem.couplingManager().getPairBulkVolVars(elementIdx, lowDimPosition);
        const auto& pairFluxVars = problem.couplingManager().getPairBulkFluxVarsCache(elementIdx, lowDimPosition);
        const auto& pairFvGeometry = problem.couplingManager().getPairFvGeometry(elementIdx, lowDimPosition);
        const auto& pairScvf = pairFvGeometry.scvf(pairScvfIdx);
        const auto& pairFluxVarsCache = pairFluxVars[pairScvf];
        const auto& pairShapeValues = pairFluxVarsCache.shapeValues();
        // pairFvGeometry.bind(pairBulk);

        ForceVector pairDispl(0.0);
        for (int dir = 0; dir < dimWorld; ++dir)
            for (const auto& scv : scvs(pairFvGeometry))
                pairDispl[dir] += pairShapeValues[scv.indexInElement()]*pairVolVars[scv.indexInElement()].displacement(dir);
        
        displ -= pairDispl;
        
        //obtain normal and tangent displr
        ForceVector rdispl(0.0);
        rotationMatrix.mv(displ, rdispl);

        const auto& spatialParams = problem.spatialParams();
        const auto& stiffMatrix = spatialParams.interfaceElasticMatrix(element, scvf, fvGeometry, elemVolVars, fluxVarCache, rdispl);

        stiffMatrix.mv(rdispl, traction);	
        traction *= -1.0;

        auto initTractionVector = fluxVarCache.stressVectorAtFace();
        ForceVector initTraction(0.0);
        for(int i = 0; i < dimWorld; i++)
        {
          traction[i] += initTractionVector[i];
          initTraction[i] = initTractionVector[i];
        }

	
        const auto yf = spatialParams.interfaceYieldFunction(element, fvGeometry, traction);
        const auto tol = StressLaw::getYieldFunctionTolerance();

        if(yf > tol)
          FacetReturn::returnAlgorithm(element, fvGeometry,elemVolVars,fluxVarCache,spatialParams, stiffMatrix,initTraction, traction, yf);
        //return stress variation

      /*
        if(!NumericDifferentiation::jacobianAssembly and initialized){
          const auto eIdx = problem.gridGeometry().elementMapper().index(element);
          facetEffTractions_[eIdx][scvf.index()] = traction;
        }
          */
        traction -= initTraction;

        if(totalStress)
        {
          const auto biotCoeff = problem.spatialParams().interfaceBiotCoefficient(element, fvGeometry, elemVolVars, fluxVarCache);
          const auto effPress = problem.interfaceEffectivePorePressure(lowDimElemIdx, lowDimScvIdx);
          const auto bcp = biotCoeff*effPress;

          //porepressure affects normal stress only
          traction[0] -= bcp;
        }
        
              return traction;
    }

    template<class Problem, class ElementVolumeVariables, class FluxVarsCache>
    static FacetTraction updatedFacetEffectiveTraction(const Problem& problem,
						       const Element& element,
						       const FVElementGeometry& fvGeometry,
						       const ElementVolumeVariables& elemVolVars,
						       const FluxVarsCache& fluxVarCache,
						       const SubControlVolumeFace& scvf)
    {
     using FacetReturn = GetPropType<TypeTag, Properties::InterfaceReturnAlgorithm>;
        auto normalToFacet = scvf.unitOuterNormal();

        //obtain rotation matrix
        RotationMatrix rotationMatrix(0.0);
        rotationMatrix[0] = normalToFacet;
        rotationMatrix[1][0] = normalToFacet[1];
        rotationMatrix[1][1] = -normalToFacet[0];
	
        //calculate normal and tangential stresses 
        FacetTraction traction(0.0);
        ForceVector displ(0.0);

        const auto& shapeValues = fluxVarCache.shapeValues();
        const auto& elementIdx = problem.gridGeometry().elementMapper().index(element);
        for (int dir = 0; dir < dimWorld; ++dir)
            for (const auto& scv : scvs(fvGeometry))
                displ[dir] += shapeValues[scv.indexInElement()]*elemVolVars[scv.indexInElement()].displacement(dir);
        
        const auto& lowDimStencil = problem.couplingManager().couplingElementStencil(elementIdx);
        const auto lowDimElemIdx = problem.couplingManager().getLowDimElementIndex(elementIdx, scvf);
        auto it = std::find(lowDimStencil.begin(), lowDimStencil.end(), lowDimElemIdx);
        const auto lowDimPosition = std::distance(lowDimStencil.begin(), it);

        const auto pairBulkIdx = problem.couplingManager().bulkPairElementIdx(elementIdx, lowDimPosition);
        const auto lowDimScvIdx = problem.couplingManager().getCoupledLowDimScvIdx(elementIdx, scvf);
        const auto scvfIndices = problem.couplingManager().getCoincidingBulkScvfs(lowDimElemIdx, pairBulkIdx);
        const auto pairScvfIdx = scvfIndices[lowDimScvIdx];        
        const auto& pairVolVars = problem.couplingManager().getPairBulkVolVars(elementIdx, lowDimPosition);
        const auto& pairFluxVars = problem.couplingManager().getPairBulkFluxVarsCache(elementIdx, lowDimPosition);
        const auto& pairFvGeometry = problem.couplingManager().getPairFvGeometry(elementIdx, lowDimPosition);
        const auto& pairScvf = pairFvGeometry.scvf(pairScvfIdx);
        const auto& pairFluxVarsCache = pairFluxVars[pairScvf];
        const auto& pairShapeValues = pairFluxVarsCache.shapeValues();
        // pairFvGeometry.bind(pairBulk);

        ForceVector pairDispl(0.0);
        for (int dir = 0; dir < dimWorld; ++dir)
            for (const auto& scv : scvs(pairFvGeometry))
                pairDispl[dir] += pairShapeValues[scv.indexInElement()]*pairVolVars[scv.indexInElement()].displacement(dir);
        
        displ -= pairDispl;
        
        //obtain normal and tangent displr
        ForceVector rdispl(0.0);
        rotationMatrix.mv(displ, rdispl);

        const auto& spatialParams = problem.spatialParams();
        const auto& stiffMatrix = spatialParams.interfaceElasticMatrix
              (element, scvf, fvGeometry, elemVolVars, fluxVarCache, rdispl);

              stiffMatrix.mv(rdispl, traction);	
        traction *= -1.0;

	auto initTractionVector = fluxVarCache.stressVectorAtFace();
	ForceVector initTraction(0.0);
	for(int i = 0; i < dimWorld; i++)
	{
	  traction[i] += initTractionVector[i];
	  initTraction[i] = initTractionVector[i];
	}

	
	const auto yf = spatialParams.interfaceYieldFunction(element, fvGeometry, traction);
	const auto tol = StressLaw::getYieldFunctionTolerance();

	if(yf > tol)
	  FacetReturn::returnAlgorithm(element, fvGeometry,elemVolVars,fluxVarCache,spatialParams, stiffMatrix,initTraction, traction, yf);
	//return stress variation

        return traction;
    }
  
  static void init(const GridGeometry& gridGeometry)
  {
      const auto numElems = gridGeometry.gridView().size(0);
       
      facetEffTractions_.resize(numElems);
    
      for(const auto& element : elements(gridGeometry.gridView()))
      {
          const auto eIdx = gridGeometry.elementMapper().index(element);
          auto fvGeometry = localView(gridGeometry);
          fvGeometry.bind(element);
          facetEffTractions_[eIdx].resize(fvGeometry.numScvf());
       }

      initialized = true;

  }

  static inline std::vector<std::vector<ForceVector>> facetEffTractions_;
  static inline bool initialized = false;
    
};
}
#endif
