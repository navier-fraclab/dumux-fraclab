// -*- mode: C++; tab-width: 4; indent-tabs-mode: nil; c-basic-offset: 4 -*-
// vi: set et ts=4 sw=4 sts=4:
/*****************************************************************************
 *   See the file COPYING for full copying permissions.                      *
 *                                                                           *
 *   This program is free software: you can redistribute it and/or modify    *
 *   it under the terms of the GNU General Public License as published by    *
 *   the Free Software Foundation, either version 3 of the License, or       *
 *   (at your option) any later version.                                     *
 *                                                                           *
 *   This program is distributed in the hope that it will be useful,         *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of          *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the            *
 *   GNU General Public License for more details.                            *
 *                                                                           *
 *   You should have received a copy of the GNU General Public License       *
 *   along with this program.  If not, see <http://www.gnu.org/licenses/>.   *
 *****************************************************************************/
/*!
 * \file
 * \ingroup FacetCoupling
 * \copydoc Dumux::FacetCouplingManager
 */
#ifndef DUMUX_BOX_INTERFACECOUPLING_MANAGER_HH
#define DUMUX_BOX_INTERFACECOUPLING_MANAGER_HH

#include <dumux/multidomain/facet/box/couplingmanager.hh>
#include <dumux/discretization/box/boxgeometryhelper.hh>
/*!
 * \ingroup FacetCoupling
 * \brief Manages the coupling between two bulk elements that share an interface element for which the coupling 
 * \ depends on the properties of the interface element */
namespace Dumux{

  template<class MDTraits, class CouplingMapper, std::size_t bulkDomainId = 0, std:: size_t lowDimDomainId = 1>
  class InterfaceBoxCouplingManager
  : public virtual CouplingManager< MDTraits >
  {
    using ParentType = CouplingManager< MDTraits >;
    
     // convenience aliases and instances of the two domain ids
    using BulkIdType = typename MDTraits::template SubDomain<bulkDomainId>::Index;
    using LowDimIdType = typename MDTraits::template SubDomain<lowDimDomainId>::Index;
    static constexpr auto bulkId = BulkIdType();
    static constexpr auto lowDimId = LowDimIdType();

    // the sub-domain type tags
    template<std::size_t id> using SubDomainTypeTag = typename MDTraits::template SubDomain<id>::TypeTag;
    
    // Grid related types. These can be either from bulk or low dimensional domain
    template<std::size_t id> using GridGeometry = GetPropType<SubDomainTypeTag<id>, Properties::GridGeometry>;
    template<std::size_t id> using FVElementGeometry = typename GridGeometry<id>::LocalView;
    template<std::size_t id> using SubControlVolume = typename GridGeometry<id>::SubControlVolume;
    template<std::size_t id> using SubControlVolumeFace = typename GridGeometry<id>::SubControlVolumeFace;
    template<std::size_t id> using GridView = typename GridGeometry<id>::GridView;
    template<std::size_t id> using Element = typename GridView<id>::template Codim<0>::Entity;
    template<std::size_t id> using GridIndexType = typename GridView<id>::IndexSet::IndexType;
    template<std::size_t id> using GeometryHelper = BoxGeometryHelper<GridView<id>, GridView<id>::dimension, SubControlVolume<id>, SubControlVolumeFace<id>>;
    template<std::size_t id> using GlobalPosition = typename Dune::FieldVector<GetPropType<SubDomainTypeTag<id>,Properties::Scalar>, GridView<bulkId>::dimensionworld>;
    // further types specific to the bulk domain (facets are not used in the problem solution)
    using BulkTag = SubDomainTypeTag<bulkId>;

    using PrimaryVariables = GetPropType<BulkTag, Properties::PrimaryVariables>;
    using Problem = GetPropType<BulkTag, Properties::Problem>;
    using NumEqVector = GetPropType<BulkTag, Properties::NumEqVector>;
    using ElementBoundaryTypes = GetPropType<BulkTag, Properties::ElementBoundaryTypes>;
    using LocalResidual = GetPropType<BulkTag, Properties::LocalResidual>;
    using GridVariables = GetPropType<BulkTag, Properties::GridVariables>;
    using GridVolumeVariables = typename GridVariables::GridVolumeVariables;
    using ElementVolumeVariables = typename GridVolumeVariables::LocalView;
    using VolumeVariables = typename ElementVolumeVariables::VolumeVariables;
    using GridFluxVariablesCache = typename GridVariables::GridFluxVariablesCache;
    using ElementFluxVariablesCache = typename GridFluxVariablesCache::LocalView;   
    using Scalar = GetPropType<BulkTag, Properties::Scalar>;
    
    // extract corresponding grid ids from the mapper
    static constexpr int bulkDim = GridView<bulkDomainId>::dimension;
    static constexpr int lowDimDim = GridView<lowDimDomainId>::dimension;
    static constexpr auto bulkGridId = CouplingMapper::template gridId<bulkDim>();
    static constexpr auto lowDimGridId = CouplingMapper::template gridId<lowDimDim>();
    static constexpr int dimWorld = GridView<bulkDomainId>::dimensionworld;
    static constexpr bool volVarsCache = GridVolumeVariables::cachingEnabled;

    using RotationMatrix = Dune::FieldMatrix<Scalar,dimWorld,dimWorld>;

    //TODO: return error if discr model is not box
    static constexpr bool lowDimUsesBox = GridGeometry<lowDimId>::discMethod == DiscretizationMethod::box;

    struct BulkCouplingContext
    {
        static constexpr int bulkDimWorld = GridView<bulkId>::dimensionworld;
        static constexpr bool bulkIsSurfaceGrid = bulkDim != bulkDimWorld;

        bool isSet;
        GridIndexType< bulkId > elementIdx;
        std::vector< GridIndexType<bulkId>> bulkPairsIdx;
        std::vector< FVElementGeometry<lowDimId> > lowDimFvGeometries;
        std::vector<std::unique_ptr<ElementVolumeVariables>>  bulkElemVolVars;
        std::vector<std::unique_ptr<ElementFluxVariablesCache>>  bulkElemFluxVarsCache;
        std::vector<std::unique_ptr<FVElementGeometry<bulkId>>>  bulkFvGeometries;
        
        void reset()
        {
            lowDimFvGeometries.clear();
            bulkPairsIdx.clear();
            auto resetPtr = [&] (auto&& uniquePtr) { uniquePtr.reset(); };
            std::for_each(bulkFvGeometries.begin(), bulkFvGeometries.end(), resetPtr);
            std::for_each(bulkElemVolVars.begin(), bulkElemVolVars.end(), resetPtr);
            std::for_each(bulkElemFluxVarsCache.begin(), bulkElemFluxVarsCache.end(), resetPtr);
            bulkElemVolVars.clear();
            bulkElemFluxVarsCache.clear();
            bulkFvGeometries.clear();
            isSet = false;
        }
    };
public:

    //! types used for coupling stencils
    template<std::size_t i, std::size_t j = (i == bulkId) ? lowDimId : bulkId>
    using CouplingStencilType = typename CouplingMapper::template Stencil< CouplingMapper::template gridId<GridView<j>::dimension>() >;
            
    //! the type of the solution vector
    using SolutionVector = typename MDTraits::SolutionVector;

     /*!
     * \brief Initialize the coupling manager.
     *
     * \param bulkProblem The problem to be solved on the bulk domain
     * \param lowDimProblem The problem to be solved on the lower-dimensional domain
     * \param couplingMapper The mapper object containing the connectivity between the domains
     * \param curSol The current solution
     */
    void init(std::shared_ptr< Problem> bulkProblem,
              std::shared_ptr< GridGeometry<lowDimId> > lowDimGridGeometry,
              std::shared_ptr< CouplingMapper > couplingMapper,
              const SolutionVector& curSol)
    {
        couplingMapperPtr_ = couplingMapper;

        // set the sub problems
        this->setSubProblem(bulkProblem, bulkId);
        //this->setSubProblem(lowDimProblem, lowDimId);
        lowDimGridGeometry_ = lowDimGridGeometry;
        // copy the solution vector
        ParentType::updateSolution(curSol);

        // determine all bulk elements that couple to low dim elements
        const auto& bulkMap = couplingMapperPtr_->couplingMap(bulkGridId, lowDimGridId);
        bulkElemIsCoupled_.assign(bulkProblem->gridGeometry().gridView().size(0), false);
        std::for_each( bulkMap.begin(),
                       bulkMap.end(),
                       [&] (const auto& entry) { bulkElemIsCoupled_[entry.first] = true; });
    }

    const CouplingStencilType<bulkId>& couplingStencil(BulkIdType domainI,
                                                       const Element<bulkId>& element,
                                                       LowDimIdType domainJ) const
    {
        const auto eIdx = this->problem(domainI).gridGeometry().elementMapper().index(element);

        if (bulkElemIsCoupled_[eIdx])
        {
            const auto& map = couplingMapperPtr_->couplingMap(bulkGridId, lowDimGridId);
            auto it = map.find(eIdx);
            assert(it != map.end());
            return it->second.couplingStencil;
        }

        return getEmptyStencil(lowDimId);
    }

     /*!
     * \brief The coupling stencil of the lower-dimensional domain with the bulk domain.
     */
    const CouplingStencilType<lowDimId>& couplingStencil(LowDimIdType domainI,
                                                         const Element<lowDimId>& element,
                                                         BulkIdType domainJ) const
    {
        const auto eIdx = lowDimGridGeometry_->elementMapper().index(element);
        const auto& map = couplingMapperPtr_->couplingMap(lowDimGridId, bulkGridId);
        auto it = map.find(eIdx);
        if (it != map.end()) return it->second.couplingStencil;
        else return getEmptyStencil(bulkId);
    }

     /*!
     * \brief returns true if a bulk scvf flux depends on data in the facet domain.
     * \note for box this is the case if an scvf lies on an interior boundary
     */
    bool isCoupled(const Element<bulkId>& element,
                   const SubControlVolumeFace<bulkId>& scvf) const
    { return scvf.interiorBoundary(); }
      
     /*!
     * \brief returns true if a bulk scvf coincides with a facet element.
     * \note for box, this is always true for coupled scvfs
     */
     const auto coupledGlobalDofsIdx(GridIndexType<bulkId> bulkElemIdx,
                                     GridIndexType<lowDimId> lowDimElemIdx) const
    {
        std::vector<GridIndexType<bulkId>> globalDofsIdx;
        
        const auto& bulkElement = this->problem(bulkId).gridGeometry().element(bulkElemIdx);
        const auto& bulkGridGeometry = this->problem(bulkId).gridGeometry();
        auto fvGeometry = localView(bulkGridGeometry);
        fvGeometry.bindElement(bulkElement);
        
        const auto coupledScvfIndices = getCoincidingBulkScvfs(lowDimElemIdx, bulkElemIdx);
        
        for(const auto scvfIdx : coupledScvfIndices)
        {
            const auto& scvf = fvGeometry.scvf(scvfIdx);
            const auto& scv = fvGeometry.scv(scvf.insideScvIdx());
            globalDofsIdx.emplace_back(scv.dofIndex());
        }
        
        return globalDofsIdx;
    }

    const auto coupledScvLocalIdx(GridIndexType<bulkId> bulkElemIdx,
                                  GridIndexType<lowDimId> lowDimElemIdx) const
    {
        std::vector<GridIndexType<bulkId>> scvIdx;
        
        const auto& bulkElement = this->problem(bulkId).gridGeometry().element(bulkElemIdx);
        const auto& bulkGridGeometry = this->problem(bulkId).gridGeometry();
        auto fvGeometry = localView(bulkGridGeometry);
        fvGeometry.bindElement(bulkElement);
        
        const auto coupledScvfIndices = getCoincidingBulkScvfs(lowDimElemIdx, bulkElemIdx);
        
        for(const auto scvfIdx : coupledScvfIndices)
        {
            const auto& scvf = fvGeometry.scvf(scvfIdx);
            scvIdx.emplace_back(scvf.insideScvIdx());
        }
        
        return scvIdx;
    }
      
    bool isOnInteriorBoundary(const Element<bulkId>& element,
                              const SubControlVolumeFace<bulkId>& scvf) const
    { return isCoupled(element, scvf); }

    const auto& getCoincidingBulkScvfs(GridIndexType<lowDimId> lowDimElemIdx,
                                       GridIndexType<bulkId> bulkElemIdx) const
    {
        const auto& map = couplingMapperPtr_->couplingMap(bulkGridId, lowDimGridId);
        auto it = map.find(bulkElemIdx);  

        const auto& scvfMap = it -> second.elementToScvfMap;
        auto itm = scvfMap.find(lowDimElemIdx);
        
        const auto& coincidingScvfs = itm->second;
        
        return coincidingScvfs;
    }

    
    bool isCoupled(const GridIndexType<bulkId>& eIdx) const
    {
        return bulkElemIsCoupled_[eIdx]; 
    }
    
    const auto& bulkPairsList(const GridIndexType<bulkId>& eIdx) const
    {
        assert(bulkContext_.isSet);
        assert(eIdx == bulkContext_.elementIdx);

        return bulkContext_.bulkPairsIdx;     
    }

    const Element<bulkId> bulkPairElement(const GridIndexType<bulkId>& refElementIdx,
                                          unsigned int position) const
    {
        assert(bulkContext_.isSet);
        assert(refElementIdx == bulkContext_.elementIdx);
        const auto eIdx = bulkContext_.bulkPairsIdx[position];
        const auto& pairBulk = this->problem(bulkId).gridGeometry().element(eIdx);   
        return pairBulk;   
    }

    const GridIndexType<bulkId> bulkPairElementIdx(const GridIndexType<bulkId>& refElementIdx,
                                                   unsigned int position) const
    {
        assert(bulkContext_.isSet);
        assert(refElementIdx == bulkContext_.elementIdx);
        const auto eIdx = bulkContext_.bulkPairsIdx[position];
        return eIdx;
    }

    const FVElementGeometry<bulkId>& getPairFvGeometry(const GridIndexType<bulkId>& eIdx,
                                                       unsigned int embedmentPosition) const
    {
       assert(bulkContext_.isSet);
       assert(bulkElemIsCoupled_[eIdx]);

       return *(bulkContext_.bulkFvGeometries[embedmentPosition]); 
    }

    const auto getBulkPairScvfShapeValues(const Element<bulkId>& element,
                                         unsigned int embedmentPosition,
                                         unsigned int pairScvfIdx) const
    {
       assert(bulkContext_.isSet);
       assert(this->problem(bulkId).gridGeometry().elementMapper().index(element) == bulkContext_.elementIdx);
       const auto& elemFluxVars = *(bulkContext_.bulkElemFluxVarsCache[embedmentPosition]);
       const auto& gridGeometry = this->problem(bulkId).gridGeometry();
       const auto& pairBulk = bulkPairElement(element, embedmentPosition);
       auto pairFvGeometry = localView(gridGeometry);
       pairFvGeometry.bind(pairBulk);
       const auto pairScvf = pairFvGeometry.scvf(pairScvfIdx);
       const auto center = pairScvf.center();
       const auto& fluxVarCache = elemFluxVars[pairScvf];
       //    std::cout<< pairScvf.interiorBoundary() << " " << center[0] << " " << center[1] << "\n"<<std::endl;
       const auto& pairShapeValues = fluxVarCache.shapeValues();

       return pairShapeValues;
    }

   const ElementVolumeVariables& getPairBulkVolVars(const GridIndexType<bulkId>& eIdx,
                                                            unsigned int embedmentPosition) const
   {
       assert(bulkContext_.isSet);
       assert(bulkElemIsCoupled_[eIdx]);

       return *(bulkContext_.bulkElemVolVars[embedmentPosition]); 
   }

   const ElementFluxVariablesCache& getPairBulkFluxVarsCache(const GridIndexType<bulkId>& eIdx,
                                                                    unsigned int embedmentPosition) const
   {
       assert(bulkContext_.isSet);
       assert(bulkElemIsCoupled_[eIdx]);

       return *(bulkContext_.bulkElemFluxVarsCache[embedmentPosition]); 
   }

    const ElementVolumeVariables& getPairBulkVolVars(const Element<lowDimId>& lowDimElem,
                                                             const Element<bulkId>& bulkElem) const
    {
        auto bulkElemIdx = this->problem(bulkId).gridGeometry().elementMapper().index(bulkElem);
        auto lowDimElemIdx = lowDimGridGeometry_->elementMapper().index(lowDimElem);
        
        if(bulkContext_.isSet){
            if(bulkElemIdx  == bulkContext_.elementIdx){
                
                const auto& map = couplingMapperPtr_->couplingMap(bulkGridId, lowDimGridId);
                auto itb = map.find(bulkElemIdx); assert(itb != map.end());
                const auto& elementStencil = itb->second.couplingElementStencil;
                auto itl = std::find(elementStencil.begin(), elementStencil.end(), lowDimElemIdx);
                const auto lowDimELemPosition = std::distance(elementStencil.begin(), itl);
                return *(bulkContext_.bulkElemVolVars[lowDimELemPosition]);              
            }
        }
        // else error
    }

    unsigned int getCoupledLowDimScvIdx(const GridIndexType<bulkId> eIdx,
                                        const SubControlVolumeFace<bulkId>& scvf) const
    {
        assert(bulkContext_.isSet);
        assert(bulkElemIsCoupled_[eIdx]);

        const auto& map = couplingMapperPtr_->couplingMap(bulkGridId, lowDimGridId);
        const auto& couplingData = map.find(eIdx)->second;

        // search the low dim element idx this scvf is embedded in
        // and find out the local index of the scv it couples to
        unsigned int coupledScvIdx;
        auto it = std::find_if( couplingData.elementToScvfMap.begin(),
                                couplingData.elementToScvfMap.end(),
                                [&] (auto& dataPair)
                                {
                                    const auto& scvfList = dataPair.second;
                                    auto it = std::find(scvfList.begin(), scvfList.end(), scvf.index());
                                    coupledScvIdx = std::distance(scvfList.begin(), it);
                                    return it != scvfList.end();
                                } );

        assert(it != couplingData.elementToScvfMap.end());

        return coupledScvIdx;
    }

     const SubControlVolumeFace<bulkId> getCoupledBulkScvf(GridIndexType<lowDimId> lowDimElemIdx,
                                                           GridIndexType<bulkId> bulkElemIdx,
                                                           unsigned int scvIdx) const
    {
        const auto coincidingScvfsIndices = getCoincidingBulkScvfs(lowDimElemIdx, bulkElemIdx);
        const auto scvfIdx = coincidingScvfsIndices[scvIdx];
        const auto& gridGeometry = this->problem(bulkId).gridGeometry();
        const auto& bulkElem = this->problem(bulkId).gridGeometry().element(bulkElemIdx);
        auto fvGeometry = localView(gridGeometry);
        fvGeometry.bind(bulkElem);
        const auto& scvf = fvGeometry.scvf(scvfIdx);
        return scvf;
        //return scvfIdx;
    }

    
    /*!
     * \brief returns the lower-dimensional element coinciding with a bulk scvf.
     */
    const Element<lowDimId> getLowDimElement(const GridIndexType<bulkId>& elementIdx,
                                             const SubControlVolumeFace<bulkId>& scvf) const
    {
        const auto lowDimElemIdx = getLowDimElementIndex(elementIdx, scvf);
        const auto& lowDimElem = lowDimGridGeometry_->element(lowDimElemIdx);
        return lowDimElem;
    }

    const GlobalPosition<lowDimId> getLowDimElementTangent(const Element<lowDimId>& element,
                                                           const  unsigned int scvIdx) const
    {
        GeometryHelper<lowDimId> geometryHelper(element.geometry());
        auto scvfCorners = geometryHelper.getScvfCorners(scvIdx);
        std::vector<unsigned int> scvIndices;
        scvIndices.push_back(0);
        return geometryHelper.normal(scvfCorners, scvIndices);
    }

        /*!
     * \brief returns the index of the lower-dimensional element coinciding with a bulk scvf.
     */
    const GridIndexType<lowDimId> getLowDimElementIndex(const GridIndexType<bulkId>& eIdx,
                                                        const SubControlVolumeFace<bulkId>& scvf) const
    {
        assert(bulkElemIsCoupled_[eIdx]);
        const auto& map = couplingMapperPtr_->couplingMap(bulkGridId, lowDimGridId);
        const auto& couplingData = map.at(eIdx);

        // search the low dim element idx this scvf is embedded in
        auto it = std::find_if( couplingData.elementToScvfMap.begin(),
                                couplingData.elementToScvfMap.end(),
                                [&] (auto& dataPair)
                                {
                                    const auto& scvfList = dataPair.second;
                                    auto it = std::find(scvfList.begin(), scvfList.end(), scvf.index());
                                    return it != scvfList.end();
                                } );

        assert(it != couplingData.elementToScvfMap.end());
        return it->first;
    }

    //! The bulk domain extended jacobian pattern
    template<class JacobianPattern>
    void extendJacobianPattern(JacobianPattern& pattern) const
    {
        const auto& map = couplingMapperPtr_->couplingMap(lowDimGridId, bulkGridId);

        for(auto const& facet : map)
        {
            const auto& embedments = facet.second.embedments;
            const auto facetIdx = facet.first;
            const auto firstBulkIdx = embedments[0].first;
            const auto secondBulkIdx = embedments[1].first;
            const auto firstCoupledDofs = coupledGlobalDofsIdx(firstBulkIdx, facetIdx);
            const auto secondCoupledDofs = coupledGlobalDofsIdx(secondBulkIdx, facetIdx);

            for(auto const& dof1 : firstCoupledDofs){
                for(auto const& dof2 : secondCoupledDofs) {
                    pattern.add(dof1, dof2);
                    pattern.add(dof2, dof1);
                }
            }

        }
        
    }
      
    template<class LocalAssembler>
    typename LocalResidual::ElementResidualVector
    evalCouplingResidual(const LocalAssembler& localAssembler,
                         GridIndexType<bulkId> dofIdxGlobal)
    {
      const auto& fvGeometry = localAssembler.fvGeometry();
      typename LocalResidual::ElementResidualVector res(fvGeometry.numScv());
      res = 0.0;
      return res;
    }

    const CouplingStencilType<bulkId>& couplingElementStencil(const GridIndexType<bulkId>& elementIdx) const 
    {
        const auto& map = couplingMapperPtr_->couplingMap(bulkGridId, lowDimGridId);
        auto it = map.find(elementIdx); assert(it != map.end());
        const auto& elementStencil = it->second.couplingElementStencil;

        return elementStencil;
    }

    template< class Assembler >
    void bindCouplingContext(const Element<bulkId>& element, const Assembler& assembler)
    {
        // clear context
        bulkContext_.reset();

        // set index in context in any case
        const auto bulkElemIdx = this->problem(bulkId).gridGeometry().elementMapper().index(element);
        bulkContext_.elementIdx = bulkElemIdx;

        // if element is coupled, actually set the context
        if (bulkElemIsCoupled_[bulkElemIdx])
        {
            const auto& bulkSolution = Assembler::isImplicit() ? this->curSol() : assembler.prevSol();
            
            const auto& map = couplingMapperPtr_->couplingMap(bulkGridId, lowDimGridId);

            auto it = map.find(bulkElemIdx); assert(it != map.end());
            const auto& elementStencil = it->second.couplingElementStencil;
            bulkContext_.lowDimFvGeometries.reserve(elementStencil.size());
            bulkContext_.bulkElemVolVars.reserve(elementStencil.size());//!
            bulkContext_.bulkElemFluxVarsCache.reserve(elementStencil.size());
            bulkContext_.bulkPairsIdx.reserve(elementStencil.size());
            
            // local view on the bulk fv geometry
            auto bulkFvGeometry = localView(this->problem(bulkId).gridGeometry());
            bulkFvGeometry.bindElement(element);

            for (const auto lowDimElemIdx : elementStencil)
            {
                const auto& ldGridGeometry = *lowDimGridGeometry_;
                const auto elemJ = ldGridGeometry.element(lowDimElemIdx);
                
                auto fvGeom = localView(ldGridGeometry);
                fvGeom.bindElement(elemJ);                                                                             

                bulkContext_.isSet = true;
                bulkContext_.lowDimFvGeometries.emplace_back( std::move(fvGeom) );

                const auto& map = couplingMapperPtr_->couplingMap(lowDimGridId, bulkGridId);
                auto it = map.find(lowDimElemIdx);
                const auto& embedments = it->second.embedments;
                const auto numEmbedments = embedments.size();
                //std::vector<std::unique_ptr<ElementVolumeVariables<bulkId>> > bulkVolVars;
                            
                for (unsigned int i = 0; i < numEmbedments; ++i){

                    const auto curBulkElem = this->problem(bulkId).gridGeometry().element(embedments[i].first);
                    const auto curBulkElemIdx = this->problem(bulkId).gridGeometry().elementMapper().index(curBulkElem);
		    
                    if(bulkElemIdx == curBulkElemIdx) continue;
		    
                    auto bulkFvGeom = localView(this->problem(bulkId).gridGeometry());
                    bulkFvGeom.bind(curBulkElem);
                    auto curBulkElemVolVars = localView(assembler.gridVariables().curGridVolVars());
                    auto curBulkElemFluxVarsCache = localView(assembler.gridVariables().gridFluxVarsCache());
		    
                    curBulkElemVolVars.bind(curBulkElem, bulkFvGeom, bulkSolution);
                    curBulkElemFluxVarsCache.bind(curBulkElem, bulkFvGeom, curBulkElemVolVars);

                    bulkContext_.bulkFvGeometries.emplace_back(std::make_unique< FVElementGeometry<bulkId> >(std::move(bulkFvGeom)));
		            bulkContext_.bulkPairsIdx.emplace_back(curBulkElemIdx);
                    bulkContext_.bulkElemVolVars.emplace_back(std::make_unique< ElementVolumeVariables>(std::move(curBulkElemVolVars)));
                    bulkContext_.bulkElemFluxVarsCache.emplace_back(std::make_unique< ElementFluxVariablesCache>(std::move(curBulkElemFluxVarsCache)));
                }       
            }
        }
    }

    void bindCouplingContext(const Element<bulkId>& element, 
                             GridVariables& gridVariables,
                             const SolutionVector& bulkSolution)
    {
        // clear context
        bulkContext_.reset();

        // set index in context in any case
        const auto bulkElemIdx = this->problem(bulkId).gridGeometry().elementMapper().index(element);
        bulkContext_.elementIdx = bulkElemIdx;

        // if element is coupled, actually set the context
        if (bulkElemIsCoupled_[bulkElemIdx])
        {
            const auto& map = couplingMapperPtr_->couplingMap(bulkGridId, lowDimGridId);

            auto it = map.find(bulkElemIdx); assert(it != map.end());
            const auto& elementStencil = it->second.couplingElementStencil;
            bulkContext_.lowDimFvGeometries.reserve(elementStencil.size());
            bulkContext_.bulkElemVolVars.reserve(elementStencil.size());//!
            bulkContext_.bulkElemFluxVarsCache.reserve(elementStencil.size());
            bulkContext_.bulkPairsIdx.reserve(elementStencil.size());
            
            // local view on the bulk fv geometry
            auto bulkFvGeometry = localView(this->problem(bulkId).gridGeometry());
            bulkFvGeometry.bindElement(element);

            for (const auto lowDimElemIdx : elementStencil)
            {
                const auto& ldGridGeometry = *lowDimGridGeometry_;
                const auto elemJ = ldGridGeometry.element(lowDimElemIdx);
                
                auto fvGeom = localView(ldGridGeometry);
                fvGeom.bindElement(elemJ);                                                                             

                bulkContext_.isSet = true;
                bulkContext_.lowDimFvGeometries.emplace_back( std::move(fvGeom) );

                const auto& map = couplingMapperPtr_->couplingMap(lowDimGridId, bulkGridId);
                auto it = map.find(lowDimElemIdx);
                const auto& embedments = it->second.embedments;
                const auto numEmbedments = embedments.size();
                //std::vector<std::unique_ptr<ElementVolumeVariables<bulkId>> > bulkVolVars;
                            
                for (unsigned int i = 0; i < numEmbedments; ++i){

                    const auto curBulkElem = this->problem(bulkId).gridGeometry().element(embedments[i].first);
                    const auto curBulkElemIdx = this->problem(bulkId).gridGeometry().elementMapper().index(curBulkElem);
		    
                    if(bulkElemIdx == curBulkElemIdx) continue;
		    
                    auto bulkFvGeom = localView(this->problem(bulkId).gridGeometry());
                    bulkFvGeom.bind(curBulkElem);
                    auto curBulkElemVolVars = localView(gridVariables.curGridVolVars());
                    auto curBulkElemFluxVarsCache = localView(gridVariables.gridFluxVarsCache());
		    
                    curBulkElemVolVars.bind(curBulkElem, bulkFvGeom, bulkSolution);
                    curBulkElemFluxVarsCache.bind(curBulkElem, bulkFvGeom, curBulkElemVolVars);

                    bulkContext_.bulkFvGeometries.emplace_back(std::make_unique< FVElementGeometry<bulkId> >(std::move(bulkFvGeom)));
		            bulkContext_.bulkPairsIdx.emplace_back(curBulkElemIdx);
                    bulkContext_.bulkElemVolVars.emplace_back(std::make_unique< ElementVolumeVariables>(std::move(curBulkElemVolVars)));
                    bulkContext_.bulkElemFluxVarsCache.emplace_back(std::make_unique< ElementFluxVariablesCache>(std::move(curBulkElemFluxVarsCache)));
                }       
            }
        }
    }
    
    template<class BulkLocalAssembler, class T = BulkTag, typename std::enable_if_t<!getPropValue<T, Properties::EnableGridVolumeVariablesCache>(), int> = 0>
    void updateCouplingContext(const BulkLocalAssembler& localAssembler,
                               GridVariables& gridVariables,
                               GridIndexType<bulkId> dofIdxGlobalJ,
                               const PrimaryVariables& priVarsJ,
                               unsigned int pvIdxJ,
                               unsigned int embedmentIdx)
    {
        const auto element = localAssembler.element();
        const auto eIdx = this->problem(bulkId).gridGeometry().elementMapper().index(element);
        const auto pairBulk = bulkPairElement(eIdx, embedmentIdx);

        auto& curSol_ = this->curSol();
        curSol_[dofIdxGlobalJ][pvIdxJ] = priVarsJ[pvIdxJ];
        auto& fvGeometry = *(bulkContext_.bulkFvGeometries[embedmentIdx]);		    
        auto& elemVolVars = *(bulkContext_.bulkElemVolVars[embedmentIdx]);
        elemVolVars.bindElement(pairBulk, fvGeometry, curSol_);	
    }


    template<class BulkLocalAssembler, class T = BulkTag, typename std::enable_if_t<getPropValue<T, Properties::EnableGridVolumeVariablesCache>(), int> = 0>
    void updateCouplingContext(const BulkLocalAssembler& localAssembler,
                               GridVariables& gridVariables,
                               GridIndexType<bulkId> dofIdxGlobalJ,
                               const PrimaryVariables& priVarsJ,
                               unsigned int pvIdxJ,
                               unsigned int embedmentIdx)
    {
        const auto element = localAssembler.element();
        const auto eIdx = this->problem(bulkId).gridGeometry().elementMapper().index(element);
        const auto pairBulk = bulkPairElement(eIdx, embedmentIdx);
        const auto eIdxPair = this->problem(bulkId).gridGeometry().elementMapper().index(pairBulk);

        auto& curSol_ = this->curSol();
        curSol_[dofIdxGlobalJ][pvIdxJ] = priVarsJ[pvIdxJ];
        auto& fvGeometry = *(bulkContext_.bulkFvGeometries[embedmentIdx]);		    
        auto& elemVolVars = *(bulkContext_.bulkElemVolVars[embedmentIdx]);
        elemVolVars.bindElement(pairBulk, fvGeometry, curSol_);	

        auto& gridVolVars = gridVariables.curGridVolVars();
        auto elemSol = elementSolution(pairBulk, curSol_, fvGeometry.gridGeometry());
        for (auto&& scv : scvs(fvGeometry)){
            auto&& volVars = gridVolVars.volVars(eIdxPair, scv.indexInElement()); 
            volVars.update(elemSol, localAssembler.problem(), element, scv);    
        }
    }

    const auto interfaceJacobian(GridIndexType<lowDimId> eIdx, GridIndexType<lowDimId> scvIdx) const 
    {
        const auto& element = lowDimGridGeometry_->element(eIdx);
        auto fvGeometry = localView(*lowDimGridGeometry_);
        fvGeometry.bind(element);
        const auto& scv = fvGeometry.scv(scvIdx);
        const auto scvCenter = scv.geometry().center();

        const auto& ipLocal = element.geometry().local(scvCenter);
        const auto jac = element.geometry().jacobianTransposed(ipLocal);
        return jac;
    }

    /*
       Compute rotation matrix of interface element
       according to formulation in Cerfontaine et al (2015):
       Cerfontaine B, AC Dieudonné, Radu JP, Collin FP, Charlier R. 2015. 3D zero-thickness coupled interface finite element: 
       Formulation and application. Computers and Geotechnics 69:124-140
    */

    const RotationMatrix interfaceRotationMatrix(const SubControlVolumeFace<bulkId>& scvf,
                                                  const GridIndexType<lowDimId> lowDimElemIdx,
                                                  const GridIndexType<lowDimId> lowDimScvIdx) const
    {
        const auto faceNormal = scvf.unitOuterNormal();
        auto facetJac = this->interfaceJacobian(lowDimElemIdx, lowDimScvIdx);
        const auto normJac = facetJac[0].two_norm();
        facetJac /= normJac; // first tangential direction

        RotationMatrix rot(0.0);
        rot[0] = faceNormal;
        rot[1] = facetJac[0];

        // if 3d, compute second tangent direction
        if(dimWorld == 3){
            const auto sec_tangent = Dumux::crossProduct(faceNormal,facetJac[0]);
            rot[2] = sec_tangent;
        }
        return rot;

    }

    //! Empty stencil to be returned for elements that aren't coupled
    template<std::size_t id, std::enable_if_t<(id == bulkId || id == lowDimId), int> = 0>
    const typename CouplingMapper::template Stencil<id>&
    getEmptyStencil(Dune::index_constant<id>) const
    { return std::get<(id == bulkId ? 0 : 1)>(emptyStencilTuple_); }

    const CouplingMapper& couplingMapper()
    {return *couplingMapperPtr_;}

    const GridGeometry<lowDimId>& lowDimGridGeometry() const
    {return *lowDimGridGeometry_;}
      
    protected:

      std::shared_ptr<CouplingMapper> couplingMapperPtr_;
      std::shared_ptr< GridGeometry<lowDimId> > lowDimGridGeometry_;
      //! store bools for all bulk elements that indicate if they
      //! are coupled, so that we don't have to search in the map every time
      std::vector<bool> bulkElemIsCoupled_;
      
      //! empty stencil to return for non-coupled elements
      using BulkStencil = typename CouplingMapper::template Stencil<bulkId>;
      using LowDimStencil = typename CouplingMapper::template Stencil<lowDimId>;
      std::tuple<BulkStencil, LowDimStencil> emptyStencilTuple_;
      
      //! The coupling context
      BulkCouplingContext bulkContext_;
      
  };
}
#endif
