#ifndef DUMUX_MULTIDOMAIN_GEOMECHANICS_PERIODIC_BOX_ASSEMBLER_HH
#define DUMUX_MULTIDOMAIN_GEOMECHANICS_PERIODIC_BOX_ASSEMBLER_HH

#include "geomechanicsassembler.hh"

namespace Dumux {

template<class TypeTag, class CMType, class PCMType, DiffMethod diffMethod, bool useImplicitAssembly = true>
class MultiDomainPeriodicGeomechanicsAssembler
{
    static constexpr DiscretizationMethod discMethod = GetPropType<TypeTag, Properties::GridGeometry>::discMethod;
    static_assert(discMethod == DiscretizationMethod::box, "MultiDomainGeomechanicsAssembler not implemented for methods other than Box ");

public:

    using Scalar = GetPropType<TypeTag, Properties::Scalar>;
    using LocalResidual = GetPropType<TypeTag, Properties::LocalResidual>;
    using GridVariables = GetPropType<TypeTag, Properties::GridVariables>;
    using GridGeometry = GetPropType<TypeTag, Properties::GridGeometry>;
    using Problem = GetPropType<TypeTag, Properties::Problem>;
    using JacobianMatrix = GetPropType<TypeTag, Properties::JacobianMatrix>;
    using SolutionVector = GetPropType<TypeTag, Properties::SolutionVector>;      
    using ResidualType = SolutionVector;
    using CouplingManager = CMType;
    using PeriodicCouplingManager = PCMType;

    /*!
     * \brief Returns true if the assembler considers implicit assembly.
     */
    static constexpr bool isImplicit()
    { return useImplicitAssembly; }

protected:

    using TimeLoop = TimeLoopBase<Scalar>;
    using ThisType = MultiDomainPeriodicGeomechanicsAssembler<TypeTag, CouplingManager, PeriodicCouplingManager,diffMethod, isImplicit()>;
    using LocalAssembler = BulkInterfaceBoxLocalAssembler<TypeTag, ThisType, diffMethod, isImplicit()>;

public:

    /*!
     * \brief The constructor for stationary problems
     * \note the grid variables might be temporarily changed during assembly (if caching is enabled)
     *       it is however guaranteed that the state after assembly will be the same as before
     */
    MultiDomainPeriodicGeomechanicsAssembler(std::shared_ptr<const Problem> problem,
                                            std::shared_ptr<const GridGeometry> gridGeometry,
                                            std::shared_ptr<GridVariables> gridVariables,
                                            std::shared_ptr<CouplingManager> couplingManager,
                                            std::shared_ptr<PeriodicCouplingManager> periodicCouplingManager)
    : problem_(problem)
    , gridGeometry_(gridGeometry)
    , gridVariables_(gridVariables)
    , couplingManager_(couplingManager)
    , periodicCouplingManager_(periodicCouplingManager)
    , timeLoop_()
    , isStationaryProblem_(true)
    {
        static_assert(isImplicit(), "Explicit assembler for stationary problem doesn't make sense!");
        std::cout << "Instantiated assembler for a stationary problem." << std::endl;
    }

    /*!
     * \brief The constructor for instationary problems
     * \note the grid variables might be temporarily changed during assembly (if caching is enabled)
     *       it is however guaranteed that the state after assembly will be the same as before
     */
    MultiDomainPeriodicGeomechanicsAssembler(std::shared_ptr<const Problem> problem,
                                            std::shared_ptr<const GridGeometry> gridGeometry,
                                            std::shared_ptr<GridVariables> gridVariables,
                                            std::shared_ptr<CouplingManager> couplingManager,
                                            std::shared_ptr<PeriodicCouplingManager> periodicCouplingManager,
                                            std::shared_ptr<const TimeLoop> timeLoop,
                                            const SolutionVector& prevSol)
    : problem_(problem)
    , gridGeometry_(gridGeometry)
    , gridVariables_(gridVariables)
    , couplingManager_(couplingManager)
    , periodicCouplingManager_(periodicCouplingManager)
    , timeLoop_(timeLoop)
    , prevSol_(&prevSol)
    , isStationaryProblem_(false)
    {
        std::cout << "Instantiated assembler for an instationary problem." << std::endl;
    }

     /*!
     * \brief Assembles the global Jacobian of the residual
     *        and the residual for the current solution.
     */
    void assembleJacobianAndResidual(const SolutionVector& curSol)
    {
        checkAssemblerState_();
        resetJacobian_();
        resetResidual_();

        assemble_([&](const auto& element)
        {
            LocalAssembler localAssembler(*this, element, curSol, *couplingManager_);
            localAssembler.assembleJacobianAndResidual(*jacobian_, *residual_, this-> gridVariables());
        });

        if(periodicCouplingManager_)
            periodicCouplingManager_-> enforceSystemPeriodicConstraints(*jacobian_, *residual_, curSol);
    }

    //! compute the residuals using the internal residual
    void assembleResidual(const SolutionVector& curSol)
    {
        resetResidual_();
        assembleResidual(*residual_, curSol);
    }

    //! assemble a residual r
    void assembleResidual(ResidualType& r, const SolutionVector& curSol)
    {
        checkAssemblerState_();

        // update the grid variables for the case of active caching
        updateGridVariables(curSol);

        assemble_([&](const auto& element)
        {
             LocalAssembler localAssembler(*this, element, curSol, *couplingManager_);
             localAssembler.assembleResidual(r);
        });	
    }

    //! compute the residual and return it's vector norm
    //! TODO: this needs to be adapted in parallel
    Scalar residualNorm(const SolutionVector& curSol)
    {
        ResidualType residual;
        setResidualSize(residual);
        assembleResidual(residual, curSol);

        // calculate the square norm of the residual
        const Scalar result2 = residual.two_norm2();
        using std::sqrt;
        return sqrt(result2);
    }

    /*!
     * \brief Tells the assembler which jacobian and residual to use.
     *        This also resizes the containers to the required sizes and sets the
     *        sparsity pattern of the jacobian matrix.
     */
    void setLinearSystem(std::shared_ptr<JacobianMatrix> A,
                         std::shared_ptr<SolutionVector> r)
    {
        jacobian_ = A;
        residual_ = r;

        setJacobianBuildMode(*jacobian_);
        setJacobianPattern(*jacobian_);
        setResidualSize(*residual_);
    }

    /*!
     * \brief The version without arguments uses the default constructor to create
     *        the jacobian and residual objects in this assembler if you don't need them outside this class
     */
    void setLinearSystem()
    {
        jacobian_ = std::make_shared<JacobianMatrix>();
        residual_ = std::make_shared<SolutionVector>();

        setJacobianBuildMode(*jacobian_);
        setJacobianPattern(*jacobian_);
        setResidualSize(*residual_);
    }

    /*!
     * \brief Sets the jacobian build mode
     */
    void setJacobianBuildMode(JacobianMatrix& jac) const
    {
        jac.setBuildMode(JacobianMatrix::random);	
    }

    /*!
     * \brief Sets the jacobian sparsity pattern.
     */
    void setJacobianPattern(JacobianMatrix& jac) const
    {
      const auto numDofs = this->numDofs();
      jac.setSize(numDofs, numDofs);

      auto occupationPattern = getJacobianPattern<isImplicit()>(this->gridGeometry());
      couplingManager_->extendJacobianPattern(occupationPattern);
      if(periodicCouplingManager_)
            periodicCouplingManager_->extendJacobianPattern(occupationPattern);
      occupationPattern.exportIdx(jac);
    }

    /*!
     * \brief Resizes the residual
     */
    void setResidualSize(SolutionVector& res) const
    {
        res.resize(this->numDofs());
    }

    /*!
     * \brief Updates the grid variables with the given solution
     */
    void updateGridVariables(const SolutionVector& curSol)
    {
      this->gridVariables().update(curSol);
    }

    /*!
     * \brief Resets the grid variables to the last time step
     */
    void resetTimeStep(const SolutionVector& curSol)
    {
      this->gridVariables().resetTimeStep(curSol);
    }

    std::size_t numDofs() const
    { return gridGeometry_->numDofs(); }

    const auto& problem() const
    { return *problem_; }

    const auto& gridGeometry() const
    { return *gridGeometry_; }

    const auto& gridView() const
    { return gridGeometry().gridView(); }

    GridVariables& gridVariables()
    { return *gridVariables_; }

    //! the grid variables of domain i
    const GridVariables& gridVariables() const
    { return *gridVariables_; }

    //! the coupling manager
    const CouplingManager& couplingManager() const
    { return *couplingManager_; }

    //! the full Jacobian matrix
    JacobianMatrix& jacobian()
    { return *jacobian_; }

    //! the full residual vector
    SolutionVector& residual()
    { return *residual_; }

    //! the solution of the previous time step
    const SolutionVector& prevSol() const
    { return *prevSol_; }

    /*!
     * \brief Set time loop for instationary problems
     * \note calling this turns this into a stationary assembler
     */
    void setTimeManager(std::shared_ptr<const TimeLoop> timeLoop)
    { timeLoop_ = timeLoop; isStationaryProblem_ = !(static_cast<bool>(timeLoop)); }

    /*!
     * \brief Sets the solution from which to start the time integration. Has to be
     *        called prior to assembly for time-dependent problems.
     */
    void setPreviousSolution(const SolutionVector& u)
    { prevSol_ = &u; }

    /*!
     * \brief Whether we are assembling a stationary or instationary problem
     */
    bool isStationaryProblem() const
    { return isStationaryProblem_; }

    LocalResidual localResidual() const
    { return LocalResidual(problem_.get(), timeLoop_.get()); }

protected:
    // reset the residual vector to 0.0
    void resetResidual_()
    {
        if(!residual_)
        {
            residual_ = std::make_shared<SolutionVector>();
            setResidualSize(*residual_);
        }

        (*residual_) = 0.0;
    }

    // reset the jacobian vector to 0.0
    void resetJacobian_()
    {
        if(!jacobian_)
        {
            jacobian_ = std::make_shared<JacobianMatrix>();
            setJacobianBuildMode(*jacobian_);
            setJacobianPattern(*jacobian_);
        }

       (*jacobian_)  = 0.0;
    }

    // check if the assembler is in a correct state for assembly
    void checkAssemblerState_() const
    {
        if (!isStationaryProblem_ && !prevSol_)
            DUNE_THROW(Dune::InvalidStateException, "Assembling instationary problem but previous solution was not set!");

        if (isStationaryProblem_ && prevSol_)
            DUNE_THROW(Dune::InvalidStateException, "Assembling stationary problem but a previous solution was set."
                                                    << " Did you forget to set the timeLoop to make this problem instationary?");
    }


    /*!
     * \brief A method assembling something per element
     * \note Handles exceptions for parallel runs
     * \throws NumericalProblem on all processes if something throwed during assembly
     * TODO: assemble in parallel
     */
    template<class AssembleElementFunc>
    void assemble_(AssembleElementFunc&& assembleElement) const
    {
        // let the local assembler add the element contributions
        for (const auto& element : elements(gridView()))
            assembleElement(element);
    }

    
      //! pointer to the problem to be solved
    std::shared_ptr<const Problem> problem_;

    //! the finite volume geometry of the grid
    std::shared_ptr<const GridGeometry> gridGeometry_;

    //! the variables container for the grid
    std::shared_ptr<GridVariables> gridVariables_;
    
    //! the coupling manager coupling the sub domains
    std::shared_ptr<CouplingManager> couplingManager_;
    //! the variables container for the grid
    std::shared_ptr<PeriodicCouplingManager> periodicCouplingManager_;

    //! the time loop for instationary problem assembly
    std::shared_ptr<const TimeLoop> timeLoop_;

    //! an observing pointer to the previous solution for instationary problems
    const SolutionVector* prevSol_ = nullptr;

    //! if this assembler is assembling an instationary problem
    bool isStationaryProblem_;

    //! shared pointers to the jacobian matrix and residual
    
    std::shared_ptr<JacobianMatrix> jacobian_;
    std::shared_ptr<SolutionVector> residual_;

};
}
#endif