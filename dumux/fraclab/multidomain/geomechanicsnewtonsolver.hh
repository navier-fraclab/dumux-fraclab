// -*- mode: C++; tab-width: 4; indent-tabs-mode: nil; c-basic-offset: 4 -*-
// vi: set et ts=4 sw=4 sts=4:
/****************************************************************************
 *   See the file COPYING for full copying permissions.                      *
 *                                                                           *
 *   This program is free software: you can redistribute it and/or modify    *
 *   it under the terms of the GNU General Public License as published by    *
 *   the Free Software Foundation, either version 3 of the License, or       *
 *   (at your option) any later version.                                     *
 *                                                                           *
 *   This program is distributed in the hope that it will be useful,         *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of          *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the            *
 *   GNU General Public License for more details.                            *
 *                                                                           *
 *   You should have received a copy of the GNU General Public License       *
 *   along with this program.  If not, see <http://www.gnu.org/licenses/>.   *
 *****************************************************************************/
/*!
 * \file
 * \ingroup Nonlinear
 * \ingroup MultiDomain
 * \copydoc Dumux::MultiDomainNewtonSolver
 */
#ifndef DUMUX_MULTIDOMAIN_INTERFACE_NEWTON_SOLVER_HH
#define DUMUX_MULTIDOMAIN_INTERFACE_NEWTON_SOLVER_HH

#include <memory>
#include <dumux/nonlinear/newtonsolver.hh>

namespace Dumux {

namespace Detail {

template<class Assembler, class Index>
using DetectPVSwitchMultiDomain = typename Assembler::template GridVariables<Index::value>::VolumeVariables::PrimaryVariableSwitch;

template<class Assembler, std::size_t i>
using GetPVSwitchMultiDomain = Dune::Std::detected_or<int, DetectPVSwitchMultiDomain, Assembler, Dune::index_constant<i>>;

} // end namespace Detail

/*!
 * \ingroup Nonlinear
 * \ingroup MultiDomain
 * \brief Newton sover for coupled problems
 */
template <class Assembler, class LinearSolver, class CouplingManager,
          class Reassembler = DefaultPartialReassembler,
          class Comm = Dune::CollectiveCommunication<Dune::MPIHelper::MPICommunicator> >
class MultiDomainInterfaceNewtonSolver: public NewtonSolver<Assembler, LinearSolver, Reassembler, Comm>
{
    using ParentType = NewtonSolver<Assembler, LinearSolver, Reassembler, Comm>;
    using SolutionVector = typename Assembler::ResidualType;
public:

    /*!
     * \brief The constructor
     */
    MultiDomainInterfaceNewtonSolver(std::shared_ptr<Assembler> assembler,
                                     std::shared_ptr<LinearSolver> linearSolver,
                                     std::shared_ptr<CouplingManager> couplingManager,
                                     const Comm& comm = Dune::MPIHelper::getCollectiveCommunication(),
                                     const std::string& paramGroup = "")
    : ParentType(assembler, linearSolver, comm, paramGroup),
      couplingManager_(couplingManager)
    {
        //    std::cout << comm.size() <<std::endl;
    };

    /*!
     * \brief Indicates the beginning of a Newton iteration.
     */
    void newtonBeginStep(const SolutionVector& uCurrentIter) override
    {
      ParentType::newtonBeginStep(uCurrentIter);
      couplingManager_->updateSolution(uCurrentIter);
    }


    /*!
     * \brief Indicates that one Newton iteration was finished.
     *
     * \param uCurrentIter The solution after the current Newton iteration
     * \param uLastIter The solution at the beginning of the current Newton iteration
     */
    void newtonEndStep(SolutionVector &uCurrentIter, const SolutionVector &uLastIter) override
    {
      ParentType::newtonEndStep(uCurrentIter, uLastIter);
      couplingManager_->updateSolution(uCurrentIter);
    }

private:

  /*!
   * \brief Reset the privar switch state, noop if there is no priVarSwitch
   */
  /*template<std::size_t i>
  void initPriVarSwitch_(SolutionVector&, Dune::index_constant<i> id, std::false_type) {}

  template<std::size_t i>
  void initPriVarSwitch_(SolutionVector& sol, Dune::index_constant<i> id, std::true_type)
  {
  };

  template<class SubSol, std::size_t i>
  void invokePriVarSwitch_(SubSol&, Dune::index_constant<i> id, std::false_type) {}
 
  */
  /*  template<class SubSol, std::size_t i>
  void invokePriVarSwitch_(SubSol& uCurrentIter, Dune::index_constant<i> id, std::true_type)
  {
  };*/
  
  //! the coupling manager
  std::shared_ptr<CouplingManager> couplingManager_;
};

} // end namespace Dumux

#endif
