// -*- mode: C++; tab-width: 4; indent-tabs-mode: nil; c-basic-offset: 4 -*-
// vi: set et ts=4 sw=4 sts=4:
/*****************************************************************************
 *   See the file COPYING for full copying permissions.                      *
 *                                                                           *
 *   This program is free software: you can redistribute it and/or modify    *
 *   it under the terms of the GNU General Public License as published by    *
 *   the Free Software Foundation, either version 3 of the License, or       *
 *   (at your option) any later version.                                     *
 *                                                                           *
 *   This program is distributed in the hope that it will be useful,         *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of          *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the            *
 *   GNU General Public License for more details.                            *
 *                                                                           *
 *   You should have received a copy of the GNU General Public License       *
 *   along with this program.  If not, see <http://www.gnu.org/licenses/>.   *
 *****************************************************************************/
#ifndef DUMUX_FRACLAB_PROPERTIES_HH
#define DUMUX_FRACLAB_PROPERTIES_HH
 
// explicitly guard the include so that the property system
// header doesn't need to be opened and checked all the time
#ifndef DUMUX_PROPERTY_SYSTEM_HH
#include <dumux/common/properties/propertysystem.hh>
#include <dumux/common/properties.hh>
// per default, we do not allow the old property macros
// remove this after release 3.2
#ifndef DUMUX_ENABLE_OLD_PROPERTY_MACROS
#define DUMUX_ENABLE_OLD_PROPERTY_MACROS 0
#endif
 
// remove this after release 3.2 to remove macros completely
#if DUMUX_ENABLE_OLD_PROPERTY_MACROS
#include <dumux/common/properties/propertysystemmacros.hh>
#endif // DUMUX_ENABLE_OLD_PROPERTY_MACROS
#endif // DUMUX_PROPERTY_SYSTEM_HH
 
#include <dumux/fraclab/flux/returnalgorithm.hh>
#include <dumux/fraclab/multidomain/facet/box/plasticcorrection.hh>

namespace Dumux {
namespace Properties {

//Properties used by elastoplastic models

template<class TypeTag, class MyTypeTag>
struct ReturnAlgorithm { 
private:
    using Scalar = GetPropType<TypeTag, Properties::Scalar>;
    using GridGeometry =  GetPropType<TypeTag, Properties::GridGeometry>;
public:
    using type = Dumux::ReturnAlgorithmBase<Scalar,GridGeometry>;
};    

template<class TypeTag, class MyTypeTag>
struct InterfaceReturnAlgorithm { 
private:
    using Scalar = GetPropType<TypeTag, Properties::Scalar>;
    using GridGeometry =  GetPropType<TypeTag, Properties::GridGeometry>;
public:
    using type = Dumux::FacetReturnBase<Scalar,GridGeometry>;
};    

// Propeties for assembler

template<class TypeTag, class MyTypeTag>
struct LocalTangentMatrix { using type = UndefinedProperty; };    

template<class TypeTag, class MyTypeTag>
struct HydroMechanicalCouplingManager { using type = UndefinedProperty; };    

}
}
#endif