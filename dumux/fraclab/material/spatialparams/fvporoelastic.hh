// -*- mode: C++; tab-width: 4; indent-tabs-mode: nil; c-basic-offset: 4 -*-
// vi: set et ts=4 sw=4 sts=4:
/*****************************************************************************
 *   See the file COPYING for full copying permissions.                      *
 *                                                                           *
 *   This program is free software: you can redistribute it and/or modify    *
 *   it under the terms of the GNU General Public License as published by    *
 *   the Free Software Foundation, either version 3 of the License, or       *
 *   (at your option) any later version.                                     *
 *                                                                           *
 *   This program is distributed in the hope that it will be useful,         *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of          *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the            *
 *   GNU General Public License for more details.                            *
 *                                                                           *
 *   You should have received a copy of the GNU General Public License       *
 *   along with this program.  If not, see <http://www.gnu.org/licenses/>.   *
 *****************************************************************************/
/*!
 * \file
 * \ingroup SpatialParameters
 * \brief The class for spatial parameters for hydromechanical
 * elastic problems coupled with the fixed stress split 
 * (it demands the drained bulk modulus)
 */
#ifndef DUMUX_FRACLAB_POROELASTIC_FV_SPATIAL_PARAMS_HH
#define DUMUX_FRACLAB_POROELASTIC_FV_SPATIAL_PARAMS_HH

#include <memory>

#include <dune/common/exceptions.hh>
#include <dumux/common/parameters.hh>
#include <dumux/common/typetraits/isvalid.hh>
#include <dumux/material/spatialparams/fvporoelastic.hh>

namespace Dumux {

#ifndef DOXYGEN
namespace Detail {

template<class GlobalPosition>
struct hasBiotModulusAtPos
{
    template<class SpatialParams>
    auto operator()(const SpatialParams& a)
    -> decltype(a.biotModulusAtPos(std::declval<GlobalPosition>()))
    {}
};

template<class GlobalPosition>
struct hasBulkModulusAtPos
{
    template<class SpatialParams>
    auto operator()(const SpatialParams& a)
    -> decltype(a.bulkModulusAtPos(std::declval<GlobalPosition>()))
    {}
};

} // end namespace Detail
#endif

/*!
 * \ingroup SpatialParameters
 * \brief The base class for spatial parameters of linear elastic geomechanical problems
 */
template<class Scalar, class GridGeometry, class Implementation>
class FracLabSpatialParamsPoroelastic
: public FVSpatialParamsPoroElastic<Scalar, GridGeometry, Implementation>
{
    using ParentType = FVSpatialParamsPoroElastic<Scalar, GridGeometry, Implementation>;
    using FVElementGeometry = typename GridGeometry::LocalView;
    using SubControlVolume = typename GridGeometry::SubControlVolume;
    using SubControlVolumeFace = typename GridGeometry::SubControlVolumeFace;
    using GridView = typename GridGeometry::GridView;
    using Element = typename GridView::template Codim<0>::Entity;
    using GlobalPosition = typename Element::Geometry::GlobalCoordinate;
    enum { dimWorld = GridView::dimensionworld };

public:
    //! The constructor
    FracLabSpatialParamsPoroelastic(std::shared_ptr<const GridGeometry> gridGeometry)
    : ParentType(gridGeometry)
    {
    }

    /*!
     * \brief Returns the Biot coefficient in an element
     * \note  This is possibly solution dependent and is evaluated
     *        for an integration point inside the element. Therefore,
     *        a flux variables cache object is passed to this function
     *        containing data on shape functions at the integration point.
     *
     * \param element The current element
     * \param fvGeometry The local finite volume geometry
     * \param elemVolVars Primary/Secondary variables inside the element
     * \param fluxVarsCache Contains data on shape functions at the integration point
     * \return Biot coefficient
     */
    template<class ElemVolVars, class FluxVarsCache>
    Scalar biotCoefficient(const Element& element,
                           const FVElementGeometry& fvGeometry,
                           const ElemVolVars& elemVolVars,
                           const FluxVarsCache& fluxVarsCache) const
    {
        static_assert(decltype(isValid(Detail::hasBiotCoeffAtPos<GlobalPosition>())(this->asImp_()))::value," \n\n"
        "   Your spatial params class has to either implement\n\n"
        "         const LameParams& biotCoefficientAtPos(const GlobalPosition& globalPos) const\n\n"
        "   or overload this function\n\n"
        "         template<class ElementSolution>\n"
        "         const LameParams& biotCoefficient(const Element& element,\n"
        "                                      const FVElementGeometry& fvGeometry,\n"
        "                                      const ElemVolVars& elemVolVars,\n"
        "                                      const FluxVarsCache& fluxVarsCache) const\n\n");

        return asImp_().biotCoefficientAtPos(fluxVarsCache.ipGlobal());
    }

    template<class ElemVolVars, class FluxVarsCache>
    decltype(auto) biotModulus(const Element& element,
                               const SubControlVolume& scv,
                               const FVElementGeometry& fvGeometry,
                               const ElemVolVars& elemVolVars,
                               const FluxVarsCache& fluxVarsCache) const
    {
        static_assert(decltype(isValid(Detail::hasBiotModulusAtPos<GlobalPosition>())(this->asImp_()))::value," \n\n"
        "   Your spatial params class has to either implement\n\n"
        "         const biotModulusAtPos(const GlobalPosition& globalPos) const\n\n"
        "   or overload this function\n\n"
        "         template<class ElementSolution>\n"
        "         const biotModulus(const Element& element,\n"
        "         const SubControlVolume& scv,\n"
        "         const FVElementGeometry& fvGeometry,\n"
        "         const ElemVolVars& elemVolVars,\n"
        "         const FluxVarsCache& fluxVarsCache) const\n\n");

        return asImp_().biotModulusAtPos(fluxVarsCache.ipGlobal());
    }

    template<class ElemVolVars, class FluxVarsCache>
    decltype(auto) bulkModulus(const Element& element,
                               const SubControlVolume& scv,
                               const FVElementGeometry& fvGeometry,
                               const ElemVolVars& elemVolVars,
                               const FluxVarsCache& fluxVarsCache) const
    {
        static_assert(decltype(isValid(Detail::hasBulkModulusAtPos<GlobalPosition>())(this->asImp_()))::value," \n\n"
        "   Your spatial params class has to either implement\n\n"
        "         const bulkModulusAtPos(const GlobalPosition& globalPos) const\n\n"
        "   or overload this function\n\n"
        "         template<class ElementSolution>\n"
        "         const bulkModulus(const Element& element,\n"
        "         const SubControlVolume& scv,\n"
        "         const FVElementGeometry& fvGeometry,\n"
        "         const ElemVolVars& elemVolVars,\n"
        "         const FluxVarsCache& fluxVarsCache) const\n\n");

        return asImp_().bulkModulusAtPos(fluxVarsCache.ipGlobal());
    }

    private:
    Implementation &asImp_()
    { return *static_cast<Implementation*>(this); }

    const Implementation &asImp_() const
    { return *static_cast<const Implementation*>(this); }
};
} // end namespace Dumuxs
#endif
