// -*- mode: C++; tab-width: 4; indent-tabs-mode: nil; c-basic-offset: 4 -*-
// vi: set et ts=4 sw=4 sts=4:
/*****************************************************************************
 *   See the file COPYING for full copying permissions.                      *
 *                                                                           *
 *   This program is free software: you can redistribute it and/or modify    *
 *   it under the terms of the GNU General Public License as published by    *
 *   the Free Software Foundation, either version 3 of the License, or       *
 *   (at your option) any later version.                                     *
 *                                                                           *
 *   This program is distributed in the hope that it will be useful,         *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of          *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the            *
 *   GNU General Public License for more details.                            *
 *                                                                           *
 *   You should have received a copy of the GNU General Public License       *
 *   along with this program.  If not, see <http://www.gnu.org/licenses/>.   *
 *****************************************************************************/

#ifndef DUMUX_INTERFACE_PLASTIC_FV_SPATIAL_PARAMS_HH
#define DUMUX_INTERFACE_PLASTIC_FV_SPATIAL_PARAMS_HH

#include "fvplastic.hh"

namespace Dumux {

#ifndef DOXYGEN
namespace Detail {
 
template<class GlobalPosition>
struct hasInterfacePlasticModelAtPos
{
    template<class SpatialParams>
    auto operator()(const SpatialParams& a)
    -> decltype(a.interfacePlasticModelAtPos(std::declval<GlobalPosition>()))
    {}
};
    
template<class GlobalPosition>
struct hasInterfaceElasticMatrixAtPos
{
    template<class SpatialParams>
    auto operator()(const SpatialParams& a)
    -> decltype(a.interfaceElasticMatrixAtPos(std::declval<GlobalPosition>()))
    {}
};

} // end namespace Detail
#endif
/*!
 * \ingroup SpatialParameters
 * \brief The base class for spatial parameters of elastoplastic  geomechanical problems
 */
template<class Scalar, class GridGeometry, class Implementation>
class FVSpatialParamsPlasticInterface
: public FVSpatialParamsPlastic<Scalar,GridGeometry,Implementation>
{
    using Parent = FVSpatialParamsPlastic<Scalar,GridGeometry,Implementation>;
    using FVElementGeometry = typename GridGeometry::LocalView;
    using SubControlVolume = typename GridGeometry::SubControlVolume;
    using SubControlVolumeFace = typename GridGeometry::SubControlVolumeFace;
    using GridView = typename GridGeometry::GridView;
    using Element = typename GridView::template Codim<0>::Entity;
    using GlobalPosition = typename Element::Geometry::GlobalCoordinate;
    static constexpr int dim = GridView::dimension;
    static constexpr int dimWorld = GridView::dimensionworld;
    static constexpr int numStre = (dim == 2 && dimWorld == 2) ? 4 : 2*dim;
    static constexpr int numStra = (dim == 2 && dimWorld == 2) ? 3 : 2*dim;

public:
    //! The constructor
    FVSpatialParamsPlasticInterface()
    : Parent()
    {
    };

    template<class ElemVolVars,class FluxVarsCache>
    decltype(auto) interfacePlasticModel(const Element& element,
                                         const FVElementGeometry& fvGeometry,
                                         const ElemVolVars& elemVolVars,
                                         const FluxVarsCache& fluxVarCache) const
    {
        static_assert(decltype(isValid(Detail::hasInterfacePlasticModelAtPos<GlobalPosition>())(this->asImp_()))::value," \n\n"
        "   Your spatial params class has to either implement\n\n"
        "         plasticModelAtPos(const GlobalPosition& globalPos) \n\n"
        "   or overload this function\n\n"
        "         template<class ElementSolution>\n"
        "         interfacePlasticModel(const Element& element,\n"
        "                               const SubControlVolumeFace& scvf, \n"
        "                               const FVElementGeometry& fvGeometry,\n"
        "                               const ElemVolVars& elemVolVars) const\n\n");

       return asImp_().interfacePlasticModelAtPos(element.geometry().center());
    }
    

    template<class ElemVolVars, class FluxVarsCache>
    decltype(auto) interfaceElasticMatrix(const Element& element,
                                          const FVElementGeometry& fvGeometry,
                                          const ElemVolVars& elemVolVars,
                                          const FluxVarsCache& fluxVarsCache) const
    {
        static_assert(decltype(isValid(Detail::hasElasticMatrixAtPos<GlobalPosition>())(this->asImp_()))::value," \n\n"
        "   Your spatial params class has to either implement\n\n"
        "         interfaceElasticMatrixAtPos(const GlobalPosition& globalPos) const\n\n"
        "   or overload this function\n\n"
        "         template<class ElementSolution>\n"
        "         interfaceElasticMatrix(const Element& element,\n"
        "                       const FVElementGeometry& fvGeometry,\n"
        "                       const ElemVolVars& elemVolVars,\n"
        "                       const FluxVarsCache& fluxVarsCache) const\n\n");


       return asImp_().interfaceElasticMatrixAtPos(fluxVarsCache.ipGlobal());
    }

private:
    Implementation &asImp_()
    { return *static_cast<Implementation*>(this); }

    const Implementation &asImp_() const
    { return *static_cast<const Implementation*>(this); }
};
} // end namespace Dumuxs
#endif
