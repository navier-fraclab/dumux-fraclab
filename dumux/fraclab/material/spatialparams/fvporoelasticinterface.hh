// -*- mode: C++; tab-width: 4; indent-tabs-mode: nil; c-basic-offset: 4 -*-
// vi: set et ts=4 sw=4 sts=4:
/*****************************************************************************
 *   See the file COPYING for full copying permissions.                      *
 *                                                                           *
 *   This program is free software: you can redistribute it and/or modify    *
 *   it under the terms of the GNU General Public License as published by    *
 *   the Free Software Foundation, either version 3 of the License, or       *
 *   (at your option) any later version.                                     *
 *                                                                           *
 *   This program is distributed in the hope that it will be useful,         *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of          *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the            *
 *   GNU General Public License for more details.                            *
 *                                                                           *
 *   You should have received a copy of the GNU General Public License       *
 *   along with this program.  If not, see <http://www.gnu.org/licenses/>.   *
 *****************************************************************************/
/*!
 * \file
 * \ingroup SpatialParameters
 * \brief The class for spatial parameters of for fractured domains where both
 * the fractures and the matrix are elastic
 */
#ifndef DUMUX_FRACLAB_POROELASTIC_INTERFACE_FV_SPATIAL_PARAMS_HH
#define DUMUX_FRACLAB_POROELASTIC_INTERFACE_FV_SPATIAL_PARAMS_HH

#include <memory>

#include <dune/common/exceptions.hh>
#include <dumux/common/parameters.hh>
#include <dumux/common/typetraits/isvalid.hh>
#include "fvporoelastic.hh"

namespace Dumux {

#ifndef DOXYGEN
namespace Detail {

template<class GlobalPosition>
struct hasInterfaceStiffnessParamsAtPos
{
    template<class SpatialParams>
    auto operator()(const SpatialParams& a)
    -> decltype(a.interfaceStiffnessParamsAtPos(std::declval<GlobalPosition>()))
    {}
};

struct hasInterfaceBiotCoefficient
{
    template<class SpatialParams>
    auto operator()(const SpatialParams& a)
    -> decltype(a.interfaceBiotCoefficient())
    {}
};

struct hasInterfaceBiotModulus
{
    template<class SpatialParams>
    auto operator()(const SpatialParams& a)
    -> decltype(a.interfaceBiotModulus())
    {}
};

struct hasInterfaceBulkModulus
{
    template<class SpatialParams>
    auto operator()(const SpatialParams& a)
    -> decltype(a.interfaceBulkModulus())
    {}
};

} // end namespace Detail
#endif

template<class Scalar, class GridGeometry, class Implementation>
class FVSpatialParamsPoroelasticInterface
: public FracLabSpatialParamsPoroelastic<Scalar, GridGeometry, Implementation>
{
    using ParentType = FracLabSpatialParamsPoroelastic<Scalar, GridGeometry, Implementation>;
    using FVElementGeometry = typename GridGeometry::LocalView;
    using SubControlVolume = typename GridGeometry::SubControlVolume;
    using SubControlVolumeFace = typename GridGeometry::SubControlVolumeFace;
    using GridView = typename GridGeometry::GridView;
    using Element = typename GridView::template Codim<0>::Entity;
    using GlobalPosition = typename Element::Geometry::GlobalCoordinate;
    enum { dimWorld = GridView::dimensionworld };

public:
    //! The constructor
    FVSpatialParamsPoroelasticInterface(std::shared_ptr<const GridGeometry> gridGeometry)
    : ParentType(gridGeometry)
    {
    }

    template<class ElemVolVars, class FluxVarsCache, class PrimaryVariables>
    decltype(auto) interfaceStiffnessParams(const Element& element,
                                            const SubControlVolumeFace& scvf,
                                            const FVElementGeometry& fvGeometry,
                                            const ElemVolVars& elemVolVars,
                                            const FluxVarsCache& fluxVarsCache,
                                            const PrimaryVariables& variables) const
    {
        static_assert(decltype(isValid(Detail::hasInterfaceStiffnessParamsAtPos<GlobalPosition>())(this->asImp_()))::value," \n\n"
        "   Your spatial params class has to either implement\n\n"
        "         const interfaceStiffnessParamsAtPos(const GlobalPosition& globalPos) const\n\n"
        "   or overload this function\n\n"
        "         const interfaceStiffnessParams(const Element& element,\n"
        "                                        const FVElementGeometry& fvGeometry,\n"
        "                                        const ElemVolVars& elemVolVars,\n"
        "                                        const FluxVarsCache& fluxVarsCache) const\n\n");

        return asImp_().interfaceStiffnessParamsAtPos(fluxVarsCache.ipGlobal());
    }

    template<class LowDimElement, class LowDimScv>
    decltype(auto) interfaceBiotCoefficient(const LowDimElement& lowDimElem,
                                            const LowDimScv& lowDimScv) const
    {
        static_assert(decltype(isValid(Detail::hasInterfaceBiotCoefficient())(this->asImp_()))::value," \n\n"
        "   Your spatial params class has to either implement\n\n"
        "         const interfaceBiotCoefficient() const\n\n"
        "   or overload this function\n\n"
        "         const interfaceBiotCoefficient(const LowDimElem& element,\n"
        "                                        const LowDimScv& scv) const\n\n");

        return asImp_().interfaceBulkModulus();
    }

    template<class LowDimElement, class LowDimScv>
    decltype(auto) interfaceBiotModulus(const LowDimElement& lowDimElem,
                                        const LowDimScv& lowDimScv) const
    {
        static_assert(decltype(isValid(Detail::hasInterfaceBiotModulus())(this->asImp_()))::value," \n\n"
        "   Your spatial params class has to either implement\n\n"
        "         const interfaceBiotModulus() const\n\n"
        "   or overload this function\n\n"
        "         const interfaceBiotModulus(const LowDimElem& element,\n"
        "                                    const LowDimScv& scv) const\n\n");

        return asImp_().interfaceBulkModulus();
    }

    template<class LowDimElement, class LowDimScv>
    decltype(auto) interfaceBulkModulus(const LowDimElement& lowDimElem,
                                        const LowDimScv& lowDimScv) const
    {
        static_assert(decltype(isValid(Detail::hasInterfaceBulkModulus())(this->asImp_()))::value," \n\n"
        "   Your spatial params class has to either implement\n\n"
        "         const interfaceBulkModulus() const\n\n"
        "   or overload this function\n\n"
        "         const interfaceBulkModulus(const LowDimElem& element,\n"
        "                                    const LowDimScv& scv) const\n\n");

        return asImp_().interfaceBulkModulus();
    }

private:
    Implementation &asImp_()
    { return *static_cast<Implementation*>(this); }

    const Implementation &asImp_() const
    { return *static_cast<const Implementation*>(this); }
};
} // end namespace Dumuxs
#endif
