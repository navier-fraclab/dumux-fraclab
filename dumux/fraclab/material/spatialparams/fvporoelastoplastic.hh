// -*- mode: C++; tab-width: 4; indent-tabs-mode: nil; c-basic-offset: 4 -*-
// vi: set et ts=4 sw=4 sts=4:
/*****************************************************************************
 *   See the file COPYING for full copying permissions.                      *
 *                                                                           *
 *   This program is free software: you can redistribute it and/or modify    *
 *   it under the terms of the GNU General Public License as published by    *
 *   the Free Software Foundation, either version 3 of the License, or       *
 *   (at your option) any later version.                                     *
 *                                                                           *
 *   This program is distributed in the hope that it will be useful,         *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of          *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the            *
 *   GNU General Public License for more details.                            *
 *                                                                           *
 *   You should have received a copy of the GNU General Public License       *
 *   along with this program.  If not, see <http://www.gnu.org/licenses/>.   *
 *****************************************************************************/
/*!
 * \file
 * \ingroup SpatialParameters
 * \brief The class for spatial parameters for hydromechanical
 * elastoplastic problems coupled with the fixed stress split 
 * (it demands the drained bulk modulus)
 */
#ifndef DUMUX_FRACLAB_POROELASTOPLASTIC_FV_SPATIAL_PARAMS_HH
#define DUMUX_FRACLAB_POROELASTOPLASTIC_FV_SPATIAL_PARAMS_HH

#include <memory>

#include <dune/common/exceptions.hh>
#include <dumux/common/parameters.hh>
#include <dumux/common/typetraits/isvalid.hh>
#include "fvporoelastic.hh"
#include "fvplastic.hh"

namespace Dumux {

template<class Scalar, class GridGeometry, class Implementation>
class FvSpatialParamsPoroplastic
: public FracLabSpatialParamsPoroelastic<Scalar, GridGeometry, Implementation>,
  public FVSpatialParamsPlastic<Scalar, GridGeometry, Implementation>
{
    using ParentType = FracLabSpatialParamsPoroelastic<Scalar, GridGeometry, Implementation>;
    using OtherParentType = FVSpatialParamsPlastic<Scalar, GridGeometry, Implementation>;
    using FVElementGeometry = typename GridGeometry::LocalView;
    using SubControlVolume = typename GridGeometry::SubControlVolume;
    using SubControlVolumeFace = typename GridGeometry::SubControlVolumeFace;
    using GridView = typename GridGeometry::GridView;
    using Element = typename GridView::template Codim<0>::Entity;
    using GlobalPosition = typename Element::Geometry::GlobalCoordinate;
    enum { dimWorld = GridView::dimensionworld };

public:
    //! The constructor
    FracLabSpatialParamsPoroplastic(std::shared_ptr<const GridGeometry> gridGeometry)
    : ParentType(gridGeometry), OtherParentType()
    {
    }

    private:
    Implementation &asImp_()
    { return *static_cast<Implementation*>(this); }

    const Implementation &asImp_() const
    { return *static_cast<const Implementation*>(this); }
};
} // end namespace Dumuxs
#endif
