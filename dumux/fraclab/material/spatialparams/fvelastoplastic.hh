// -*- mode: C++; tab-width: 4; indent-tabs-mode: nil; c-basic-offset: 4 -*-
// vi: set et ts=4 sw=4 sts=4:
/*****************************************************************************
 *   See the file COPYING for full copying permissions.                      *
 *                                                                           *
 *   This program is free software: you can redistribute it and/or modify    *
 *   it under the terms of the GNU General Public License as published by    *
 *   the Free Software Foundation, either version 3 of the License, or       *
 *   (at your option) any later version.                                     *
 *                                                                           *
 *   This program is distributed in the hope that it will be useful,         *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of          *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the            *
 *   GNU General Public License for more details.                            *
 *                                                                           *
 *   You should have received a copy of the GNU General Public License       *
 *   along with this program.  If not, see <http://www.gnu.org/licenses/>.   *
 *****************************************************************************/
/*!
 * \file
 * \ingroup SpatialParameters
 * \brief The base class for spatial parameters of linear elastic geomechanical problems
 */
#ifndef DUMUX_FRACLAB_ELASTOPLASTIC_FV_SPATIAL_PARAMS_HH
#define DUMUX_FRACLAB_ELASTOPLASTIC_FV_SPATIAL_PARAMS_HH

#include <dumux/material/spatialparams/fvelastic.hh>
#include "fvplastic.hh"

namespace Dumux {

/*!
 * \ingroup SpatialParameters
 * \brief The base class for spatial parameters of elastoplastic  geomechanical problems
 */
template<class Scalar, class GridGeometry, class Implementation>
class FVSpatialParamsElastoplastic
: public FVSpatialParamsElastic<Scalar, GridGeometry, Implementation>,
  public FVSpatialParamsPlastic<Scalar, GridGeometry, Implementation>
{
    using ParentType = FVSpatialParamsElastic<Scalar, GridGeometry, Implementation>;
    using OtherParentType = FVSpatialParamsPlastic<Scalar, GridGeometry, Implementation>;
    using FVElementGeometry = typename GridGeometry::LocalView;
    using SubControlVolume = typename GridGeometry::SubControlVolume;
    using SubControlVolumeFace = typename GridGeometry::SubControlVolumeFace;
    using GridView = typename GridGeometry::GridView;
    using Element = typename GridView::template Codim<0>::Entity;
    using GlobalPosition = typename Element::Geometry::GlobalCoordinate;

    static constexpr int dim = GridView::dimension;
    static constexpr int dimWorld = GridView::dimensionworld;
    static constexpr int numStre = (dim == 2 && dimWorld == 2) ? 4 : 2*dim;
    static constexpr int numStra = (dim == 2 && dimWorld == 2) ? 3 : 2*dim;

public:
    //! The constructor
    FVSpatialParamsElastoplastic(std::shared_ptr<const GridGeometry> gridGeometry)
    : ParentType(gridGeometry), OtherParentType()
    {};

private:
    Implementation &asImp_()
    { return *static_cast<Implementation*>(this); }

    const Implementation &asImp_() const
    { return *static_cast<const Implementation*>(this); }
};
} // end namespace Dumuxs
#endif
