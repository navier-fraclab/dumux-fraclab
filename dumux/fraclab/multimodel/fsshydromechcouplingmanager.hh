// -*- mode: C++; tab-width: 4; indent-tabs-mode: nil; c-basic-offset: 4 -*-
// vi: set et ts=4 sw=4 sts=4:
/*****************************************************************************
 *   See the file COPYING for full copying permissions.                      *
 *                                                                           *
 *   This program is free software: you can redistribute it and/or modify    *
 *   it under the terms of the GNU General Public License as published by    *
 *   the Free Software Foundation, either version 3 of the License, or       *
 *   (at your option) any later version.                                     *
 *                                                                           *
 *   This program is distributed in the hope that it will be useful,         *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of          *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the            *
 *   GNU General Public License for more details.                            *
 *                                                                           *
 *   You should have received a copy of the GNU General Public License       3*
 *   along with this program.  If not, see <http://www.gnu.org/licenses/>.   *
 *****************************************************************************/
/*!
 * \file
 * \ingroup MultiModel
 * maneger for hydromechanical coupling with the Fixed Stress Split Algorithm in a single domain
 */
#ifndef DUMUX_FRACLAB_FIXSTRESSSPLIT_HYDROMECHANICAL_COUPLING_MANAGER_HH
#define DUMUX_FRACLAB_FIXSTRESSSPLIT_HYDROMECHANICAL_COUPLING_MANAGER_HH

#include <dumux/multidomain/facet/box/couplingmanager.hh>
#include <dumux/discretization/box/boxgeometryhelper.hh>

namespace Dumux
{

  template<class FlowTypeTag, class MechanicsTypeTag, class FlowAssembler, class MechanicsAssembler>
  class FixedStressSplitBoxHMCouplingManager
  {
    
    // This class onlly works when the same mesh is used for flow and mechanics

    using GridGeometry = GetPropType<FlowTypeTag, Properties::GridGeometry>;
    using FVElementGeometry = typename GridGeometry::LocalView;
    using SubControlVolume = typename GridGeometry::SubControlVolume;
    using SubControlVolumeFace = typename GridGeometry::SubControlVolumeFace;
    using GridView = typename GridGeometry::GridView;
    using Element = typename GridView::template Codim<0>::Entity;
    using GridIndexType = typename GridView::IndexSet::IndexType;
    using Scalar = GetPropType<FlowTypeTag, Properties::Scalar>;      
    using MechanicsGridVariables = GetPropType<MechanicsTypeTag, Properties::GridVariables>;
    using MechanicsGridFluxVariablesCache = typename MechanicsGridVariables::GridFluxVariablesCache;
    using MechanicsFluxVariablesCache = typename MechanicsGridFluxVariablesCache::FluxVariablesCache;  
    using MechanicsElementFluxVariablesCache = typename MechanicsGridFluxVariablesCache::LocalView;
    using MechanicsGridVolumeVariables = typename MechanicsGridVariables::GridVolumeVariables;
    using MechanicsElementVolumeVariables = typename MechanicsGridVolumeVariables::LocalView;
    using FlowGridVariables = GetPropType<FlowTypeTag, Properties::GridVariables>;
    using FlowGridFluxVariablesCache = typename FlowGridVariables::GridFluxVariablesCache;
    using FlowFluxVariablesCache = typename FlowGridFluxVariablesCache::FluxVariablesCache;  
    using FlowElementFluxVariablesCache = typename FlowGridFluxVariablesCache::LocalView;
    using FlowSolutionVector = GetPropType<FlowTypeTag, Properties::SolutionVector>;
    using MechanicsSolutionVector = GetPropType<MechanicsTypeTag, Properties::SolutionVector>;
    using ScalarVector = std::vector<Scalar>;

    static constexpr int dimWorld = GridView::dimensionworld;
    static constexpr int dim = GridView::dimension;
      
public:

    // constructor to initialize entire domain with a constant porosity 

    FixedStressSplitBoxHMCouplingManager(std::shared_ptr<const GridGeometry> gridGeometry,Scalar initPorosity)
    :gridGeometry_(gridGeometry)
    {
       volStrainVar_.resize(gridGeometry->gridView().size(dim));
       dofElements_.resize(gridGeometry->gridView().size(dim));
       bulkPorosities_.resize(gridGeometry->gridView().size(dim), initPorosity);
       boxAreas_.resize(gridGeometry->gridView().size(dim), 0.0);
       setGlobalPorosity(initPorosity);

       for(const auto& element : elements(gridGeometry->gridView()))
       {
           const auto eIdx = gridGeometry->elementMapper().index(element);
           auto fvGeometry = localView(*gridGeometry);
           fvGeometry.bind(element);
           for(const auto& scv : scvs(fvGeometry)){
               boxAreas_[scv.dofIndex()] += scv.volume();
               dofElements_[scv.dofIndex()].insert(eIdx);
           }
       }
    }
    
    // constructor to initialize domain with node-specific porosities
    FixedStressSplitBoxHMCouplingManager(std::shared_ptr<const GridGeometry> gridGeometry,ScalarVector bulkPorosities)
    :gridGeometry_(gridGeometry)
    {
        const auto numDofs = gridGeometry->gridView().size(dim);
        volStrainVar_.resize(numDofs);
        dofElements_.resize(numDofs);
        boxAreas_.resize(numDofs, 0.0);

        if(bulkPorosities.size() != numDofs)
            DUNE_THROW(Dune::InvalidStateException, "Invalid dimensions of the vector that stores initial porosities");

        bulkPorosities_ = bulkPorosities;
        for(const auto& element : elements(gridGeometry->gridView()))
        {
            const auto eIdx = gridGeometry->elementMapper().index(element);
            auto fvGeometry = localView(*gridGeometry);
            fvGeometry.bind(element);
            for(const auto& scv : scvs(fvGeometry)){
                boxAreas_[scv.dofIndex()] += scv.volume();
                dofElements_[scv.dofIndex()].insert(eIdx);
            }
        }
    }

    void init(std::shared_ptr<FlowAssembler> flowAssembler,
              std::shared_ptr<MechanicsAssembler> mechanicsAssembler,
              FlowSolutionVector& fluxSol,
              MechanicsSolutionVector& mechSol)
    {
        setFluxCurrentSolution(fluxSol);
        setMechanicsCurrentSolution(mechSol);
        flowAssembler_ = flowAssembler;
        mechanicsAssembler_ = mechanicsAssembler;
        updateStrainsAndPorosities();
        //updateTotalStrains();
        initialized = true;
    }

    void updateStrainsAndPorosities()
    {
        for(int i = 0; i < volStrainVar_.size(); i++) volStrainVar_[i] = 0.0;
            
        // we update the strains for each node by averaging the strains at its correspondent box
        // volume average with strains at the boundaries of the box (faces)

        const auto& gridGeometry = *gridGeometry_;
        const auto& prevMechSol = mechanicsAssembler_->prevSol();

        for(const auto& element : elements(gridGeometry.gridView()))
        {
            auto elemFluxVars = localView(mechanicsAssembler_->gridVariables().gridFluxVarsCache());
            auto elemVolVars = localView(mechanicsAssembler_->gridVariables().curGridVolVars());
        
            auto fvGeometry = localView(gridGeometry);
            fvGeometry.bind(element);
            elemVolVars.bind(element, fvGeometry, *(curFluxSol));                                                                                                      
            elemFluxVars.bind(element, fvGeometry, elemVolVars);
        
            for(const auto& scvf : scvfs(fvGeometry))
            {
                Dune::FieldVector<Scalar, dim> displ(0.0);
                const auto shapeValues = elemFluxVars[scvf].shapeValues();
                const auto normal = scvf.unitOuterNormal();
                const auto& insideScv = fvGeometry.scv(scvf.insideScvIdx());
                const auto& outsideScv = fvGeometry.scv(scvf.outsideScvIdx());
                
                for (int dir = 0; dir < dim; ++dir){
                    for (const auto& scvK : scvs(fvGeometry)){
                        displ[dir] += shapeValues[scvK.indexInElement()]*
                            ((*curMechSol)[scvK.dofIndex()][dir] -
                             prevMechSol[scvK.dofIndex()][dir]);
                    }
                    volStrainVar_[insideScv.dofIndex()] += displ[dir]*normal[dir]*scvf.area();
                    if(!scvf.boundary())
                        volStrainVar_[outsideScv.dofIndex()] -= displ[dir]*normal[dir]*scvf.area();
                }
            }
        }

        for(int i = 0; i < volStrainVar_.size(); i++)
            volStrainVar_[i] /= boxAreas_[i];

        std::vector<bool> isAssigned(gridGeometry.numDofs(), false);

        for(const auto& element : elements(gridGeometry.gridView()))
        {
            auto fvGeometry = localView(gridGeometry);
            fvGeometry.bind(element);
            for(const auto& scv : scvs(fvGeometry)){
                if(isAssigned[scv.dofIndex()]) continue;
                bulkPorosities_[scv.dofIndex()] = updatedPorosity(element, scv);
                isAssigned[scv.dofIndex()] = true;
            }
        }

        isAssigned.clear();
        
    }
    
    const ScalarVector getVolumetricStrainVariation() const
    {return volStrainVar_;};

    // returns total pore pressure on the correspondent location. Should be used only
    // with linear mechanical problems

    Scalar effectivePorePressure(const Element& element,
                                 const MechanicsFluxVariablesCache& fluxVarCache) const
    {
        const auto& bulkGridGeometry = flowAssembler_->problem().gridGeometry();
        auto fvGeometry = localView(bulkGridGeometry);
        fvGeometry.bind(element);

        const auto curSol = *curFluxSol;
        const auto& shapeValues = fluxVarCache.shapeValues();
        Scalar effPress(0.0);
        for (auto& scv : scvs(fvGeometry)){
            auto press = curSol[scv.dofIndex()][0];
            effPress += shapeValues[scv.indexInElement()]*press;
        }
        return effPress;     
    }

    // return pore pressure variation in the current time step 

    Scalar effectivePorePressureVariation(const Element& element,
                                          const MechanicsFluxVariablesCache& fluxVarCache) const
    {
        const auto& prevFluxSol = flowAssembler_->prevSol();
        const auto& bulkGridGeometry = flowAssembler_->problem().gridGeometry();
        auto fvGeometry = localView(bulkGridGeometry);
        fvGeometry.bind(element);

        const auto curSol = *curFluxSol;
        const auto& shapeValues = fluxVarCache.shapeValues();
        Scalar effPress(0.0);
        for (auto& scv : scvs(fvGeometry)){
            auto press = curSol[scv.dofIndex()][0] - prevFluxSol[scv.dofIndex()][0];
            effPress += shapeValues[scv.indexInElement()]*press;
        }
        return effPress;     
    }

    // compute porosity with converged results 

    Scalar updatedPorosity(const Element& element,
                          const SubControlVolume& scv)
    {
        auto fvGeometry = localView(*gridGeometry_);
        fvGeometry.bind(element);

        // Flow variables
        const auto& prevGridVolVars = flowAssembler_->gridVariables().prevGridVolVars();
        auto prevElemVolVars = localView(prevGridVolVars);
        const auto& prevFluxSol = flowAssembler_->prevSol();
        prevElemVolVars.bind(element, fvGeometry, prevFluxSol);

        // mech variables 

        const auto& curGridMechVolVars = mechanicsAssembler_->gridVariables().curGridVolVars();     
        auto curElemMechVolVars = localView(curGridMechVolVars);

        curElemMechVolVars.bind(element, fvGeometry, *curMechSol);
        MechanicsFluxVariablesCache elemMechFluxVars;
        elemMechFluxVars.update(mechanicsAssembler_->problem(), element, fvGeometry, curElemMechVolVars, scv.center());
        
        const auto elemSol = elementSolution(element, *curMechSol, fvGeometry.gridGeometry());
        
        // get previous porosity

        Scalar prevPorosity = prevElemVolVars[scv].porosity();

        // get relevant params
        const auto biotCoeff =  mechanicsAssembler_->problem().spatialParams().biotCoefficient(element,fvGeometry,curElemMechVolVars,elemMechFluxVars);
        const auto biotModulus = mechanicsAssembler_->problem().spatialParams().biotModulus(element,scv,fvGeometry,curElemMechVolVars,elemMechFluxVars);

        // compute porosity variation due to vol strains and pressure var
        Scalar ev = volStrainVar_[scv.dofIndex()];
        Scalar dp = (*curFluxSol)[scv.dofIndex()] - prevFluxSol[scv.dofIndex()];
        Scalar dpor = biotCoeff * ev + dp/biotModulus;

        Scalar porosity = prevPorosity + dpor;
        return porosity;

    }
     
    // get estimator of vol strain variation according to the fixed stress split algorithm 

    template <class ElementSolution>
    Scalar volStrainEstimator(const Element& element,
                              const SubControlVolume& scv,
                              const ElementSolution& elemSol) 
    {
        if(!initialized) return 0.0;

        auto fvGeometry = localView(*gridGeometry_);
        fvGeometry.bind(element);

        // flux variables

        const auto& curGridVolVars = flowAssembler_->gridVariables().curGridVolVars();
        auto curElemVolVars = localView(curGridVolVars);       
        const auto prevEffPressure = (*prevIterFluxSol)[scv.dofIndex()];
        curElemVolVars.bind(element, fvGeometry, *curFluxSol);
        const auto curEffPressure = curElemVolVars[scv].pressure(0);
        
        const auto dpress = curEffPressure - prevEffPressure;

        // mechanics

        const auto& curGridMechVolVars = mechanicsAssembler_->gridVariables().curGridVolVars();     
        auto curElemMechVolVars = localView(curGridMechVolVars);
        MechanicsFluxVariablesCache elemMechFluxVars;

        curElemMechVolVars.bind(element, fvGeometry, *curMechSol);
        elemMechFluxVars.update(mechanicsAssembler_->problem(), element, fvGeometry, curElemMechVolVars, scv.center());

        const auto b =  mechanicsAssembler_->problem().spatialParams().biotCoefficient(element,fvGeometry,curElemMechVolVars,elemMechFluxVars);
        const auto M = mechanicsAssembler_->problem().spatialParams().biotModulus(element,scv,fvGeometry,curElemMechVolVars,elemMechFluxVars);
        const auto Kdr = mechanicsAssembler_->problem().spatialParams().bulkModulus(element,scv,fvGeometry,curElemMechVolVars,elemMechFluxVars);

        // return estimator

        Scalar mv = (1.0/M + std::pow(b,2.0)/Kdr);
        return  mv*dpress;
    }

    // porosity calculated for the flow eq using the volume strain predictor of the fixed-stress split
    template<class ElementSolution>
    Scalar flowPorosity(const Element& element,
                        const SubControlVolume& scv,
                        const ElementSolution& elemSol)
    {
        Scalar dporPress(0.0);
        dporPress = volStrainEstimator(element, scv, elemSol);
        return bulkPorosities_[scv.dofIndex()] + dporPress;
    }

    void setPorosity(const SubControlVolume& scv,
                       const Scalar porosity)
    {bulkPorosities_[scv.dofIndex()] = porosity;}

    void setFluxPrevIterSolution(FlowSolutionVector& sol)
    { prevIterFluxSol = &sol;}
      
    void setFluxCurrentSolution(FlowSolutionVector& sol)
    {curFluxSol = &sol;}

    void setMechanicsCurrentSolution(MechanicsSolutionVector& sol)
    {curMechSol = &sol;}

    void setGlobalPorosity(const Scalar porosity)
    {
        for(const auto& element : elements(gridGeometry_->gridView()))
        {
            auto fvGeometry = localView(*gridGeometry_);
            fvGeometry.bind(element);
            for(const auto& scv : scvs(fvGeometry))
                setPorosity(scv, porosity);          
        }
    }

    Scalar volumetricStrain(GridIndexType vIdx) const
    {return volStrainVar_[vIdx];}
      
    Scalar pressureVar(GridIndexType vIdx) const
    {
        const auto prevFluxSol = flowAssembler_->prevSol();   
        return (*curFluxSol)[vIdx][0]- prevFluxSol[vIdx][0];
    }

    Scalar porosity(GridIndexType vIdx) const
    {return bulkPorosities_[vIdx];}
private:

    std::shared_ptr<FlowAssembler> flowAssembler_;
    std::shared_ptr<MechanicsAssembler> mechanicsAssembler_;
    std::vector<Scalar> volStrainVar_;
    std::vector<Scalar> bulkPorosities_;
    std::vector<Scalar> boxAreas_;
    std::shared_ptr<const GridGeometry> gridGeometry_;
    std::vector<std::set<Scalar>> dofElements_;
    MechanicsSolutionVector * curMechSol;
    FlowSolutionVector * curFluxSol;
    FlowSolutionVector * prevIterFluxSol;
      
    bool initialized = false;
  };
  
}

#endif
