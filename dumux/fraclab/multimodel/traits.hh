// -*- mode: C++; tab-width: 4; indent-tabs-mode: nil; c-basic-offset: 4 -*-
// vi: set et ts=4 sw=4 sts=4:
/*****************************************************************************
 *   See the file COPYING for full copying permissions.                      *
 *                                                                           *
 *   This program is free software: you can redistribute it and/or modify    *
 *   it under the terms of the GNU General Public License as published by    *
 *   the Free Software Foundation, either version 3 of the License, or       *
 *   (at your option) any later version.                                     *
 *                                                                           *
 *   This program is distributed in the hope that it will be useful,         *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of          *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the            *
 *   GNU General Public License for more details.                            *
 *                                                                           *
 *   You should have received a copy of the GNU General Public License       *
 *   along with this program.  If not, see <http://www.gnu.org/licenses/>.   *
 *****************************************************************************/
/*!
 * \file
 * \ingroup MultiDomain
 * \brief Traits for multimodel problems
 */

#ifndef DUMUX_MULTIMODEL_TRAITS_HH
#define DUMUX_MULTIMODEL_TRAITS_HH

#include <type_traits>
#include <tuple>
#include <utility>
#include <memory>

#include <dune/common/fmatrix.hh>
#include <dune/common/indices.hh>

#include <dune/istl/bcrsmatrix.hh>
#include <dune/istl/multitypeblockvector.hh>
#include <dune/istl/multitypeblockmatrix.hh>

#include <dumux/common/properties.hh>
#include <dumux/common/typetraits/matrix.hh>
#include <dumux/common/typetraits/utility.hh>

using Dumux::Detail
namespace Dumux {

/*
This struct contains definitions for a set of coupled models:
model id, type tag, solution vector and jacobian matrix
 */
template<typename... ModelTypeTags>
struct MultiModelTraits
{
    //! the number of models
    static constexpr std::size_t numModels = sizeof...(ModelTypeTags);
   
private:

    //! the type tag of the problem for each model
    template<std::size_t id>
    using ModelTypeTag = typename std::tuple_element_t<id, std::tuple<ModelTypeTags...>>;

    //! helper alias to construct derived multimodel types like tuples
    using Indices = std::make_index_sequence<numModels>;

    //! the scalar type of each sub domain
    template<std::size_t id>
    using ModelScalar = GetPropType<ModelTypeTag<id>, Properties::Scalar>;

    //! the jacobian type of each sub domain
    template<std::size_t id>
    using ModelJacobianMatrix = GetPropType<ModelTypeTag<id>, Properties::JacobianMatrix>;

    //! the solution type of each sub domain
    template<std::size_t id>
    using ModelSolutionVector = GetPropType<ModelTypeTag<id>, Properties::SolutionVector>;

public:

    /*
     * \brief model types
     */
    //\{

    template<std::size_t id>
    struct Model
    {
        using Index = Dune::index_constant<id>;
        using TypeTag = ModelTypeTag<id>;
        using Grid = GetPropType<ModelTypeTag<id>, Properties::Grid>;
        using GridGeometry = GetPropType<ModelTypeTag<id>, Properties::GridGeometry>;
        using Problem = GetPropType<ModelTypeTag<id>, Properties::Problem>;
        using GridVariables =GetPropType<ModelTypeTag<id>, Properties::GridVariables>;
        using IOFields = GetPropType<ModelTypeTag<id>, Properties::IOFields>;
        using SolutionVector = GetPropType<ModelTypeTag<id>, Properties::SolutionVector>;
    };

    //\}

    /*
     * \brief multi model types
     */
    //\{

    //! the scalar type
    using Scalar = typename makeFromIndexedType<std::common_type_t, ModelScalar, Indices>::type;

    //! the solution vector type
    using SolutionVector = typename makeFromIndexedType<Dune::MultiTypeBlockVector, ModelSolutionVector, Indices>::type;

    //! the jacobian type
    using JacobianMatrix = typename Detail::MultiDomainMatrixType<ModelJacobianMatrix, Indices, Scalar>::type;

    //\}

    /*
     * \brief helper aliases to contruct derived tuple types
     */
    //\{

    //! helper alias to create tuple<...> from indexed type
    template<template<std::size_t> class T>
    using Tuple = typename makeFromIndexedType<std::tuple, T, Indices>::type;

    //! helper alias to create tuple<std::shared_ptr<...>> from indexed type
    template<template<std::size_t> class T>
    using TupleOfSharedPtr = typename Detail::MultiDomainTupleSharedPtr<T, Indices>::type;

    //! helper alias to create tuple<std::shared_ptr<const ...>> from indexed type
    template<template<std::size_t> class T>
    using TupleOfSharedPtrConst = typename Detail::MultiDomainTupleSharedPtrConst<T, Indices>::type;

    //\}
};

} //end namespace Dumux

#endif
