// -*- mode: C++; tab-width: 4; indent-tabs-mode: nil; c-basic-offset: 4 -*-
// vi: set et ts=4 sw=4 sts=4:
/*****************************************************************************
 *   See the file COPYING for full copying permissions.                      *
 *                                                                           *
 *   This program is free software: you can redistribute it and/or modify    *
 *   it under the terms of the GNU General Public License as published by    *
 *   the Free Software Foundation, either version 3 of the License, or       *
 *   (at your option) any later version.                                     *
 *                                                                           *
 *   This program is distributed in the hope that it will be useful,         *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of          *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the            *
 *   GNU General Public License for more details.                            *
 *                                                                           *
 *   You should have received a copy of the GNU General Public License       *
 *   along with this program.  If not, see <http://www.gnu.org/licenses/>.   *
 *****************************************************************************/
/*!
 * \file
 * \ingroup 
 * Manager for full coupling of flow and mechanical models
 */

#ifndef DUMUX_BOX_HYDROMECH_FULL_COUPLING_MANAGER_HH
#define DUMUX_BOX_HYDROMECH_FULL_COUPLING_MANAGER_HH

#include <type_traits>
#include <tuple>

#include <dumux/assembly/numericepsilon.hh>
#include <dumux/multidomain/facet/box/couplingmanager.hh>
#include <dumux/discretization/box/boxgeometryhelper.hh>

namespace Dumux
{

/*!
 * \ingroup MultiModel
 * \brief Manages the FULL coupling between a flow and a mechanical model
 *        Both problems need to use the Box discretization and have the same mesh
 *
 * \tparam HMTraits The multimodel traits containing the types of the coupled models
 * \tparam flowModelId The id of the flow model
 * \tparam mechModelId The id ot the mechanical model
 */
template<class HMTraits, std::size_t flowModelId = 0, std::size_t mechModelId = 1>
class BoxHydromechFullCouplingManager
{
    // convenience aliases and instances of the two domain ids
    using FlowIdType = typename HMTraits::template SubDomain<flowModelId>::Index;
    using MechIdType = typename HMTraits::template SubDomain<mechModelId>::Index;
    static constexpr auto flowId = FlowIdType();
    static constexpr auto mechId = MechIdType();

    // the sub-domain type tags
    template<std::size_t id> using ModelTypeTag = typename HMTraits::template SubDomain<id>::TypeTag;

    // further types specific to the sub-problems
    template<std::size_t id> using Scalar = GetPropType<ModelTypeTag<id>, Properties::Scalar>;  
    template<std::size_t id> using GridGeometry = GetPropType<ModelTypeTag<id>, Properties::GridGeometry>;
    template<std::size_t id> using FVElementGeometry = typename GridGeometry<id>::LocalView;
    template<std::size_t id> using SubControlVolume = typename GridGeometry<id>::SubControlVolume;
    template<std::size_t id> using SubControlVolumeFace = typename GridGeometry<id>::SubControlVolumeFace;
    template<std::size_t id> using GridView = typename GridGeometry<id>::GridView;
    template<std::size_t id> using Element = typename GridView<id>::template Codim<0>::Entity;
    template<std::size_t id> using GridIndexType = typename GridView<id>::IndexSet::IndexType;

    template<std::size_t id> using PrimaryVariables = GetPropType<ModelTypeTag<id>, Properties::PrimaryVariables>;
    template<std::size_t id> using Problem = GetPropType<ModelTypeTag<id>, Properties::Problem>;
    template<std::size_t id> using NumEqVector = GetPropType<ModelTypeTag<id>, Properties::NumEqVector>;
    template<std::size_t id> using ElementBoundaryTypes = GetPropType<ModelTypeTag<id>, Properties::ElementBoundaryTypes>;
    template<std::size_t id> using LocalResidual = GetPropType<ModelTypeTag<id>, Properties::LocalResidual>;
      
    template<std::size_t id> using GridVariables = GetPropType<ModelTypeTag<id>, Properties::GridVariables>;
    template<std::size_t id> using GridVolumeVariables = typename GridVariables<id>::GridVolumeVariables;
    template<std::size_t id> using ElementVolumeVariables = typename GridVolumeVariables<id>::LocalView;
    template<std::size_t id> using VolumeVariables = typename ElementVolumeVariables<id>::VolumeVariables;
    template<std::size_t id> using GridFluxVariablesCache = typename GridVariables<id>::GridFluxVariablesCache;
    template<std::size_t id> using ElementFluxVariablesCache = typename GridFluxVariablesCache<id>::LocalView;
    template<std::size_t id> using FluxVariablesCache = typename GridFluxVariablesCache<id>::FluxVariablesCache;  
    
    using SolutionVector = typename HMTraits::SolutionVector;
    static constexpr int dimWorld = GridView<flowId>::dimensionworld;
    static constexpr int dim = GridView<flowId>::dimension;

    static constexpr bool flowFluxVarsCache = getPropValue<ModelTypeTag<flowId>, Properties::EnableGridFluxVariablesCache>();
    static constexpr bool flowVolVarsCache = getPropValue<ModelTypeTag<flowId>, Properties::EnableGridVolumeVariablesCache>();
    static constexpr bool mechFluxVarsCache = getPropValue<ModelTypeTag<mechId>, Properties::EnableGridFluxVariablesCache>();
    static constexpr bool mechVolVarsCache = getPropValue<ModelTypeTag<mechId>, Properties::EnableGridVolumeVariablesCache>();

    using ProblemTuple = typename HMTraits::template TupleOfSharedPtrConst<Problem>;
    using GridGeometryTuple = typename HMTraits::template TupleOfSharedPtrConst<GridGeometry>;
    using GridVariablesTuple = typename HMTraits::template TupleOfSharedPtr<GridVariables>;

  public:

    BoxHydromechFullCouplingManager( GridGeometryTuple&& gridGeometry,
                                     Scalar<flowId> initialPorosity)
    : gridGeometry_ (gridGeometry)
    {
        const auto& flowGG = this->gridGeometry(flowId);
        const auto& mechGG = this->gridGeometry(mechId);

        if(flowGG.gridView().size(dim) != mechGG.gridView().size(dim) or flowGG.numDofs() != mechGG.numDofs())
            DUNE_THROW(Dune::InvalidStateException, "Flow and mechanical models do not employ the same mesh.");
       
        volStrainVar_.resize(flowGG.gridView().size(0));
        porosities_.resize(flowGG.gridView().size(0));
        boxAreas_.resize(flowGG.gridView().size(dim), 0.0);
        nodeElements_.resize(flowGG.numDofs());

        for(const auto& element : elements(flowGG.gridView()))
        {
            const auto eIdx = flowGG.elementMapper().index(element);
            auto fvGeometry = localView(flowGG);
            fvGeometry.bind(element);
            const auto numScv = fvGeometry.numScv();
            volStrainVar_[eIdx].resize(numScv, 0.0);
            porosities_[eIdx].resize(numScv, initialPorosity);
            for(const auto& scv : scvs(fvGeometry)){
                boxAreas_[scv.dofIndex()] += scv.volume();
                nodeElements_[scv.dofIndex()].push_back(eIdx);
            }
        }

    };

    void init(ProblemTuple&& problem,
              GridVariablesTuple&& gridVariables,
              SolutionVector& curSol,
              const SolutionVector& prevSol)
    {
        problem_ = problem;
        gridVariables_ = gridVariables;
        curSol_ = &curSol;
        prevSol_ = &prevSol;
        initialized_ = true;
    }

    void updateSolution(const SolutionVector& sol)
    {}
    
    void updateStrains(GridIndexType<flowId> dofIdx, const Element<mechId>& element)
    {   
        if(!initialized_) return;
        const auto& mechGG = this->gridGeometry(mechId);
        auto fvGeometry = localView(mechGG);
        fvGeometry.bind(element);

        for(const auto& scv : scvs(fvGeometry)){
            if(scv.dofIndex() == dofIdx)
                updateStrains(scv, element, fvGeometry);
        }
    }

    void updateStrains(const Element<mechId>& element)
    {
        const auto& mechGG = this->gridGeometry(mechId);
        auto fvGeometry = localView(mechGG);
        fvGeometry.bind(element);

        for(const auto& scv : scvs(fvGeometry))
            updateStrains(scv, element, fvGeometry);        
    }

    void updateStrains(const SubControlVolume<flowId>& scv, const Element<flowId>& element)
    {
        if(!initialized_) return;
       
        const auto& mechGG = this->gridGeometry(mechId);
        auto fvGeometry = localView(mechGG);
        fvGeometry.bind(element);

        const auto eIdx = mechGG.elementMapper().index(element);
        auto elemVolVars = localView(this->gridVariables(mechId).curGridVolVars());
        
        const auto& curMechSol = (*curSol_)[mechId];
        elemVolVars.bind(element, fvGeometry, curMechSol);      

        FluxVariablesCache<mechId> fluxVarCache;
        fluxVarCache.update(this->problem(mechId), element, fvGeometry, elemVolVars, scv.geometry().center());

        Scalar<mechId> ev(0.0);

        for (int dir = 0; dir < dim; ++dir)
            for (const auto& scv : scvs(fvGeometry))
                ev += elemVolVars[scv.indexInElement()].displacement(dir)*fluxVarCache.gradN(scv.indexInElement())[dir];
           
        volStrainVar_[eIdx][scv.indexInElement()] = ev;
    }

    void updateStrains(const SubControlVolume<flowId>& scv, const Element<flowId>& element, 
                                        FVElementGeometry<flowId>& fvGeometry)
    {
        const auto& mechGG = this->gridGeometry(mechId);
        const auto eIdx = mechGG.elementMapper().index(element);
        auto elemVolVars = localView(this->gridVariables(mechId).curGridVolVars());
        
        const auto& curMechSol = (*curSol_)[mechId];
        elemVolVars.bind(element, fvGeometry, curMechSol);      

        FluxVariablesCache<mechId> fluxVarCache;
        fluxVarCache.update(this->problem(mechId), element, fvGeometry, elemVolVars, scv.geometry().center());

        Scalar<mechId> ev(0.0);

        for (int dir = 0; dir < dim; ++dir)
            for (const auto& scv : scvs(fvGeometry))
                ev += elemVolVars[scv.indexInElement()].displacement(dir)*fluxVarCache.gradN(scv.indexInElement())[dir];
           
        volStrainVar_[eIdx][scv.indexInElement()] = ev;
    }

    void updateStrains()
    {
        const auto& flowGG = this->gridGeometry(mechId);

        for(const auto& element : elements(flowGG.gridView()))
        {
            auto fvGeometry = localView(flowGG);
            fvGeometry.bind(element);
        
            for(const auto& scv : scvs(fvGeometry))
                updateStrains(scv, element, fvGeometry);
        }
    }

    Scalar<mechId> effectivePorePressure(const Element<mechId>& element,
                                         const FluxVariablesCache<mechId>& fluxVarCache) const
    {
        const auto& prevFlowSol = (*prevSol_)[flowId];
        const auto& flowGG = this->gridGeometry(mechId);
        auto fvGeometry = localView(flowGG);
        fvGeometry.bind(element);

        const auto& curFlowSol = (*curSol_)[flowId];
        const auto& shapeValues = fluxVarCache.shapeValues();
        Scalar<mechId> effPress(0.0);
        for (auto& scv : scvs(fvGeometry)){
            auto press = curFlowSol[scv.dofIndex()][0] - prevFlowSol[scv.dofIndex()][0];
            effPress += shapeValues[scv.indexInElement()]*press;
        }
        
        return effPress;     
    }

    template< class ModelIdType, class Assembler >
    void bindCouplingContext(ModelIdType modelId, const Element<flowId>& element, const Assembler& assembler)
    {
        //if(modelId == flowId) updateStrains(element);
    };

    template< class LocalAssembler >
    void updateCouplingContext(MechIdType modelI, 
                               const LocalAssembler& localAssembler, 
                               FlowIdType modelJ, 
                               GridIndexType<flowId> dofIdxGlobalJ,
                               const PrimaryVariables<flowId>& priVarsJ,
                               unsigned int pvIdxJ)
    {
        auto& curSolJ = (*curSol_)[flowId];
        curSolJ[dofIdxGlobalJ][pvIdxJ] = priVarsJ[pvIdxJ];
    }
    
    template< class LocalAssembler >
    void updateCouplingContext(FlowIdType modelI, 
                               const LocalAssembler& localAssembler, 
                               MechIdType modelJ, 
                               GridIndexType<mechId> dofIdxGlobalJ,
                               const PrimaryVariables<mechId>& priVarsJ,
                               unsigned int pvIdxJ)
    {
        auto& curSolJ = (*curSol_)[mechId];
        curSolJ[dofIdxGlobalJ][pvIdxJ] = priVarsJ[pvIdxJ];
        //const auto& element = localAssembler.element();
        //updateStrains(element);

        //todo: if flow vol vars cache, update vol vars
    }

    template< class PMFlowLocalAssembler, class ElemVolVars, class UpdatableFluxVarCache >
    void updateCoupledVariables(FlowIdType modelI,
                                PMFlowLocalAssembler& pmFlowLocalAssembler,
                                ElemVolVars& elemVolVars,
                                UpdatableFluxVarCache& elemFluxVarsCache)
    {
        if(mechVolVarsCache){
            const auto& mechSol = (*curSol_)[mechId];
            const auto& element = pmFlowLocalAssembler.element();
            const auto& fvGeometry = pmFlowLocalAssembler.fvGeometry();
            auto elemMechSol = elementSolution(element, mechSol, fvGeometry.gridGeometry());
       
            for (auto&& scv : scvs(fvGeometry))
                this->gridVariables(mechId).curGridVolVars().volVars(scv).update(elemMechSol, this->problem(mechId), element, scv);
        }

        if(flowVolVarsCache){
            const auto& sol = (*curSol_)[flowId];
            const auto& element = pmFlowLocalAssembler.element();
            const auto& fvGeometry = pmFlowLocalAssembler.fvGeometry();
            auto elemSol = elementSolution(element, sol, fvGeometry.gridGeometry());
       
            for (auto&& scv : scvs(fvGeometry))
                this->gridVariables(flowId).curGridVolVars().volVars(scv).update(elemSol, this->problem(flowId), element, scv);
        } else  elemVolVars.bind(pmFlowLocalAssembler.element(),pmFlowLocalAssembler.fvGeometry(),this->curSol()[modelI]);

        /*      
        // update the element volume variables to obtain the updated porosity/permeability
        elemVolVars.bind(pmFlowLocalAssembler.element(),
                         pmFlowLocalAssembler.fvGeometry(),
                         this->curSol()[modelI]);
 
        // update the transmissibilities subject to the new permeabilities
        elemFluxVarsCache.bind(pmFlowLocalAssembler.element(),pmFlowLocalAssembler.fvGeometry(),elemVolVars);
         */
    }
 
    template< class PoroMechLocalAssembler, class ElemVolVars,class UpdatableFluxVarCache >
    void updateCoupledVariables(MechIdType modelI,
                                PoroMechLocalAssembler& poroMechLocalAssembler,
                                ElemVolVars& elemVolVars,
                                UpdatableFluxVarCache& elemFluxVarsCache)
    {
         /* 
        elemVolVars.bind(poroMechLocalAssembler.element(),
                         poroMechLocalAssembler.fvGeometry(),
                         this->curSol()[modelI]);
                          */
    }

    void resetVolumetricStrains()
    {
        for(auto& elemStr : volStrainVar_)
            for(auto& scvStr : elemStr)
                scvStr = 0.0;
    }
    
    void updatePorosities()
    {
        const auto& curFluxSol = (*curSol_)[flowId];
        const auto& gg = this->gridGeometry(flowId);
        for(const auto& element : elements(gg.gridView()))
        {
            const auto eIdx = gg.elementMapper().index(element);
            auto fvGeometry = localView(gg);
            fvGeometry.bind(element);

            for(const auto& scv : scvs(fvGeometry))
            {
                if(flowVolVarsCache){
                    porosities_[eIdx][scv.indexInElement()] = this->gridVariables(flowId).curGridVolVars().volVars(scv).porosity();
                } else {
                    auto elemVolVars = localView(this->gridVariables(flowId).curGridVolVars());
                    elemVolVars.bind(element, fvGeometry, curFluxSol);
                    porosities_[eIdx][scv.indexInElement()] = elemVolVars[scv].porosity();
                }
            }
        } 
    }

    Scalar<mechId> volumetricStrain(const SubControlVolume<mechId>& scv) const
    {
        return volStrainVar_[scv.dofIndex()];
    }

    // porosity calculated from unconverged results of displacements and pressures
    Scalar<mechId> projectedPorosity(const Element<flowId>& element,
                                     const SubControlVolume<flowId>& scv)
    {
        /*
        const auto& flowGG = this->gridGeometry(flowId);
        const auto eIdx = flowGG.elementMapper().index(element);
        updateStrains(scv, element);
        return porosities_[eIdx][scv.indexInElement()] + volStrainVar_[eIdx][scv.indexInElement()];
        */
        const auto& flowGG = this->gridGeometry(flowId);
        const auto eIdx = flowGG.elementMapper().index(element);
        const auto& porosity = initialized_ ? this->gridVariables(flowId).prevGridVolVars().volVars(scv).porosity() : 
            porosities_[eIdx][scv.indexInElement()];
        updateStrains(scv, element);
        return porosity + volStrainVar_[eIdx][scv.indexInElement()];
        //todo: add component for Biot modulus
    }

    // updated porosity, to be used after convergence is reached and porosities are updated
    Scalar<mechId> porosity(const Element<mechId>& element,
                            const SubControlVolume<mechId>& scv)
    {
        const auto& mechGG = this->gridGeometry(mechId);
        const auto eIdx = mechGG.elementMapper().index(element);
        return porosities_[eIdx][scv.indexInElement()];
    }
      
    //! the problem of model i
    template<std::size_t i>
    const auto& problem(Dune::index_constant<i> modelId) const
    { return *std::get<modelId>(problem_); }

    //! the finite volume grid geometry of model i
    template<std::size_t i>
    const auto& gridGeometry(Dune::index_constant<i> modelId) const
    { return *std::get<modelId>(gridGeometry_); }

    //! the grid variables of model i
    template<std::size_t i>
    GridVariables<i>& gridVariables(Dune::index_constant<i> modelId)
    { return *std::get<modelId>(gridVariables_); }

    const SolutionVector& curSol()
    {return *curSol_;}

    template<std::size_t i>
    decltype(auto) numericEpsilon(Dune::index_constant<i>,
                                  const std::string& paramGroup) const
    {
        constexpr auto numEq = PrimaryVariables<i>::dimension;
        return NumericEpsilon<typename HMTraits::Scalar, numEq>(paramGroup);
    }

  private:

    std::vector<std::vector<Scalar<mechId>>> volStrainVar_;
    std::vector<std::vector<Scalar<mechId>>> porosities_;
    std::vector<Scalar<mechId>> boxAreas_;
    std::vector<std::vector<int>> nodeElements_;
    
    SolutionVector * curSol_;
    const SolutionVector * prevSol_;

    //! pointer to the problem to be solved
    ProblemTuple problem_;

    //! the finite volume geometry of the grid
    GridGeometryTuple gridGeometry_;

    //! the variables container for the grid
    GridVariablesTuple gridVariables_;      

    bool initialized_ = false;
  };
  
}

#endif
