// -*- mode: C++; tab-width: 4; indent-tabs-mode: nil; c-basic-offset: 4 -*-
// vi: set et ts=4 sw=4 sts=4:
/*****************************************************************************
 *   See the file COPYING for full copying permissions.                      *
 *                                                                           *
 *   This program is free software: you can redistribute it and/or modify    *
 *   it under the terms of the GNU General Public License as published by    *
 *   the Free Software Foundation, either version 3 of the License, or       *
 *   (at your option) any later version.                                     *
 *                                                                           *
 *   This program is distributed in the hope that it will be useful,         *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of          *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the            *
 *   GNU General Public License for more details.                            *
 *                                                                           *
 *   You should have received a copy of the GNU General Public License       3*
 *   along with this program.  If not, see <http://www.gnu.org/licenses/>.   *
 *****************************************************************************/
/*!
 * \file
 * \ingroup MultiModel
 * maneger for hydromechanical coupling with the Fixed Stress Split Algorithm 
 * for domains with triple-nodded interfaces 
 */
#ifndef DUMUX_FRACLAB_INTERFACE_FIXSTRESSSPLIT_HYDROMECHANICAL_COUPLING_MANAGER_HH
#define DUMUX_FRACLAB_INTERFACE_FIXSTRESSSPLIT_HYDROMECHANICAL_COUPLING_MANAGER_HH

#include <dumux/fraclab/multimodel/fsshydromechcouplingmanager.hh>

namespace Dumux{

template<class HMTraits, class CM, class FlowAssembler, class MechAssembler, std::size_t bulkDomainId = 0, std:: size_t lowDimDomainId = 1>
class InterfaceFixedStressSplitBoxCouplingManager
{
    using CouplingMapper = CM;
    
    // only flow problem is really multidomain
    using MDTraits = typename HMTraits::FlowMultiDomainTraits;
    using BulkIdType = typename MDTraits::template SubDomain<bulkDomainId>::Index;
    using LowDimIdType = typename MDTraits::template SubDomain<lowDimDomainId>::Index;
    static constexpr auto bulkId = BulkIdType();
    static constexpr auto lowDimId = LowDimIdType();

    using MechTag = typename HMTraits::MechanicsTypeTag;

    //using MechanicsCouplingManager = GetPropType<MechTag, Properties::CouplingManager>;
    using MechanicsSolutionVector = GetPropType<MechTag, Properties::SolutionVector>;
    using Scalar = GetPropType<MechTag, Properties::Scalar>;

    //using FlowCouplingManager = typename MDTraits::CouplingManager;
    using FlowSolutionVector = typename MDTraits::SolutionVector;  
    template<std::size_t id> using FlowSubDomainTypeTag = typename MDTraits::template SubDomain<id>::TypeTag;  
    template<std::size_t id> using FlowScalar = GetPropType<FlowSubDomainTypeTag<id>, Properties::Scalar>;   
      
    // geometry should be the same in both probs
    template<std::size_t id> using GridGeometry = GetPropType<FlowSubDomainTypeTag<id>, Properties::GridGeometry>;
    template<std::size_t id> using FVElementGeometry = typename GridGeometry<id>::LocalView;
    template<std::size_t id> using SubControlVolume = typename GridGeometry<id>::SubControlVolume;
    template<std::size_t id> using SubControlVolumeFace = typename GridGeometry<id>::SubControlVolumeFace;
    template<std::size_t id> using GridView = typename GridGeometry<id>::GridView;
    template<std::size_t id> using Element = typename GridView<id>::template Codim<0>::Entity;
    template<std::size_t id> using GridIndexType = typename GridView<id>::IndexSet::IndexType;

    using MechanicsGridVariables = GetPropType<MechTag, Properties::GridVariables>;
    using MechanicsGridFluxVariablesCache = typename MechanicsGridVariables::GridFluxVariablesCache;
    using MechanicsFluxVariablesCache = typename MechanicsGridFluxVariablesCache::FluxVariablesCache;  
    using MechanicsElementFluxVariablesCache = typename MechanicsGridFluxVariablesCache::LocalView;
    //using MechanicsGridVolumeVariables = typename MechanicsGridVariables::GridVolumeVariables;
    //using MechanicsElementVolumeVariables = typename MechanicsGridVolumeVariables::LocalView;
    using NodalDisplVector = GetPropType<MechTag, Properties::PrimaryVariables>;

    template<std::size_t id> using FlowGridVariables = GetPropType<FlowSubDomainTypeTag<id>, Properties::GridVariables>;
    template<std::size_t id> using FlowGridFluxVariablesCache = typename FlowGridVariables<id>::GridFluxVariablesCache;
    template<std::size_t id> using FlowFluxVariablesCache = typename FlowGridFluxVariablesCache<id>::FluxVariablesCache;  
    template<std::size_t id> using FlowElementFluxVariablesCache = typename FlowGridFluxVariablesCache<id>::LocalView;

    static constexpr int dimWorld = GridView<bulkId>::dimensionworld;
    static constexpr int bulkDim = GridView<bulkId>::dimension;
    static constexpr int lowDim = GridView<lowDimId>::dimension;

    using PorosityVector = std::vector<Scalar>;
    using ApertureVector = std::vector<FlowScalar<lowDimId>>;
      
public:

    // constructor that sets constant initial porosities and apertures 

    InterfaceFixedStressSplitBoxCouplingManager(std::shared_ptr<const GridGeometry<bulkId>> bulkGridGeometry,
                                                std::shared_ptr<const GridGeometry<lowDimId>> lowDimGridGeometry,
                                                const Scalar initBulkPorosity,
                                                const Scalar initAperture,
                                                Scalar minAperture = 0.0)
    : bulkGridGeometry_(bulkGridGeometry)
    , lowDimGridGeometry_(lowDimGridGeometry)
    , minimumAperture_(minAperture)
    {
        volStrainVar_.resize(bulkGridGeometry_->numDofs(),0.0);
        facetDisplJumpVar_.resize(lowDimGridGeometry_->numDofs());
        facetDisplJump_.resize(lowDimGridGeometry_->numDofs());
        for(auto& displ : facetDisplJumpVar_) displ = 0.0;
        for(auto& displ : facetDisplJump_) displ = 0.0;

        bulkPorosities_.resize(bulkGridGeometry_->numDofs(),initBulkPorosity);
        facetApertures_.resize(lowDimGridGeometry_->numDofs(),initAperture);

        // here, facet porosity = aperture/initial aperture
        // given aperture has to be effective aperture and thus already consider
        // eventual porosity of the filling

        facetPorosities_.resize(lowDimGridGeometry_->numDofs(),1.0);
        facetInitApertures_ = facetApertures_;
        setVolumeVectors();
    }
    

    InterfaceFixedStressSplitBoxCouplingManager(std::shared_ptr<const GridGeometry<bulkId>> bulkGridGeometry,
                                                std::shared_ptr<const GridGeometry<lowDimId>> lowDimGridGeometry,
                                                const std::vector<Scalar> initPorosities,
                                                const std::vector<Scalar> initApertures,
                                                const Scalar minAperture = 0.0)
    : bulkGridGeometry_(bulkGridGeometry)
    , lowDimGridGeometry_(lowDimGridGeometry)
    , minimumAperture_(minAperture)
    {
        volStrainVar_.resize(bulkGridGeometry_->numDofs(),0.0);
        facetDisplJumpVar_.resize(lowDimGridGeometry_->numDofs());
        for(auto& displ : facetDisplJumpVar_) displ = 0.0;
        facetPorosities_.resize(lowDimGridGeometry_->numDofs(),1.0);

        bulkPorosities_ = initPorosities;
        facetApertures_ = initApertures;
        facetInitApertures_ = facetApertures_;

        setVolumeVectors();
    }
    
    void setVolumeVectors()
    {        
        boxVolumes_.resize(bulkGridGeometry_->numDofs(), 0.0);
        facetAreas_.resize(lowDimGridGeometry_->numDofs(), 0.0);
        
        for(const auto& element : elements(bulkGridGeometry_->gridView()))
        {
            auto fvGeometry = localView(*bulkGridGeometry_);
            fvGeometry.bind(element);
            for(const auto& scv : scvs(fvGeometry))
                boxVolumes_[scv.dofIndex()] += scv.volume();
        }
        
        for(const auto& element : elements(lowDimGridGeometry_->gridView()))
        {
            auto fvGeometry = localView(*lowDimGridGeometry_);
            fvGeometry.bind(element);
            for(const auto& scv : scvs(fvGeometry))
                facetAreas_[scv.dofIndex()] += scv.volume();
        }
    }

    void init(std::shared_ptr<CouplingMapper> couplingMapper,
              std::shared_ptr<FlowAssembler> flowAssembler,
              std::shared_ptr<MechAssembler> mechanicsAssembler,
              FlowSolutionVector& flowSol,
              FlowSolutionVector& prevIterFlowSol,
              MechanicsSolutionVector& mechSol)
    {
        couplingMapper_ = couplingMapper;
        flowAssembler_ = flowAssembler;
        mechanicsAssembler_ = mechanicsAssembler;
        setFluxCurrentSolution(flowSol);
        setFluxPrevIterSolution(prevIterFlowSol);
        setMechanicsCurrentSolution(mechSol);
        initialized = true;
    }

    void updateStrains()
    {
        updateStrainsAndPorosities();
        updateDisplJumpsAndApertures();
    }

    void updateStrainsAndPorosities()
    {
        for(auto& vstr : volStrainVar_) vstr = 0.0;
   
        // we update the strains for each node by averaging the strains at its correspondent box
        // volume average with strains at the boundaries of the box (faces)

        const auto& gridGeometry = *bulkGridGeometry_;

        for(const auto& element : elements(gridGeometry.gridView()))
        {
            auto elemFluxVars = localView(mechanicsAssembler_->gridVariables().gridFluxVarsCache());
            auto elemVolVars = localView(mechanicsAssembler_->gridVariables().curGridVolVars());
        
            auto fvGeometry = localView(gridGeometry);
            fvGeometry.bind(element);
            const auto& curPress = (*curFlowSol)[bulkId];
            elemVolVars.bind(element, fvGeometry, curPress);                                                                                                      
            elemFluxVars.bind(element, fvGeometry, elemVolVars);
        
            for(const auto& scvf : scvfs(fvGeometry))
            {
                NodalDisplVector displ(0.0);
                const auto shapeValues = elemFluxVars[scvf].shapeValues();
                const auto normal = scvf.unitOuterNormal();
                const auto& insideScv = fvGeometry.scv(scvf.insideScvIdx());
                const auto& outsideScv = fvGeometry.scv(scvf.outsideScvIdx());
                
                for (int dir = 0; dir < dimWorld; ++dir){
                    for (const auto& scvK : scvs(fvGeometry)){
                        displ[dir] += shapeValues[scvK.indexInElement()]*
                            (*curMechSol)[scvK.dofIndex()][dir];
                    }
                    volStrainVar_[insideScv.dofIndex()] += displ[dir]*normal[dir]*scvf.area();
                    if(!scvf.boundary())
                        volStrainVar_[outsideScv.dofIndex()] -= displ[dir]*normal[dir]*scvf.area();
                }
            }
        }

        for(int i = 0; i < volStrainVar_.size(); i++)
            volStrainVar_[i] /= boxVolumes_[i];

        std::vector<bool> isAssigned(gridGeometry.numDofs(), false);

        for(const auto& element : elements(gridGeometry.gridView()))
        {
            auto fvGeometry = localView(gridGeometry);
            fvGeometry.bind(element);
            for(const auto& scv : scvs(fvGeometry)){
                if(isAssigned[scv.dofIndex()]) continue;
                updatePorosity(element, scv);
                isAssigned[scv.dofIndex()] = true;
            }
        }

        isAssigned.clear();
        
    }
    
    void updateDisplJumpsAndApertures()
    {
        for(auto& disj : facetDisplJumpVar_) disj = 0.0;
        const auto& gridGeometry = *lowDimGridGeometry_;

        for(const auto& element : elements(gridGeometry.gridView()))
        {
            auto fvGeometry = localView(gridGeometry);
            fvGeometry.bind(element);
            for(const auto& scv : scvs(fvGeometry))
            {
                const auto du = facetDisplacementJump(element, scv);
                //const auto& un = facetDisplacementJump(element, scv)[0];
                facetDisplJumpVar_[scv.dofIndex()] += du* scv.volume(); 
            }                          
        }

        for(int i = 0; i < facetDisplJumpVar_.size(); i++) 
            facetDisplJumpVar_[i] /= facetAreas_[i];

        std::vector<bool> isAssigned(gridGeometry.numDofs(), false);

        for(const auto& element : elements(gridGeometry.gridView()))
        {
            auto fvGeometry = localView(gridGeometry);
            fvGeometry.bind(element);
            for(const auto& scv : scvs(fvGeometry)){
                if(isAssigned[scv.dofIndex()]) continue;
                //facetApertures_[scv.dofIndex()] = updatedAperture(element, scv);
                updatePorosityAndAperture(element,scv);
                isAssigned[scv.dofIndex()] = true;
            }
        }

        isAssigned.clear();

        for(int i = 0; i < facetDisplJump_.size(); i++) 
            facetDisplJump_[i] += facetDisplJumpVar_[i];
    }

    const NodalDisplVector facetDisplacementJump(const Element<lowDimId>& element,
                                                 const SubControlVolume<lowDimId>& scv) const
    {
        const auto lowDimElemIdx = flowAssembler_->problem(lowDimId).gridGeometry().elementMapper().index(element);
        const auto& map = couplingMapper_->couplingMap(lowDimId, bulkId);
        auto it = map.find(lowDimElemIdx);
        const auto& embedments = it->second.embedments;

        const auto& bulkElemIdx1 = embedments[0].first;
        const auto& coincidingScvfs1 = embedments[0].second;
        const auto& bulkScvfIdx1 =  coincidingScvfs1[scv.localDofIndex()];

        const auto& bulkElemIdx2 = embedments[1].first;
        const auto& coincidingScvfs2 = embedments[1].second;
        const auto& bulkScvfIdx2 = coincidingScvfs2[scv.localDofIndex()];

        const auto& bulkMechSol = *curMechSol;
        const auto& prevMechSol = mechanicsAssembler_->prevSol();
        
        const auto& bulkGridGeometry = *bulkGridGeometry_;
        const auto& bulkElem1 = bulkGridGeometry.element(bulkElemIdx1);
        const auto& bulkElem2 = bulkGridGeometry.element(bulkElemIdx2);

        auto bulkFvGeometry1 = localView(bulkGridGeometry);
        auto bulkFvGeometry2 = localView(bulkGridGeometry);
         
        auto bulkCurElemVolVars1 = localView(mechanicsAssembler_->gridVariables().curGridVolVars());
        auto bulkCurElemVolVars2 = localView(mechanicsAssembler_->gridVariables().curGridVolVars());
         
        auto bulkElemFluxVarsCache1 = localView(mechanicsAssembler_->gridVariables().gridFluxVarsCache());
        auto bulkElemFluxVarsCache2 = localView(mechanicsAssembler_->gridVariables().gridFluxVarsCache());
         
        bulkFvGeometry1.bind(bulkElem1);
        bulkCurElemVolVars1.bind(bulkElem1, bulkFvGeometry1, bulkMechSol);
        bulkElemFluxVarsCache1.bind(bulkElem1, bulkFvGeometry1, bulkCurElemVolVars1);

        bulkFvGeometry2.bind(bulkElem2);
        bulkCurElemVolVars2.bind(bulkElem2, bulkFvGeometry2, bulkMechSol);
        bulkElemFluxVarsCache2.bind(bulkElem2, bulkFvGeometry2, bulkCurElemVolVars2);

        const auto& scvf1 = bulkFvGeometry1.scvf(bulkScvfIdx1);
        const auto& scvf2 = bulkFvGeometry2.scvf(bulkScvfIdx2);
        const auto& fluxVarCache1 = bulkElemFluxVarsCache1[scvf1];
        const auto& fluxVarCache2 = bulkElemFluxVarsCache2[scvf2];
        const auto& shapeValues1 = fluxVarCache1.shapeValues();
        const auto& shapeValues2 = fluxVarCache2.shapeValues();
         
        Dune::FieldVector<Scalar, dimWorld> displ(0.0), rdispl(0.0);
        for (int dir = 0; dir < dimWorld; ++dir)
        {
            for (const auto& scvI : scvs(bulkFvGeometry1)){
                displ[dir] += shapeValues1[scvI.indexInElement()]*(bulkCurElemVolVars1[scvI.indexInElement()].displacement(dir));
                if(!mechanicsAssembler_->isStationaryProblem()) 
                    displ[dir] -= shapeValues1[scvI.indexInElement()]*(prevMechSol[scvI.dofIndex()][dir]);
            }

            for (const auto& scvJ : scvs(bulkFvGeometry2)){
                displ[dir] -= shapeValues2[scvJ.indexInElement()]*(bulkCurElemVolVars2[scvJ.indexInElement()].displacement(dir));
                if(!mechanicsAssembler_->isStationaryProblem()) 
                    displ[dir] += shapeValues2[scvJ.indexInElement()]*(prevMechSol[scvJ.dofIndex()][dir]);
            }
        }

        const auto& couplingManager = mechanicsAssembler_->problem().couplingManager();
        const auto rotationMatrix = couplingManager.interfaceRotationMatrix(scvf1,lowDimElemIdx,scv.indexInElement());

        rotationMatrix.mv(displ, rdispl);

        // change size to make traction normal displ jumps (opening) positive
        rdispl *= -1.0;
        return rdispl;
     }


    const PorosityVector getVolumetricStrainVariation() const
    {return volStrainVar_;};

    // return pore pressure variation in the current time step 

    FlowScalar<bulkId> effectivePorePressureVariation(const Element<bulkId>& element,
                                                      const MechanicsFluxVariablesCache& fluxVarCache) const
    {
        const auto& curSol = (*curFlowSol)[bulkId];
        const auto& prevSol = flowAssembler_->prevSol()[bulkId];
        
        auto bulkFvGeometry = localView(*bulkGridGeometry_);
        auto curElemVolVars = localView(flowAssembler_->gridVariables(bulkId).curGridVolVars());
        auto fvGeometry = localView(*bulkGridGeometry_);
        fvGeometry.bind(element);

        curElemVolVars.bind(element, fvGeometry, curSol);
        const auto& shapeValues = fluxVarCache.shapeValues();

        FlowScalar<bulkId> effPress(0.0);
        for (auto&& scv : scvs(fvGeometry))
            effPress += shapeValues[scv.indexInElement()]*(curSol[scv.dofIndex()][0]-prevSol[scv.dofIndex()][0]);
        return effPress; 
    }

    Scalar facetEffectivePorePressureVariation(const GridIndexType<lowDimId> eIdx,
                                               const GridIndexType<lowDimId> scvIdx) const
    {
        const auto& element = lowDimGridGeometry_->element(eIdx);
        const auto& curSol = (*curFlowSol)[lowDimId];
        const auto& prevSol = flowAssembler_->prevSol()[lowDimId];

        auto curElemVolVars = localView(flowAssembler_->gridVariables(lowDimId).curGridVolVars());
        auto fvGeometry = localView(*lowDimGridGeometry_);
        fvGeometry.bind(element);
        const auto& scv = fvGeometry.scv(scvIdx);

        curElemVolVars.bind(element, fvGeometry, curSol);
        
        FlowFluxVariablesCache<lowDimId> fluxVarCache;
        fluxVarCache.update(flowAssembler_->problem(lowDimId), element, fvGeometry, curElemVolVars, scv.geometry().center());
        const auto shapeValues = fluxVarCache.shapeValues();

        Scalar effPress(0.0);
        for (const auto& scvK : scvs(fvGeometry))
            effPress += shapeValues[scvK.indexInElement()]*(curSol[scvK.dofIndex()][0] - prevSol[scvK.dofIndex()][0]);
        
        return effPress;
    }

    // compute porosity with converged results 

    void updatePorosity(const Element<bulkId>& element,
                        const SubControlVolume<bulkId>& scv)
    {
        auto fvGeometry = localView(*bulkGridGeometry_);
        fvGeometry.bind(element);

        // Flow variables
        const auto& prevGridVolVars = flowAssembler_->gridVariables(bulkId).prevGridVolVars();
        auto prevElemVolVars = localView(prevGridVolVars);
        const auto& prevFluxSol = flowAssembler_->prevSol()[bulkId];
        prevElemVolVars.bind(element, fvGeometry, prevFluxSol);

        const auto& curGridMechVolVars = mechanicsAssembler_->gridVariables().curGridVolVars();     
        auto curElemMechVolVars = localView(curGridMechVolVars);

        curElemMechVolVars.bind(element, fvGeometry, *curMechSol);
        MechanicsFluxVariablesCache elemMechFluxVars;
        elemMechFluxVars.update(mechanicsAssembler_->problem(), element, fvGeometry, curElemMechVolVars, scv.center());
        
        // get previous porosity

        Scalar prevPorosity = prevElemVolVars[scv].porosity();

        // get relevant params
        const auto biotCoeff =  mechanicsAssembler_->problem().spatialParams().biotCoefficient(element,fvGeometry,curElemMechVolVars,elemMechFluxVars);
        const auto biotModulus = mechanicsAssembler_->problem().spatialParams().biotModulus(element,scv,fvGeometry,curElemMechVolVars,elemMechFluxVars);
        // compute porosity variation due to vol strains and pressure var
        const auto& curPress = (*curFlowSol)[bulkId];
        Scalar ev = volStrainVar_[scv.dofIndex()];
        Scalar dp = curPress[scv.dofIndex()] - prevFluxSol[scv.dofIndex()];
        Scalar dpor = biotCoeff * ev + dp/biotModulus;

        Scalar porosity = std::max(prevPorosity + dpor, 0.0);
        bulkPorosities_[scv.dofIndex()] = porosity;

    }
    
    Scalar updatedAperture(const Element<lowDimId>& element,
                           const SubControlVolume<lowDimId>& scv)
    {
        auto fvGeometry = localView(*lowDimGridGeometry_);
        fvGeometry.bind(element);

        // Flow variables
        const auto& prevFluxSol = flowAssembler_->prevSol()[lowDimId];
        const auto& prevGridVolVars = flowAssembler_->gridVariables(lowDimId).prevGridVolVars();
        auto prevElemVolVars = localView(prevGridVolVars);
        const auto& facetPrevPress = flowAssembler_->prevSol()[lowDimId];
        prevElemVolVars.bind(element, fvGeometry, prevFluxSol);
        const auto& facetCurPress = (*curFlowSol)[lowDimId];
        // get previous aperture

        Scalar prevApert = prevElemVolVars[scv].extrusionFactor();

         // get relevant params
        const auto eIdx = lowDimGridGeometry_->elementMapper().index(element);
        const auto sIdx = scv.indexInElement();
        const auto biotCoeff =  mechanicsAssembler_->problem().spatialParams().interfaceBiotCoefficient(eIdx,sIdx);
        const auto biotModulus = mechanicsAssembler_->problem().spatialParams().interfaceBiotModulus(eIdx,sIdx);

        // compute aperture var due to pressure change and normal displ jumps
        Scalar un = facetDisplJumpVar_[scv.dofIndex()][0]; 
        Scalar dp = facetCurPress[scv.dofIndex()] - facetPrevPress[scv.dofIndex()];
        Scalar dapert = biotCoeff * un + dp/biotModulus;
        //const Scalar initApert = facetInitApertures_[scv.dofIndex()];
        //Scalar dpor = dapert/initApert;

        Scalar aperture = prevApert + dapert;

        return std::max(aperture,minimumAperture_);
    }

    void updatePorosityAndAperture(const Element<lowDimId>& element,
                                   const SubControlVolume<lowDimId>& scv)
    {
        auto fvGeometry = localView(*lowDimGridGeometry_);
        fvGeometry.bind(element);

        // Flow variables
        const auto& prevFluxSol = flowAssembler_->prevSol()[lowDimId];
        const auto& prevGridVolVars = flowAssembler_->gridVariables(lowDimId).prevGridVolVars();
        auto prevElemVolVars = localView(prevGridVolVars);
        const auto& facetPrevPress = flowAssembler_->prevSol()[lowDimId];
        prevElemVolVars.bind(element, fvGeometry, prevFluxSol);
        const auto& facetCurPress = (*curFlowSol)[lowDimId];
        
        // get previous porosity

        Scalar prevPorosity = prevElemVolVars[scv].porosity();

        // get relevant params
        const auto eIdx = lowDimGridGeometry_->elementMapper().index(element);
        const auto sIdx = scv.indexInElement();
        const auto biotCoeff =  mechanicsAssembler_->problem().spatialParams().interfaceBiotCoefficient(eIdx,sIdx);
        const auto biotModulus = mechanicsAssembler_->problem().spatialParams().interfaceBiotModulus(eIdx,sIdx);

        // compute aperture var due to pressure change and normal displ jumps
        Scalar un = facetDisplJumpVar_[scv.dofIndex()][0]; 
        Scalar dp = facetCurPress[scv.dofIndex()] - facetPrevPress[scv.dofIndex()];
        Scalar dapert = biotCoeff * un + dp/biotModulus;
        const Scalar initApert = facetInitApertures_[scv.dofIndex()];
        const Scalar dpor = dapert/initApert;

        Scalar apert = facetApertures_[scv.dofIndex()] + dapert;
        Scalar porosity = prevPorosity + dpor;
        
        facetApertures_[scv.dofIndex()] = std::max(apert,minimumAperture_);
        facetPorosities_[scv.dofIndex()] = std::max(porosity,minimumAperture_/initApert);
    }

    // get estimator of vol strain variation according to the fixed stress split algorithm 
    template<class ElementSolution>
    Scalar volStrainEstimator(const Element<bulkId>& element,
                              const SubControlVolume<bulkId>& scv,
                              const ElementSolution& elemSol) const
    {
        if(!initialized) return 0.0;

        auto fvGeometry = localView(*bulkGridGeometry_);
        fvGeometry.bind(element);

        // flux variables

        const auto& prevBulkFlowSol = (*prevIterFlowSol)[bulkId];    
        const auto prevEffPressure = prevBulkFlowSol[scv.dofIndex()];
        const auto curEffPressure = elemSol[scv.localDofIndex()][0];
        
        const auto dpress = curEffPressure - prevEffPressure;

        // mechanics

        const auto& curGridMechVolVars = mechanicsAssembler_->gridVariables().curGridVolVars();     
        auto curElemMechVolVars = localView(curGridMechVolVars);
        MechanicsFluxVariablesCache elemMechFluxVars;

        curElemMechVolVars.bind(element, fvGeometry, *curMechSol);
        elemMechFluxVars.update(mechanicsAssembler_->problem(), element, fvGeometry, curElemMechVolVars, scv.center());

        const auto b =  mechanicsAssembler_->problem().spatialParams().biotCoefficient(element,fvGeometry,curElemMechVolVars,elemMechFluxVars);
        const auto M = mechanicsAssembler_->problem().spatialParams().biotModulus(element,scv,fvGeometry,curElemMechVolVars,elemMechFluxVars);
        const auto Kdr = mechanicsAssembler_->problem().spatialParams().bulkModulus(element,scv,fvGeometry,curElemMechVolVars,elemMechFluxVars);
        // return estimator

        Scalar mv = (1.0/M + std::pow(b,2.0)/Kdr);
        return  mv*dpress;
    }

    // estimator of aperture change for fracture 
    template<class ElementSolution>
    Scalar apertureVarEstimator(const Element<lowDimId>& element,
                                const SubControlVolume<lowDimId>& scv,
                                const ElementSolution& elemSol) const
    {
        if(!initialized) return 0.0;

        auto fvGeometry = localView(*lowDimGridGeometry_);
        fvGeometry.bind(element);

        // flux variables
        const auto& prevFacetFlowSol = (*prevIterFlowSol)[lowDimId];      
        const auto& prevEffPressure = prevFacetFlowSol[scv.dofIndex()];
        const auto& curEffPressure = elemSol[scv.localDofIndex()][0];
        
        const auto dpress = curEffPressure - prevEffPressure;

        // mechanics

        const auto eIdx = lowDimGridGeometry_->elementMapper().index(element);
        const auto sIdx = scv.indexInElement();
        const auto b =  mechanicsAssembler_->problem().spatialParams().interfaceBiotCoefficient(eIdx,sIdx);
        const auto M = mechanicsAssembler_->problem().spatialParams().interfaceBiotModulus(eIdx,sIdx);
        const auto Kdr = mechanicsAssembler_->problem().spatialParams().interfaceBulkModulus(eIdx,sIdx);
        
        // return estimator

        Scalar mv = (1.0/M + std::pow(b,2.0)/Kdr);
        return  mv*dpress;
    }

    // estimator of aperture change for fracture 
    template<class ElementSolution>
    Scalar volStrainEstimator(const Element<lowDimId>& element,
                              const SubControlVolume<lowDimId>& scv,
                              const ElementSolution& elemSol) const
    {
        if(!initialized) return 0.0;

        auto fvGeometry = localView(*lowDimGridGeometry_);
        fvGeometry.bind(element);

        // flux variables
        const auto& prevFacetFlowSol = (*prevIterFlowSol)[lowDimId];      
        const auto& prevEffPressure = prevFacetFlowSol[scv.dofIndex()];
        const auto& curEffPressure = elemSol[scv.localDofIndex()][0];
        
        const auto dpress = curEffPressure - prevEffPressure;

        // mechanics

        const auto eIdx = lowDimGridGeometry_->elementMapper().index(element);
        const auto sIdx = scv.indexInElement();
        const auto b =  mechanicsAssembler_->problem().spatialParams().interfaceBiotCoefficient(eIdx,sIdx);
        const auto M = mechanicsAssembler_->problem().spatialParams().interfaceBiotModulus(eIdx,sIdx);
        const auto Kdr = mechanicsAssembler_->problem().spatialParams().interfaceBulkModulus(eIdx,sIdx);
        const auto initAper = facetInitApertures_[scv.dofIndex()];
        // return estimator

        Scalar mv = (1.0/M + std::pow(b,2.0)/Kdr);
        mv /= initAper;
        return  mv*dpress;
    }

    template < class ElementSolution>
    Scalar flowPorosity(const Element<bulkId>& element,
                             const SubControlVolume<bulkId>& scv,
                             const ElementSolution& elemSol) const
    {
        Scalar dporPress(0.0);
        dporPress = volStrainEstimator(element, scv, elemSol);
        return bulkPorosities_[scv.dofIndex()] + dporPress;
    }

    template <class ElementSolution>
    Scalar flowPorosity(const Element<lowDimId>& element,
                        const SubControlVolume<lowDimId>& scv,
                        const ElementSolution& elemSol) const
    {
        Scalar dpor(0.0);        
        dpor = volStrainEstimator(element, scv, elemSol);
        const auto projPor = facetPorosities_[scv.dofIndex()] + dpor;
        const auto initApert = facetInitApertures_[scv.dofIndex()];
        return std::max(projPor,minimumAperture_/initApert);
    }

    template <class ElementSolution>
    Scalar flowAperture(const Element<lowDimId>& element,
                             const SubControlVolume<lowDimId>& scv,
                             const ElementSolution& elemSol) const
    {
        Scalar dapert(0.0);        
        dapert = apertureVarEstimator(element, scv, elemSol);
        const auto projApert = facetApertures_[scv.dofIndex()] + dapert;
        return std::max(projApert,minimumAperture_);
    }

    void setPorosity(const SubControlVolume<bulkId>& scv,
                     const Scalar porosity)
    {       
        bulkPorosities_[scv.dofIndex()] = porosity;
    }
    
    void setFluxPrevIterSolution(FlowSolutionVector& sol)
    {
        prevIterFlowSol = &sol;
    }
      
    void setFluxCurrentSolution(FlowSolutionVector& sol)
    {
        curFlowSol = &sol;
    }

    void setMechanicsCurrentSolution(MechanicsSolutionVector& sol)
    {
        curMechSol = &sol;
    }

    void setGlobalPorosity(const Scalar porosity)
    {
        for(const auto& element : elements(bulkGridGeometry_->gridView()))
        {
            auto fvGeometry = localView(*bulkGridGeometry_);
            fvGeometry.bind(element);
            for(const auto& scv : scvs(fvGeometry))
                setPorosity(scv, porosity);          
        }
    }

    // only use functions below after current solution has converged 

    Scalar porosity(GridIndexType<bulkId> vIdx) const
    {
        return bulkPorosities_[vIdx];
    }

    Scalar volumetricStrain(GridIndexType<bulkId> vIdx) const
    {
        return volStrainVar_[vIdx];
    }

    Scalar aperture(GridIndexType<lowDimId> vIdx) const
    {
        return facetApertures_[vIdx];
    }
    
    Scalar previousAperture(GridIndexType<lowDimId> vIdx) const
    {
        return facetInitApertures_[vIdx];
    }

    NodalDisplVector displacementJump(GridIndexType<lowDimId> vIdx) const
    {
        return facetDisplJumpVar_[vIdx];
    }

    NodalDisplVector totalDisplacementJump(GridIndexType<lowDimId> vIdx) const
    {
        return facetDisplJump_[vIdx];
    }

    void advanceTimeStep()
    {
        facetInitApertures_ = facetApertures_;
    }    

    std::vector<Scalar> getFacetApertures()
    {return facetApertures_;}

    std::vector<Scalar> getBulkPorosities()
    {return bulkPorosities_;}

    const MechAssembler& mechanicsAssembler()
    {return *mechanicsAssembler_;}

  private:

    std::shared_ptr<FlowAssembler> flowAssembler_;
    std::shared_ptr<MechAssembler> mechanicsAssembler_;
    std::shared_ptr<const GridGeometry<bulkId>> bulkGridGeometry_;
    std::shared_ptr<const GridGeometry<lowDimId>> lowDimGridGeometry_; 
    std::shared_ptr<CouplingMapper> couplingMapper_;
    std::vector<Scalar> volStrainVar_;
    std::vector<NodalDisplVector> facetDisplJumpVar_;
    std::vector<NodalDisplVector> facetDisplJump_;
    std::vector<Scalar> bulkPorosities_;
    std::vector<Scalar> facetPorosities_;
    std::vector<Scalar> facetApertures_;
    std::vector<Scalar> facetInitApertures_;

    MechanicsSolutionVector * curMechSol;
    FlowSolutionVector * curFlowSol;
    FlowSolutionVector * prevIterFlowSol;
    std::vector<Scalar> boxVolumes_;
    std::vector<Scalar> facetAreas_;
    Scalar minimumAperture_;
      
    bool initialized = false;
  };
  
}

#endif
